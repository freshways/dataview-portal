import {Guid} from "@/utils/guid";

/**
 * 控件名:浮动菜单
 * 用途描述：
 * 绑定到指定元素，该元素点击时会弹出一个浮动菜单
 */

var _controlStorage = {};
var _menuShow = false;
var _menuFocus = false;
/**
 * 绑定
 * @param target {Element} 绑定的目标元素
 * @param option {Object} 配置
 */
var bind = function (target, option) {
  var controlid = $(target).attr("data-floatmenuid");
  if (!controlid) {
    controlid = Guid.NewGuid().ToString('D');
    $(target).attr("data-floatmenuid", controlid);
  }
  var control = _controlStorage[controlid];
  if (!control) {
    _controlStorage[controlid] = {
      id: controlid,
      target: target,
      option: option
    };
    //$(target).bind("click", ontargetclick);
  }
  if (target.tId != undefined) {
    ontargetclick2(target);
  }
  return control;
};
var bind2 = function (controlid, option) {
  var control = _controlStorage[controlid];
  if (!control) {
    _controlStorage[controlid] = {
      id: controlid,
      option: option
    };
  }
  return control;
};
//解除绑定并销毁菜单
var unbind = function (target) {
  var controlid = $(target).attr("data-floatmenuid");
  if (controlid) {
    $(target).unbind("click", ontargetclick);
    var control = _controlStorage[controlid];
    if (control) {
      if (control.$menu) {
        control.$menu.remove();
      }
      delete _controlStorage[controlid];
    }
  }
};
//销毁菜单
var unbind2 = function (controlid) {
  var control = _controlStorage[controlid];
  if (control) {
    if (control.$menu) {
      control.$menu.remove();
    }
    delete _controlStorage[controlid];
  }
};
//目标元素点击事件
var ontargetclick = function (e) {
  var event = e || window.event;
  event.stopPropagation();
  if ($(this).attr("aria-menuitem")) {
    return false;
  }
  var controlid = $(this).attr("data-floatmenuid");
  var control = _controlStorage[controlid];
  if (!control) {
    return false;
  }
  control.target = $(this);
  if (!control.$menu) {
    control.$menu = $("<ul>").addClass("dropdown-menu floatmenu").css({"z-index": control.option.zindex || 9999}).appendTo("body");
    control.option.menuitems.forEach(function (item) {
      item.mid = Guid.NewGuid().ToString('D');
      $("<li>").append(function () {
        return $("<a>").attr({
          "href": "javascript:void(0)",
          "data-floatmenuid": controlid,
          "aria-menuitem": true,
          "data-mid": item.mid
        }).text(item.label);
      }).attr({"title": item.tips || item.label}).appendTo(control.$menu);
    });
    control.$menu.bind("mouseover", function () {
      control.mouseover = true;
    }).bind("mouseleave", function () {
      control.mouseover = false;
    });
    $(document).bind("click", function () {
      if (!control.mouseover && control.isShow) {
        control.$menu.hide();
        control.isShow = false;
      }
    });
  }
  if (!control.isShow) {
    var rect_target = this.getBoundingClientRect();
    var x = rect_target.left;
    var y = rect_target.top + rect_target.height + window.scrollY;
    if (x + control.$menu.width() > window.innerWidth) {
      x -= control.$menu.width() - rect_target.width;
    }
    if (y + control.$menu.height() > window.innerHeight) {
      y -= control.$menu.height() + rect_target.height + parseFloat(control.$menu.css("padding-top")) + parseFloat(control.$menu.css("padding-bottom"));
    }
    $(".floatmenu").hide();
    control.$menu.css({"top": y, "left": x}).show();
    control.isShow = true;
    _menuShow = true;
  } else {
    control.$menu.hide();
    control.isShow = false;
    _menuShow = false;
  }
  return false;
};
var ontargetclick2 = function (target, e) {
  var event = e || window.event;
  event.stopPropagation();
  if ($(this).attr("aria-menuitem")) {
    return false;
  }
  var controlid = $(target).attr("data-floatmenuid");
  var control = _controlStorage[controlid];
  if (!control) {
    return false;
  }
  if (!control.$menu) {
    control.$menu = $("<ul>").addClass("dropdown-menu floatmenu").css({"z-index": control.option.zindex || 9999}).appendTo("body");
    control.option.menuitems.forEach(function (item) {
      item.mid = Guid.NewGuid().ToString('D');
      $("<li>").append(function () {
        return $("<a>").attr({
          "href": "javascript:void(0)",
          "data-floatmenuid": controlid,
          "aria-menuitem": true,
          "data-mid": item.mid
        }).text(item.label);
      }).attr({"title": item.tips || item.label}).appendTo(control.$menu);
    });
    control.$menu.bind("mouseover", function () {
      control.mouseover = true;
    }).bind("mouseleave", function () {
      control.mouseover = false;
    });
    $(document).bind("click", function () {
      if (!control.mouseover && control.isShow) {
        control.$menu.hide();
        control.isShow = false;
      }
    });
  }
  if (!control.isShow) {
    var rect_target = document.getElementById(target.tId).getBoundingClientRect();
    var x = rect_target.left;
    var y = rect_target.top + rect_target.height + window.scrollY;
    if (x + control.$menu.width() > window.innerWidth) {
      x -= control.$menu.width() - rect_target.width;
    }
    if (y + control.$menu.height() > window.innerHeight) {
      y -= control.$menu.height() + rect_target.height + parseFloat(control.$menu.css("padding-top")) + parseFloat(control.$menu.css("padding-bottom"));
    }
    $(".floatmenu").hide();
    control.$menu.css({"top": y, "left": x}).show();
    control.isShow = true;
  } else {
    control.$menu.hide();
    control.isShow = false;
  }
  return false;
};

//独立显示菜单，不绑定元素
const show = function (controlId, menus) {
  let control = _controlStorage[controlId];
  if (!control) {
    control = {
      id: controlId,
      option: {}
    };
    _controlStorage[controlId] = control;
  }
  control.option.menuitems = menus;
  if (!control.$menu) {
    control.$menu = $("<ul>").addClass("dropdown-menu floatmenu").css({"z-index": 9999}).appendTo("body");
    control.$menu.bind("mouseover", function () {
      control.mouseover = true;
    }).bind("mouseleave", function () {
      control.mouseover = false;
    });
    $(document).bind("click", function () {
      if (!control.mouseover && control.isShow) {
        control.$menu.hide();
        control.isShow = false;
      }
    });
  }
  control.$menu.empty();
  menus.forEach(item => {
    item.mid = Guid.NewGuid().ToString('D');
    $("<li>").append(function () {
      return $("<a>").attr({
        "href": "javascript:void(0)",
        "data-floatmenuid": controlId,
        "aria-menuitem": true,
        "data-mid": item.mid
      }).text(item.label);
    }).attr({"title": item.tips || item.label}).appendTo(control.$menu);
  });
  if(control.isShow){
    control.$menu.hide();
    control.isShow = false;
    _menuShow = false;
  }
  if (!control.isShow) {
    let x = event.clientX;
    let y = event.clientY + window.scrollY;
    if (x + control.$menu.width() > window.innerWidth) {
      x -= control.$menu.width();
    }
    if (y + control.$menu.height() > window.innerHeight) {
      y -= control.$menu.height() + parseFloat(control.$menu.css("padding-top")) + parseFloat(control.$menu.css("padding-bottom"));
    }
    $(".floatmenu").hide();
    control.$menu.css({"top": y, "left": x}).show();
    control.isShow = true;
    _menuShow = true;
  }
};
//生成菜单项点击事件
var onmenuitemclick = function () {
  var controlid = $(this).attr("data-floatmenuid");
  var mid = $(this).attr("data-mid");
  var control = _controlStorage[controlid];
  if (control) {
    var mitem = control.option.menuitems.find(function (d) {
      return d.mid === mid;
    });
    if (mitem) {
      control.$menu.hide();
      control.isShow = false;
      _menuShow = false;
      _menuFocus = false;
      mitem.onclick.call(this, event, control);
    }
  }
};
$(document).delegate("[data-floatmenuid]", "click", ontargetclick);
$(document).delegate("[aria-menuitem]", "click", onmenuitemclick);
export const FloatMenu = {
  bind: bind,
  bind2: bind2,
  unbind: unbind,
  unbind2: unbind2,
  show: show
};

