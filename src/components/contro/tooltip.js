/**
 * 工具提示配置
 */

var modules = {};
//当前编辑的格式
var _tooltipFormat;
var events_onformatchange = [];
var regu = /^[0-9a-zA-Z\u4e00-\u9fa5\_\/\(\)\-\<\>\s:：]+$/;
//生成默认格式
var defaultFormat = function (fields) {
  var format = "";
  var hash = {};
  fields.forEach(function (field) {
    if (!hash[field.name]) {
      format += field.name + ":<" + field.name + ">\n";
      hash[field.name] = true;
    }
  });
  if (format) {
    format = format.substr(0, format.length - 1);
  }
  return format;
};
//显示工具提示
var showTooltips = function (format, fields) {
  var $tooltips = $("#chart_tooltips");
  if (!$tooltips.length) {
    $tooltips = $("<div>").addClass("chart_tooltips").attr({"id": "chart_tooltips"}).appendTo("body");
  }
  $tooltips.removeClass("chart_tooltips_chartthumbnail");
  var content = format;
  fields.forEach(function (field) {
    var pattern = field.name.replace(/\(/g, "\\(").replace(/\)/g, "\\)");
    var reg = new RegExp("<" + pattern + ">", "g");
    content = content.replace(reg, field.value);
  });
  content = content.replace(/\n/g, "<br>");
  $tooltips.html(content);
  var tips_x = event.clientX + 10;
  var tips_y = event.clientY + 10;
  var tips_width = $tooltips.width();
  var tips_height = $tooltips.height();
  if (tips_width + tips_x > window.innerWidth) {
    tips_x -= tips_width + 40;
  }
  if (tips_height + tips_y > window.innerHeight) {
    tips_y -= tips_height + 40;
  }
  $tooltips.css({"left": tips_x, "top": tips_y}).show();
};
var showStringTooltips = function (str) {
  var $tooltips = $("#chart_tooltips");
  if (!$tooltips.length) {
    $tooltips = $("<div>").addClass("chart_tooltips").attr({"id": "chart_tooltips"}).appendTo("body");
  }
  $tooltips.removeClass("chart_tooltips_chartthumbnail");
  var content = str;
  content = content.replace(/\n/g, "<br>");
  $tooltips.html(content);
  var tips_x = event.clientX + 10;
  var tips_y = event.clientY + 10;
  var tips_width = $tooltips.width();
  var tips_height = $tooltips.height();
  if (tips_width + tips_x > window.innerWidth) {
    tips_x -= tips_width + 20;
  }
  if (tips_height + tips_y > window.innerHeight) {
    tips_y -= tips_height + 20;
  }
  $tooltips.css({"left": tips_x, "top": tips_y}).show();
};
var showStringTooltipsByElementXY = function (str, el) {
  var $tooltips = $("#chart_tooltips");
  if (!$tooltips.length) {
    $tooltips = $("<div>").addClass("chart_tooltips").attr({"id": "chart_tooltips"}).appendTo("body");
  }
  $tooltips.addClass("chart_tooltips_chartthumbnail");
  var content = str;
  content = content.replace(/\n/g, "<br>");
  $tooltips.html(content);
  var tips_x = el.parent(0).offset().left;
  var tips_y = el.parent(0).offset().top;
  var tips_width = $tooltips.width();
  var tips_height = $tooltips.height();
  tips_x -= tips_width + 20;
  if (tips_height + tips_y > window.innerHeight) {
    tips_y -= tips_height + 20;
  }
  $tooltips.css({"left": tips_x, "top": tips_y}).show();
};
//隐藏工具提示
var hideTooltips = function () {
  var $tooltips = $("#chart_tooltips");
  if ($tooltips.length) {
    $tooltips.hide();
  }
};
export const Tooltip = {
  getFormat: function () {
    return _tooltipFormat;
  },
  setFormat: function (format) {
    _tooltipFormat = format;
  },
  defaultFormat: defaultFormat,
  show: showTooltips,
  showString: showStringTooltips,
  showStringByElementXY: showStringTooltipsByElementXY,
  hide: hideTooltips
};

//# sourceMappingURL=tooltip.js.map
