/* global Highcharts, d3, undersore, _, require, define */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {
        var measure;

        if (!columnFields.length) { return false; }
        measure = columnFields.find(function (d) {
            return d.slaveType === 1;
        });
        if( !measure ){ return false; }
        measure = measure.name;

        var chart = Hcharts.histogram(svgSelector);
        (function () {
            var points, color, hConfig;

            hConfig = options.chartConfig || {
                GlobalColor: "#4E79A7",
                axisStyle: {
                    tickColor : "#000",
                    fontColor : "#000",
                    fontFamily: "Arial",
                    fontSize  : 15
                },
                textStyle: {
                    "display"     : "none",
                    "font-family" : "Arial",
                    "font-size"   : 15,
                    "font-style"  : "normal",
                    "font-weight" : "normal",
                    "fill"        : "#333",
                    //"position"    : "top"
                }
            };

            points = data.map(d => d[measure]);
            color = options.colorMark.GlobalColor || "black";
            if (options.size) {
                chart.config.chart = {
                    width: options.size.width,
                    height: options.size.height
                };
            }

            chart.config.legend = { enabled: false };

            chart.config.xAxis = [
                _.extend({ tickAmount: 10, alignTicks: false }, chart.getAxisStyle(hConfig.axisStyle)),
                { title: { text: "Data" }, alignTicks: false, visible: false },
            ];
            chart.config.yAxis = [
                 _.extend({ title: { text: "" }, allowDecimals: false },
                          chart.getAxisStyle(hConfig.axisStyle)),
            ];
            chart.config.tooltip = {
                formatter: function () {
                            return "总共有：<span style='font-weight:bold; color:green;'>" +
                                    this.y + "</span> 个值位于:<br>" +
                                   "[ <span style='font-weight:bold; color:green;'>" +
                                    Highcharts.numberFormat(this.point.options.x) +
                                    "</span> - <span style='font-weight:bold; color:green;'>" +
                                    Highcharts.numberFormat(this.point.options.x2) +
                                    "</span> ] 区间内";
                        }
            };

            chart.config.series = [{
                    id: "s0",
                    type: 'histogram', // bellcurve
                    xAxis: 0,
                    yAxis: 0,
                    baseSeries: "s1",
                    zIndex: -1,
                    binsNumber: 10,
                    dataLabels: chart.getDataLabels(hConfig.textStyle)
                },
                {
                    data: points,
                    type: "scatter",
                    visible: false,
                    id: "s1",
                    marker: { radius: 1.5 }
            }];

        })(); // IIFE END

        chart.init();
        return chart;
    };


    module.exports = {
        build: buildChart
    };

});
