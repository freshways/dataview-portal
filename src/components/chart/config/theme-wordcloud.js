﻿/**
 * 字符云设置
 */
define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../../data/sharedata')
    }

    var dict_theme = [
        {
            Name: "圆",
            Iocn: "circle.png",
            ClassName: "circle",
            Order: 0
        }, {
            Name: "心",
            Iocn: "cardioid.png",
            ClassName: "cardioid",
            Order: 1
        }, {
            Name: "棱形",
            Iocn: "diamond.png",
            ClassName: "diamond",
            Order: 2
        }, {
            Name: "三角形",
            Iocn: "triangle.png",
            ClassName: "triangle-forward",
            Order: 3
        }, {
            Name: "五边形",
            Iocn: "pentagon.png",
            ClassName: "pentagon",
            Order: 4
        }, {
            Name: "五角星",
            Iocn: "star.png",
            ClassName: "star",
            Order: 5
        }
    ];

    var $modal;
    var app;
    var event_confirm;
    var showModal = function (config, callback) {
        $modal.modal("show");
        app.init();
        event_confirm = callback;
        if (config && config.ThemeType) {
            app.ThemeType = config.ThemeType;
        }
    }
    var init = function () {
        $modal = $("#modalWordcloudDesign");
        app = new Vue({
            el: "#modalWordcloudDesign .modal-content",
            data: {
                ThemeType: "999"
            },
            methods: {
                init: function () {
                    //初始化tab0
                    var $tab0 = $("#panel-WordcloudDesign-00 .panel-body .wordcloudList").empty();
                    dict_theme = Enumerable.from(dict_theme).orderBy("x=>x.Order").toArray();
                    dict_theme.forEach(function (item) {
                        $("<div>").addClass("tabledesign_mode").append(function () {
                            return $("<a>").addClass("thumbnail").attr({ "href": "###" }).append(function () {
                                return $("<img>").data("theme", item.ClassName).attr({ "src": SiteRootPath + "/Content/Images/Wordcloud/" + item.Iocn });
                            });
                        }).appendTo($tab0);
                    });
                    $("#panel-WordcloudDesign-00 .panel-body .tabledesign_mode img").bind("click", function () {
                        app.ThemeType = $(this).data("theme");
                    });
                    
                    this.ThemeType = "999";
                },
                confirm: function () {
                    $modal.modal("hide");
                    if (event_confirm) {
                        var config = {
                            ThemeType: this.ThemeType
                        }
                        event_confirm(config);
                    }
                },
                change: function (event) {
                    //暂无使用
                    console.log(event);
                }
            }
        });
    }

    module.exports = {
        init: init,
        show: showModal
    }
});