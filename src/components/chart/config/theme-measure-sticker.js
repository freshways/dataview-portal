﻿/**
 * 指标贴设置
 */
define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../../data/sharedata')
    }

    var dict_theme = [
        {
            Name: "black1",
            Iocn: "black1.png",
            ClassName: "",
            Order: 0
        }, {
            Name: "black2",
            Iocn: "black2.png",
            ClassName: "blackMeasureSticker",
            Order: 1
        }, {
            Name: "blue1",
            Iocn: "blue1.png",
            ClassName: "blueMeasureSticker_1",
            Order: 2
        }, {
            Name: "blue2",
            Iocn: "blue2.png",
            ClassName: "blueMeasureSticker_2",
            Order: 3
        }, {
            Name: "murasaki1",
            Iocn: "murasaki1.png",
            ClassName: "purpleMeasureSticker_1",
            Order: 4
        }, {
            Name: "murasaki2",
            Iocn: "murasaki2.png",
            ClassName: "purpleMeasureSticker_2",
            Order: 5
        }
    ];

    var dict_theme_b = [
        {
            Name: "red",
            Iocn: "b/red.png",
            ClassName: "b_redMeasureSticker",
            Order: 0
        }, {
            Name: "blue",
            Iocn: "b/blue.png",
            ClassName: "b_blueMeasureSticker",
            Order: 1
        }, {
            Name: "yellow",
            Iocn: "b/yellow.png",
            ClassName: "b_yellowMeasureSticker",
            Order: 2
        }
    ];

    var $modal;
    var app;
    var event_confirm;
    var showModal = function (config, callback) {
        $modal.modal("show");
        app.init();
        event_confirm = callback;
        if (config && config.ThemeType) {
            app.ThemeType = config.ThemeType;

            if (config.ShowConfig) {
                if (!config.ShowConfig.mainColor) {
                    config.ShowConfig.mainColor = "";
                }
                if (!config.ShowConfig.bgColor) {
                    config.ShowConfig.bgColor = "";
                }
                if (!config.ShowConfig.bgNoColor) {
                    config.ShowConfig.bgNoColor = false;
                }
                if (!config.ShowConfig.showType) {
                    config.ShowConfig.showType = "0";
                }
                if (!config.ShowConfig.showIcon) {
                    config.ShowConfig.showIcon = "";
                }
                if (!config.ShowConfig.showIconColor) {
                    config.ShowConfig.showIconColor = "";
                }
                if (!config.ShowConfig.showIconFontSize) {
                    config.ShowConfig.showIconFontSize = "64px";
                }
                if (!config.ShowConfig.showTitleColor) {
                    config.ShowConfig.showTitleColor = "";
                }
                if (!config.ShowConfig.showTitleFontSize) {
                    config.ShowConfig.showTitleFontSize = "14px";
                }
                if (!config.ShowConfig.showTitleFontFamily) {
                    config.ShowConfig.showTitleFontFamily = "微软雅黑";
                }
                if (!config.ShowConfig.showPercentColor) {
                    config.ShowConfig.showPercentColor = "";
                }
                if (!config.ShowConfig.showPercentColor2) {
                    config.ShowConfig.showPercentColor2 = "";
                }
                if (!config.ShowConfig.showPercentFontSize) {
                    config.ShowConfig.showPercentFontSize = "14px";
                }
                if (!config.ShowConfig.showPercentFontFamily) {
                    config.ShowConfig.showPercentFontFamily = "微软雅黑";
                }
                if (!config.ShowConfig.fontFamily) {
                    config.ShowConfig.fontFamily = "微软雅黑";
                }
                if (!config.ShowConfig.mainSpace) {
                    config.ShowConfig.mainSpace = "10px";
                }
                if (!config.ShowConfig.mainPadding) {
                    config.ShowConfig.mainPadding = "";
                }
                if (!config.ShowConfig.borderType) {
                    config.ShowConfig.borderType = "0";
                }
                app.ShowConfig = config.ShowConfig;
            }
        }
    }
    var init = function () {
        $modal = $("#modalMeasureStickerDesign");
        app = new Vue({
            el: "#modalMeasureStickerDesign .modal-content",
            data: {
                ThemeType: "999",
                fonts: modules.ShareData.Fonts,
                ShowConfig: {
                    showTitle: '',
                    showTitleColor: '',
                    showTitleFontSize: '14px',
                    showTitleFontFamily: '微软雅黑',
                    showPercentColor: '',
                    showPercentColor2: '',
                    showPercentFontSize: '14px',
                    showPercentFontFamily: '微软雅黑',
                    fontsize: '30px',
                    fontFamily: '微软雅黑',
                    mainColor: '',
                    bgColor: '',
                    bgNoColor: false,
                    showType: '0',    //0=完整模式，1=精简模式
                    showIcon: '',
                    showIconColor: '',
                    showIconFontSize: '64px',
                    borderType: '0',      //0=保留，1=去除
                    mainSpace: '10px',
                    mainPadding: ''
                }
            },
            methods: {
                init: function () {
                    //初始化tab0
                    var $tab0 = $("#panel-MeasureStickerDesign-00 .panel-body").empty();
                    if (SiteTheme === 'mac-mojave') {
                        dict_theme = Enumerable.from(dict_theme_b).orderBy("x=>x.Order").toArray();
                    }
                    else {
                        dict_theme = Enumerable.from(dict_theme).orderBy("x=>x.Order").toArray();
                    }
                    //dict_theme = Enumerable.from(dict_theme).orderBy("x=>x.Order").toArray();
                    dict_theme.forEach(function (item) {
                        $("<div>").addClass("tabledesign_mode").append(function () {
                            return $("<a>").addClass("thumbnail").attr({ "href": "###" }).append(function () {
                                return $("<img>").data("theme", item.ClassName).attr({ "src": SiteRootPath + "/Content/Images/MeasureSticker/" + item.Iocn });
                            });
                        }).appendTo($tab0);
                    });
                    $("#panel-MeasureStickerDesign-00 .panel-body .tabledesign_mode img").bind("click", function () {
                        app.ThemeType = $(this).data("theme");
                        app.ShowConfig.mainColor = '';
                        app.ShowConfig.bgColor = '';
                        app.ShowConfig.showType = '0';
                        app.ShowConfig.showIcon = '';
                        app.ShowConfig.showIconColor = '';
                        app.ShowConfig.showTitleColor = '';
                        app.ShowConfig.borderType = '0';
                        app.ShowConfig.mainPadding = "";
                    });

                    //初始化tab1
                    //var $tab1 = $("#panel-MeasureStickerDesign-01 .panel-body").empty();
                    //dict_theme_b = Enumerable.from(dict_theme_b).orderBy("x=>x.Order").toArray();
                    //dict_theme_b.forEach(function (item) {
                    //    $("<div>").addClass("tabledesign_mode").append(function () {
                    //        return $("<a>").addClass("thumbnail").attr({ "href": "###" }).append(function () {
                    //            return $("<img>").data("theme", item.ClassName).attr({ "src": SiteRootPath + "/Content/Images/MeasureSticker/" + item.Iocn });
                    //        });
                    //    }).appendTo($tab1);
                    //});
                    //$("#panel-MeasureStickerDesign-01 .panel-body .tabledesign_mode img").bind("click", function () {
                    //    app.ThemeType = $(this).data("theme");
                    //});

                    this.ThemeType = "999";
                },
                confirm: function () {
                    $modal.modal("hide");
                    if (event_confirm) {
                        var config = {
                            ThemeType: this.ThemeType,
                            ShowConfig: this.ShowConfig
                        }
                        event_confirm(config);
                    }
                }
            }
        });
    }

    module.exports = {
        init: init,
        show: showModal
    }
});