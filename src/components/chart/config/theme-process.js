﻿/**
 * 进度图设置
 */
define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../../data/sharedata')
    }

    var dict_theme = [
        {
            Name: "圆",
            Iocn: "circle.png",
            ClassName: "circle",
            Order: 0
        }, {
            Name: "水平",
            Iocn: "horizontal.png",
            ClassName: "horizontal",
            Order: 1
        }, {
            Name: "垂直",
            Iocn: "vertical.png",
            ClassName: "vertical",
            Order: 2
        }
    ];

    var $modal;
    var app;
    var event_confirm;
    var showModal = function (config, callback) {
        $modal.modal("show");
        app.init();
        event_confirm = callback;
        if (config && config.ThemeType) {
            app.ThemeType = config.ThemeType;

            if (config.ShowConfig) {
                if (!config.ShowConfig.ShowTitle) {
                    config.ShowConfig.ShowTitle = "";
                }
                if (!config.ShowConfig.StatisticType) {
                    config.ShowConfig.StatisticType = "1";
                }
                if (!config.ShowConfig.StatisticMaxValue) {
                    config.ShowConfig.StatisticMaxValue = "";
                }
                app.ShowConfig = config.ShowConfig;
            }
        }
    }
    var init = function () {
        $modal = $("#modalProcessDesign");
        app = new Vue({
            el: "#modalProcessDesign .modal-content",
            data: {
                ThemeType: "999",   //horizontal==水平，vertical==垂直，circle==圆
                ShowConfig: {
                    ShowTitle:"",
                    StatisticType: "1",
                    StatisticMaxValue: ""
                }
            },
            methods: {
                init: function () {
                    //初始化tab0
                    
                    this.ThemeType = "999";
                },
                confirm: function () {
                    $modal.modal("hide");
                    if (event_confirm) {
                        var config = {
                            ThemeType: this.ThemeType,
                            ShowConfig: this.ShowConfig
                        }
                        event_confirm(config);
                    }
                },
                change: function (event) {
                    //暂无使用
                    console.log(event);
                }
            }
        });
    }

    module.exports = {
        init: init,
        show: showModal
    }
});