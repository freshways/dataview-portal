﻿/**
 * 计数器的个性化配置
 */
define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../../data/sharedata')
    }
    var $modal;
    var app;
    var event_confirm;
    var showModal = function (config,callback) {
        $modal.modal("show");
        app.init();
        event_confirm = callback;
        if (config && typeof config.leftText != "undefined") {
            app.leftText = config.leftText;
            app.leftTextColor = config.leftTextColor;
            app.leftTextFamily =config.leftTextFamily;
            app.leftTextSize = config.leftTextSize;
            app.rightTextColor = config.rightTextColor;
            app.rightTextFamily = config.rightTextFamily;
            app.rightTextSize=config.rightTextSize;
            app.rectBgColor = config.rectBgColor;
            app.align = config.align||"left";
        }
    }
    var init = function () {
        $modal = $("#modalFlipperCounterConfig");
        app = new Vue({
            el: "#modalFlipperCounterConfig .modal-content",
            data: {
                fonts: modules.ShareData.Fonts,
                leftText: "",
                leftTextColor: "gary",
                leftTextFamily: "微软雅黑",
                leftTextSize: 12,
                rightTextColor: "#fa8c16",
                rightTextFamily: "微软雅黑",
                rightTextSize: 24,
                rectBgColor: "#595959",
                align:"left"
            },
            methods: {
                init: function () {
                    this.leftText = "";
                    this.leftTextColor = "gary";
                    this.leftTextFamily = "微软雅黑";
                    this.leftTextSize = 12;
                    this.rightTextColor = "#fa8c16";
                    this.rightTextFamily = "微软雅黑";
                    this.rightTextSize = 24;
                    this.rectBgColor = "#595959";
                    this.align = "left";
                },
                confirm:function(){
                    $modal.modal("hide");
                    if(event_confirm){
                         var config={
                        leftText:this.leftText,
                        leftTextColor:this.leftTextColor,
                        leftTextFamily:this.leftTextFamily,
                        leftTextSize:Number(this.leftTextSize),
                        rightTextColor:this.rightTextColor,
                        rightTextFamily:this.rightTextFamily,
                        rightTextSize:Number(this.rightTextSize),
                             rectBgColor: this.rectBgColor,
                             align: this.align
                    }
                        event_confirm(config);
                    }
                }
            }
        });
    }
    module.exports = {
        init: init,
        show: showModal
    }
});