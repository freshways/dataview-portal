// import * as matter from 'gray-matter';
import {URL_PARAMS} from "@/utils/utils";

export const store = {
  cdnRoot: '',
  version: '',
  locale: '',
  isEdit: true,
  darkMode: true,
  enableDecal: 'decal' in URL_PARAMS,
  renderer: URL_PARAMS.renderer || 'canvas',

  typeCheck: URL_PARAMS.editor === 'monaco',
  useDirtyRect: 'useDirtyRect' in URL_PARAMS,

  runCode: '',
  sourceCode: '',
  chartDict: [],
  runHash: 0,
  isMobile: window.innerWidth < 600,

  editorStatus: {
    type: '',
    message: ''
  },

  globalFilter: {
    date: {}
  }
};

export function updateRunHash() {
  store.runHash++;
}

export function checkOpt(columns, rows, subDimension, rule) {
  let flag = true;
  if (!rows) {
    rows = []
  }
  if (!columns) {
    columns = []
  }
  if (!subDimension) {
    subDimension = []
  }
  if (rule.col) {
    flag = flag && columns.length >= rule.col
  }
  if (rule.row) {
    flag = flag && rows.length >= rule.row
  }
  if (rule.sub) {
    flag = flag && subDimension.length >= rule.sub
  }
  const fields = rows.concat(columns);
  if (rule.time) {
    flag = flag && fields.find(t => t.dataType === 4)
  }
  if (rule.geo) {
    flag = flag && fields.find(t => t.geoMarker)
  }
  return flag;
}
