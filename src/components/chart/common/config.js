import {store} from "@/components/chart/common/store";

const THEMES = {
  technology: [
    '#3F51B5',
    '#03A9F4',
    '#4CAF50',
    '#f9e264',
    '#FF9800',
    '#f47a75',
    '#05f8d6',
    '#009db2',
    '#024b51',
    '#0780cf',
    '#765005'
  ],
  dark: [
    '#4992ff',
    '#7cffb2',
    '#fddd60',
    '#ff6e76',
    '#58d9f9',
    '#05c091',
    '#ff8a45',
    '#8d48e3',
    '#dd79ff'
  ],
  default: [
    '#5470c6',
    '#91cc75',
    '#fac858',
    '#ee6666',
    '#73c0de',
    '#3ba272',
    '#fc8452',
    '#9a60b4',
    '#ea7ccc'
  ]
};

/**
 * 通用图表配置
 */
const presets = [
  {
    id: "1",
    title: "亮调",
    config: {
      GlobalColor: "#0082fc",
      MultiColors: THEMES.technology,
      axisStyle: {
        tickColor: "#e5e5e5",
        fontColor: "#999999",
      },
      textStyle: {
        "fill": "#000000",
      },
      style: {
        //标题
        title: {
          color: 'gray'//颜色
        },
        //度量
        measure: {
          color: '#1b2b38'//颜色
        }
      }
    },
    chartConfig: [
      {
        Id: 0,
        Name: "LineChart",
        Cname: "线图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          xAxisLabelRotate: 0,
          fillArea: 0,
          showSplitLine: true,
          showAxisLine: true,
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          yAxisName2: {show: true, name: ""},
          lineSmooth: 0,
          showAxisLabel: {
            category: true,
            value: true,
            value2: true
          },
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          labelFormater: "name:value",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "名称:值", value: "name:value"}, {name: "值", value: "value"}
          ],
          valueRange: {
            min: null,
            max: null
          },
          valueRange2: {
            min: null,
            max: null
          }
        }
      },
      {
        Id: 1,
        Name: "BarChart",
        Cname: "条形图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          //谷值
          valley: {enable: false, value: 0, color: "#FF0000"},
          //峰值
          peak: {enable: false, value: 100, color: "#FF3300"},
          xAxisLabelRotate: 0,
          labelRotate: 0,
          showSplitLine: true,
          showAxisLine: true,
          labelPosition: "foot",
          splitLineMode: "all",
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          yAxisName2: {show: true, name: ""},
          lineSmooth: 0,
          labelFormater: "name:value",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "名称:值", value: "name:value"}, {name: "值", value: "value"}
          ],
          showAxisLabel: {
            category: true,
            value: true,
            value2: true
          },
          showToolbox: true,
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          valueRange: {
            min: null,
            max: null
          },
          valueRange2: {
            min: null,
            max: null
          },
          barBorderRadius: [0, 0, 0, 0]
        }
      },
      {
        Id: 2,
        Name: "MapChart",
        Cname: "地图",
        Config: {
          scale: 0.8,
          borderColor: "#000000",
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          showLegend: true,
          legendPosition: "right",
          legendPostions: [{label: "左上", value: "left"}, {label: "左下", value: "leftBottom"}, {
            label: "右上",
            value: "right"
          }, {label: "右下", value: "rightBottom"}]
        }
      },
      {
        Id: 3,
        Name: "PieChart",
        Cname: "饼图",
        Config: {
          scale: 0.12,
          scaleLabel: "内圈缩放比例",
          roseType: "radius",
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          labelFormater: "<%=name%>: <%=value%>",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "维度", value: "<%=name%>"}, {name: "占比", value: "<%=ratio%>"}, {
              name: "维度:度量值",
              value: "<%=name%>: <%=value%>"
            }, {name: "维度:[占比]", value: "<%=name%>: [<%=ratio%>]"}, {
              name: "维度:度量值[占比]",
              value: "<%=name%>: <%=value%> [<%=ratio%>]"
            }
          ],
          showLegend: true,
          legendPosition: "top",
          outerScale: 0.8,
          outerScaleLabel: "外圈缩放比例",
          legendPostions: [
            {label: "顶部", value: "top"},
            {label: "左上", value: "left"},
            {label: "左下", value: "leftBottom"},
            {label: "右上", value: "right"},
            {label: "右下", value: "rightBottom"},
            {label: "底部", value: "bottom"}
          ]
        }
      },
      {
        Id: 4,
        Name: "StackBarChart",
        Cname: "堆积柱图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          xAxisLabelRotate: 0,
          labelRotate: 0,
          showSplitLine: true,
          showAxisLine: true,
          labelPosition: "foot",
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          showAxisLabel: {
            category: true,
            value: true
          },
          showToolbox: true,
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          labelFormater: "name:value",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "名称:值", value: "name:value"}, {name: "值", value: "value"}
          ],
          barBorderRadius: [0, 0, 0, 0]
        }
      },
      {
        Id: 5,
        Name: "StackLineChart",
        Cname: "堆积图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          xAxisLabelRotate: 0,
          fillArea: 0,
          showSplitLine: true,
          showAxisLine: true,
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          lineSmooth: 0,
          showAxisLabel: {
            category: true,
            value: true
          },
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}]
        }
      },
      {
        Id: 6,
        Name: "TableChart",
        Cname: "文本表格",
        Config: {
          HeaderConfig: {
            fontfamily: "微软雅黑",
            fontsize: 14,
            fontweight: "bold", //normal bold
            fontstyle: "normal", //normal italic
            fontcolor: "#000000",
            bgcolor: "#ffffff",
            border_bottom_color: "#555555",
            border_bottom: 5
          },
          TextConfig: {
            fontfamily: "微软雅黑",
            fontsize: 12,
            fontweight: "normal", //normal bold
            fontstyle: "normal", //normal italic
            fontcolor: "#000000",
            bgcolor_odd: "",
            bgcolor_even: "",
            padding: ""
          },
          BorderConfig: {
            vertical: {
              show: true,
              style: "solid", //solid dotted dashed
              width: 1, //1~3
              color: "#dddddd"
            },
            horizontal: {
              show: true,
              style: "solid", //solid dotted dashed
              width: 1, //1~3
              color: "#dddddd"
            }
          }
        }
      },
      {
        Id: 7,
        Name: "ScatterChart",
        Cname: "散点图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 8,
        Name: "TreeMapChart",
        Cname: "矩形树图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          labelFormater: "<%=name%>: <%=value%>",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "维度", value: "<%=name%>"}, {name: "占比", value: "<%=ratio%>"}, {
              name: "维度:度量值",
              value: "<%=name%>: <%=value%>"
            }, {name: "维度:[占比]", value: "<%=name%>: [<%=ratio%>]"}, {
              name: "维度:度量值[占比]",
              value: "<%=name%>: <%=value%> [<%=ratio%>]"
            }
          ],
        }
      },
      {
        Id: 9,
        Name: "Heatmap_calendar",
        Cname: "日历热图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 10,
        Name: "BoxplotChart",
        Cname: "盒须图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 11,
        Name: "PyramidChart",
        Cname: "锥形图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 12,
        Name: "HistogramChart",
        Cname: "直方图",
        Config: {
          GlobalColor: "#001eff",
          MultiColors: [],
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 13,
        Name: "BellcurveChart",
        Cname: "正态分布曲线图",
        Config: {
          GlobalColor: "#001eff",
          MultiColors: [],
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 14,
        Name: "RadarChart",
        Cname: "雷达图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          fillArea: 0,
          showLegend: true,
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {
            label: "左下",
            value: "leftBottom"
          }, {label: "右上", value: "right"}, {label: "右下", value: "rightBottom"}, {label: "底部", value: "bottom"}]
        }
      },
      {
        Id: 15,
        Name: "GaugeChart",
        Cname: "仪表盘",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#ffffff",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 12
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#333333",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 16,
        Name: "SolidGaugeChart",
        Cname: "实线仪表盘",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#ffffff",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 12
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#333333",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 17,
        Name: "CityMapChart",
        Cname: "省市版图",
        Config: {
          GlobalColor: "#26a2ff",
          MultiColors: ["#26a2ff"],
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          showLegend: true,
          legendPosition: "leftBottom",
          legendPostions: [{label: "左上", value: "left"}, {label: "左下", value: "leftBottom"}, {
            label: "右上",
            value: "right"
          }, {label: "右下", value: "rightBottom"}]
        }
      },
      {
        Id: 18,
        Name: "FlipperChart",
        Cname: "计数器",
        Config: {
          leftText: "",
          leftTextColor: "#666",
          leftTextFamily: "微软雅黑",
          leftTextSize: 18,
          rightTextColor: "#ffd231",
          rightTextFamily: "微软雅黑",
          rightTextSize: 36,
          rectBgColor: "#0f60a7"
        }
      },
      {
        Id: 19,
        Name: "MeasureStickerChart",
        Cname: "指标贴",
        Config: {
          style: {
            padding: '24px 0px 0px 20px',
            backgroundColor: '',
            //标题
            title: {
              content: '',//内容
              fontFamily: '微软雅黑', //字体
              fontSize: '14px', //字体大小
              'padding-left': '20px',
              'margin-bottom': '0px'
            },
            //比值
            percent: {
              color1: '#FF5290',//提升的颜色
              color2: '#00DEAA',//下降的颜色
              fontFamily: '微软雅黑', //字体
              fontSize: '14px', //字体大小
              'padding-left': '16px'
            },
            //度量
            measure: {
              fontFamily: '微软雅黑', //字体
              fontSize: '24px', //字体大小
              padding: '10px 0 10px 20px'
            },
            //图标
            icon: {
              class: '', //类名
              color: '', //颜色
              fontSize: '14px' //大小
            }
          }
        }
      },
      {
        Id: 20,
        Name: "MapPieChart",
        Cname: "饼型地图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 21,
        Name: "AbreastBarChart",
        Cname: "并排条形图",
        Config: {
          GlobalColor: "#26a2ff",
          MultiColors: ["#26a2ff", "#26cd7b", "#ffd231", "#ff8854", "#0f60a7", "#ff5750", "#ff8abe", "#8dabff", "#908dff", "#6ebbfe", "#7ab77f", "#f2db97", "#d9e4ac", "#13c2c2", "#9a97a8"],
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          xAxisLabelRotate: 0,
          showSplitLine: true,
          showAxisLine: true,
          labelPosition: "foot",
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          showAxisLabel: {
            category: true,
            value: true
          },
          showToolbox: true,
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          barBorderRadius: [0, 0, 0, 0]
        }
      },
      {
        Id: 22,
        Name: "LiquidfillChart",
        Cname: "水球图",
        Config: {
          ThemeType: "circle",
          ShowConfig: {
            statisticType: 0,
            borderType: 0,
            waveAnimation: 1
          },
          GlobalColor: "#4d9de2",
          MultiColors: ["#4d9de2", "#d4edff"],
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 22,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#4d9de2",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          labelFormater: "<%=name%>:<%=value%>",
          labelFormaterTemps: [
            {name: "维度:度量值", value: "<%=name%>:<%=value%>"}, {name: "度量值", value: "<%=value%>"}
          ]
        }
      },
      {
        Id: 23,
        Name: "WordcloudChart",
        Cname: "文字云",
        Config: {
          GlobalColor: "#26a2ff",
          MultiColors: ["#26a2ff", "#26cd7b", "#ffd231", "#ff8854", "#0f60a7", "#ff5750", "#ff8abe", "#8dabff", "#908dff", "#6ebbfe", "#7ab77f", "#f2db97", "#d9e4ac", "#13c2c2", "#9a97a8"],
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 25,
        Name: "Bar3DChart",
        Cname: "3D柱状图",
        Config: {
          GlobalColor: "#26a2ff",
          MultiColors: ["#001eff", "#6678ff", "#f9a400", "#00e916"],
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 26,
        Name: "ProcessChart",
        Cname: "进度图",
        Config: {
          ThemeType: "circle",
          ShowConfig: {
            statisticType: 0,
            borderType: 0
          },
          GlobalColor: "#26a2ff",
          MultiColors: [],
          scale: 0.8,
          axisStyle: {
            tickColor: "#001EFF",
            fontColor: "#001EFF",
            fontFamily: "微软雅黑",
            fontSize: 14
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          showLegend: true,
          legendPosition: "center",
          legendPostions: [{label: "中间", value: "center"}, {label: "右边", value: "right"}]
        }
      },
      {
        Id: 27,
        Name: "CardChart",
        Cname: "动态卡片",
        Config: {
          GlobalColor: "#ffffff",
          MultiColors: [],
          scale: 0.8,
          axisStyle: {
            tickColor: "#333333",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 14
          }
        }
      }
    ]
  },
  {
    id: "2",
    title: "暗调",
    config: {
      GlobalColor: "#4992ff",
      MultiColors: THEMES.dark,
      axisStyle: {
        tickColor: "#ffffff",
        fontColor: "#ffffff"
      },
      textStyle: {
        "fill": "#ffffff"
      },
      style: {
        //标题
        title: {
          color: '#D3D3D3'//颜色
        },
        //度量
        measure: {
          color: '#FAD860'//颜色
        }
      }
    },
    chartConfig: [
      {
        Id: 0,
        Name: "LineChart",
        Cname: "线图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          xAxisLabelRotate: 0,
          fillArea: 0,
          showSplitLine: true,
          showAxisLine: true,
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          yAxisName2: {show: true, name: ""},
          lineSmooth: 0,
          showAxisLabel: {
            category: true,
            value: true,
            value2: true
          },
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          labelFormater: "name:value",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "名称:值", value: "name:value"}, {name: "值", value: "value"}
          ],
          valueRange: {
            min: null,
            max: null
          },
          valueRange2: {
            min: null,
            max: null
          }
        }
      },
      {
        Id: 1,
        Name: "BarChart",
        Cname: "条形图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          //谷值
          valley: {enable: false, value: 0, color: "#FF0000"},
          //峰值
          peak: {enable: false, value: 100, color: "#FF3300"},
          xAxisLabelRotate: 0,
          labelRotate: 0,
          showSplitLine: true,
          showAxisLine: true,
          labelPosition: "foot",
          splitLineMode: "all",
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          yAxisName2: {show: true, name: ""},
          lineSmooth: 0,
          labelFormater: "name:value",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "名称:值", value: "name:value"}, {name: "值", value: "value"}
          ],
          showAxisLabel: {
            category: true,
            value: true,
            value2: true
          },
          showToolbox: true,
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          valueRange: {
            min: null,
            max: null
          },
          valueRange2: {
            min: null,
            max: null
          },
          barBorderRadius: [0, 0, 0, 0]
        }
      },
      {
        Id: 2,
        Name: "MapChart",
        Cname: "地图",
        Config: {
          borderColor: "#ffffff",
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          showLegend: true,
          legendPosition: "right",
          legendPostions: [{label: "左上", value: "left"}, {label: "左下", value: "leftBottom"}, {
            label: "右上",
            value: "right"
          }, {label: "右下", value: "rightBottom"}]
        }
      },
      {
        Id: 3,
        Name: "PieChart",
        Cname: "饼图",
        Config: {
          scale: 0.12,
          scaleLabel: "内圈缩放比例",
          roseType: "radius",
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          labelFormater: "<%=name%>: <%=value%>",
          labelFormaterTemps: [
            {name: "维度", value: "<%=name%>"}, {name: "占比", value: "<%=ratio%>"}, {
              name: "维度:度量值",
              value: "<%=name%>: <%=value%>"
            }, {name: "维度:[占比]", value: "<%=name%>: [<%=ratio%>]"}, {
              name: "维度:度量值[占比]",
              value: "<%=name%>: <%=value%> [<%=ratio%>]"
            }
          ],
          showLegend: true,
          legendPosition: "top",
          outerScale: 0.8,
          outerScaleLabel: "外圈缩放比例",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {
            label: "左下",
            value: "leftBottom"
          }, {label: "右上", value: "right"}, {label: "右下", value: "rightBottom"}, {label: "底部", value: "bottom"}]
        }
      },
      {
        Id: 4,
        Name: "StackBarChart",
        Cname: "堆积柱图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          xAxisLabelRotate: 0,
          labelRotate: 0,
          showSplitLine: true,
          showAxisLine: true,
          labelPosition: "foot",
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          showAxisLabel: {
            category: true,
            value: true
          },
          showToolbox: true,
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          labelFormater: "name:value",
          //标签模板候选项
          labelFormaterTemps: [
            {name: "名称:值", value: "name:value"}, {name: "值", value: "value"}
          ],
          barBorderRadius: [0, 0, 0, 0]
        }
      },
      {
        Id: 5,
        Name: "StackLineChart",
        Cname: "堆积图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          xAxisLabelRotate: 0,
          fillArea: 0,
          showSplitLine: true,
          showAxisLine: true,
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          lineSmooth: 0,
          showAxisLabel: {
            category: true,
            value: true
          },
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}]
        }
      },
      {
        Id: 6,
        Name: "TableChart",
        Cname: "文本表格",
        Config: {
          ThemeType: "b_blueTable",
          HeaderConfig: {
            fontfamily: "微软雅黑",
            fontsize: 14,
            fontweight: "bold", //normal bold
            fontstyle: "normal", //normal italic
            fontcolor: "#FFFFFF",
            bgcolor: "#001eff",
            border_bottom_color: "",
            border_bottom: 0
          },
          TextConfig: {
            fontfamily: "微软雅黑",
            fontsize: 12,
            fontweight: "normal", //normal bold
            fontstyle: "normal", //normal italic
            fontcolor: "#FFFFFF",
            bgcolor_odd: "",
            bgcolor_even: "#1F2D4B",
            padding: ""
          },
          BorderConfig: {
            vertical: {
              show: true,
              style: "solid", //solid dotted dashed
              width: 1, //1~3
              color: "#707070"
            },
            horizontal: {
              show: true,
              style: "solid", //solid dotted dashed
              width: 1, //1~3
              color: "#707070"
            }
          }
        }
      },
      {
        Id: 7,
        Name: "ScatterChart",
        Cname: "散点图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 8,
        Name: "TreeMapChart",
        Cname: "矩形树图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 9,
        Name: "Heatmap_calendar",
        Cname: "日历热图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 10,
        Name: "BoxplotChart",
        Cname: "盒须图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 11,
        Name: "PyramidChart",
        Cname: "锥形图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 12,
        Name: "HistogramChart",
        Cname: "直方图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 13,
        Name: "BellcurveChart",
        Cname: "正态分布曲线图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 14,
        Name: "RadarChart",
        Cname: "雷达图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          fillArea: 0,
          showLegend: true,
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {
            label: "左下",
            value: "leftBottom"
          }, {label: "右上", value: "right"}, {label: "右下", value: "rightBottom"}, {label: "底部", value: "bottom"}]
        }
      },
      {
        Id: 15,
        Name: "GaugeChart",
        Cname: "仪表盘",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 12
          }
        }
      },
      {
        Id: 16,
        Name: "SolidGaugeChart",
        Cname: "实线仪表盘",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 12
          }
        }
      },
      {
        Id: 17,
        Name: "CityMapChart",
        Cname: "省市版图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          showLegend: true,
          legendPosition: "leftBottom",
          legendPostions: [{label: "左上", value: "left"}, {label: "左下", value: "leftBottom"}, {
            label: "右上",
            value: "right"
          }, {label: "右下", value: "rightBottom"}]
        }
      },
      {
        Id: 18,
        Name: "FlipperChart",
        Cname: "计数器",
        Config: {
          leftText: "",
          leftTextColor: "#ffffff",
          leftTextFamily: "微软雅黑",
          leftTextSize: 18,
          rightTextColor: "#ff7800",
          rightTextFamily: "微软雅黑",
          rightTextSize: 36,
          rectBgColor: "#060C25"
        }
      },
      {
        Id: 19,
        Name: "MeasureStickerChart",
        Cname: "指标贴",
        Config: {
          style: {
            padding: '24px 0px 0px 20px',
            backgroundColor: '',
            //标题
            title: {
              content: '',//内容
              fontFamily: '微软雅黑', //字体
              fontSize: '14px', //字体大小
              'padding-left': '20px',
              'margin-bottom': '0px'
            },
            //比值
            percent: {
              color1: '#FF5290',//提升的颜色
              color2: '#00DEAA',//下降的颜色
              fontFamily: '微软雅黑', //字体
              fontSize: '14px', //字体大小
              'padding-left': '16px'
            },
            //度量
            measure: {
              fontFamily: '微软雅黑', //字体
              fontSize: '24px', //字体大小
              padding: '10px 0 10px 20px'
            },
            //图标
            icon: {
              class: '', //类名
              color: '', //颜色
              fontSize: '14px' //大小
            }
          }
        }
      },
      {
        Id: 20,
        Name: "MapPieChart",
        Cname: "饼型地图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 21,
        Name: "AbreastBarChart",
        Cname: "并排条形图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          xAxisLabelRotate: 0,
          labelRotate: 0,
          showSplitLine: true,
          showAxisLine: true,
          labelPosition: "foot",
          showLegend: true,
          xAxisName: {show: true, name: ""},
          yAxisName: {show: true, name: ""},
          showAxisLabel: {
            category: true,
            value: true
          },
          showToolbox: true,
          legendPosition: "top",
          legendPostions: [{label: "顶部", value: "top"}, {label: "左上", value: "left"}, {label: "右上", value: "right"}],
          barBorderRadius: [0, 0, 0, 0]
        }
      },
      {
        Id: 22,
        Name: "LiquidfillChart",
        Cname: "水球图",
        Config: {
          ThemeType: "circle",
          ShowConfig: {
            statisticType: 0,
            borderType: 0,
            waveAnimation: 1
          },
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 22,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#4d9de2",
            "position": "top" // "top", "right", "bottom" or "left"
          },
          labelFormater: "<%=name%>:<%=value%>",
          labelFormaterTemps: [
            {name: "维度:度量值", value: "<%=name%>:<%=value%>"}, {name: "度量值", value: "<%=value%>"}
          ]
        }
      },
      {
        Id: 23,
        Name: "WordcloudChart",
        Cname: "文字云",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 11
          }
        }
      },
      {
        Id: 25,
        Name: "Bar3DChart",
        Cname: "3D柱状图",
        Config: {
          scale: 0.8,
          axisStyle: {
            tickColor: "#e5e5e5",
            fontColor: "#999999",
            fontFamily: "微软雅黑",
            fontSize: 11
          },
          textStyle: {
            "display": "block",
            "font-family": "Arial",
            "font-size": 12,
            "font-style": "normal",
            "font-weight": "normal",
            "fill": "#666",
            "position": "top" // "top", "right", "bottom" or "left"
          }
        }
      },
      {
        Id: 26,
        Name: "ProcessChart",
        Cname: "进度图",
        Config: {
          ThemeType: "circle",
          ShowConfig: {
            statisticType: 0,
            borderType: 0
          },
          GlobalColor: "#cccccc",
          MultiColors: [],
          scale: 0.8,
          axisStyle: {
            tickColor: "#616161",
            fontColor: "#ffffff",
            fontFamily: "微软雅黑",
            fontSize: 12
          },
          showLegend: true,
          legendPosition: "center",
          legendPostions: [{label: "中间", value: "center"}, {label: "右边", value: "right"}]
        }
      },
      {
        Id: 27,
        Name: "CardChart",
        Cname: "动态卡片",
        Config: {
          GlobalColor: "#383838",
          MultiColors: [],
          scale: 0.8,
          axisStyle: {
            tickColor: "#FFFFFF",
            fontColor: "#FFFFFF",
            fontFamily: "微软雅黑",
            fontSize: 14
          }
        }
      }
    ]
  }
];

//切换亮调/暗调
export const changeTone = workBook => {
  let index = store.darkMode ? 1 : 0;
  const item = presets[index]
  const tempConfigDefault = Object.assign({}, item.config);
  _.merge(workBook.chartConfig, tempConfigDefault)
}

/**
 * 获取默认图表配置
 * @param chartMode 图表类型
 * @param index 0深色模式 1浅色模式，如不传，默认按照当前系统模式
 * @returns {*}
 */
export const getChartConfigByChartMode = chartMode => {
  let index = store.darkMode ? 1 : 0;
  const p = presets[index];
  const chartConfig = p.chartConfig.find(d => d.Id === chartMode)

  let config = Object.assign({}, p.config);
  if (chartConfig) {
    return _.merge(chartConfig.Config, config);
  }
}
