﻿

/**
 * bar chart
 */
/* global d3, undersore, _, document, window */

define(function (require, exports, module) {
    var modules={
        ShareData:require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options){


        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }
        var dimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
        var measure   = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];

        var values, category, reverse, tipFormat, color, scale;
        var config = options.chartConfig;
        if (!config) {
            config = {
                GlobalColor: "#4E79A7",
                scale: 0.8,
                axisStyle: {
                    tickColor : "#000",
                    fontColor : "#000",
                    fontFamily: "Arial",
                    fontSize  : 15
                },
                textStyle: {
                    "display"     : "none",
                    "font-family" : "Arial",
                    "font-size"   : 15,
                    "font-style"  : "normal",
                    "font-weight" : "normal",
                    "fill"        : "#333",
                    "position"    : "top"
                }
                /*
                valley: { enable: false, value: 0, color: "#ff0000" },
                peak: { enable: false, value: 100, color: "#0fff00" }
                */
            };
        }

        (function () {
            var dKey = dimension.name, mKey = measure.name;

            // 如果为时间（数值）升序排列
            if (dimension.dataType === 4 || dimension.dataType === 2) {
                data.sort(function(a, b) { return a[dKey] - b[dKey]; });
            }

            values   = data.map(function (d) { return d[mKey]; });
            category = data.map(function (d) { return d[dKey]; });

            tipFormat = d3.util.tipParse(
                [{ key: dKey, value: "x" }, { key: mKey, value: "y" }],
                options.TooltipFormat);

            color = (config.GlobalColor + "$")
                    .repeat(values.length)
                    .split("$");
            color.pop();

        })();

        /* SlaveType: 1 -> dimension || 0 -> measure */
        reverse = !!columnFields[0].slaveType;
        scale = config.scale;


        return bar(svgSelector, {
            values: values,
            category: category,
            isReverse: reverse,
            scale: scale,
            color: color,
            tipFormat: tipFormat,
            colDimension: dimension,
            rowMeasure: measure,
            size:options.size,
            textStyle: config.textStyle,
            axisStyle: config.axisStyle,
            peak: config.peak,
            valley: config.valley
        });
    };



function bar (selector, options) {

    var opt = {
        //size    : { width: 450, height: 300 },
        category: [],
        values  : [],
        color   : d3.schemeCategory20c,
        isReverse   : false,

        scale   : 0.8, // range: [0, 1], oScale padding
        tipFormat   : null,
        colDimension: null,
        rowMeasure  : null,
        textStyle   : {},
        axisStyle   : {},
        /*
        valley: { enable: false, value: 0, color: "#ff0000" },
        peak: { enable: false, value: 100, color: "#0fff00" }
        */
    };

    // output object
    var chart = {
        status: opt.isReverse ? "herizontal" : "vertical",
        onselected: null
    };


    opt = d3.util.extend(opt, options);

    var container = d3.select(selector).node().parentElement;
    if (!opt.size) {
        opt.size = d3.util.getInnerBoxDim (container);
    }

    // preserve the original size
    // used for chart type reverse
    opt.size._w = opt.size.width;
    opt.size._h = opt.size.height;

    var svg = d3.select(container)
        .html("")
        .append("svg")
        .attr("class", "Chart-To-Be-Export")
        .attr("width", opt.size.width)
        .attr("height", opt.size.height)
        .attr("version", "1.1")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .style("font", "12px arial, sans-serif");


    // ordinalScale for category, linearScale for values
    var oScale = d3.scaleBand().domain(opt.category).padding(1 - opt.scale),
        lScale = d3.scaleLinear(),
        oAxis, lAxis, lAxisGen, oAxisGen,
        gMain, gYaxis, gXaxis, gBrush, gChart, gText,
        minGraphDim = 100,
        nodes, // data to be bind on bars
        ceil = Math.ceil,
        //numFix = d3.format(",." + (opt.rowMeasure.NumberFixed || 0) + "f"),
        brush,
        tooltip, contextMenu,
        textStyle = {
            "display"     : "none",    // "none" or "block"
            "font-family" : "Arial",   // "Arial", "Helvetica", "Times"
            "font-size"   : 12,
            "font-style"  : "normal",  // "normal" or "italic"
            "font-weight" : "normal",  // "normal" or "bold"
            "fill"        : "#ff0000", // text color
            "position"    : "top"      // "top", "right", "bottom" or "left"
        },
        labelD = opt.colDimension.Cname || "",
        labelM = opt.rowMeasure.Cname || "",
        margin = { top: 10, right: 10, bottom: 10, left: 10 };

    // overwrite the default text label setting
    _.extend(textStyle, opt.textStyle);

    // singolton function
    var getTooltip = function () {
        return tooltip || (tooltip = d3.select(document.body).append("div").attr("class", "d3-ttip").node());
    };

    var menuOnSingleItem = [
        {title: "删除当前节点", symbol: "&#10008;", action: "delete"},
        {title: "升序排列", symbol: "&#8648;", action: "ascend"},
        {title: "降序排列", symbol: "&#8650;", action: "descend"},
        { title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg" },
        { title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png" }
    ];
    var menuOnBlank = menuOnSingleItem.slice(1);
    var menuOnBrush = [
        {title: "删除框选节点", symbol: "&#10008;", action: "deleteNodes"}
    ];
    contextMenu = d3.util.getChartContextMenu();
    contextMenu.clickFun = function (command, target) {
        this.hide();
        switch (command.action) {
            case "delete" : chart.delete([d3.select(target).datum()._index]); break;
            case "ascend" : chart.ascend(); break;
            case "descend": chart.descend(); break;
            case "export" : chart.export(command.p1); break;
            case "deleteNodes" :
                chart.delete(target.map(node => { return d3.select(node).datum()._index; }));
                break;
            default: console.log("... Catch YOU! ...");
        }
    };

    function svgSizeCheck (yAxis, xAxis, svgSize) {

        var temp, yBox, xBox, size;

        // Step1: get width of yAxis
        var yWidth = yAxis.getSize().width;

        // Step2: set xAxis scale range and get xAxis height
        temp = svgSize.width - margin.left - margin.right - yWidth;
        var xHeight = xAxis.range([0,
                        temp < minGraphDim ? minGraphDim : temp])
                    .getSize().height;

        // Step3: set yAxis range (height)
        temp = svgSize.height - margin.top - margin.bottom - xHeight;
        yAxis.range([0,
            temp < minGraphDim ? minGraphDim : temp]);

        // re-assure
        yBox = yAxis.getSize();
        xBox = xAxis.getSize();
        size = {
            width: yBox.width + xBox.width + margin.left + margin.right,
            height: xBox.height + yBox.height + margin.top + margin.bottom
        };

        if (svgSize.width < size.width) { svgSize.width = size.width; }
        if (svgSize.height < size.height) { svgSize.height = size.height; }

        return { yBox: yBox, xBox: xBox };
    }
    function normalise (boo) { // boo - true | false
        svg.html("");

        // In case memory leak after redrawing
        gMain  = null; gYaxis = null; gXaxis = null;
        gBrush = null; gChart = null; gText = null;

        gMain  = svg.append("g").attr("class", "g-main");
        gYaxis = gMain.append("g").attr("class", "axis g-axisY");
        gXaxis = gMain.append("g").attr("class", "axis g-axisX");
        gBrush = gMain.append("g").attr("class", "g-brush");
        gChart = gMain.append("g").attr("class", "g-chart");
        gText  = gMain.append("g").attr("class", "g-text");

        var processFun = (function () {
            var peak = function () {},
                valley = function () {};
            function fx (d, i) {
                return {
                        x     : opt.category[i],
                        y     : d,
                        color : peak(d) || valley(d) || opt.color[i] || "black",
                        _index: i
                    };
            }

            fx.peak = function (fun) {
                if (typeof fun != "function") { return peak; }
                peak = fun;
                return fx;
            };
            fx.valley = function (fun) {
                if (typeof fun != "function") { return valley; }
                valley = fun;
                return fx;
            };

            return fx;
        })();

        if (opt.peak && opt.peak.enable) {
            processFun.peak(function (d) {
                return d >= opt.peak.value ? opt.peak.color : false;
            });
        }
        if (opt.valley && opt.valley.enable) {
            processFun.valley(function (d) {
                return d <= opt.valley.value ? opt.valley.color : false;
            });
        }
        // follow data is using for binding
        // if "boo" is true "nodes" will be re-calculated
        if (!nodes || boo) {
            nodes = opt.values.map(processFun);
        }

        var spanOnYaxis = d3.extent(nodes, function (d) { return d.y; });
        if (spanOnYaxis[0] > 0) { lScale.domain([0, spanOnYaxis[1] * 1.1]); }
        else { lScale.domain([spanOnYaxis[0] * 1.1, spanOnYaxis[1] * 1.1]); }
    }

    function verticalBar () {

        // bar status change
        chart.status = "vertical";

        lScale.range([0, minGraphDim]);
        oScale.range([0, minGraphDim]);

        lAxis = d3.axisLeft().scale(lScale);
        oAxis = d3.axisBottom().scale(oScale);

        lAxisGen = d3.util.axis.linear({
            scale : lScale,
            axis  : lAxis,
            holder: gYaxis,
            orient: "left",

            label: labelM,
            style: opt.axisStyle
        });
        oAxisGen = d3.util.axis.ordinal({
            scale : oScale,
            axis  : oAxis,
            holder: gXaxis,
            orient: "bottom",

            label: labelD,
            style: opt.axisStyle
        });

        var boxes = svgSizeCheck(lAxisGen, oAxisGen, opt.size);
        var left, bottom, vHeigth, hWidth;
        left   = boxes.yBox;
        bottom = boxes.xBox;
        vHeigth = left.height;
        hWidth = bottom.width;

        svg.attr("width", opt.size.width)
            .attr("height", opt.size.height);

        /* brush setting and utilising */
        brush = d3.brushX()
            /* using "scale.range()" will be more scure */
            /* since "axis.call(axisGen)" may extent scale's range */
            .extent([[0, 0], [oScale.range()[1], lScale.range()[1]]])
            .filter(brushFilter)
            .on("start", onBrushStart)
            .on("brush", onBrushMove)
            .on("end",   onBrushEnd);


        // re-assign dimension for each group and generate axis
        gMain.attr("transform", "translate(" +
                   [left.width + margin.left, margin.top] + ")");
        gXaxis.attr("transform", "translate(0," + vHeigth + ")");
        //gYaxis.attr("transform", "translate(0,0)");
        oAxisGen.drawAxis();
        lAxisGen.drawAxis();
        gBrush.call(brush);

        gBrush.select(".selection").on("contextmenu", onBrushContextMenu);
        gBrush.select(".overlay").on("contextmenu", onBlankBrushContextMenu);

        gChart.selectAll(".g-rect")
            .data(nodes)
            .enter()
            .append("g")
            .attr("class", "g-rect")
            .attr("transform", function (d, i) {
                return "translate(" + oScale(d.x) + ",0)";
            })
            .on("mouseenter",  onMouseEnter)
            .on("mouseleave",  onMouseLeave)
            .on("contextmenu", onContextMenu)
            .on("click", onClick)
            .append("rect")
            .each(function (d, i) {
                var rect = d3.select(this);
                rect.attr("width", oScale.bandwidth())
                    .attr("height", Math.abs(lScale(0) - lScale(d.y)))
                    .attr("y", d.y > 0 ? vHeigth - lScale(d.y) : vHeigth - lScale(0))
                    .attr("fill", d.color)
                    .attr("stroke", "black")
                    .attr("stroke-width", 0);
            });

        gChart.append("line")
            .attr("class", "d3-baseline")
            .style("display", lScale.domain()[0] < 0 ? "block" : "none")
            .attr("stroke", "gray")
            .attr("stroke-width", 1)
            .attr("x1", hWidth)
            .attr("y1", vHeigth - lScale(0))
            .attr("y2", vHeigth - lScale(0));

        var textCoordinates;
        textCoordinates = nodes.map(function (d) {
            return {
                x: oScale(d.x) + (oScale.bandwidth() * 0.5),
                y: vHeigth - lScale(d.y),
                //text: numFix(d.y),
                text: d.y.toMeasureFormart(opt.rowMeasure.numberFormat),
                _index: d._index
            };
        });

        gText.selectAll("text")
            .data(textCoordinates)
            .enter()
            .append("text")
            .each(function (d) {
                d3.select(this)
                    .attr("x", d.x).attr("y", d.y).text(d.text);
            });

        chart.label = null;
        chart.label = new d3.util.TextDecorate(gText, 3, [[0,0], [hWidth, vHeigth]], textStyle);
        chart.label.init();

    } // verticalBar END

    function herizontalBar () {

        // chart status change
        chart.status = "herizontal";

        lScale.range([0, minGraphDim]);
        oScale.range([0, minGraphDim]);

        lAxis = d3.axisBottom().scale(lScale);
        oAxis = d3.axisLeft().scale(oScale);

        lAxisGen = d3.util.axis.linear({
            scale : lScale,
            axis  : lAxis,
            holder: gXaxis,
            orient: "bottom",

            label: labelM,
            style: opt.axisStyle
        });
        oAxisGen = d3.util.axis.ordinal({
            scale : oScale,
            axis  : oAxis,
            holder: gYaxis,
            orient: "left",

            label: labelD,
            style: opt.axisStyle
        });


        var boxes = svgSizeCheck(oAxisGen, lAxisGen, opt.size);
        var hWidth, vHeigth, left, bottom;
        left   = boxes.yBox;
        bottom = boxes.xBox;
        vHeigth = left.height;
        hWidth = bottom.width;

        svg.attr("width", opt.size.width)
            .attr("height", opt.size.height);


        /* brush setting and utilising */
        brush = d3.brushY()
            /* using "scale.range()" will be more scure */
            /* since "axis.call(axisGen)" may extent scale's range */
            .extent([[0, -oScale.range()[1]], [lScale.range()[1], 0]])
            .filter(brushFilter)
            .on("start", onBrushStart)
            .on("brush", onBrushMove)
            .on("end",   onBrushEnd);

        gMain.attr("transform", "translate(" +
                   [margin.left + left.width, margin.top] + ")");
        gChart.attr("transform", "translate(0," + vHeigth + ")");
        gText.attr("transform", "translate(0," + vHeigth + ")");
        gBrush.attr("transform", "translate(0," + vHeigth + ")");
        gXaxis.attr("transform", "translate(0," + vHeigth + ")");
        lAxisGen.drawAxis();
        oAxisGen.drawAxis();
        gBrush.call(brush);

        gBrush.select(".selection").on("contextmenu", onBrushContextMenu);
        gBrush.select(".overlay").on("contextmenu", onBlankBrushContextMenu);

        gChart.selectAll(".g-rect")
            //.data(opt.values)
            .data(nodes)
            .enter()
            .append("g")
            .attr("class", "g-rect")
            .attr("transform", function (d, i) {
                return "translate(0," +
                    (-oScale(d.x) - oScale.bandwidth()) + ")";
            })
            .on("mouseenter",  onMouseEnter)
            .on("mouseleave",  onMouseLeave)
            .on("contextmenu", onContextMenu)
            .on("click", onClick)
            .append("rect")
            .each(function (d, i) {
                var rect = d3.select(this);
                rect.attr("width", 0)
                    .attr("height", oScale.bandwidth())
                    .attr("fill", d.color)
                    .attr("width", Math.abs(lScale(0) - lScale(d.y)))
                    .attr("x", d.y > 0 ? lScale(0) : lScale(d.y))
                    .attr("stroke", "black")
                    .attr("stroke-width", 0);
            });

        gChart.append("line")
            .attr("class", "d3-baseline")
            .style("display", lScale.domain()[0] < 0 ? "block" : "none")
            .attr("stroke", "gray")
            .attr("stroke-width", 1)
            .attr("x1", lScale(0))
            .attr("x2", lScale(0))
            .attr("y1", 0)
            .attr("y2", -vHeigth);

        var textCoordinates;
        textCoordinates = nodes.map(function (d) {
            return {
                x: lScale(d.y),
                y: -oScale(d.x),
                //text: numFix(d.y),
                text: d.y.toMeasureFormart(opt.rowMeasure.numberFormat),
                _index: d._index
            };
        });

        gText.selectAll("text")
            .data(textCoordinates)
            .enter()
            .append("text")
            .each(function (d) {
                d3.select(this)
                    .attr("x", d.x).attr("y", d.y).text(d.text);
            });

        chart.label = null;
        chart.label = new d3.util.TextDecorate(gText, 3,
                    [[0,-vHeigth], [hWidth, 0]], textStyle);
        chart.label.init();


    } // herizontalBar END


    // initial drawing start
    normalise();
    if (opt.isReverse) { herizontalBar(); }
    else { verticalBar (); }

    function getRoundedData (datum) {
        return {
            x: datum.x,
            // Number API from "extensions.js"
            //y: numFix(datum.y)
            y: datum.y.toMeasureFormart(opt.rowMeasure.numberFormat)
        };
    }
    // interactive function for rect
    function onMouseEnter (d, i) {
        if (d3.brushSelection(gBrush.node())) { return; } // brush show time

        var rect = d3.select(this).select("rect"),
            rectBox = rect.node().getBoundingClientRect(),
            pText;

        try {
            pText = _.template(opt.tipFormat.output());
        }
        catch (err) {
            pText = _.template("X: <span><%= x %></span><br/>" +
                               "Y: <span><%= y %></span>");
        }


        rect.attr("fill", "#d8f210");

        tooltip = getTooltip();
        d3.select(tooltip)
            .html("")
            .style("display", "block")
            .append("p")
            .attr("class", "d3-ttip-content")
            .html(pText(getRoundedData(rect.datum())));
        var scrollTop  = window.pageYOffset,
            scrollLeft = window.pageXOffset;

        tooltip.style.left = scrollLeft + rectBox.left +
            0.5 * (rectBox.width - tooltip.offsetWidth) + "px";
        tooltip.style.top  = scrollTop + rectBox.top - tooltip.offsetHeight + 10 + "px";
    }
    function onMouseLeave (d, i) {
        if (d3.brushSelection(gBrush.node())) { return; } // brush show time

        var rect = d3.select(this).select("rect");
        rect.transition("colorReverce-B")
            .attr("fill", d.color);
        d3.select(tooltip).style("display", "none");
    }

    function onClick (d) {
        if (chart.onselected) {
            chart.onselected([{
                field: opt.colDimension,
                value: [ d.x ]
            }]);
        }
    }



    // right click menue on single item
    function onContextMenu (d, i) {
        if (d3.brushSelection(gBrush.node())) {
            return onBrushContextMenu();
        }
        d3.event.preventDefault();
        var target = d3.select(this).select("rect").node();
        contextMenu.setTargetNode(target)
                   .setCurrentMenu(menuOnSingleItem)
                   .show();
    }


    // Brush related events
    function brushFilter () {
        return (d3.event.button !== 1);
    }
    function onBrushStart () {
        gChart.selectAll("rect").classed("d3-active", false);
        contextMenu.hide();
    }
    function onBrushMove () {
        var range = d3.event.selection ||
                    d3.brushSelection(gBrush.node());

        if (chart.status == "herizontal") {
            // reverse "range" sine the chart structure
            range = [ Math.abs(range[1]), Math.abs(range[0]) ];
        }

        gChart.selectAll("rect")
            .classed("d3-active", function (d) {
                return (oScale(d.x) >= range[0]) &&
                      ((oScale(d.x) + oScale.bandwidth()) <= range[1]);
            });
    }
    function onBrushEnd () {
        var xRange, nodes, values;
        xRange = d3.brushSelection(this);
        if (!xRange || !chart.onselected) { return; }

        nodes = gChart.selectAll("rect.d3-active").nodes();
        if (nodes.length === 0) { return; }


        values = nodes.map(function (node) {
                return d3.select(node).datum().x;
            });

        chart.onselected([{ field: opt.colDimension, value: values }]);
    }

    // Only aplly on Brush
    function onBrushContextMenu () {
        d3.event.preventDefault();
        onBrushMove();

        var nodes = gChart.selectAll(".d3-active").nodes();
        if (nodes.length === 0) { return; }

        contextMenu.setTargetNode(nodes)
                   .setCurrentMenu(menuOnBrush)
                   .show();
    }

    function onBlankBrushContextMenu () {
        d3.event.preventDefault();
        contextMenu.setCurrentMenu(menuOnBlank).show();
    }



    // reverse layout
    chart.reverse = function () {

        normalise();
        opt.size.width  = opt.size._w;
        opt.size.height = opt.size._h;

        // Restore the origin dimmension
        svg.attr("width", opt.size.width).attr("height", opt.size.height);

        if (this.status == "vertical") { herizontalBar(); }
        else { verticalBar(); }

        return this;
    };

    /* Reset chart to initial status, */
    /* EXCEPT chart dimension that modified by "chart.setSize" */
    chart.reset = function () {
        if (this.status == "vertical") { normalise(true); verticalBar(); }
        else { normalise(true); herizontalBar(); }

        return this;
    };

    // change the bar node color;
    function setColor (color) {
        var rects = svg.selectAll(".g-rect rect");
        if (typeof color == "string") {
            rects.each(function (d) { d.color = color; });
            nodes.forEach(function (d) { d.color = color; });
        } else {
            rects.each(function (d, i) { d.color = color[i] || "black"; });
            nodes.forEach(function (d, i) { d.color = color[i] || "black"; });
        }
        rects.transition().attr("fill", function (d) { return d.color; });
        return this;
    }

    // unify all bars color
    chart.setGlobalColor = setColor;

    // assign color by catorgory
    chart.sequenceColor  = setColor;


    chart.sort = function (type) {
        function ascend (a, b) { return b.y - a.y; }
        function descend (a, b) { return a.y - b.y; }

        if (type) { nodes.sort(ascend); }
        else { nodes.sort(descend); }

        normalise();
        oScale.domain( nodes.map(function (d) { return d.x; }) );
        if (this.status == "vertical") { verticalBar(); }
        else if (this.status == "herizontal") { herizontalBar(); }
    };
    chart.ascend  = function () { this.sort(false); return this; };
    chart.descend = function () { this.sort(true);  return this; };

    /* delete node(s) */
    /* This methord can NOT chainable */
    chart.delete = function (_indexs) {

        if (_indexs.length == nodes.length) { return; }
        var parts = _.partition(nodes, function (node) {
            return _.findIndex(_indexs, index => { return index == node._index; }) == -1;
        });
        nodes = parts[0];

        oScale.domain(nodes.map(function (d) { return d.x; }));
        lScale.domain([0, d3.max(nodes, function (d) { return d.y; })]);

        // chart redraw
        if (this.status == "vertical") { normalise(); verticalBar(); }
        else { normalise(); herizontalBar(); }

        if (this.onRemoveItem) {
            this.onRemoveItem([
                { field: opt.colDimension, value: parts[1].map(node => node.x) }
            ]);
        }
    };

    // customized tooltip content formate
    chart.tipFormat = function (s) {
        if (!arguments.length) { return opt.tipFormat; }
        opt.tipFormat.input(s);
        return this;
    };

    // change chart dimension: { width: 500, height: 300 }
    chart.setSize = function (size) {
        if (!arguments.length) { return opt.size; }
        opt.size.width  = size.width;
        opt.size.height = size.height;

        // chart redraw
        svg.attr("width", size.width).attr("height", size.height);
        if (this.status == "vertical") { normalise(); verticalBar(); }
        else { normalise(); herizontalBar(); }

        return this;
    };

    chart.getLegends = function () { return []; };


    chart.getScale = function () {
        return 1 - oScale.padding();
    };
    chart.setScale = function (val) {
        if (typeof val === "string") { val = +val; }
        if (val > 1 || val < 0) { return; }

        oScale.padding(1 - val);

        // chart redraw
        if (this.status == "vertical") { normalise(); verticalBar(); }
        else { normalise(); herizontalBar(); }
    };

    chart.export = function (type) {
        d3.util.exportChart(svg.node(), type || "png");
    };

    chart.removeChart = function () {
        svg.remove();
        if (tooltip) { d3.select(tooltip).remove(); }
        contextMenu.remove();

        tooltip     = null;
        contextMenu = null;
        container   = null;
        opt         = null;

        for (var pp in chart) { delete chart[pp]; }
        chart = null;
    };


    return chart;
}


module.exports = {
    build: buildChart
};

});
