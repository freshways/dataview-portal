﻿//仪表盘
define(function (require, exports, module) {
    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {
        //度量
        var measureField = rowFields.find(function (d) { return d.slaveType === 1; }) || columnFields.find(function (d) { return d.slaveType === 1; });
        if (!measureField || !data.length) {
            return false;
        }
        var config = options.chartConfig;
        if (!config) {
            config = {
                GlobalColor: "#4E79A7",
                MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
                scale: 0.8,
                axisStyle: {
                    tickColor: "#000",
                    fontColor: "#000",
                    fontFamily: "Arial",
                    fontSize: 12
                },
                textStyle: {
                    "display": "none",
                    "font-family": "Arial",
                    "font-size": 12,
                    "font-style": "normal",
                    "font-weight": "normal",
                    "fill": "#333",
                    "position": "top"
                }
            }
        }
        var valueCur = data[0][measureField.name];
        var valueMax, valueMin;
        //确定刻度范围
        if (options.gauge) {
            valueMin = options.gauge.min;
            valueMax = options.gauge.max;
        } else {
            if (valueCur > 0) {
                valueMin = 0;
                valueMax = valueCur * 2;
            } else if (valueCur < 0) {
                valueMax = 0;
                valueMin = valueCur * 2;
            } else {
                valueMin = 0;
                valueMax = 100;
            }
        }
        var option_chart = {
            series: [
                {
                    type: 'gauge',
                    detail: {
                        show: config.textStyle["display"] !== "none",
                        fontSize: config.textStyle["font-size"],
                        fontFamily: config.textStyle["font-family"],
                        color: config.textStyle["fill"]
                    },
                    title: {
                        color: config.axisStyle.fontColor,
                        fontSize: config.axisStyle.fontSize,
                        fontFamily: config.axisStyle.fontFamily
                    },
                    axisTick: {
                        lineStyle: {
                            color: config.axisStyle.tickColor
                        }
                    },
                    axisLabel: {
                        color: config.axisStyle.fontColor,
                        fontSize: config.axisStyle.fontSize,
                        fontFamily: config.axisStyle.fontFamily
                    },
                    axisLine: {
                        lineStyle: {
                            color: [[0.2, config.MultiColors[0] || "green"], [0.8, config.MultiColors[1] || "blue"], [1, config.MultiColors[2]||"red"]]
                        }
                    },
                    min: valueMin,
                    max: valueMax,
                    data: [{ value: valueCur, name: measureField.name }]
                }
            ],
            others: {
                measureField: measureField
            }
        }
        var dom_container = $(svgSelector).children('#chart')[0];
        var chartInstance = new chart(dom_container, option_chart);
        return chartInstance;
    }
    var chart = function (dom_container, opt) {
        var self = this;
        var myChart = echarts.init(dom_container);
        var chart_opt = {}
        var labelFormatter = function (value) {
            return Number(value).toMeasureFormart(chart_opt.others.measureField.numberFormat);
        }
        var setOption = function (opt) {
            chart_opt = $.extend(true, chart_opt, opt);
            chart_opt.series[0].detail.formatter = labelFormatter;
            chart_opt.series[0].axisLabel.formatter = labelFormatter;
            myChart.setOption(chart_opt);
        }
        if (opt) {
            setOption(opt);
        }
        this.setOption = setOption;
        this.removeChart = function () {
            myChart.dispose();
        }
        this.getRange = function () {
            return {
                max: chart_opt.series[0].max,
                min: chart_opt.series[0].min
            }
        }
        this.setRange = function (gauge) {
            chart_opt.series[0].max = gauge.max;
            chart_opt.series[0].min = gauge.min;
            setOption(chart_opt);
            //opt.gauge = gauge;
        }
        this.update = function (data) {
            var valueCur = data[0][chart_opt.others.measureField.name];
            chart_opt.series[0].data[0].value = valueCur;
            setOption(chart_opt);
        }
    }
    module.exports = {
        build: buildChart
    };
});
