﻿const Card = function (selector, options) {
  var opt = {
    tipFormat: null,
    MeasureMap: null
  };
  _.extend(opt, options);


  var chart = {
    svg: null,
    onselected: null,
    onDeleteFilter: null
  };

  var rowData = opt.data.sourceData[0];

  //build chart
  $(selector).hide();
  var selectorParent = $(selector)[0].parentNode;
  $(selectorParent).find(".cardContain").remove();

  var style = '';
  style += 'font-family:' + opt.config.axisStyle.fontFamily + ';';
  style += 'font-size:' + opt.config.axisStyle.fontSize + 'px;';
  style += 'color:' + opt.config.axisStyle.fontColor + ';';
  style = 'style="' + style + '"';
  var bgColor = opt.config.GlobalColor;
  if (opt.config && opt.config.GlobalNoColor && opt.config.GlobalNoColor === true) {
    bgColor = "transparent";
  }

  var $cardContain = $('<div class="cardContain" style="background:' + bgColor + ';"></div>');
  var $cardItem = $('<div class="cardItem" ' + style + '> <span class="cardItemTitle">{{cardItemTitle}}：</span> <span class="cardItemValue">{{cardItemValue}}</span> </div>');

  var index = 0;
  var mainKey = '';
  var mainValue = '';
  for (var key in rowData) {
    var cardItemTitle = key;
    var cardItemValue = rowData[key];

    if (opt.MeasureMap[cardItemTitle]) {
      cardItemValue = Number(cardItemValue).toMeasureFormart(opt.MeasureMap[cardItemTitle].numberFormat);
    }

    var $cardItem_temp = $cardItem.prop("outerHTML");
    var $cardItem_temp = $cardItem_temp.replace('{{cardItemTitle}}', cardItemTitle);
    var $cardItem_temp = $cardItem_temp.replace('{{cardItemValue}}', cardItemValue);

    $cardContain.append($($cardItem_temp));

    if (index === 0) {
      mainKey = cardItemTitle;
      mainValue = cardItemValue;
    }
    index++;
  }

  $cardContain.bind("click", onClick);
  $(selector).after($cardContain);

  function onClick() {
    if (chart.onselected) {
      chart.onselected([{
        field: opt.colDimension,
        value: [mainValue]
      }]);
    }
  }

  return chart;

};
/**
 * 动态卡片
 */
const buildChart = function (svgSelector, data, columnFields, rowFields, options) {
  var cOpt = {};

  var config = options.chartConfig;
  if (!config || !config.GlobalColor) {
    config = {
      GlobalColor: "#4E79A7",
      MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
      scale: 0.8,
      axisStyle: {
        tickColor: "#000",
        fontColor: "#000",
        fontFamily: "Arial",
        fontSize: 12
      },
      textStyle: {
        "display": "none",
        "font-family": "Arial",
        "font-size": 12,
        "font-style": "normal",
        "font-weight": "normal",
        "fill": "#333",
        "position": "top"
      },
      ThemeType: 999
    }
  }
  cOpt.config = config;

  var dimension = (columnFields.length > 0 && columnFields[0].slaveType === 0) ? columnFields[0] : rowFields[0];
  var measure = (rowFields.length > 0 && rowFields[0].slaveType === 1) ? rowFields[0] : columnFields[0];
  cOpt.colDimension = dimension;
  cOpt.rowMeasure = measure;

  //获取所有度量字段
  var measureFields_c = Enumerable.from(columnFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields_r = Enumerable.from(rowFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields = measureFields_c.concat(measureFields_r);
  var measureMap = {};
  measureFields.forEach(function (f) {
    measureMap[f.name] = f;
  });
  cOpt.MeasureMap = measureMap;

  const sourceData = data.concat();//原始数据
  cOpt.data = {sourceData: sourceData};

  return Card(svgSelector, cOpt);
};


export const CardChart = {
  build: buildChart
};

