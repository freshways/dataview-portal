import {getChartList} from "@/api/chart";
import {TableChart} from './table'
import {PieChart} from './echart-pie-borderRadius'
import LineChart from './echart-lineArea'
import StackLineChart from './echart-lineArea'
import {BarChart} from './echart-bar'
import {MeasureStickerChart} from './jq-measure-sticker'
import {StackBarChart} from './echart-stackbar'
import {MapChart} from './echart-map'
import {AbreastBarChart} from './echart-abreastbar'
import {ScatterChart} from './d3-scatter'
import {TreeMapChart} from './echart-treemap'
import {WordcloudChart} from './echart-wordcloud'
import {FlipperChart} from './d3-flipper'
import {ProcessChart} from './jq-process'
import {CardChart} from './jq-card'
import {Bar3DChart} from './echart-bar3d'
import {LiquidfillChart} from './echart-liquidfill'
import {RadarChart} from './echart-radar'
import {PyramidChart} from './echart-funnel'
import {TrendChart} from "./echart-trend"

/**
 * 图表
 */
const modules = {
  PieChart,
  TableChart,
  LineChart,
  StackLineChart,
  BarChart,
  MapChart,
  MeasureStickerChart, StackBarChart,
  AbreastBarChart, ScatterChart, TreeMapChart,
  FlipperChart, ProcessChart, CardChart,
  Bar3DChart,
  LiquidfillChart,
  WordcloudChart,
  RadarChart,
  PyramidChart,
  TrendChart
};

let _currentChartMode = -1;

//图表字典
const chartDict = [];

export const loadChartDict = async () => {
  if (!chartDict || !chartDict.length) {
    let resp = await getChartList();
    resp.data.forEach(t=>{
      chartDict.push(t);
    })
  }
  return chartDict;
}

const buildChart = (svgSelector, data, columnFields, rowFields, chartMode, options) => {
  let fieldName;
  let builder;
  let chartModel;
  _currentChartMode = chartMode;
  const _chartmode = chartMode !== undefined ? chartMode : _currentChartMode;
  if (_chartmode && _chartmode !== -1) {
    // tableContainer.hide();
    chartModel = chartDict.find(item => {
      return item.id === _chartmode
    });
    builder = modules[chartModel.name];
    if (builder) {
      if (_chartmode === 0) { //线图
        delete options.stackType;
        //有颜色配置，颜色配置字段与维度字段一致，自动切换到饼图
        if (options.colorMark && options.colorMark.annexField) {
          fieldName = "";
          if (columnFields.length > 0 && columnFields[0].slaveType === 0) {
            fieldName = columnFields[0].name;
          }
          if (rowFields.length > 0 && rowFields[0].slaveType === 0) {
            fieldName = rowFields[0].name;
          }
          if (fieldName === options.colorMark.annexField.name) {
            chartModel = chartDict.find(t => t.id == 3);
            builder = modules[chartModel.name];
            _currentChartMode = 3;
          }
        }
      } else if (_chartmode === 1) { //条形图
        //条形图出现颜色配置项时自动变成并列条形图，颜色配置字段与维度字段一致，自动切换到饼图
        if (options.colorMark && options.colorMark.annexField) {
          fieldName = "";
          if (columnFields.length > 0 && columnFields[0].slaveType === 0) {
            fieldName = columnFields[0].name;
          }
          if (rowFields.length > 0 && rowFields[0].slaveType === 0) {
            fieldName = rowFields[0].name;
          }
          if (fieldName === options.colorMark.annexField.name) {
            chartModel = chartDict.find(t => t.id == 3);
            builder = modules[chartModel.name];
            _currentChartMode = 3;
          } else {
            builder = modules.AbreastBarChart;
            _currentChartMode = 21;
          }
        } else if (options.isYoy) {
          builder = modules.TrendChart;
        }
      } else if (_chartmode === 2) { //地图
        options.isArea = true;
        //地图出现颜色配置项时，并且非城市字段，自动变成堆叠柱图
        if (options.colorMark && options.colorMark.annexField) {
          var cityFieldName = "";
          if (columnFields.length > 0
            && columnFields[0].geoMarker != undefined
            && columnFields[0].geoMarker === "Geo-C") {
            cityFieldName = columnFields[0].name;
          }
          if (rowFields.length > 0
            && rowFields[0].geoMarker != undefined
            && rowFields[0].geoMarker === "Geo-C") {
            cityFieldName = columnFields[0].name;
          }
          if (options.colorMark.annexField.name != cityFieldName) {
            chartModel = chartDict.find(t => t.id == 4);
            builder = modules[chartModel.name];
            options.stackType = "bar";
            _currentChartMode = 4;
          }
        }
      } else if (_chartmode === 3 || _chartmode === 8) { //饼图/矩形图/锥形图
        options.stackType = "bar";
        //有颜色配置，颜色配置字段与维度字段不一致，自动切换到堆积柱图
        if (options.colorMark && options.colorMark.annexField) {
          fieldName = "";
          if (columnFields.length > 0 && columnFields[0].slaveType === 0) {
            fieldName = columnFields[0].name;
          }
          if (rowFields.length > 0 && rowFields[0].slaveType === 0) {
            fieldName = rowFields[0].name;
          }
          if (fieldName !== options.colorMark.annexField.name) {
            chartModel = chartDict.find(t => t.id == 4);
            builder = modules[chartModel.name];
            _currentChartMode = 4;
          }
        }
      } else if (_chartmode === 4) { //堆积柱图
        options.stackType = "bar";
        //没有颜色配置时自动切换到条形图
        if (!options.colorMark || !options.colorMark.annexField) {
          chartModel = chartDict.find(t => t.id == 1);
          builder = modules[chartModel.name];
          _currentChartMode = 1;
        }
      } else if (_chartmode === 5) { //堆积图
        options.stackType = "line";
        //没有颜色配置时自动切换到线图
        if (!options.colorMark || !options.colorMark.annexField) {
          chartModel = chartDict.find(t => t.id == 0);
          builder = modules[chartModel.name];
          _currentChartMode = 0;
        }
      } else if (_chartmode === 6) { //文本表格
        //有颜色配置，颜色配置字段与维度字段一致，自动切换到饼图
        if (options.colorMark && options.colorMark.annexField) {
          fieldName = "";
          if (columnFields.length > 0 && columnFields[0].slaveType === 0) {
            fieldName = columnFields[0].name;
          }
          if (rowFields.length > 0 && rowFields[0].slaveType === 0) {
            fieldName = rowFields[0].name;
          }
          if (fieldName === options.colorMark.annexField.name) {
            chartModel = chartDict.find(t => t.id == 3);
            builder = modules[chartModel.name];
            _currentChartMode = 3;
          }
        }
        //有颜色配置，颜色配置字段与维度字段不一致，自动切换到堆积柱图
        if (options.colorMark && options.colorMark.annexField) {
          fieldName = "";
          if (columnFields.length > 0 && columnFields[0].slaveType === 0) {
            fieldName = columnFields[0].name;
          }
          if (rowFields.length > 0 && rowFields[0].slaveType === 0) {
            fieldName = rowFields[0].name;
          }
          if (fieldName !== options.colorMark.annexField.name) {
            chartModel = chartDict.find(t => t.id == 4);
            builder = modules[chartModel.name];
            _currentChartMode = 4;
          }
        }
      } else if (_chartmode === 7) { //散点图
      } else if (_chartmode === 9) { //日历热图
        var dateField = columnFields.find(function (d) {
          return d.slaveType === 0 && d.dataType === 4;
        }) || rowFields.find(function (d) {
          return d.slaveType === 0 && d.dataType === 4;
        });
        //出现颜色配置自动切换到堆叠柱图
        if (options.colorMark && options.colorMark.annexField) {
          _currentChartMode = 4;
          return buildChart(svgSelector, data, columnFields, rowFields, _currentChartMode, options);
        } else if (!dateField) { //缺少日期维度自动切换到文本表
          chartModel = chartDict.find(t => t.id == 6);
          builder = modules[chartModel.name];
          _currentChartMode = 6;
        } else if (dateField.DateTimeSub !== "_ymd") { //日期没有细分到【年月日】切换到水平条
          if (dateField.UniqueName !== undefined && dateField.name === "日期") {
            //排除Cube，日期字段
            dateField.DateTimeSub = "_ymd";
          } else {
            chartModel = chartDict.find(t => t.id == 1);
            builder = modules[chartModel.name];
            _currentChartMode = 1;
          }
        }
      } else if (_chartmode === 21) { //并列条形图
        if (!options.colorMark || !options.colorMark.annexField) {
          builder = modules.BarChart;
          _currentChartMode = 1;
        }
      }
      //检查字段，如果没有分组字段或没有聚合字段则切换到文本表
      let exemptionChartMode = [10, 12, 13, 15, 16, 18, 19, 22, 26, 27];
      if (!rowFields.length && !exemptionChartMode.includes(chartMode)) {
        var _finded = columnFields.find(function (d) {
          if (d.slaveType === 0) {
            if (!d.group) {
              return true;
            }
          } else {
            if (d.aggregateMode === 0) {
              return true;
            }
          }
        });
        if (_finded) {
          chartModel = chartDict.find(t => t.id == 6);
          builder = modules[chartModel.name];
          _currentChartMode = 6;
        }
      } else
        //不符合条件，切换到文本表格
      if (!checkOpt(columnFields, rowFields, chartDict.find(t => t.id == chartMode).rule)) {
        chartModel = chartDict.find(t => t.id == 6);
        builder = modules[chartModel.name];
        _currentChartMode = 6;
      }
      return builder.build(svgSelector, data, columnFields, rowFields, options);
    }
  } else {
    chartModel = chartDict.find(t => t.id == 6);
    builder = modules[chartModel.name];
    _currentChartMode = 6;
    return builder.build(svgSelector, data, columnFields, rowFields, options);
  }
};

//检查指定配置是否符合图表的要求

export const Chart = {
  chartDict: chartDict,
  build: buildChart,
  checkOpt: checkOpt
};

