﻿const MeasureSticker = function (selector, options) {
  const opt = {
    tipFormat: null,
    MeasureMap: null
  };
  _.extend(opt, options);

  const chart = {
    svg: null,
    onselected: null,
    onDeleteFilter: null
  };

  //var msMainValue = "8000.00万";
  //var msMainTitle = "住院费用";
  //var msFooter_0_Value = "6000.00万";
  //var msFooter_0_Title = "同期费用";
  //var msFooter_1_Value = "-10%";
  //var msFooter_1_Title = "增长比率";

  const rowData = opt.data.sourceData[0];
  chart.msMainTitle = opt.rowMeasure.name;
  chart.msMainValue = Number(rowData[opt.rowMeasure.name]).toMeasureFormart(opt.MeasureMap[chart.msMainTitle].numberFormat);
  chart.msFooter_0_Value = rowData["YoYValue"];
  chart.msFooter_0_Title = "同比值";
  chart.msFooter_1_Value = Number(rowData["YoYRate"]/100).toMeasureFormart({
    "num_format_e": 2,
    "num_format_s": 0.01,
    "num_format_p": {
      "prefix": "",
      "subfix": ""
    },
    "num_format_i": true
  });
  chart.msFooter_1_Title = "同比率";
  chart.msFooter_1_Flag = chart.msFooter_0_Value > 0 ? "up" : "down";

  chart.onClick = ()=> {
    if (chart.onselected) {
      chart.onselected([{
        field: opt.rowMeasure,
        value: [rowData[opt.rowMeasure.name]]
      }]);
    }
  }

  return chart;
};
/**
 * MeasureSticker chart
 */
const buildChart = function (svgSelector, data, columnFields, rowFields, options) {
  const cOpt = {};

  if (!data || data.length == 0) {
    return null;
  }

  cOpt.MeasureStickerTheme = options.chartConfig ? options.chartConfig : options.tableTheme;

  const measure = columnFields.concat(rowFields).find(t => t.slaveType === 1);

  cOpt.rowMeasure = measure;

  cOpt.data = {sourceData: data};

  return MeasureSticker(svgSelector, cOpt);
};


export const MeasureStickerChart = {
  build: buildChart
}

