/* global d3, undersore, _, require, define, window */



define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {

        /*
            cOpt.colDimension.dataType:
                1 : String
                2 : Number
                3 : Boolean
                4 : Date in Number ("xxxx" -> year, "xx" -> day/month)

            SlaveType: 0 -> dimension, 1 -> measure
        */

        var cOpt = {}, rows, colKey, rowKey, zKey,
            omission = [], oMsg = "", tipFormat,
            config = options.chartConfig;

        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }

        if (!config) {
            config = {
                GlobalColor: "#4E79A7",
                MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
                scale: 0.5,
                axisStyle: {
                    tickColor : "#000",
                    fontColor : "#000",
                    fontFamily: "Arial",
                    fontSize  : 15
                },
                textStyle: {
                    "display"     : "none",
                    "font-family" : "Arial",
                    "font-size"   : 15,
                    "font-style"  : "normal",
                    "font-weight" : "normal",
                    "fill"        : "#333",
                    "position"    : "top"
                }
            };
        }

        cOpt.colDimension = columnFields[0].slaveType === 0 ?
                            columnFields[0] : rowFields[0];
        cOpt.rowMeasure = rowFields[0].slaveType === 1 ?
                            rowFields[0] : columnFields[0];

        colKey = cOpt.colDimension.name;
        rowKey = cOpt.rowMeasure.name;
        zKey   = options.colorMark.annexField.name;

        rows = _.chain(data)
                .map(function (d) { return  d[zKey]; })
                .uniq()
                .value();
        cOpt.reverse = !!cOpt.colDimension.slaveType;


        cOpt.values = _.chain(data)
            .groupBy(function (d) { return d[colKey]; })
            .map(function (val, key) {
                var obj = {};
                obj[colKey] = key;

                rows.forEach(function (s) {
                    var o = val.find(function (j) { return s === j[zKey]; });

                    if (o) { obj[s] = o[rowKey]; }
                    else {
                        /* WARNNING: When data omission, set it to ZERO!! */
                        /* WARNNING: Fake data!! */
                        obj[s] = 0;
                        omission.push([colKey, key, s]);
                    }
                });

                return obj;
            })
            .value();


        if (cOpt.colDimension.dataType === 4 ||
            cOpt.colDimension.dataType === 2) {
            cOpt.values.sort(function (a, b) {
                a[colKey] = +a[colKey];
                b[colKey] = +b[colKey];
                return a[colKey] - b[colKey];
            });
        }

        (function () {
            if (!config.MultiColors.length) {
                config.MultiColors = ["#4E79A7", "#A0CBE8", "#F28E2B"];
            }
            var colors = [];
            while (colors.length < cOpt.values.length) {
                colors = colors.concat(config.MultiColors);
            }
            cOpt.color = colors;
        })();

        if (options.colorMark &&
            options.colorMark.annexField &&
            options.colorMark.annexField.slaveType === 0) {
            cOpt.annexField = options.colorMark.annexField;
        }

        cOpt.annexField = cOpt.annexField || {};
        cOpt.stackType = options.stackType || "bar";

        tipFormat = d3.util.tipParse(
            [
                { key: colKey, value: "x" },
                { key: rowKey, value: "y" },
                { key: zKey, value: "key" }
            ],
            ("TooltipFormat" in options) ? options.TooltipFormat : ""
        );
        cOpt.tipFormat = tipFormat;

        if (options.size) {
            cOpt.size = options.size;
        }

        if ("scale" in config) { cOpt.scale = config.scale; }
        if ("textStyle" in config) { cOpt.textStyle = config.textStyle; }
        if ("axisStyle" in config) { cOpt.axisStyle = config.axisStyle; }

        /*
        if (omission.length) {
            oMsg += "当前创建的分组中，检测到如下数据有缺失：\n";
            omission.forEach(function (d) {
                oMsg += "在 \"" + d[0] + "\" 为 \"" + d[1] + "\" 中的数据 \"" + d[2] + "\"\n";
            });
            oMsg += "选择继续并将以上数据设置为0，请选择： y/n ?";
            if (window.prompt(oMsg, "y") !== "y") { return; }
        }
        */

        return stack(svgSelector, cOpt);
    };


function stack (selector, options) {

    var opt = {
        // size: { width: 500, height: 400 },
        color: d3.schemeCategory20,
        values: [],
        isReverse: false,
        stackType: "bar", // bar, line

        scale: 0.8, // control oScale padding
        tipFormat: null,
        textStyle: {},
        axisStyle: {},
        AnnexField: {},
        colDimension: {},
        rowMeasure: {}
    };

    _.extend(opt, options);


    var chart = {
        status: opt.isReverse ? "herizontal" : "vertical",
        onselected: null
    };


    var container = d3.select(selector).node().parentElement;
    if (!opt.size) {
        opt.size = d3.util.getInnerBoxDim (container);
    }

    // preserve the original size
    // used for chart type reverse
    opt.size._w = opt.size.width;
    opt.size._h = opt.size.height;

    var svg = d3.select(container)
        .html("")
        .append("svg")
        .attr("class", "Chart-To-Be-Export")
        .attr("width", opt.size.width)
        .attr("height", opt.size.height)
        .attr("version", "1.1")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .style("font", "12px arial, sans-serif");

    var lScale, oScale, lAxis, oAxis, lAxisGen, oAxisGen,
        labelD = opt.colDimension.name || "",
        labelM = opt.rowMeasure.name || "",
        xDim   = opt.colDimension.name || "",
        oScalePadding = 1 - opt.scale,

        category,  // ordinal scale domain
        sKeys,     // stack keys
        margin = { top: 30, right: 10, bottom: 10, left: 10 },
        abs = Math.abs, ceil = Math.ceil, floor = Math.floor,
        brush,
        cMin = 2, cMax = 6, // points radius range
        //numFix = d3.format(",." + (opt.rowMeasure.NumberFixed || 0) + "f"),
        minGraphDim = 100,
        cData,
        tooltip, contextMenu,
        gLeft, gBottom, gBrush, gChart, gText, gLegend,
        legend = d3.util.hlegend().groupMaxLength(90),
        textStyle = {
            "display"     : "none",    // "none" or "block"
            "font-family" : "Arial",   // "Arial", "Helvetica", "Times"
            "font-size"   : 12,
            "font-style"  : "normal",  // "normal" or "italic"
            "font-weight" : "normal",  // "normal" or "bold"
            "fill"        : "#ff0000", // text color
            "position"    : "top"      // "top", "right", "bottom" or "left"
        },

        area = d3.area()
            .defined(function (d) { return d[1] - d[0]; })
            .curve(d3.curveLinear),
        getStack = d3.stack().offset(d3.stackOffsetDiverging);

    // overwrite the default text label setting
    _.extend(textStyle, opt.textStyle);

    // singolton function
    var getTooltip = function () {
        return tooltip || (tooltip = d3.select(document.body).append("div").attr("class", "d3-ttip").node());
    };


    var menuOnSingleItem = [
        { title: "剔除数据", symbol: "&#10008;",action: "delete" },  // ✘
        { title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg" },
        { title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png" }
    ];
    var menuOnBlank = menuOnSingleItem.slice(1);
    var menuOnBrush = [
        {title: "删除框选节点", symbol: "&#10008;", action: "deleteNodes"}
    ];

    contextMenu = d3.util.getChartContextMenu();
    contextMenu.clickFun = function (command, target) {
        this.hide();
        switch (command.action) {
            case "delete": chart.delete(target.series, target.dim); break;
            case "reverse": chart.reverse(); break;
            case "linear": chart.curveType(d3.curveLinear); break;
            case "curve": chart.curveType(d3.curveCardinal); break;
            case "export": chart.export(command.p1); break;
            case "deleteNodes" :
                chart.deleteStack(target);
                break;
            default: console.log("... Catch YOU! ...");
        }
    };


    function tweenPath (generator) {
        return function  (d) {
            var length = d.length;
            function interpolate (t) {
                return d.slice(0, Math.ceil(length * t));
            }
            return function (t) { return generator(interpolate(t)); };
        };
    }

    function initialize (force) {

        gLeft = null; gBottom = null; gLegend = null;
        gBrush = null; gChart = null; gText = null;
        svg.html(""); // clear content

        var yMin, yMax, extent;

        // if cData is undefined or force cData recalculated
        if (!cData || force) {

            sKeys = _.keys(opt.values[0]);

            if (_.contains(sKeys, xDim)) {
                // Get all keys except colKey
                sKeys = sKeys.filter(function (d) { return d !== xDim; });
            }
            else {
                _.each(opt.values[0], function (val, key) {
                    if (typeof(val) !== "number") { xDim = "" + key; }
                });
                if (!xDim) { return alert("数据有误！"); }
                sKeys = sKeys.filter(function (d) { return d !== xDim; });
            }

            cData = getStack.keys(sKeys)(opt.values);

            // assign color for every seriers
            // TODO: [ color to assigned according to key value ]
            cData.forEach(function (arr, index) {
                var c = opt.color[index] || "black";
                arr.color = c;
            });

        } // IF END

        // ordinal scale domain
        category = cData[0].map(function (d) { return d.data[xDim]; });


        // linear scale extrem value
        extent = d3.extent(_.flatten(cData.map(function (arr) {
            return arr.map(function (d) {
                return [d[0], d[1]];
            });
        })));

        yMax = extent[1] > 0 ? ceil(extent[1] * 1.1) : ceil(extent[1] * 0.9);
        yMin = extent[0] > 0 ? 0 : floor(extent[0] * 1.1);

        lScale = d3.scaleLinear().domain([yMin, yMax]);
        if (opt.stackType == "line") {
            oScale = d3.scalePoint().domain(category).padding(oScalePadding);
        }
        else if (opt.stackType == "bar") {
            oScale = d3.scaleBand().domain(category).padding(oScalePadding);
        }
        else{
            return alert("堆积图形式只有：'line'和'bar'\n请更正！");
        }


    } // initialize END

    function svgSizeCheck (yAxis, xAxis, svgSize) {

        var temp, yBox, xBox, size;

        // Step1: get width of yAxis
        var yWidth = yAxis.getSize().width;

        margin.top = yAxis._renderResults.tick.height + 15;

        // Step2: set xAxis scale range and get xAxis height
        temp = svgSize.width - margin.left - margin.right - yWidth;
        var xHeight = xAxis.range([0,
                        temp < minGraphDim ? minGraphDim : temp])
                    .getSize().height;

        // Step3: set yAxis range (height)
        temp = svgSize.height - margin.top - margin.bottom - xHeight;
        yAxis.range([0,
            temp < minGraphDim ? minGraphDim : temp]);

        // re-assure
        yBox = yAxis.getSize();
        xBox = xAxis.getSize();
        size = {
            width: yBox.width + xBox.width + margin.left + margin.right,
            height: xBox.height + yBox.height + margin.top + margin.bottom
        };

        if (svgSize.width < size.width) { svgSize.width = size.width; }
        if (svgSize.height < size.height) { svgSize.height = size.height; }

        return { yBox: yBox, xBox: xBox };
    }

    function vertical () {

        chart.status = "vertical";

        lScale.range([0, minGraphDim]);
        oScale.range([0, minGraphDim]);
        lAxis = d3.axisLeft().scale(lScale);
        oAxis = d3.axisBottom().scale(oScale);


        gLeft = svg.append("g").attr("class", "axis axis-y");
        gBottom = svg.append("g").attr("class", "axis axis-x");

        lAxisGen = d3.util.axis.linear({
            scale : lScale,
            axis  : lAxis,
            holder: gLeft,
            orient: "left",

            label: labelM,
            style: opt.axisStyle
        });
        oAxisGen = d3.util.axis.ordinal({
            scale : oScale,
            axis  : oAxis,
            holder: gBottom,
            orient: "bottom",

            label: labelD,
            style: opt.axisStyle
        });

        var boxes = svgSizeCheck(lAxisGen, oAxisGen, opt.size);
        var left, bottom, vHeight, hWidth;
        left   = boxes.yBox;
        bottom = boxes.xBox;
        vHeight = left.height;
        hWidth = bottom.width;

        svg.attr("width", opt.size.width)
            .attr("height", opt.size.height);

        area.y1(function (d) { return -lScale(d[1]); })
            .y0(function (d) { return -lScale(d[0]); })
            .x (function (d) { return  oScale(d.data[xDim]); });

        brush = d3.brushX()
            .extent([[0, -lScale.range()[1]], [oScale.range()[1], 0]])
            .filter(brushFilter)
            .on("start", onBrushStart)
            .on("brush", onBrushMove)
            .on("end",   onBrushEnd);


        // dimension re-asign
        gLegend = svg.append("g")
            .attr("class", "gLegend")
            .attr("transform", "translate(" + [left.width + margin.left, 10] + ")");
        gLeft.attr("transform", "translate(" +
            [left.width + margin.left, margin.top] + ")");
        gBottom.attr("transform", "translate(" +
            [left.width + margin.left, margin.top + left.height] + ")");
        gBrush = svg.append("g")
            .attr("class", "gBrush")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gChart = svg.append("g")
            .attr("class", "gChart")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gText = svg.append("g")
            .attr("class", "gText")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");


        oAxisGen.drawAxis();
        lAxisGen.drawAxis();
        gBrush.call(brush);
        if (lScale.domain()[0] < 0) {
            gLeft.append("line")
                .attr("class", "zeroBaseLine")
                .attr("x1", 0)
                .attr("x2", hWidth)
                .attr("y1", vHeight - lScale(0))
                .attr("y2", vHeight - lScale(0))
                .attr("stroke-dasharray", "5 5")
                .attr("stroke", "gray")
                .attr("stroke-width", 1);
        }

        gBrush.select(".selection").on("contextmenu", onBrushContextMenu);
        gBrush.select(".overlay").on("contextmenu", onBlankBrushContextMenu);

        // create legends
        legend.constraintWidth(bottom.width)
            .container(gLegend)
            .data(chart.getLegends())
            .fontFamily(lAxisGen.style.fontFamily)
            .fontSize(lAxisGen.style.fontSize)
            .fontColor(lAxisGen.style.fontColor)
            .constraintedSketch();

        var chartType = gChart.selectAll(".series")
            .data(cData)
            .enter()
            .append("g")
            .attr("class", "series")
            .attr("fill", function (d) { return d.color; })
            .on("contextmenu", onContextMenu)
            .on("mousemove", onMouseMove)
            .on("mouseleave", onMouseLeave);

        if (opt.stackType == "line") {

            chartType.each(function (d) {
                d3.select(this)
                    .append("path")
                    .attr("class", "area")
                    //.attr("d", area)
                    .attr("stroke", "#333")
                    .attr("stroke-dasharray", "5 5")
                    .attr("stroke-width", 0)
                    .transition()
                    .duration(1500)
                    .attrTween("d", tweenPath(area));
            });
        }
        else if (opt.stackType == "bar") {

            chartType.each(function (d) {
                d3.select(this)
                    .selectAll("rect")
                    .data(d)
                    .enter()
                    .append("rect")
                    .on("click", onClick)
                    .attr("x", function (j) { return  oScale(j.data[xDim]); })
                    .attr("y", function (j) { return -lScale(j[1]); })
                    .attr("width", oScale.bandwidth())
                    .attr("height", function (j) { return lScale(j[1]) - lScale(j[0]); })
                    .attr("stroke", "#333")
                    .attr("stroke-dasharray", "5 5")
                    .attr("stroke-width", 0);
            });
        }


        // construct new circle pairs
        chartType.each(function (d) {
            var delta = (opt.stackType == "bar") ? (oScale.bandwidth() * 0.5) : 0;
            var circles = _.flatten(d.map(function (j) {
                if (j[0] === j[1]) { return []; }

                return [
                    { x: j.data[xDim], y: j[0], key: d.key, _y: j[1] },
                    { x: j.data[xDim], y: j[1], key: d.key, dy: j[1] - j[0] }
                ];
            }));
            d3.select(this)
                .selectAll("circle")
                .data(circles)
                .enter()
                .append("circle")
                .on("click", onClick)
                .attr("r", cMin)
                .attr("cx", function (j) { return  oScale(j.x) + delta; })
                .attr("cy", function (j) { return -lScale(j.y); })
                .attr("stroke-width", 0)
                .attr("stroke", "white")
                .attr("fill", "black");
        });


        // the caluction is redundant
        var textCoordinates = (function () {
            var delta = (opt.stackType == "bar") ? (oScale.bandwidth() * 0.5) : 0;
            return [].concat.apply([], cData)
                .map(function (d) {
                    var y = lScale(abs(d[1]) > abs(d[0]) ? d[1] : d[0]);
                    var text = abs(d[1] - d[0]);
                    return {
                        x   : oScale(d.data[xDim]) + delta,
                        y   : -y,
                        //text: numFix(d[1])
                        //text: y.toMeasureFormart(opt.rowMeasure.numberFormat)
                        text: text ? text.toMeasureFormart(opt.rowMeasure.numberFormat) : ""
                    };
                });
        })();

        gText.selectAll("text")
            .data(textCoordinates)
            .enter()
            .append("text")
            .each(function (d) {
                d3.select(this)
                    .attr("x", d.x).attr("y", d.y).text(d.text);
            });

        chart.label = null;
        chart.label = new d3.util.TextDecorate(gText, 3, [[0,-vHeight], [hWidth, 0]], textStyle);
        chart.label.init();


    } // vertical END

    function herizontal () {

        chart.status = "herizontal";

        lScale.range([0, minGraphDim]);
        oScale.range([0, minGraphDim]);
        lAxis = d3.axisBottom().scale(lScale);
        oAxis = d3.axisLeft().scale(oScale);


        gLeft = svg.append("g").attr("class", "axis axis-y");
        gBottom = svg.append("g").attr("class", "axis axis-x");

        lAxisGen = d3.util.axis.linear({
            scale : lScale,
            axis  : lAxis,
            holder: gBottom,
            orient: "bottom",

            label: labelM,
            style: opt.axisStyle
        });
        oAxisGen = d3.util.axis.ordinal({
            scale : oScale,
            axis  : oAxis,
            holder: gLeft,
            orient: "left",

            label: labelD,
            style: opt.axisStyle
        });


        var boxes = svgSizeCheck(oAxisGen, lAxisGen, opt.size);
        var hWidth, vHeight, left, bottom;
        left   = boxes.yBox;
        bottom = boxes.xBox;
        vHeight = left.height;
        hWidth = bottom.width;

        svg.attr("width", opt.size.width)
            .attr("height", opt.size.height);


        area.x1(function (d) { return lScale(d[1]); })
            .x0(function (d) { return lScale(d[0]); })
            .y (function (d) { return -oScale(d.data[xDim]); });
        brush = d3.brushY()
            .extent([[0, -oScale.range()[1]], [lScale.range()[1], 0]])
            .filter(brushFilter)
            .on("start", onBrushStart)
            .on("brush", onBrushMove)
            .on("end",   onBrushEnd);

        // dimension re-asign
        gLegend = svg.append("g")
            .attr("class", "gLegend")
            .attr("transform", "translate(" + [left.width + margin.left, 10] + ")");
        gLeft.attr("transform", "translate(" +
                [left.width + margin.left, margin.top] + ")");
        gBottom.attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gBrush = svg.append("g")
            .attr("class", "gBrush")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gChart = svg.append("g")
            .attr("class", "gChart")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gText = svg.append("g")
            .attr("class", "gText")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");


        oAxisGen.drawAxis();
        lAxisGen.drawAxis();
        gBrush.call(brush);
        if (lScale.domain()[0] < 0) {
            gLeft.append("line")
                .attr("class", "zeroBaseLine")
                .attr("x1", lScale(0))
                .attr("x2", lScale(0))
                .attr("y1", 0)
                .attr("y2", vHeight)
                .attr("stroke-dasharray", "5 5")
                .attr("stroke", "gray")
                .attr("stroke-width", 1);
        }

        gBrush.select(".selection").on("contextmenu", onBrushContextMenu);
        gBrush.select(".overlay").on("contextmenu", onBlankBrushContextMenu);

        // create legends
        legend.constraintWidth(bottom.width)
            .container(gLegend)
            .data(chart.getLegends())
            .fontFamily(lAxisGen.style.fontFamily)
            .fontSize(lAxisGen.style.fontSize)
            .fontColor(lAxisGen.style.fontColor)
            .constraintedSketch();

        var chartType = gChart.selectAll(".series")
            .data(cData)
            .enter()
            .append("g")
            .attr("class", "series")
            .attr("fill", function (d) { return d.color; })
            .on("contextmenu", onContextMenu)
            .on("mousemove", onMouseMove)
            .on("mouseleave", onMouseLeave);

        if (opt.stackType == "line") {
            chartType.each(function (d) {
                d3.select(this).append("path")
                    .attr("class", "area")
                    //.attr("d", area)
                    .attr("stroke-dasharray", "5 5")
                    .attr("stroke", "#333")
                    .attr("stroke-width", 0)
                    .transition()
                    .duration(1500)
                    .attrTween("d", tweenPath(area));
            });
        }
        else if (opt.stackType == "bar") {
            chartType.each(function (d) {
                d3.select(this)
                    .selectAll("rect")
                    .data(d)
                    .enter()
                    .append("rect")
                    .on("click", onClick)
                    .attr("x", function (j) { return  lScale(j[0]); })
                    .attr("y", function (j) { return -oScale(j.data[xDim]) - oScale.bandwidth(); })
                    .attr("width", function (j) { return lScale(j[1]) - lScale(j[0]); })
                    .attr("height", oScale.bandwidth())
                    .attr("stroke", "#333")
                    .attr("stroke-dasharray", "5 5")
                    .attr("stroke-width", 0);
            });
        }

        // construct new circle pairs
        chartType.each(function (d) {
            var delta = (opt.stackType == "bar") ? (oScale.bandwidth() * 0.5) : 0;
            var circles = _.flatten(d.map(function (j) {
                if (j[0] === j[1]) { return []; }

                return [
                    { x: j.data[xDim], y: j[0], key: d.key, _y: j[1] },
                    { x: j.data[xDim], y: j[1], key: d.key, dy: j[1] - j[0] }
                ];
            }));
            d3.select(this).selectAll("circle")
                .data(circles)
                .enter()
                .append("circle")
                .on("click", onClick)
                .attr("r", cMin)
                .attr("cx", function (j) { return lScale(j.y); })
                .attr("cy", function (j) { return -oScale(j.x) - delta; })
                .attr("stroke-width", 0)
                .attr("stroke", "white")
                .attr("fill", "black");
        });

        // the caluction is redundant
        var textCoordinates = (function () {
            var delta = (opt.stackType == "bar") ? (oScale.bandwidth() * 0.5) : 0;
            return [].concat.apply([], cData)
                .map(function (d) {
                    var x = lScale( abs(d[1]) > abs(d[0] ) ? d[1] : d[0]);
                    var text = abs(d[1]- d[0]);
                    return {
                        x   : x,
                        y   : -oScale(d.data[xDim]) - delta,
                        //text: numFix(d[1])
                        //text: x.toMeasureFormart(opt.rowMeasure.numberFormat)
                        text: text ? text.toMeasureFormart(opt.rowMeasure.numberFormat) : ""
                    };
                });
        })();

        gText.selectAll("text")
            .data(textCoordinates)
            .enter()
            .append("text")
            .each(function (d) {
                d3.select(this)
                    .attr("x", d.x).attr("y", d.y).text(d.text);
            });

        chart.label = null;
        chart.label = new d3.util.TextDecorate(gText, 3, [[0,-vHeight], [hWidth, 0]], textStyle);
        chart.label.init();

    } // herizontal END


    function setTooltip (node) {

        var box = node.getBoundingClientRect(),
            dm = d3.select(node).datum(),
            scrollTop = window.pageYOffset,
            scrollLeft = window.pageXOffset,
            pText;

        dm = {
            x   : dm.x,
            //y   : numFix(dm.dy), // construct new "y"
            y   : dm.dy.toMeasureFormart(opt.rowMeasure.numberFormat),
            key : dm.key
        };

        try {
            pText = _.template(opt.tipFormat.output());
        }
        catch (err) {
            pText = _.template("X: " + "<span><%= x %></span><br/>" +
                               "Y: " + "<span><%= y %></span><br/>" +
                               "key: " + "<span><%= key %></span>");
        }

        tooltip = getTooltip();
        d3.select(tooltip)
            .html("")
            .style("display", "block")
            .append("p")
            .attr("class", "d3-ttip-content")
            .html(pText(dm));

        tooltip.style.left = scrollLeft + box.left +
            0.5 * (box.width - tooltip.offsetWidth) + "px";
        tooltip.style.top  = scrollTop + box.top - tooltip.offsetHeight + 10 + "px";

    } // setTooltip END

    function onMouseMove (d) {
        if (d3.brushSelection(gBrush.node())) { return; }

        var self  = d3.select(this),
            mouse = d3.mouse(this),
            mc    = (chart.status == "vertical") ? mouse[0] : mouse[1],
            cxy   = (chart.status == "vertical") ? "cx" : "cy",
            nodes;

        self.selectAll("circle")
            .attr("r", cMin)
            .attr("stroke-width", 0);

        nodes = self.selectAll("circle").nodes()
            .sort(function (a, b) {
                return abs( +a.getAttribute(cxy) - mc ) -
                       abs( +b.getAttribute(cxy) - mc );
            });

        // highlight the first two closest circles
        d3.select(nodes[0]).attr("r", cMax).attr("stroke-width", 2);
        d3.select(nodes[1]).attr("r", cMax).attr("stroke-width", 2);
        if (chart.status == "vertical") {
            (+nodes[0].getAttribute("cy") > +nodes[1].getAttribute("cy")) ?
                setTooltip(nodes[1]) : setTooltip(nodes[0]);
        }
        else {
            (+nodes[0].getAttribute("cx") > +nodes[1].getAttribute("cx")) ?
                setTooltip(nodes[0]) : setTooltip(nodes[1]);
        }

        self.raise();
        self.select("path").attr("stroke-width", 2);
        self.selectAll("rect").attr("stroke-width", 2);

    } // onMouseMove END

    function onMouseLeave (d) {
        if (d3.brushSelection(gBrush.node())) { return; }

        var self = d3.select(this);
        self.select("path").attr("stroke-width", 0);
        self.selectAll("rect").attr("stroke-width", 0);
        self.selectAll("circle").attr("r", cMin).attr("stroke-width", 0);
        d3.select(tooltip).style("display", "none");
    }

    function onContextMenu (d, idx) {

        if (d3.brushSelection(gBrush.node())) {
            return onBrushContextMenu();
        }
        d3.event.preventDefault();
        d3.event.stopPropagation();

        var series, dim;
        if ( this.classList.contains("series") ) {
            series = d3.select(this).datum().key;
            dim = d3.select(this)
                .selectAll("circle")
                .filter(function () {
                    return +d3.select(this).attr("r") == cMax;
                })
                .datum().x;
        }

        contextMenu.setTargetNode({series: series, dim: dim})
               .setCurrentMenu(menuOnSingleItem)
               .show();

    } // contextMenu END

    function onClick (d) {
        var tag = this.tagName.toUpperCase(), parent;
        if (!chart.onselected || d3.event.selection) { return; }

        parent = d3.select(this.parentElement).datum();
        if (tag === "CIRCLE") {
            chart.onselected([
                { field: opt.colDimension, value: [ d.x ] },
                { field: opt.annexField, value: [ parent.key ] }
            ]);
        }
        if (tag === "RECT") {
            chart.onselected([
                { field: opt.colDimension, value: [ d.data[xDim] ] },
                { field: opt.annexField, value: [ parent.key ] }
            ]);
        }
    }

    // Brush related events
    function brushFilter () {
        return (d3.event.button !== 1);
    }
    function onBrushStart () {
        gChart.selectAll("circle")
            .attr("r", cMin)
            .classed("d3-active", false);
        contextMenu.hide();
    }
    function onBrushMove () {
        var range = d3.event.selection ||
                    d3.brushSelection(gBrush.node());

        if (chart.status == "herizontal") {
            gChart.selectAll("circle")
                .classed("d3-active", function (d) {
                    var cc = d3.select(this),
                        cy = +cc.attr("cy"),
                        boo = (cy >= range[0]) && (cy <= range[1]);

                    cc.attr("r", boo ? cMax : cMin);

                    return boo;
                });
        }
        else {
            gChart.selectAll("circle")
                .classed("d3-active", function (d) {
                    var cc = d3.select(this),
                        cx = +cc.attr("cx"),
                        boo = (cx >= range[0]) && (cx <= range[1]);

                    cc.attr("r", boo ? cMax : cMin);

                    return boo;
                });
        }
    }
    function onBrushEnd () {
        var xRange, nodes, xValues = [];

        xRange = d3.brushSelection(this);
        if (!chart.onselected || !xRange) { return; }

        nodes = gChart.selectAll(".d3-active");
        if (nodes.empty()) { return; }
        nodes.nodes().map(function (node) {
            xValues.push(d3.select(node).datum().x);
        });

        xValues = d3.set(xValues).values();

        chart.onselected([{
            field: opt.colDimension,
            value: xValues
        }]);

    } // onBrushEnd END
    function onBrushContextMenu () {
        d3.event.preventDefault();
        onBrushMove();

        var range = d3.brushSelection(gBrush.node());
        var stackNames = [];
        if (chart.status == "vertical") {
            gBottom.selectAll(".tick").nodes().forEach(function (node) {
                var xOffset = parseFloat(node.getAttribute("transform")
                                     .split(",")[0]
                                     .split("(")[1]);
                if (xOffset >= range[0] && xOffset <= range[1]) {
                    stackNames.push(d3.select(node).datum());
                }
            });
        }
        else {
            gLeft.selectAll(".tick").nodes().forEach(function (node) {
                var yOffset = parseFloat(node.getAttribute("transform").split(",")[1]);
                yOffset = yOffset - oScale.range()[1];
                if (yOffset >= range[0] && yOffset <= range[1]) {
                    stackNames.push(d3.select(node).datum());
                }
            });
        }

        if (stackNames.length === 0) { return; }

        contextMenu.setTargetNode(stackNames)
                   .setCurrentMenu(menuOnBrush)
                   .show();
    } // onBrushContextMenu END
    function onBlankBrushContextMenu () {
        d3.event.preventDefault();
        contextMenu.setCurrentMenu(menuOnBlank).show();
    }




    chart.curveType = function (type) {
        if (opt.stackType == "bar") { return; }

        gChart.selectAll(".series path")
              .attr("d", area.curve(type));
        return this;
    };

    chart.setColor = function (color) {
        cData.forEach(function (arr, index) {
            var c = color[index] || "black";
            arr.color = c;
        });

        initialize();
        if (this.status == "vertical") { vertical(); }
        else { herizontal(); }

        return this;
    };

    chart.reverse = function () {

        initialize();
        opt.size.width  = opt.size._w;
        opt.size.height = opt.size._h;

        // Restore the origin dimmension
        svg.attr("width", opt.size.width).attr("height", opt.size.height);

        if (this.status == "vertical") { herizontal(); }
        else { vertical(); }

        return this;
    };

    chart.delete = function (series, dim) {

        var theStack = opt.values.find(function (d) { return d[xDim] == dim; });
        if (theStack) { theStack[series] = 0; } else { return; }

        initialize(true); // force recalculate the cData
        if (this.status == "vertical") { vertical(); }
        else { herizontal(); }

        if (this.onRemoveItem) {
            this.onRemoveItem([
                { field: opt.colDimension, value: [ dim ] },
                { field: opt.annexField, value: [ series ] }
            ]);
        }
    };

    chart.deleteStack = function (names) {
        // people can not delete all stacks
        if (names.length == opt.values.length) { return; }

        var uploads = [];
        _.each(names, function (name) {
            var index = _.findIndex(opt.values, function (item) {
                return item[xDim] === name;
            });
            if (index > -1) {
                uploads.push(opt.values.splice(index, 1)[0]);
            }
        });

        initialize(true); // force recalculate the cData
        if (this.status == "vertical") { vertical(); }
        else { herizontal(); }

        if (this.onRemoveItem) {
            this.onRemoveItem([
                { field: opt.colDimension, value: names },
                { field: opt.annexField, value: sKeys }
            ]);
        }
    };

    chart.stackType = function (type) {
        if (!arguments.length || (opt.stackType == type)) { return opt.stackType; }

        if (type == "bar" || type == "line") {
            opt.stackType = type;

            initialize();
            if (chart.status == "herizontal") { herizontal(); }
            else { vertical(); }
        }
    };

    chart.setSize = function (size) {
        if (!arguments.length) { return opt.size; }
        opt.size.width  = size.width;
        opt.size.height = size.height;

        svg.attr("width", size.width).attr("height", size.height);

        initialize ();
        if (this.status == "herizontal") { herizontal(); }
        else { vertical(); }

        return this;
    };

    chart.tipFormat = function (s) {
        if (!arguments.length) { return opt.tipFormat; }
        opt.tipFormat.input(s);
        return this;
    };

    chart.getLegends = function () {
        return cData.map(function (d) {
            return { name: d.key, color: d.color };
        });
    };

    chart.getScale = function () {
        return 1 - oScalePadding;
    };

    chart.setScale = function (val) {
        if (!arguments.length) { return; }
        if (typeof val === "string") { val = +val; }
        if (val > 1 || val < 0) { return; }
        oScalePadding = 1 - val;

        initialize ();
        if (this.status == "herizontal") { herizontal(); }
        else { vertical(); }
    };

    chart.export = function (type) {
        d3.util.exportChart(svg.node(), type || "png");
    };

    chart.removeChart = function () {
        svg.remove();
        if (tooltip) { d3.select(tooltip).remove(); }
        contextMenu.remove();

        tooltip      = null;
        contextMenu  = null;
        container    = null;
        opt          = null;

        for (var pp in chart) { delete chart[pp]; }
        chart = null;
    };

    initialize ();
    if (opt.isReverse) { herizontal(); } else { vertical(); }

    return chart;

} // stack chart END


    module.exports = {
        build: buildChart
    };

});


