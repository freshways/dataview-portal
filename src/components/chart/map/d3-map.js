﻿import {Tooltip} from "@/components/contro/tooltip";
import {colorHelper} from '@/utils/colorHelper';
import d3 from '@/components/d3-util'

/**
 * map chart 2：中国地图：省（主色渐变）、市（多色）
 */
const _themeColor = {
  //"murasaki": { peakColor: "#05a066", valleyColor: "#e2f7fc" },
  //"black": { peakColor: "#b93907", valleyColor: "#fcd3c4" },
  "blue": {peakColor: "#2C5985", valleyColor: "#BAE3D7"},
  "mac-mojave": {peakColor: "#b93907", valleyColor: "#fcd3c4"}
};

const SiteTheme = "blue"
const _peakColor = "#2C5985";//最大
const _valleyColor = "#BAE3D7";//最小
let _geoCitySource = []; //城市原始数据

//预设颜色
//var _precolor = ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"];
const _precolor = ["#5271FF", "#001EFF", "#FAAD14", "#FF7800", "#FF3600", "#52C41A", "#00FF18", "#13C2C2", "#B37FEB", "#722ED1", "#FF85C0", "#EB2F96", "#FADB16", "#FFF566", "#A3BAFF", "#5271FF", "#001EFF", "#FAAD14", "#FF7800", "#FF3600"];

const buildChart = function (svgSelector, data, columnFields, rowFields, options) {
  //_peakColor = _themeColor[SiteTheme].peakColor;
  //_valleyColor = _themeColor[SiteTheme].valleyColor;

  var dimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
  var measure = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];
  //过滤空值
  data = Enumerable.from(data).where(function (d) {
    if (d[dimension.name] != "") {
      return d;
    }
  }).toArray();
  var sourceData = data.concat();//原始数据
  var maxValue = 100;
  var minValue = 0;
  var geoType = 0; //默认0=Province，1=City
  var geoCity = [];

  // 如果用户输入数据不足直接退出
  if (!columnFields[0] || !rowFields[0]) {
    return;
  }

  //样式配置对象
  let config = options.chartConfig;
  if (!config) {
    config = {
      GlobalColor: "#4E79A7",
      MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
      scale: 0.5,
      axisStyle: {
        tickColor: "#000",
        fontColor: "#000",
        fontFamily: "Arial",
        fontSize: 15
      },
      textStyle: {
        "display": "none",
        "font-family": "Arial",
        "font-size": 15,
        "font-style": "normal",
        "font-weight": "normal",
        "fill": "#333",
        "position": "top"
      }
    };
  }

  //获取所有度量字段
  var measureFields_c = Enumerable.from(columnFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields_r = Enumerable.from(rowFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields = measureFields_c.concat(measureFields_r);
  var measureMap = {};
  measureFields.forEach(function (f) {
    measureMap[f.name] = f;
  });

  if (dimension.geoMarker === "Geo-C") {
    geoType = 1;
    //加载全国城市信息
    $.ajax({
      url: './mapdata/china-Lng-Lat.json',
      data: {},
      type: "get",
      async: false,//使用同步的方式
      dataType: "json",
      error: function () {
        alert("获取信息时服务端出错");
      },
      success: function (result) {
        _geoCitySource = result;
      }
    });
  }

  var values, category, reverse, tipFormat, color, peakColor, valleyColor, scale;

  (function () {

    var dKey = dimension.name, mKey = measure.name;

    if (dimension.dataType === 4 || dimension.dataType === 2) { // 如果为时间（数值）升序排列
      data.sort(function (a, b) {
        return a[dKey] - b[dKey];
      });
    }

    values = data.map(function (d) {
      return d[mKey];
    });
    category = data.map(function (d) {
      return d[dKey];
    });

    maxValue = Math.max(...values);
    minValue = Math.min(...values);

    tipFormat = options.TooltipFormat;
    if (!tipFormat) {
      var tipFormatStr = '';
      tipFormatStr += dimension.name + '：<' + dimension.name + '>';
      tipFormatStr += '\r\n' + measure.name + '：<' + measure.name + '>';
      tipFormat = tipFormatStr;
    }

    //color = options.colorMark.GlobalColor
    //if (options.colorMark && options.colorMark.GlobalColor) {
    //    peakColor = options.colorMark.GlobalColor;
    //    valleyColor = "#FFFFFF";
    //}
    //else {
    //    peakColor = _themeColor[SiteTheme].peakColor;
    //    valleyColor = _themeColor[SiteTheme].valleyColor;

    //    peakColor = valleyColor = "#FFFFFF";

    //    color = options.colorMark.MultiColors;
    //}
    if (geoType === 0 && config.GlobalColor) {
      peakColor = config.GlobalColor;
      valleyColor = "#FFFFFF";
    } else {
      peakColor = _themeColor[SiteTheme].peakColor;
      valleyColor = _themeColor[SiteTheme].valleyColor;

      peakColor = valleyColor = "#FFFFFF";

      color = [];
      var colorMatch = config.MultiColors;
      category.forEach(function (catItem, i) {
        var tempColor = _precolor[i % 20];
        if (i < colorMatch.length) {
          tempColor = colorMatch[i];
        }
        color.push({Key: catItem, Color: tempColor});
      });
    }
  })();

  //console.log(options.TooltipFormat);

  /* SlaveType: 1 -> dimension || 0 -> measure */
  reverse = !!columnFields[0].slaveType;

  scale = ("scale" in options) ? options.scale : 0.8;

  if ("scale" in config) {
    scale = config.scale;
  }
  if ("textStyle" in config) {
    textStyle = config.textStyle;
  }

  return map(svgSelector, {
    values: values,
    category: category,
    isReverse: reverse,
    scale: scale,
    peakColor: peakColor,
    valleyColor: valleyColor,
    color: color,
    tipFormat: tipFormat,
    colDimension: dimension,
    rowMeasure: measure,
    MeasureMap: measureMap,
    size: options.size,
    textStyle: textStyle || {},
    config: config,
    data: {sourceData: sourceData, maxValue: maxValue, minValue: minValue, geoType: geoType, geoCity: geoCity}
  });
};

function map(selector, options) {
  var opt = {
    //size    : { width: 450, height: 300 },
    category: [],
    values: [],
    //color: d3.schemeCategory20c,
    isReverse: false,
    scale: 0.8, // range: [0, 1], oScale padding
    tipFormat: null,
    colDimension: null,
    rowMeasure: null,
    MeasureMap: null,
    textStyle: {},
    config: {},
    data: null
  };

  // output object
  var chart = {
    status: opt.isReverse ? "herizontal" : "vertical",
    onselected: null
  };

  opt = d3.util.extend(opt, options);

  var container = d3.select(selector).node().parentElement;
  if (!opt.size) {
    opt.size = d3.util.getInnerBoxDim(container);
  } else if (opt.size.height <= 160) {
    opt.size.height = 160;
  }

  var svg = d3.select(container)
    .html("")
    .append("svg")
    .attr("width", opt.size.width)
    .attr("height", opt.size.height)
    //.attr("viewBox", "0 0 " + opt.size.width + " " + opt.size.height)
    .attr("version", "1.1")
    .attr("xmlns", "http://www.w3.org/2000/svg")
    .style("font", "12px arial, sans-serif");

  var _chinaMap = svg.append('g')
    //.attr("id", "chinaMap")
  ;
  var _georoot;
  var _projection;
  var _path;
  var _color = d3.scaleOrdinal(d3.schemeCategory20);//opt.color;
  var _colorFill = "#4E79A7";
  var width = opt.size.width;
  var height = opt.size.height;
  var convertData = [];
  var tipDiv = d3.select(container).append('div').attr("class", "tooltip").style("opacity", 0);

  var brush, gBrush;
  var rect;
  var clickTime = "";
  var startLoc = [];
  var endLoc = [];
  var mapSelectFlag = false;
  var contextMenu;

  //draw china map
  verticalMap();

  function do_animation(path) {
    var totalLength = path.node().getTotalLength();
    path
      .attr("stroke-dasharray", totalLength + " " + totalLength)
      .attr("stroke-dashoffset", totalLength)
      .transition()
      .duration(2000)
      .attr("stroke-dashoffset", 0);
  }

  function fill_provinceName() {
    var gText = _chinaMap.append('g')
      .selectAll(".place-label")
      .data(_georoot.features)
      .enter()
      .append("text")
      .attr("class", "place-label")
      .attr("transform", function (d) {
        var xyPoint = _projection(d.properties.cp);
        //console.log(xyPoint);
        d.x = xyPoint[0];
        d.y = xyPoint[1];
        //d3.select(this)
        //    .attr("x", d.x)
        //    .attr("y", d.y);

        return "translate(" + xyPoint + ")";
      })
      .attr("dy", ".35em")
      .text(function (d) {
        if (opt.data.geoType === 0) {
          var name = d.properties.name;
          var provinceName = getProvinceName(name);
          d.provinceName = provinceName;
          var provinceValue = getProvinceValue(name);

          provinceValue = Number(provinceValue).toMeasureFormart(opt.rowMeasure.numberFormat);

          return provinceValue;
        } else {
          //return d.properties.name;
          return "";
        }
      })
    ;

    chart.label = null;
    chart.label = new d3.util.TextDecorate(gText, 0, [[0, 0], [0, 0]], opt.textStyle);
    chart.label.init();
  }

  function addBasicData(cityData) {
    var circle_color = circle_outter = opt.peakColor;
    var points = _chinaMap.append('g')
      .selectAll("circle")
      .data(cityData)
      .enter()
      .append('circle')
      .attr("class", "basic-data")
      .attr("transform", function (d) {
        var xyPoint = _projection([d.city.Lng, d.city.Lat]);
        //console.log(xyPoint);
        d.x = xyPoint[0];
        d.y = xyPoint[1];
        //d3.select(this)
        //    .attr("x", d.x)
        //    .attr("y", d.y);

        return "translate(" + xyPoint + ")";
      })
      .attr("r", function (d, i) {
        var tempDr = d.r * opt.scale;
        if (tempDr < 1)
          tempDr = 1;
        return tempDr;
      })
      .attr("fill", function (d) {
        if (opt.data.geoType === 1 && opt.color != undefined) {
          var name = d.cityName;
          circle_color = circle_outter = getCityColor(name);
        }
        return circle_color;
      })
      .attr("stroke-width", "3px")
      .attr("stroke", function (d) {
        if (opt.data.geoType === 1 && opt.color != undefined) {
          var name = d.cityName;
          circle_color = circle_outter = getCityColor(name);
        }
        return circle_outter;
      })
      .style("cursor", "pointer")
      .on("mouseover", function (d, i) {

        var fName = opt.colDimension.name;
        var name = d.city.name;
        var tipFields = [{name: fName, value: name}];

        var dataRow = getDataRow(name);
        if (dataRow.length === 0) {
          for (var f in opt.data.sourceData[0]) {
            if (f != opt.colDimension.name) {
              var tempValue = 0;
              if (opt.MeasureMap[f] !== undefined) {
                tipFields.push({name: f, value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)});
              }
            }
          }
        } else {
          dataRow.forEach(function (row) {
            for (var f in row) {
              if (f != opt.colDimension.name) {
                var tempValue = row[f];
                if (opt.MeasureMap[f] !== undefined) {
                  tipFields.push({name: f, value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)});
                }
              }
            }
          });
        }

        modules.Tooltip.show(opt.tipFormat, tipFields);

        var tempDr = d.r * 2;
        if (tempDr < 1)
          tempDr = 1;

        d3.select(this)
          .attr("r", tempDr)
          .attr("opacity", "0.5");
      })
      .on("mouseout", function (d, i) {
        modules.Tooltip.hide();

        var tempDr = d.r * opt.scale;
        if (tempDr < 1)
          tempDr = 1;

        d3.select(this)
          .attr("r", tempDr)
          .attr("opacity", "1.0");
      })
      .on("contextmenu.circle", function (d, i) {
        var activeFlag = true;
        d3.select(this).classed("d3-active-map", activeFlag);
        //onClick(d, activeFlag);
        onContextMenu();
      })
      .on("click.circle", function (d, i) {
        var activeFlag = d3.select(this).classed("d3-active-map");
        d3.select(this).classed("d3-active-map", !activeFlag);
        onClick(d, activeFlag);
      })
    ;

    var gText = _chinaMap.append('g')
      .selectAll(".place-label")
      .data(cityData)
      .enter()
      .append("text")
      .attr("class", "place-label")
      .attr("transform", function (d) {
        var xyPoint = _projection([d.city.Lng, d.city.Lat]);
        //console.log(xyPoint);
        d.x = xyPoint[0];
        d.y = xyPoint[1];
        //d3.select(this)
        //    .attr("x", d.x)
        //    .attr("y", d.y);
        xyPoint[0] = xyPoint[0] + 6;
        xyPoint[1] = xyPoint[1];
        return "translate(" + xyPoint + ")";
      })
      .attr("dy", ".35em")
      .text(function (d) {
        var name = d.city.name;
        var cityValue = getCityValue(name);

        cityValue = Number(cityValue).toMeasureFormart(opt.rowMeasure.numberFormat);

        return cityValue;
      })
    ;

    chart.label = null;
    chart.label = new d3.util.TextDecorate(gText, 0, [[0, 0], [0, 0]], opt.textStyle);
    chart.label.init();
  }

  function getDataRow(rowKey) {
    if (opt === null) return null;

    var key = opt.colDimension.name;
    var fList = Enumerable.from(opt.data.sourceData).where(
      function (d) {
        if (rowKey.indexOf(d[key]) > -1) {
          return d;
        }
      }
    ).toArray();

    return fList;
  }

  function getProvinceColor(rowKey) {
    if (opt === null) return null;

    if (opt.data.geoType === 1) return opt.valleyColor;

    var index = -1;
    opt.category.some(function (d, i) {
      if (rowKey.indexOf(d) > -1) {
        index = i;
        return true;
      }
    });

    var value = 0;
    if (index > -1) {
      value = opt.values[index];
    }

    var p = parseInt((value / opt.data.maxValue) * 100);

    var color = colorHelper.calc_gradient_color(opt.peakColor, 100, p, opt.valleyColor);

    //console.log(rowKey + ":" + color + ":" + value + "/" + _maxValue);

    return color;
  }

  function getProvinceName(rowKey) {
    if (opt === null) return null;

    var name = rowKey;
    opt.category.some(function (d, i) {
      if (rowKey.indexOf(d) > -1) {
        name = d;
        return true;
      }
    });

    return name;
  }

  function getProvinceValue(rowKey) {
    if (opt === null) return null;

    var index = -1;
    opt.category.some(function (d, i) {
      if (rowKey.indexOf(d) > -1) {
        index = i;
        return true;
      }
    });

    var value = 0;
    if (index > -1) {
      value = opt.values[index];
    }

    return value;
  }

  function getCityPercent(rowKey) {
    if (opt === null) return null;

    var index = -1;
    opt.category.some(function (d, i) {
      if (rowKey.indexOf(d) > -1) {
        index = i;
        return true;
      }
    });

    var value = 0;
    if (index > -1) {
      value = opt.values[index];
    }

    var p = value / opt.data.maxValue;

    return p;
  }

  function getCityValue(rowKey) {
    if (opt === null) return null;

    var index = -1;
    opt.category.some(function (d, i) {
      if (rowKey.indexOf(d) > -1) {
        index = i;
        return true;
      }
    });

    var value = 0;
    if (index > -1) {
      value = opt.values[index];
    }

    return value;
  }

  function getCityColor(rowKey) {
    if (opt === null) return null;

    var cityColor = opt.peakColor;
    //var cityColor = _precolor[rowKey.length % 20];
    var haveColor = false;
    if (opt.data.geoType === 1 && opt.color != undefined) {
      opt.color.some(function (d) {
        if (d.Key === rowKey) {
          cityColor = d.Color;
          haveColor = true;
          return true;
        }
      });
    }

    if (!haveColor && opt.color != undefined) {
      var cityItem = Enumerable.from(opt.data.geoCity).where(
        function (d) {
          if (d.cityName === rowKey) {
            return d;
          }
        }
      ).toArray()[0];

      cityColor = _precolor[(cityItem.city.PkId / 100) % 20];
    }

    return cityColor;
  }

  function getValue(d) {
    var value = 0;
    if (opt.data.geoType === 0) {
      var key = opt.properties.name;
      value = getProvinceValue(key);
    } else {
      var key = d.cityName;
      value = getCityValue(key);
    }

    return value;
  }

  function getRowKey(d) {
    var key = "";
    if (opt.data.geoType === 0) {
      //key = d.properties.name;
      key = d.provinceName;
    } else {
      key = d.cityName;
    }

    return key;
  }

  function fillGeoCity() {
    if (opt === null) return null;

    var key = opt.colDimension.name;

    //x.Provences.Citys.name
    var cityList = Array.prototype.concat.apply([], _geoCitySource.Provinces.map(d => d.Citys));
    //console.log(_geoCitySource);
    //console.log(cityList);

    opt.data.sourceData.forEach(function (cRow) {
      var cityName = cRow[key];
      var cityItem;
      cityList.some(function (c) {
        if (c.name.indexOf(cityName) > -1) {
          cityItem = c;
          return true;
        }
      });
      if (cityItem != undefined) {
        var p = getCityPercent(cityName);
        opt.data.geoCity.push({r: 8 * p, cityName: cityName, data: cRow, city: cityItem});
      }
    });
  }

  function EventX() {
    //$(container).position().top;
    //$(container).position().left;
    //console.log($(container).position().top + "|" + d3.event.offsetX);
    //console.log($(container).position().left + "|" + d3.event.offsetX);

    if ($('#panel_chart').length > 0)
      return d3.event.pageX + 30;
    else
      return d3.event.offsetX + 30;
  }

  function EventY() {
    if ($('#panel_chart').length > 0)
      return d3.event.pageY + 30;
    else
      return d3.event.offsetY + 30;
  }

  function selectMapRect() {
    svg.on("mousedown", function () {
      clickTime = (new Date()).getTime();
      mapSelectFlag = true;
      rect.attr("transform", "translate(" + d3.event.layerX + "," + d3.event.layerY + ")");
      startLoc = [d3.event.layerX, d3.event.layerY];

      //if (opt.data.geoType === 0) {
      //    _chinaMap.selectAll("path")
      //        .classed("d3-active-map", false);
      //}
      //else {
      //    _chinaMap.selectAll("circle")
      //        .classed("d3-active-map", false);
      //}
    });

    svg.on("mousemove", function () {
      //判断事件target
      if (mapSelectFlag === true) {
        var width = d3.event.layerX - startLoc[0];
        var height = d3.event.layerY - startLoc[1];
        if (width < 0) {
          rect.attr("transform", "translate(" + d3.event.layerX + "," + startLoc[1] + ")");
        }
        if (height < 0) {
          rect.attr("transform", "translate(" + startLoc[0] + "," + d3.event.layerY + ")");
        }
        if (height < 0 && width < 0) {
          rect.attr("transform", "translate(" + d3.event.layerX + "," + d3.event.layerY + ")");
        }
        rect.attr("width", Math.abs(width)).attr("height", Math.abs(height))

        var range = [];
        range[0] = startLoc;
        range[1] = [d3.event.layerX, d3.event.layerY];

        //click事件判断
        if (range[0][0] === range[1][0] && range[0][1] === range[1][1]) {
          return false;
        }

        if (opt.data.geoType === 0) {
          _chinaMap.selectAll("path")
            .classed("d3-active-map", brushGet(range));
        } else {
          _chinaMap.selectAll("circle")
            .classed("d3-active-map", brushGet(range));
        }
      }
    });

    svg.on("mouseup", function () {
      if (mapSelectFlag === true) {
        mapSelectFlag = false;
        endLoc = [d3.event.layerX, d3.event.layerY];
        var leftTop = [];
        var rightBottom = []
        if (endLoc[0] >= startLoc[0]) {
          leftTop[0] = startLoc[0];
          rightBottom[0] = endLoc[0];
        } else {
          leftTop[0] = endLoc[0];
          rightBottom[0] = startLoc[0];
        }

        if (endLoc[1] >= startLoc[1]) {
          leftTop[1] = startLoc[1];
          rightBottom[1] = endLoc[1];
        } else {
          leftTop[1] = endLoc[1];
          rightBottom[1] = startLoc[1];
        }

        var range = [];
        range[0] = startLoc;
        range[1] = endLoc;

        console.log(startLoc);
        console.log(endLoc);

        //click事件判断
        if (range[0][0] === range[1][0] && range[0][1] === range[1][1]) {
          return false;
        }

        var paths = [], arr = [], names = [];
        if (opt.data.geoType === 0) {
          paths = _chinaMap.selectAll("path").filter(brushGet(range));
          paths.each(function (d) {
            arr.push(d);
          });
          //names = _.chain(arr).map("properties").map("provinceName").uniq().value();
          names = _.chain(arr).map("provinceName").uniq().value();
        } else {
          paths = _chinaMap.selectAll("circle").filter(brushGet(range));
          paths.each(function (d) {
            arr.push(d);
          });
          names = _.chain(arr).map("cityName").uniq().value();
        }

        console.log(names);

        if (chart.onselected != undefined) {
          chart.onselected([
            {field: opt.colDimension, value: names}
          ]);
        }
      }
      var times = (new Date()).getTime() - clickTime;
      rect.attr("width", 0).attr("height", 0);
    })
  }

  var zoom = d3.zoom()
    .on("zoom", function () { // zoom事件
      //console.log(`Zoom: ${d3.zoomTransform(svg.node())}`);
      //console.log(`D3: : ${d3.event.transform}`);
      d3.select(this).selectAll("g").attr("transform", d3.zoomTransform(svg.node()));
    });

  // singolton function
  var getTooltip = function () {
    return tooltip || (tooltip = d3.select(document.body).append("div").attr("class", "d3-ttip").node());
  };
  var getContextmenu = function () {
    return contextMenu || (contextMenu = d3.select(document.body).append("div").attr("class", "d3-contextmenu open"));
  };

  function normalise() {
    svg.html("");

    svg = d3.select(container)
      .html("")
      .append("svg")
      .attr("width", opt.size.width)
      .attr("height", opt.size.height)
      .attr("version", "1.1")
      .attr("xmlns", "http://www.w3.org/2000/svg")
      .style("font", "12px arial, sans-serif");

    tipDiv = d3.select(container).append('div').attr("class", "tooltip").style("opacity", 0);

    _chinaMap = svg.append('g')
    //.attr("id", "chinaMap")
    ;
  }

  function verticalMap() {

    //与OnMouseOver事件冲突，暂时屏蔽
    //brush = null;
    //brush = d3.brush()
    //    .extent([[0, 0], [opt.size.width, opt.size.height]])
    //    .filter(brushFilter)
    //    .on("start", onBrushStart)
    //    .on("brush", onBrushMove)
    //    .on("end", onBrushEnd);
    //gBrush = svg.append("g")
    //    .attr("class", "gBrush")
    //    //.style("fill", "#CCCCCC")
    //    ;
    //gBrush.call(brush);

    rect = svg.append("rect")
      .attr("width", 0)
      .attr("height", 0)
      .attr("fill", "#666666")
      .attr("stroke", "#333333")
      .attr("stroke-width", "1px")
      .attr("transform", "translate(0,0)")
      .attr("id", "mapSelect")
      .style("opacity", 0.5)
    ;

    selectMapRect();

    //draw china map
    d3.json('./mapdata/china.json', function (error, root) {
      if (opt === null) return null;
      if (error) {
        return console.error(error);
      }

      _georoot = root;
      var center_long = 106, center_lat = 38;

      var fWidth = width - 120;
      //if (opt.data.geoType === 1) {
      //    fWidth = width - 120;
      //}
      _projection = d3.geoMercator().fitSize([fWidth, height], _georoot);

      _path = d3.geoPath()
        .projection(_projection);

      var paths = _chinaMap.append('g')
        .selectAll("path")
        .data(_georoot.features)
        .enter()
        .append('path')
        .attr("class", "province")
        .attr("d", _path)
        .attr("fill", function (d, i) {
          var name = d.properties.name;
          //return _color[i];
          //return _color(i);
          //return _colorFill;
          return getProvinceColor(name);
        })
        .style("stroke-width", "1px")
        .style("stroke", "#333333")
        .on("mouseover", function (d, i) {
          if (opt.data.geoType === 0) {

            var fName = opt.colDimension.name;
            var name = d.properties.name;
            var tipFields = [{name: fName, value: name}];

            var dataRow = getDataRow(name);
            if (dataRow.length === 0) {
              for (var f in opt.data.sourceData[0]) {
                if (f != opt.colDimension.name) {
                  var tempValue = 0;
                  if (opt.MeasureMap[f] !== undefined) {
                    tipFields.push({
                      name: f,
                      value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)
                    });
                  }
                }
              }
            } else {
              dataRow.forEach(function (row) {
                for (var f in row) {
                  if (f != opt.colDimension.name) {
                    var tempValue = row[f];
                    if (opt.MeasureMap[f] !== undefined) {
                      tipFields.push({
                        name: f,
                        value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)
                      });
                    }
                  }
                }
              });
            }

            modules.Tooltip.show(opt.tipFormat, tipFields);

            d3.select(this)
              .style("stroke-width", "2px")
              //.style("fill", "yellow")
              .style("stroke", "#333333");
          } else {
            return false;
          }
        })
        .on("mouseout", function (d, i) {
          if (opt.data.geoType === 0) {
            modules.Tooltip.hide();

            d3.select(this)
              .style("stroke-width", "1px")
              //.style("fill", _colorFill)
              .style("stroke", "#333333");
          } else {
            return false;
          }
        })
        .on("contextmenu", onContextMenu)
        .on("click", function (d, i) {
          if (opt.data.geoType === 0) {
            var activeFlag = d3.select(this).classed("d3-active-map");
            d3.select(this).classed("d3-active-map", !activeFlag);
            onClick(d, activeFlag);
          } else {
            clearSelected();
          }
        });

      do_animation(paths);

      fill_provinceName();

      //组织城市地理信息
      if (opt.data.geoType === 1 && opt.data.geoCity.length === 0) {
        fillGeoCity();
      }
      if (opt.data.geoCity.length > 0) {
        addBasicData(opt.data.geoCity);
      }

      renderLegend();
    })

    //自定义样式
    customMapStyle();

  } // verticalMap END

  // clere contextmenu
  svg.on("click.foo", function () {
    return contextMenu && contextMenu.style("display", "none");
  });

  // avoid ugly default interface
  svg.on("contextmenu.foo", function () {
    d3.event.preventDefault();
  });

  // EVENTS DEFINE
  function onContextMenu(d) {
    d3.event.preventDefault();

    var context = getContextmenu();
    var commands = [
      {title: "删除片区", symbol: "&#10008;", action: "delete"},
      //{ title: "降序排布", symbol: "&#8650;", action: "descend" },
      //{ title: "升序排布", symbol: "&#8648;", action: "ascend" },

      {title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg"},
      {title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png"}
    ];

    // delete map slice accroding to its "_index"
    var index = -1;

    context
      .html("")
      .style("display", "block")
      .append("ul")
      .attr("class", "dropdown-menu")
      .selectAll("li")
      .data(commands)
      .enter()
      .append("li")
      .append("a")
      .html(function (j) {
        return j.symbol + "&nbsp;" + j.title;
      })
      .on("click", function (j) {
        context.style("display", "none");
        switch (j.action) {
          case "delete":
            chart.delete(index);
            break;
          case "descend":
            chart.descend();
            break;
          case "ascend":
            chart.ascend();
            break;
          case "export":
            chart.export(j.p1);
            break;
        }
      });

    context.style("left", d3.event.clientX + window.pageXOffset + 10 + "px");
    context.style("top", d3.event.clientY + window.pageYOffset + 10 + "px");
  }

  function onClick(d, activeFlag) {
    clearSelected(d, activeFlag);

    if (chart.onselected) {
      chart.onselected([{
        field: opt.colDimension,
        value: [getRowKey(d)]
      }]);
    }
  }

  //清理批量选择
  function clearSelected(item, activeFlag) {
    if (opt.data.geoType === 0) {
      _chinaMap.selectAll("path")
        .classed("d3-active-map", function (d, i) {
          if (item === d && !activeFlag) {
            return true;
          }
          return false;
        });
    } else {
      _chinaMap.selectAll("circle")
        .classed("d3-active-map", function (d, i) {
          if (item === d && !activeFlag) {
            return true;
          }
          return false;
        });
    }
  }

  /**
   * 自定义表格样式
   */
  function customMapStyle() {
    var $style = $('#css_map_theme');
    if ($style.length === 0) {
      $style = $('<style id="css_map_theme" type="text/css"></style>');
      $("head").append($style);
    }

    var cssStr = "";
    cssStr += '.d3-active-map{fill: ' + opt.config.MultiColors[0] + ';}';

    $style.html(cssStr);
  }

  //Brush 暂未使用
  function brushGet(range) {
    //console.log(range);
    return function (d) {
      var cx = d.x, cy = d.y;
      var x1 = range[0][0];
      var x2 = range[1][0];
      var y1 = range[0][1];
      var y2 = range[1][1];

      var selectValue = cx >= x1 && cx <= x2 && cy >= y1 && cy <= y2;

      if (x1 > x2) {
        selectValue = cx >= x2 && cx <= x1 && cy >= y1 && cy <= y2;

        if (y1 > y2) {
          selectValue = cx >= x2 && cx <= x1 && cy >= y2 && cy <= y1;
        }
      }
      //console.log(d);
      //console.log(selectValue);

      return selectValue;
    };
  }

  function brushFilter() {
    return (d3.event.button !== 1);
  }

  function onBrushStart() {
    if (opt.data.geoType === 0) {
      _chinaMap.selectAll("path")
        .classed("d3-active-map", false);
    } else {
      _chinaMap.selectAll("circle")
        .classed("d3-active-map", false);
    }
  }

  function onBrushMove() {
    var range = d3.event.selection;

    if (opt.data.geoType === 0) {
      _chinaMap.selectAll("path")
        .classed("d3-active-map", brushGet(range));
    } else {
      _chinaMap.selectAll("circle")
        .classed("d3-active-map", brushGet(range));
    }
  }

  function onBrushEnd() {
    var range, paths, arr = [], xVal, names;

    range = d3.brushSelection(this);
    if (!chart.onselected || !range) {
      return;
    }

    //paths = _chinaMap.selectAll("path").filter(brushGet(range));
    if (opt.data.geoType === 0) {
      paths = _chinaMap.selectAll("path").filter(brushGet(range));
      paths.each(function (d) {
        arr.push(d);
      });
      //names = _.chain(arr).map("properties").map("provinceName").uniq().value();
      names = _.chain(arr).map("provinceName").uniq().value();
    } else {
      paths = _chinaMap.selectAll("circle").filter(brushGet(range));
      paths.each(function (d) {
        arr.push(d);
      });
      names = _.chain(arr).map("cityName").uniq().value();
    }

    chart.onselected([
      {field: opt.colDimension, value: names}
    ]);

  } // onBrushEnd END

  //render legend at the right of the chart
  function renderLegend() {
    if (opt.data.geoType === 1) {
      var top10 = [];
      var bottom10 = [];
      var legendList = [];

      //var orderList = Enumerable.from(opt.data.sourceData).orderByDescending(function (d) { return d[opt.rowMeasure.name]; });
      //top10 = orderList.take(10).toArray();
      //bottom10 = orderList.skip(orderList.toArray().length - 10).toArray();

      var orderList = Enumerable.from(opt.data.geoCity).orderByDescending(function (d) {
        return d.data[opt.rowMeasure.name];
      });
      top10 = orderList.take(10).toArray().map(function (d) {
        return d.data;
      });
      bottom10 = orderList.skip(orderList.toArray().length - 10).toArray().map(function (d) {
        return d.data;
      });

      var fName = opt.colDimension.name;
      var top0 = {};
      top0[fName] = "前10";
      top10 = [top0].concat(top10);
      var bottom0 = {};
      bottom0[fName] = "后10";
      bottom10 = [bottom0].concat(bottom10);

      var topCount = top10.length;
      legendList = legendList.concat(top10);
      legendList = legendList.concat(bottom10);

      var legend = svg.selectAll(".legend")
        .data(legendList)
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", function (d, i) {
          var legendX = opt.size.width - 120;
          var legendY = i * 16;
          if (i >= topCount) {
            legendX += 60;
            legendY = (i - topCount) * 16;
          }
          return "translate(" + legendX + ", " + legendY + ")";
        });

      legend.append("rect")
        .attr("x", 0)
        .attr("y", 3)
        .attr("width", 16)
        .attr("height", 8)
        .style("fill", function (d, i) {
          var name = d[opt.colDimension.name];
          if (name === "前10" || name === "后10") {
            var cityColor = "#ffffff";

            d3.select(this)
              .attr("width", 0)
              .attr("height", 0)
            ;

            return cityColor;
          } else {

            var cityColor = getCityColor(name);
            return cityColor;
          }
        });

      legend.append("text")
        .attr("x", 20)
        .attr("y", 11)
        .classed("legendtext", true)
        .attr("fill", opt.config.textStyle.fill)
        .text(function (d, i) {
          var name = d[opt.colDimension.name];

          if (name === "前10" || name === "后10") {
            d3.select(this)
              .attr("x", 8)
              .attr("y", 11)
              .attr("font-weight", "bold")
            ;
          }

          if (name.length >= 3 && name != "前10" && name != "后10") {
            name = name.substring(0, 2) + '..';
          }

          return name;
        })
        .on("mouseover", function (d, i) {
          var name = d[opt.colDimension.name];
          if (name != "前10" && name != "后10") {
            var fName = opt.colDimension.name;
            var tipFields = [{name: fName, value: name}];

            var dataRow = getDataRow(name);
            if (dataRow.length === 0) {
              for (var f in opt.data.sourceData[0]) {
                if (f != opt.colDimension.name) {
                  var tempValue = 0;
                  if (opt.MeasureMap[f] !== undefined) {
                    tipFields.push({
                      name: f,
                      value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)
                    });
                  }
                  //tipFields.push({ name: f, value: tempValue });
                }
              }
            } else {
              dataRow.forEach(function (row) {
                for (var f in row) {
                  if (f != opt.colDimension.name) {
                    var tempValue = row[f];
                    if (opt.MeasureMap[f] !== undefined) {
                      tipFields.push({
                        name: f,
                        value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)
                      });
                    }
                    //tipFields.push({ name: f, value: tempValue });
                  }
                }
              });
            }

            modules.Tooltip.show(opt.tipFormat, tipFields);

            //modules.Tooltip.showString(name);
          }
        })
        .on("mouseout", function (d, i) {
          var name = d[opt.colDimension.name];
          if (name != "前10" && name != "后10") {
            modules.Tooltip.hide();
          }
        })
      ;
    } else {
      var legendItem = {Name: opt.rowMeasure.name, Max: opt.data.maxValue, Min: opt.data.minValue};
      var legendList = [legendItem];

      var defs = svg.append("defs");
      var linearGradient = defs.append("linearGradient").attr("id", "legendcolor");
      linearGradient.attr("x1", "0%").attr("x2", "0%").attr("y1", "100%").attr("y2", "0%");
      linearGradient.append("stop").attr("offset", "0%").attr("style", "stop-color:" + opt.valleyColor);
      linearGradient.append("stop").attr("offset", "100%").attr("style", "stop-color:" + opt.peakColor);

      var legend = svg.selectAll(".legend")
        .data(legendList)
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", function (d, i) {
          var legendX = opt.size.width * 0.85;
          var legendY = i * 16;
          return "translate(" + legendX + ", " + legendY + ")";
        });

      var legendHeight = 110;
      if (opt.size.height < 230) {
        legendHeight = 60;
      }

      legend.append("rect")
        .attr("x", 0)
        .attr("y", 6)
        .attr("width", 12)
        .attr("height", legendHeight)
        .attr("stroke", "#333333")
        .attr("stroke-width", "1px")
        .attr("fill", "url(#legendcolor)");

      //标题
      legend.append("text")
        .attr("x", -12)
        .attr("y", 6)
        .classed("legendtext", true)
        .attr("fill", opt.config.textStyle.fill)
        .text(function (d, i) {

          d3.select(this)
            .attr("writing-mode", "tb")
          ;

          var name = d.name;
          return name;
        });
      //最小值
      legend.append("text")
        //.attr("x", 16)
        //.attr("y", 136)
        .attr("transform", function (d, i) {
          var minValue = d.Min.toFixed(2);
          //if (minValue > 1000) {
          //    minValue = minValue / 1000;
          //    minValue = minValue.toFixed(2);
          //    minValue = minValue + "K";
          //}
          var name = minValue + "";
          var nameLen = name.length;
          var pointLen = 0;
          if (name.indexOf(".") > -1) {
            nameLen = nameLen - 1;
            pointLen = 1;
          }
          var y = legendHeight - nameLen * 6 - pointLen * 3;
          return "rotate(90) translate(" + y + ",-16)"
        })
        .classed("legendtext", true)
        .attr("fill", opt.config.textStyle.fill)
        .text(function (d, i) {
          var name = d.Min + '';
          if (name.indexOf(".") > -1) {
            name = d.Min.toFixed(2);
          }
          if (legendHeight === 60) {
            name = "";
          }
          return name;
        });
      //最大值
      legend.append("text")
        //.attr("x", function (d) {
        //    return 16;
        //})
        //.attr("y", 26)
        .attr("transform", "rotate(90) translate(6,-16)")
        .classed("legendtext", true)
        .attr("fill", opt.config.textStyle.fill)
        .text(function (d, i) {
          var maxValue = d.Max.toFixed(2);
          var name = maxValue;
          return name;
        });
    }
  }

  chart.reverse = function () {
    return this;
  };

  chart.reset = function () {
    return this;
  };

  function setColor(color) {

    normalise();
    peakColor = options.colorMark.GlobalColor;
    valleyColor = "#ffffff";

    opt.peakColor = color;
    opt.valleyColor = "#ffffff";

    verticalMap();

    return this;
  }

  // set province area color
  chart.setGlobalColor = setColor;

  // update map and legend
  function update(oldData, newData) {

  }

  chart.sort = function (type) {

  };
  chart.ascend = function () {
    this.sort(false);
    return this;
  };
  chart.descend = function () {
    this.sort(true);
    return this;
  };

  /* delete one node */
  chart.delete = function () {
    var paths = [], arr = [], names = [];
    if (opt.data.geoType === 0) {
      paths = _chinaMap.selectAll(".d3-active-map");
      paths.each(function (d) {
        arr.push(d);
      });
      names = _.chain(arr).map("provinceName").uniq().value();
      paths.attr("fill", "#ffffff");
    } else {
      paths = _chinaMap.selectAll(".d3-active-map");
      paths.each(function (d) {
        arr.push(d);
      });
      names = _.chain(arr).map("cityName").uniq().value();
      paths.style("display", "none");
    }

    console.log(names);

    if (this.onRemoveItem) {
      this.onRemoveItem([
        {field: opt.colDimension, value: names}
      ]);
    }
  };

  // customized tooltip content formate
  chart.tipFormat = function (s) {
    if (!arguments.length) {
      return opt.tipFormat;
    }
    opt.tipFormat = s;
    return this;
  };

  // change chart dimension: { width: 500, height: 300 }
  chart.setSize = function (size) {
    if (!arguments.length) {
      return opt.size;
    }
    //计算缩放比例
    var wScale = size.width / opt.size.width;
    var hScale = size.height / opt.size.height;
    //var scale = wScale > hScale ? hScale : wScale;
    var scale = hScale;

    opt.size.width = size.width;
    opt.size.height = size.height;

    // chart redraw
    svg.attr("width", size.width).attr("height", size.height);
    //zoom.scaleBy(svg, scale);

    normalise();
    width = opt.size.width;
    height = opt.size.height;
    verticalMap();

    return this;
  };

  chart.getLegends = function () {
    return [];
  };

  chart.getScale = function () {
    return opt.scale;
  };

  chart.setScale = function (val) {
    if (typeof val === "string") {
      val = +val;
    }
    if (val > 1 || val < 0) {
      return;
    }

    normalise();

    opt.scale = val;

    verticalMap();
  };

  chart.export = function (type) {
    d3.util.exportChart(svg.node(), type || "png");
  };

  chart.removeChart = function () {
    svg.remove();
    //if (tooltip) { d3.select(tooltip).remove(); }
    if (contextMenu) {
      contextMenu.remove();
    }

    tooltip = null;
    contextMenu = null;
    container = null;
    opt = null;

    for (var pp in chart) {
      delete chart[pp];
    }
    chart = null;
  };

  return chart;
}

export const MapChart = {
  build: buildChart
}

