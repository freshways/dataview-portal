﻿/**
 * map-city chart 17：省市地图（主色渐变）
 */
define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../../data/sharedata'),
        colorHelper: require('./../../../util/colorHelper'),
        Tooltip: require('./../../../control/tooltip')
    }

    var _themeColor = {
        //"murasaki": { peakColor: "#05a066", valleyColor: "#e2f7fc" },
        //"black": { peakColor: "#b93907", valleyColor: "#fcd3c4" },
        "blue": { peakColor: "#2C5985", valleyColor: "#BAE3D7" },
        "mac-mojave": { peakColor: "#b93907", valleyColor: "#fcd3c4" }
    };
    var _peakColor = "#2C5985";//最大
    var _valleyColor = "#BAE3D7";//最小
    var _geoSource = []; //地理原始数据
    var _geoPath = 'geometryCouties';
    var _CityList = []; //县级城市数据

    //预设颜色
    //var _precolor = ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"];
    var _precolor = ["#5271FF", "#001EFF", "#FAAD14", "#FF7800", "#FF3600", "#52C41A", "#00FF18", "#13C2C2", "#B37FEB", "#722ED1", "#FF85C0", "#EB2F96", "#FADB16", "#FFF566", "#A3BAFF", "#5271FF", "#001EFF", "#FAAD14", "#FF7800", "#FF3600"];

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {

        var dimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
        var measure = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];
        //过滤空值
        data = Enumerable.from(data).where(function (d) { if (d[dimension.name] != "") { return d; } }).toArray();
        var sourceData = data.concat();//原始数据
        var geoCityCode = "330300"; //默认温州
        var maxValue = 100;
        var minValue = 0;
        var geoCity = [];

        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }

        //样式配置对象
        var config = options.chartConfig;
        if (!config) {
            config = {
                GlobalColor: "#4E79A7",
                MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
                scale: 0.5,
                axisStyle: {
                    tickColor: "#000",
                    fontColor: "#000",
                    fontFamily: "Arial",
                    fontSize: 15
                },
                textStyle: {
                    "display": "none",
                    "font-family": "Arial",
                    "font-size": 15,
                    "font-style": "normal",
                    "font-weight": "normal",
                    "fill": "#333",
                    "position": "top"
                }
            };
        }



        //下钻兼容
        _geoPath = 'geometryCouties';
        if (options.geoinfo) {
            //加载全国省市信息
            $.ajax({
                url: SiteRootPath + '/Scripts/DataInsight/component/chart/map/mapdata/china-Lng-Lat.json',
                data: {},
                type: "get",
                async: false,//使用同步的方式
                dataType: "json",
                //cache:true,
                error: function () {
                    alert("获取信息时服务端出错");
                },
                success: function (result) {
                    _geoSource = result;
                }
            });
            if (options.geoinfo.GeoMarker === "Geo-P") {
                var oneProvince = Enumerable.from(_geoSource.Provinces).where(function (d) {
                    if (d.name.indexOf(options.geoinfo.GeoLabel) > -1) {
                        return d;
                    }
                }).toArray()[0];

                geoCityCode = (oneProvince.PkId + "").replace('0000', '');
                _geoPath = 'geometryProvince';
            }
            else if (options.geoinfo.GeoMarker === "Geo-C") {
                var cityList = Array.prototype.concat.apply([], _geoSource.Provinces.map(d => d.Citys));
                var oneCity = Enumerable.from(cityList).where(function (d) {
                    if (d.name.indexOf(options.geoinfo.GeoLabel) > -1) {
                        return d;
                    }
                }).toArray()[0];

                geoCityCode = oneCity.PkId;
            }
        }
        else {
            //省市判断
            if (dimension.GeoMarkerCode.indexOf('|') > -1) {
                if (dimension.GeoMarkerCode.split('|')[1] === "-1") {
                    geoCityCode = dimension.GeoMarkerCode.split('|')[0];
                    geoCityCode = (geoCityCode + "").replace('0000', '');
                    _geoPath = 'geometryProvince';
                }
                else
                    geoCityCode = dimension.GeoMarkerCode.split('|')[1];
            }
            else {
                geoCityCode = dimension.GeoMarkerCode;
            }
        }

        //获取所有度量字段
        var measureFields_c = Enumerable.from(columnFields).where(
            function (d) {
                if (d.slaveType === 1) {
                    return d;
                }
            }
        ).toArray();
        var measureFields_r = Enumerable.from(rowFields).where(
            function (d) {
                if (d.slaveType === 1) {
                    return d;
                }
            }
        ).toArray();
        var measureFields = measureFields_c.concat(measureFields_r);
        var measureMap = {};
        measureFields.forEach(function (f) {
            measureMap[f.name] = f;
        });

        var values, category, reverse, tipFormat, color, peakColor, valleyColor, scale;

        (function () {

            var dKey = dimension.name, mKey = measure.name;

            if (dimension.dataType === 4 || dimension.dataType === 2) { // 如果为时间（数值）升序排列
                data.sort(function (a, b) { return a[dKey] - b[dKey]; });
            }

            values = data.map(function (d) { return d[mKey]; });
            category = data.map(function (d) { return d[dKey]; });

            maxValue = Math.max(...values);
            minValue = Math.min(...values);

            tipFormat = options.TooltipFormat;
            if (!tipFormat) {
                var tipFormatStr = '';
                tipFormatStr += dimension.name + '：<' + dimension.name + '>';
                tipFormatStr += '\r\n' + measure.name + '：<' + measure.name + '>';
                tipFormat = tipFormatStr;
            }

            //color = options.colorMark.GlobalColor
            //if (options.colorMark && options.colorMark.GlobalColor) {
            //    peakColor = options.colorMark.GlobalColor;
            //    valleyColor = "#FFFFFF";
            //}
            //else {
            //    peakColor = _themeColor[SiteTheme].peakColor;
            //    valleyColor = _themeColor[SiteTheme].valleyColor;

            //    peakColor = valleyColor = "#FFFFFF";

            //    color = options.colorMark.MultiColors;
            //}
            if (config.GlobalColor) {
                peakColor = config.GlobalColor;
                valleyColor = "#FFFFFF";
                //peakColor = "#1260A8";
                //valleyColor = "#F8DB37";
            }
            else {
                peakColor = _themeColor[SiteTheme].peakColor;
                valleyColor = _themeColor[SiteTheme].valleyColor;

                peakColor = valleyColor = "#FFFFFF";

                color = config.MultiColors;
            }
        })();

        /* SlaveType: 1 -> dimension || 0 -> measure */
        reverse = !!columnFields[0].slaveType;

        scale = ("scale" in options) ? options.scale : 0.8;

        if ("scale" in config) { scale = config.scale; }
        if ("textStyle" in config) { textStyle = config.textStyle; }

        return map(svgSelector, {
            values: values,
            category: category,
            isReverse: reverse,
            scale: scale,
            peakColor: peakColor,
            valleyColor: valleyColor,
            color: color,
            tipFormat: tipFormat,
            colDimension: dimension,
            rowMeasure: measure,
            MeasureMap: measureMap,
            size: options.size,
            textStyle: textStyle || {},
            config: config,
            data: { sourceData: sourceData, maxValue: maxValue, minValue: minValue, geoCityCode: geoCityCode }
        });
    }

    function map(selector, options) {
        var opt = {
            //size    : { width: 450, height: 300 },
            category: [],
            values: [],
            //color: d3.schemeCategory20c,
            isReverse: false,
            scale: 0.8, // range: [0, 1], oScale padding
            tipFormat: null,
            colDimension: null,
            rowMeasure: null,
            MeasureMap: null,
            textStyle: {},
            config: {},
            data: null
        };

        // output object
        var chart = {
            status: opt.isReverse ? "herizontal" : "vertical",
            onselected: null
        };

        opt = d3.util.extend(opt, options);

        var container = d3.select(selector).node().parentElement;
        if (!opt.size) {
            opt.size = d3.util.getInnerBoxDim(container);
        }
        else if (opt.size.height <= 160) {
            opt.size.height = 160;
        }

        var svg = d3.select(container)
            .html("")
            .append("svg")
            .attr("width", opt.size.width)
            .attr("height", opt.size.height)
            .attr("version", "1.1")
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .style("font", "12px arial, sans-serif");

        var _cityMap = svg.append('g')
            //.attr("id", "cityMap")
            ;
        var _georoot;
        var _projection;
        var _path;
        var _color = d3.scaleOrdinal(d3.schemeCategory20);//opt.color;
        var _colorFill = "#4E79A7";
        var width = opt.size.width;
        var height = opt.size.height;
        var convertData = [];
        var tipDiv = d3.select(container).append('div').attr("class", "tooltip").style("opacity", 0);

        var contextMenu;

        //draw china map
        verticalMap();

        function do_animation(path) {
            var totalLength = path.node().getTotalLength();
            path
                .attr("stroke-dasharray", totalLength + " " + totalLength)
                .attr("stroke-dashoffset", totalLength)
                .transition()
                .duration(2000)
                .attr("stroke-dashoffset", 0);
        }

        function fill_cityName() {
            var gText = _cityMap.append('g')
                .selectAll(".place-label")
                .data(_georoot.features)
                .enter()
                .append("text")
                .attr("class", "place-label")
                .attr("transform", function (d) {
                    //var cityName = d.properties.name;
                    //var cityItem = _CityList.find(function (d) {
                    //    if (d.name.indexOf(cityName) > -1) {
                    //        return true;
                    //    }
                    //});

                    //if (cityItem !== undefined) {
                    //    var xyPoint = _projection([cityItem.Lng, cityItem.Lat]);
                    //    d.x = xyPoint[0];
                    //    d.y = xyPoint[1];

                    //    return "translate(" + xyPoint + ")";
                    //}

                    // return "translate([0,0])";

                    var xyPoint = _projection(d.properties.center ? d.properties.center : d.properties.cp);
                    d.x = xyPoint[0];
                    d.y = xyPoint[1];

                    return "translate(" + xyPoint + ")";
                })
                .attr("dy", ".35em")
                .text(function (d) {
                    var name = d.properties.name;
                    var cityName = getCityName(name);
                    d.cityName = cityName;
                    var cityValue = getCityValue(name);

                    cityValue = Number(cityValue).toMeasureFormart(opt.rowMeasure.numberFormat);

                    return cityName + '|' + cityValue;
                })
                ;

            var textNodeList = d3.selectAll(".place-label");
            textNodeList._groups[0].forEach(function (textNode) {
                var textContent = textNode.textContent;
                textNode.textContent = "";
                d3.select(textNode).selectAll("tspan")
                    .data(textContent.split('|'))
                    .enter()
                    .append("tspan")
                    .attr("x", ".5em")
                    .attr("dy", "1em")
                    .text(function (d) {
                        return d;
                    })
                    ;
            });

            chart.label = null;
            chart.label = new d3.util.TextDecorate(gText, 0, [[0, 0], [0, 0]], opt.textStyle);
            chart.label.init();
        }

        function getCityName(rowKey) {
            if (opt === null) return null;

            var name = rowKey;
            opt.category.some(function (d, i) {
                if (rowKey.indexOf(d) > -1) {
                    name = d;
                    return true;
                }
            });

            return name;
        }

        function getCityValue(rowKey) {
            if (opt === null) return null;

            var index = -1;
            opt.category.some(function (d, i) {
                if (rowKey.indexOf(d) > -1) {
                    index = i;
                    return true;
                }
            });

            var value = 0;
            if (index > -1) {
                value = opt.values[index];
            }

            return value;
        }

        function getCityColor(rowKey) {
            if (opt === null) return null;

            if (opt.data.geoType === 1) return opt.valleyColor;

            var index = -1;
            opt.category.some(function (d, i) {
                if (rowKey.indexOf(d) > -1) {
                    index = i;
                    return true;
                }
            });

            var value = 0;
            if (index > -1) {
                value = opt.values[index];
            }

            var p = parseInt((value / opt.data.maxValue) * 100);

            var color = colorHelper.calc_gradient_color(opt.peakColor, 100, p, opt.valleyColor);

            return color;
        }

        function getDataRow(rowKey) {
            if (opt === null) return null;

            var key = opt.colDimension.name;
            var fList = Enumerable.from(opt.data.sourceData).where(
                function (d) {
                    if (rowKey.indexOf(d[key]) > -1) {
                        return d;
                    }
                }
            ).toArray();

            return fList;
        }

        var zoom = d3.zoom()
            .on("zoom", function () { // zoom事件
                d3.select(this).selectAll("g").attr("transform", d3.zoomTransform(svg.node()));
            });

        // singolton function
        var getTooltip = function () {
            return tooltip || (tooltip = d3.select(document.body).append("div").attr("class", "d3-ttip").node());
        };
        var getContextmenu = function () {
            return contextMenu || (contextMenu = d3.select(document.body).append("div").attr("class", "d3-contextmenu open"));
        };

        function onClick(d, activeFlag) {
            clearSelected(d, activeFlag);

            if (chart.onselected) {
                chart.onselected([{
                    field: opt.colDimension,
                    value: [d.cityName]
                }]);
            }
        }

        //清理批量选择
        function clearSelected(item, activeFlag) {
            _cityMap.selectAll("path")
                .classed("d3-active-map", function (d, i) {
                    if (item === d && !activeFlag) {
                        return true;
                    }
                    return false;
                });
        }

        /**
        * 自定义表格样式
        */
        function customMapStyle() {
            var $style = $('#css_map_theme');
            if ($style.length === 0) {
                $style = $('<style id="css_map_theme" type="text/css"></style>');
                $("head").append($style);
            }

            var cssStr = "";
            cssStr += '.d3-active-map{fill: '+ opt.config.MultiColors[0] +';}';

            $style.html(cssStr);
        }

        function normalise() {
            svg.html("");

            svg = d3.select(container)
                .html("")
                .append("svg")
                .attr("width", opt.size.width)
                .attr("height", opt.size.height)
                .attr("version", "1.1")
                .attr("xmlns", "http://www.w3.org/2000/svg")
                .style("font", "12px arial, sans-serif");

            tipDiv = d3.select(container).append('div').attr("class", "tooltip").style("opacity", 0);

            _cityMap = svg.append('g')
                //.attr("id", "_cityMap")
                ;
        }

        function verticalMap() {

            if (opt === null) return null;

            //draw city map
            d3.json(SiteRootPath + '/Scripts/DataInsight/component/chart/map/mapdata/' + _geoPath + '/' + opt.data.geoCityCode + '.json', function (error, root) {
                if (error) {
                    return console.error(error);
                }

                _georoot = root;
                //var fWidth = width - 120;
                //var fWidth = width - 60;
                var fWidth = width;
                _projection = d3.geoMercator().fitSize([fWidth, height], _georoot);

                _path = d3.geoPath()
                    .projection(_projection);

                var paths = _cityMap.append('g')
                    .selectAll("path")
                    .data(_georoot.features)
                    .enter()
                    .append('path')
                    .attr("class", "province")
                    .attr("d", _path)
                    .attr("fill", function (d, i) {
                        var name = d.properties.name;
                        return getCityColor(name);
                    })
                    .style("stroke-width", "1px")
                    .style("stroke", "#333333")
                    .on("mouseover", function (d, i) {
                        var fName = opt.colDimension.name;
                        var name = d.properties.name;
                        var tipFields = [{ name: fName, value: name }];

                        var dataRow = getDataRow(name);
                        if (dataRow.length === 0) {
                            for (var f in opt.data.sourceData[0]) {
                                if (f != opt.colDimension.name) {
                                    var tempValue = 0;
                                    if (opt.MeasureMap[f] !== undefined) {
                                        tipFields.push({ name: f, value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat) });
                                    }
                                }
                            }
                        }
                        else {
                            dataRow.forEach(function (row) {
                                for (var f in row) {
                                    if (f != opt.colDimension.name) {
                                        var tempValue = row[f];
                                        if (opt.MeasureMap[f] !== undefined) {
                                            tipFields.push({ name: f, value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat) });
                                        }
                                    }
                                }
                            });
                        }

                        if (opt.tipFormat) {
                            modules.Tooltip.show(opt.tipFormat, tipFields);
                        }

                        d3.select(this)
                            .style("stroke-width", "2px")
                            //.style("fill", "yellow")
                            .style("stroke", "#333333");
                    })
                    .on("mouseout", function (d, i) {
                        modules.Tooltip.hide();

                        d3.select(this)
                            .style("stroke-width", "1px")
                            //.style("fill", _colorFill)
                            .style("stroke", "#333333");
                    })
                    .on("contextmenu", onContextMenu)
                    .on("click", function (d, i) {
                        var activeFlag = d3.select(this).classed("d3-active-map");
                        d3.select(this).classed("d3-active-map", !activeFlag);
                        onClick(d, activeFlag);
                    });

                //取消动画，丢失路径
                //do_animation(paths);

                fill_cityName();

                renderLegend();
            })

            //自定义样式
            customMapStyle();

        } // verticalMap END

        // clere contextmenu
        svg.on("click.foo", function () {
            return contextMenu && contextMenu.style("display", "none");
        });

        // avoid ugly default interface
        svg.on("contextmenu.foo", function () { d3.event.preventDefault(); });

        // EVENTS DEFINE
        function onContextMenu(d) {
            d3.event.preventDefault();

            var context = getContextmenu();
            var commands = [
                //{ title: "删除片区", symbol: "&#10008;", action: "delete" },
                //{ title: "降序排布", symbol: "&#8650;", action: "descend" },
                //{ title: "升序排布", symbol: "&#8648;", action: "ascend" },

                { title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg" },
                { title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png" }
            ];

            // delete map slice accroding to its "_index"
            var index = -1;

            context
                .html("")
                .style("display", "block")
                .append("ul")
                .attr("class", "dropdown-menu")
                .selectAll("li")
                .data(commands)
                .enter()
                .append("li")
                .append("a")
                .html(function (j) { return j.symbol + "&nbsp;" + j.title; })
                .on("click", function (j) {
                    context.style("display", "none");
                    switch (j.action) {
                        case "delete": chart.delete(index); break;
                        case "descend": chart.descend(); break;
                        case "ascend": chart.ascend(); break;
                        case "export": chart.export(j.p1); break;
                    }
                });

            context.style("left", d3.event.clientX + window.pageXOffset + 10 + "px");
            context.style("top", d3.event.clientY + window.pageYOffset + 10 + "px");
        }

        //render legend at the right of the chart
        function renderLegend() {
            var legendItem = { Name: opt.rowMeasure.name, Max: opt.data.maxValue, Min: opt.data.minValue };
            var legendList = [legendItem];

            var defs = svg.append("defs");
            var linearGradient = defs.append("linearGradient").attr("id", "legendcolor");
            linearGradient.attr("x1", "0%").attr("x2", "0%").attr("y1", "100%").attr("y2", "0%");
            linearGradient.append("stop").attr("offset", "0%").attr("style", "stop-color:" + opt.valleyColor);
            linearGradient.append("stop").attr("offset", "100%").attr("style", "stop-color:" + opt.peakColor);

            var legend = svg.selectAll(".legend")
                .data(legendList)
                .enter()
                .append("g")
                .attr("class", "legend")
                .attr("transform", function (d, i) {
                    var legendX = 30;
                    var legendY = opt.size.height * 0.85 - 50;
                    if (!opt.config || !opt.config.legendPosition || opt.config.legendPosition === 'leftBottom') {

                    }
                    else if (opt.config.legendPosition === 'left') {
                        legendY = 0;
                    }
                    else if (opt.config.legendPosition === 'right') {
                        legendX = opt.size.width * 0.85;
                        legendY = 0;
                    }
                    else if (opt.config.legendPosition === 'rightBottom') {
                        legendX = opt.size.width * 0.85;
                    }
                    return "translate(" + legendX + ", " + legendY + ")";
                });

            legend.append("rect")
                .attr("x", 0)
                .attr("y", 6)
                .attr("width", 12)
                .attr("height", 60)
                .attr("stroke", "#333333")
                .attr("stroke-width", "1px")
                .attr("fill", "url(#legendcolor)");

            //标题
            //legend.append("text")
            //    .classed("legendtext", true)
            //    .attr("transform", function (d, i) {
            //        return "translate(0,0)"
            //    })
            //    .text(function (d, i) {
            //        var name = "高";
            //        return name;
            //    });
            //legend.append("text")
            //    .classed("legendtext", true)
            //    .attr("transform", function (d, i) {
            //        return "translate(0,80)"
            //    })
            //    .text(function (d, i) {
            //        var name = "低";
            //        return name;
            //    });

            //最大值
            legend.append("text")
                .attr("transform", "translate(16,16)")
                .classed("legendtext", true)
                .attr("fill", opt.config.textStyle.fill)
                .text(function (d, i) {
                    var maxValue = d.Max;
                    if ((maxValue + '').indexOf('.') > -1) {
                        maxValue = d.Max.toFixed(2);
                    }
                    if (maxValue > 10000) {
                        maxValue = maxValue / 10000;
                        maxValue = maxValue.toFixed(2) + "万";
                    }
                    var name = maxValue;
                    return name;
                });

            //最小值
            legend.append("text")
                .attr("transform", function (d, i) {
                    var minValue = d.Min.toFixed(2);
                    //if (minValue > 1000) {
                    //    minValue = minValue / 1000;
                    //    minValue = minValue.toFixed(2);
                    //    minValue = minValue + "K";
                    //}
                    var name = minValue + "";
                    var nameLen = name.length;
                    var pointLen = 0;
                    if (name.indexOf(".") > -1) {
                        nameLen = nameLen - 1;
                        pointLen = 1;
                    }
                    //var y = 116 - 50 - nameLen * 6 - pointLen * 3;
                    return "translate(16,66)"
                })
                .classed("legendtext", true)
                .attr("fill", opt.config.textStyle.fill)
                .text(function (d, i) {
                    var name = d.Min + '';
                    if (name.indexOf(".") > -1) {
                        name = d.Min.toFixed(2);
                    }
                    return name;
                });
        }

        chart.reverse = function () { return this; };

        chart.reset = function () { return this; };

        function setColor(color) {

            normalise();
            peakColor = options.colorMark.GlobalColor;
            valleyColor = "#ffffff";

            opt.peakColor = color;
            opt.valleyColor = "#ffffff";

            verticalMap();

            return this;
        }

        // set province area color
        chart.setGlobalColor = setColor;

        // update map and legend
        function update(oldData, newData) { }

        chart.sort = function (type) { };
        chart.ascend = function () { this.sort(false); return this; };
        chart.descend = function () { this.sort(true); return this; };

        /* delete one node */
        chart.delete = function () { };

        // customized tooltip content formate
        chart.tipFormat = function (s) {
            if (!arguments.length) { return opt.tipFormat; }
            opt.tipFormat = s;
            return this;
        };

        // change chart dimension: { width: 500, height: 300 }
        chart.setSize = function (size) {
            if (!arguments.length) { return opt.size; }
            //计算缩放比例
            var wScale = size.width / opt.size.width;
            var hScale = size.height / opt.size.height;
            //var scale = wScale > hScale ? hScale : wScale;
            var scale = hScale;

            opt.size.width = size.width;
            opt.size.height = size.height;

            // chart redraw
            svg.attr("width", size.width).attr("height", size.height);
            //zoom.scaleBy(svg, scale);

            normalise();
            width = opt.size.width;
            height = opt.size.height;
            verticalMap();

            return this;
        };

        chart.getLegends = function () { return []; };

        chart.getScale = function () { return opt.scale; };

        chart.setScale = function (val) {
            if (typeof val === "string") { val = +val; }
            if (val > 1 || val < 0) { return; }

            normalise();

            opt.scale = val;

            verticalMap();
        };

        chart.export = function (type) { d3.util.exportChart(svg.node(), type || "png"); };

        chart.removeChart = function () {
            svg.remove();
            //if (tooltip) { d3.select(tooltip).remove(); }
            if (contextMenu) { contextMenu.remove(); }

            tooltip = null;
            contextMenu = null;
            container = null;
            opt = null;

            for (var pp in chart) { delete chart[pp]; }
            chart = null;
        };

        return chart;
    }

    module.exports = {
        build: buildChart
    }

});
