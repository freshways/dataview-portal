﻿/**
 * pie chart
 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {

        var cOpt = {}; // chart options
        var randomColor = function () {
            return d3.schemeCategory20[Math.floor(Math.random() * 20)];
        };

        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }
        cOpt.colDimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
        cOpt.rowMeasure = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];

        (function () {

            var dKey = cOpt.colDimension.name,
                mKey = cOpt.rowMeasure.name;

            if (cOpt.colDimension.dataType === 4) { // 如果为时间（数值）升序排列
                data.sort(function (a, b) { return a[dKey] - b[dKey]; });
            }

            cOpt.values = data.map(function (d) { return d[mKey]; });
            cOpt.category = data.map(function (d) { return d[dKey]; });

        })();

        var colorMatch = options.colorMark.MultiColors, color;
        if (colorMatch && colorMatch.length) {
            color = cOpt.category.map(function (d) {
                var obj = colorMatch.find(function (j) { return j.Key === d; });
                return obj ? obj.Color : randomColor();
            });
            cOpt.color = color;
        }

        if ("scale" in options) {
            cOpt.scale = options.scale;
        }
        if (options.size) {
            cOpt.size = options.size;
        }

        return pie(svgSelector, cOpt);
    };


function pie (selector, options) {

    var opt = {
        //size    : { width: 300, height: 400 },
        category: [],
        values  : [],

        scale  : 0.8, // control outerRadius [0, 1]
        color   : d3.schemeCategory20c,
        colDimension: null,
        rowMeasure: null
    };
    _.extend(opt, options);


    var innerRadius,
        outerRadius,
        padding = 35,
        minDim  = 200,    // min required width or height dimension
        arc  = d3.arc(),  // pei generator
        tArc = d3.arc(),  // generator while on transition
        lPie = d3.pie().sort(null).value(function (d) { return d.y; }),
        f    = d3.format(".2%"),
        //numFix = d3.format(",." + (opt.rowMeasure.NumberFixed || 0) + "f"),
        legend = d3.util.hlegend(),
        contextMenu;

    var cData = (function () {
        var randomColor = function () {
            return d3.schemeCategory20[Math.floor(Math.random() * 20)];
        };
        return opt.values.map(function (d, i) {
            return {
                x     : opt.category[i],
                y     : d,
                color : opt.color[i] || randomColor(),
                _index: i
            };
        });
    })();


    var menuOnSingleItem = [
        {title: "删除片区", symbol: "&#10008;", action: "delete"},
        {title: "降序排布", symbol: "&#8650;", action: "descend"},
        {title: "升序排布", symbol: "&#8648;", action: "ascend"},
        {title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg"},
        {title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png"}
    ];
    var menuOnBlank = menuOnSingleItem.slice(1);
    contextMenu = d3.util.getChartContextMenu();
    contextMenu.clickFun = function (command, target) {
        this.hide();
        switch (command.action) {
            case "delete" : chart.delete(target); break;
            case "descend": chart.descend(); break;
            case "ascend" : chart.ascend(); break;
            case "export" : chart.export(command.p1); break;
            default: console.log("... Catch YOU! ...");
        }
    };


    var container = d3.select(selector).node().parentElement;
    if (!opt.size) {
        opt.size = d3.util.getInnerBoxDim(container);
    }

    var svg = d3.select(container)
                .html("")
                .append("svg")
                .attr("class", "Chart-To-Be-Export")
                .attr("width",  opt.size.width)
                .attr("height", opt.size.height)
                .attr("version", "1.1")
                .attr("xmlns", "http://www.w3.org/2000/svg")
                .style("font", "12px arial, sans-serif");
    svg.on("contextmenu", onBlankBrushContextMenu);

    var gPie, cText,
        gLegend = svg.append("g").attr("transform",
                "translate(" + [padding, 10] + ")");

    function initial () {

        // assign the dimension
        if (opt.size.width  < minDim) { opt.size.width  = minDim; }
        if (opt.size.height < minDim) { opt.size.height = minDim; }

        // assign the dimension
        outerRadius = (d3.min([opt.size.width, opt.size.height]) -
                       (2 * padding)) / 2;
        innerRadius = outerRadius * 0.5;

        arc.innerRadius(innerRadius).outerRadius(outerRadius);
        tArc.innerRadius(innerRadius).outerRadius(outerRadius);
    }
    initial();

    gPie = svg.append("g")
        .attr("class", "gPie")
        .attr("transform",
              "translate(" + [padding + outerRadius, padding + outerRadius] + ")");

    cText = gPie.append("text")
        .text("")
        .attr("text-anchor", "middle")
        .attr("y", 15);

    gPie.selectAll("path")
        .data(lPie(cData))
        .enter()
        .append("path")
        .on("mouseenter", onMouseEnter)
        .on("mouseleave", onMouseLeave)
        .on("contextmenu", onContextMenu)
        .on("click", onClick)
        .each(function (d) {
            d3.select(this)
                .attr("fill", d.data.color)
                .attr("d", arc)
                .attr("stroke", "black")
                .attr("stroke-width", 0)
                .append("title")
                .text(d.data.x);
        });



    // EVENTS DEFINE
    function onMouseEnter (d) {
        cText.append("tspan")
            //.text(numFix(d.value))
            // Number API from "extensions.js"
            .text(d.value.toMeasureFormart(opt.rowMeasure.numberFormat))
            .attr("x", 0)
            .attr("dy", -15)
            .style("font-weight", "bold");
        cText.append("tspan")
            .text(f((d.endAngle - d.startAngle) / (Math.PI * 2)))
            .attr("x", 0)
            .attr("dy", 15);

        d3.select(this)
            .transition()
            .ease(d3.easeExpInOut)
            .attrTween("d", function (j) {
                var f = d3.interpolate(outerRadius, outerRadius * 1.1);
                return function (t) { return arc.outerRadius(f(t))(j); };
            });
    }

    function onMouseLeave (d) {
        d3.select(this)
            .transition()
            .attrTween("d", function (j) {
                var f = d3.interpolate(outerRadius * 1.1, outerRadius);
                return function (t) { return arc.outerRadius(f(t))(j); };
            });
        cText.text("");
    }

    function onContextMenu (d) {
        d3.event.preventDefault();
        d3.event.stopPropagation();

        // delete pie slice accroding to its "_index"
        var index = d.data._index;

        contextMenu.setTargetNode(index)
                   .setCurrentMenu(menuOnSingleItem)
                   .show();
    }

    function onBlankBrushContextMenu () {
        d3.event.preventDefault();
        contextMenu.setCurrentMenu(menuOnBlank).show();
    }

    function onClick (d) {
        if (chart.onselected) {
            chart.onselected([{
                field: opt.colDimension,
                value: [ d.data.x ]
            }]);
            gPie.selectAll("path").attr("stroke-width", 0);
            d3.select(this).attr("stroke-width", 2);
        }
    }



    var chart = {
        svg: svg,
        onselected: null
    };

    // update pie and legend
    function update (oldData, newData) {
        var oldPie = lPie(oldData),
            newPie = lPie(newData);

        var status = gPie.selectAll("path").data(newPie);

        status
            .exit()
            .transition()
            .attrTween("d", function (d, i) {
                var median = 0.5 * (d.startAngle + d.endAngle),
                    f1 = d3.interpolate(oldPie[i].startAngle, median),
                    f2 = d3.interpolate(oldPie[i].endAngle,median);
                return function (t) { return tArc.startAngle(f1(t)).endAngle(f2(t))(d); };
            })
            .remove();

        status
            .attr("fill", function (d) { return d.data.color; })
            .attr("stroke-width", 0)
            .transition()
            .attrTween("d", function (d, i) {
                var f1 = d3.interpolate(oldPie[i].startAngle, d.startAngle),
                    f2 = d3.interpolate(oldPie[i].endAngle, d.endAngle);
                return function (t) { return tArc.startAngle(f1(t)).endAngle(f2(t))(d); };
            })
            .select("title")
            .text(function (d) { return d.data.x; });

        legend.constraintWidth(opt.size.width - 2 * padding)
            .data(chart.getLegends())
            .constraintedSketch();
    }


    // layout pie descendly
    chart.descend = function () {
        var oldData = cData.slice();
        cData.sort(function (a, b) { return b.y - a.y; });

        update(oldData, cData);
        return this;
    };
    chart.ascend = function () {
        var oldData = cData.slice();
        cData.sort(function (a, b) { return a.y - b.y; });

        update(oldData, cData);
        return this;
    };

    // delete one slice of pie according to its predefined index
    chart.delete = function (index) {
        if (cData.length < 2) { return alert("不能再删了！"); }
        var oldData = cData.slice(), pickedItem, pickedIndex;
        pickedIndex = _.findIndex(cData, function (d) { return d._index === index; });
        pickedItem  = cData.splice(pickedIndex, 1);
        update(oldData, cData);

        if (this.onRemoveItem) {
            this.onRemoveItem([
                { field: opt.colDimension, value: [ pickedItem[0].x ] }
            ]);
        }
    };

    chart.sequenceColor = function (arr) {
        cData.map(function (d, i) {
            d.color = arr[i] || "black";
        });
        update(cData, cData);

        return this;
    };

    // if width or height less than the "minDim"
    // ignore the new value and set the dimension to "minDim"
    chart.setSize = function (size) {
        if (!arguments.length) { return opt.size; }
        opt.size = size;

        // assign new outerRadius,innerRasius,
        initial();
        legend.constraintWidth(opt.size.width - 2 * padding)
            .constraintedSketch();

        svg.attr("width", opt.size.width).attr("height", opt.size.height);
        gPie.selectAll("path").attr("d", arc);
        gPie.transition().attr("transform",
            "translate(" + [padding + outerRadius, padding + outerRadius] + ")");

        return this;
    };

    // output legend name and its color attached
    chart.getLegends = function () {
        return cData.map(function (d) {
            return { name: d.x, color: d.color };
        });
    };

    chart.setScale = function (val) {
        if (!arguments.length) { return; }
        if (typeof val === "string") { val = +val; }
        if ((val > 1) || (val < 0)) { return; }

        var maxRadius = opt.size.width * 0.5;
        outerRadius = maxRadius * val;
        innerRadius = outerRadius * 0.5;
        arc.innerRadius(innerRadius).outerRadius(outerRadius);
        tArc.innerRadius(innerRadius).outerRadius(outerRadius);

        gPie.selectAll("path").attr("d", arc);
    };
    chart.getScale = function () {
        var maxRadius = opt.size.width * 0.5;
        return outerRadius / maxRadius;
    };

    chart.export = function (type) {
        d3.util.exportChart(svg.node(), type || "png");
    };

    chart.removeChart = function () {
        svg.remove();
        contextMenu.remove();

        contextMenu = null;
        container   = null;
        opt         = null;

        for (var pp in chart) { delete chart[pp]; }
        chart = null;
    };

    // initialise legend
    legend.container(gLegend)
            .constraintWidth(opt.size.width - 2 * padding)
            .data(chart.getLegends())
            .groupMaxLength(90)
            .constraintedSketch();

    return chart;
}

    module.exports = {
        build: buildChart
    };

});
