/* global d3, undersore, _, require, define */


define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {

        /*
            cOpt.colDimension.dataType:
                1 : String
                2 : Number
                3 : Boolean
                4 : Date in Number ("xxxx" -> year, "xx" -> day/month)

            SlaveType: 0 -> dimension, 1 -> measure
        */

        var cOpt = {}, colKey, rowKey, zKey,
            isKeysOverlap = false, tipFormat,
            config = options.chartConfig;

        if (!config) {
            config = {
                GlobalColor: "#4E79A7",
                MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
                scale: 0.8,
                axisStyle: {
                    tickColor : "#000",
                    fontColor : "#000",
                    fontFamily: "Arial",
                    fontSize  : 15
                },
                textStyle: {
                    "display"     : "none",
                    "font-family" : "Arial",
                    "font-size"   : 15,
                    "font-style"  : "normal",
                    "font-weight" : "normal",
                    "fill"        : "#333",
                    "position"    : "top"
                }
            };
        }

        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }

        cOpt.colDimension = (columnFields[0].slaveType === 0) ?
                            columnFields[0] : rowFields[0];
        cOpt.rowMeasure = (rowFields[0].slaveType === 1) ?
                            rowFields[0] : columnFields[0];

        colKey = cOpt.colDimension.name;
        rowKey = cOpt.rowMeasure.name;

        if (options.colorMark.annexField &&
            options.colorMark.annexField.name) {
            zKey = options.colorMark.annexField.name;
            isKeysOverlap = (zKey == colKey ? true : false);
        }
        else {
            // in case of single line
            // using "groupBy" produce undefined intended
            zKey = " ^(# ^_^ #)^";
        }


        cOpt.isReverse = columnFields[0].slaveType === 1;

        // Number ascent order
        function numSort(a, b) { return a.x - b.x; }

        cOpt.values = _.chain(data)
            .groupBy(function (d) { return d[zKey]; })
            .map(function (val, key) {

                var points = _.map(val, function (d) {
                    return { x: d[colKey], y: d[rowKey] };
                });

                if (cOpt.colDimension.dataType === 4 ||
                    cOpt.colDimension.dataType === 2 ) {
                    points.sort(numSort);
                }

                return {
                    // when only one line titled with "untitled"
                    name : (zKey !== " ^(# ^_^ #)^") ? key : "untitled",
                    value: points,
                    //color: (zKey !== " ^(# ^_^ #)^") ? key : "untitled"
                };
            })
            .value();


        if (isKeysOverlap) {
            cOpt.values = (function () {
                var value = data.map(function (d) { return { x: d[colKey], y: d[rowKey] }; });
                if (cOpt.colDimension.dataType === 4 ||
                    cOpt.colDimension.dataType === 2 ) {
                    value.sort(numSort);
                }
                return [{ name: "untitled", value: value }];
            })();
        }

        // If the data is not Number (date), I can NOT guarantee the sequence order.
        cOpt.category = _.uniq(data, function (d) { return d[colKey]; })
                         .map(function (d) { return d[colKey]; });
        if (cOpt.colDimension.dataType === 4 ||
            cOpt.colDimension.dataType === 2 ) {
            cOpt.category.sort(function (a, b) { return a - b; });
        }

        (function () {
            if (!config.MultiColors.length) {
                config.MultiColors = ["#4E79A7", "#A0CBE8", "#F28E2B"];
            }
            var colors = [];
            while (colors.length < cOpt.values.length) {
                colors = colors.concat(config.MultiColors);
            }
            cOpt.color = colors;
        })();
        if (options.colorMark &&
            options.colorMark.annexField &&
            options.colorMark.annexField.slaveType === 0) {
            cOpt.annexField = options.colorMark.annexField || {};
        }
        cOpt.annexField = cOpt.annexField || {};
        cOpt.isArea = ("isArea" in options) ? options.isArea : false;

        if (zKey == " ^(# ^_^ #)^" || isKeysOverlap) {
            tipFormat = d3.util.tipParse(
                [
                    { key: colKey, value: "x" },
                    { key: rowKey, value: "y" }
                ],
                ("TooltipFormat" in options) ? options.TooltipFormat : ""
            );
        }
        else {
            tipFormat = d3.util.tipParse(
                [
                    { key: colKey, value: "x" },
                    { key: rowKey, value: "y" },
                    { key: zKey, value: "name" }
                ],
                ("TooltipFormat" in options) ? options.TooltipFormat : ""
            );
        }

        cOpt.tipFormat = tipFormat;


        if (options.size) {
            cOpt.size = options.size;
        }

        if ("scale" in config) {
            cOpt.scale = config.scale;
        }
        if ("textStyle" in config) {
            cOpt.textStyle = config.textStyle;
        }
        if ("axisStyle" in config) {
            cOpt.axisStyle = config.axisStyle;
        }

        return lineArea(svgSelector, cOpt);
    };


function lineArea (selector, options) {
    //
    var opt = {
        //size    : { width: 600, height: 400 },

        category: [], // content to be String or Number

        /* values:
        [
            { name: "xx", value: [ {x: t, y: 12}, {x: t, y: 15}, ... ] },
            { name: "xx", value: [ {x: t, y: 17}, {x: t, y: 45}, ... ] }
        ]
        */
        values  : [],
        color   : d3.schemeCategory20,
        isReverse : false,
        isArea: false,

        scale: 0.25, // range: [0, 1], oScale(padding)
        tipFormat: null,
        textStyle: {},
        axisStyle: {},
        AnnexField: {},
        colDimension: {},
        rowMeasure: {}
    };

    // output object
    var chart = {
        status: opt.isReverse ? "herizontal" : "vertical",
        onselected: null
    };


    opt = d3.util.extend(opt, options);

    var container = d3.select(selector).node().parentElement;
    if (!opt.size) {
        opt.size = d3.util.getInnerBoxDim (container);
    }

    // preserve the original size
    // used for chart type reverse
    opt.size._w = opt.size.width;
    opt.size._h = opt.size.height;

    var svg = d3.select(container)
        .html("")
        .append("svg")
        // A class tag for bulk exporting operation
        .attr("class", "Chart-To-Be-Export")
        .attr("width",  opt.size.width)
        .attr("height", opt.size.height)
        .attr("version", "1.1")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .style("font", "12px arial, sans-serif");


    var lScale, oScale, lAxis, oAxis, lAxisGen, oAxisGen,
        labelD = opt.colDimension.name || "",
        labelM = opt.rowMeasure.name || "",
        margin = { top: 25, right: 20, bottom: 10, left: 10 },
        sqrt = Math.sqrt, abs = Math.abs, ceil = Math.ceil,
        minGraphDim = 100,  // min gChart dimension
        cMin = 3, cMax = 6, // points radius range
        cData, yExtent,
        tooltip, contextMenu,
        maxLineWidth = 8, lineWidth = maxLineWidth * opt.scale,
        brush,
        //numFix = d3.format(",." + (opt.rowMeasure.NumberFixed || 0) + "f"),
        textStyle = {
            "display"     : "none",    // "none" or "block"
            "font-family" : "Arial",   // "Arial", "Helvetica", "Times"
            "font-size"   : 12,
            "font-style"  : "normal",  // "normal" or "italic"
            "font-weight" : "normal",  // "normal" or "bold"
            "fill"        : "#ff0000", // text color
            "position"    : "top"      // "top", "right", "bottom" or "left"
        },
        gLeft, gBottom, gBrush, gChart, gText, gLegend,
        legend = d3.util.hlegend().groupMaxLength(90),

        curveType = d3.curveLinear,
        line = d3.line().defined(function (d) { return typeof(d.y) === "number"; }),
        area = d3.area().defined(function (d) { return typeof(d.y) === "number"; });

    // overwrite the default text label setting
    _.extend(textStyle, opt.textStyle);

    // singolton function
    var getTooltip = function () {
        return tooltip || (tooltip = d3.select(document.body).append("div").attr("class", "d3-ttip").node());
    };

    // Context Menu for right click event
    var menuOnSingleItem = [
        { title: "剔除数据", symbol: "&#10008;", action: "delete" },  // ✘
        {title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg"},
        {title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png"},
    ];
    var menuOnBlank = menuOnSingleItem.slice(1);
    var menuOnBrush = [
        {title: "删除框选节点", symbol: "&#10008;", action: "deleteNodes"}
    ];
    contextMenu = d3.util.getChartContextMenu();
    contextMenu.clickFun = function (command, target) {
        this.hide();
        switch (command.action) {
            case "delete": chart.delete([target]); break;
            case "reverse": chart.reverse(); break;
            case "linear": chart.curveType(d3.curveLinear); break;
            case "curve": chart.curveType(d3.curveCardinal); break;
            case "export": chart.export(command.p1); break;
            case "deleteNodes":
                chart.delete(target.map(node => { return d3.select(node).datum(); }));
                break;
        }
    };

    function getLegends () {
        if (cData.length < 2) { return []; }
        return _.chain(cData)
                .flatten()
                .uniq(function (d) { return d.name; })
                .map(function (d) {
                    return { name: d.name, color: d.color };
                })
                .value();
    }

    function tweenPath (generator) {
        return function  (d) {
            var length = d.length;
            function interpolate (t) {
                return d.slice(0, Math.ceil(length * t));
            }
            return function (t) { return generator(interpolate(t)); };
        };
    }
    function svgSizeCheck (yAxis, xAxis, svgSize) {

        var temp, yBox, xBox, size;

        // Step1: get width of yAxis
        var yWidth = yAxis.getSize().width;

        // Space for legend
        if (getLegends().length) {
            margin.top = yAxis._renderResults.tick.height + 15;
        }
        else { margin.top = 5; }

        // Step2: set xAxis scale range and get xAxis height
        temp = svgSize.width - margin.left - margin.right - yWidth;
        var xHeight = xAxis.range([0,
                        temp < minGraphDim ? minGraphDim : temp])
                    .getSize().height;

        // Step3: set yAxis range (height)
        temp = svgSize.height - margin.top - margin.bottom - xHeight;
        yAxis.range([0,
            temp < minGraphDim ? minGraphDim : temp]);

        // re-assure
        yBox = yAxis.getSize();
        xBox = xAxis.getSize();
        size = {
            width: yBox.width + xBox.width + margin.left + margin.right,
            height: xBox.height + yBox.height + margin.top + margin.bottom
        };

        if (svgSize.width < size.width) { svgSize.width = size.width; }
        if (svgSize.height < size.height) { svgSize.height = size.height; }

        return { yBox: yBox, xBox: xBox };
    }


    // vertical layout when opt.isReverse is false
    function vertical () {

        chart.status = "vertical";

        lScale.range([0, minGraphDim]);
        oScale.range([0, minGraphDim]);
        lAxis = d3.axisLeft().scale(lScale);
        oAxis = d3.axisBottom().scale(oScale);

        gLeft = svg.append("g").attr("class", "axis axis-y");
        gBottom = svg.append("g").attr("class", "axis axis-x");


        lAxisGen = d3.util.axis.linear({
            scale : lScale,
            axis  : lAxis,
            holder: gLeft,
            orient: "left",

            label: labelM,
            style: opt.axisStyle
        });
        oAxisGen = d3.util.axis.ordinal({
            scale : oScale,
            axis  : oAxis,
            holder: gBottom,
            orient: "bottom",

            label: labelD,
            style: opt.axisStyle
        });

        var boxes = svgSizeCheck(lAxisGen, oAxisGen, opt.size);
        var left, bottom, vHeight, hWidth;
        left   = boxes.yBox;
        bottom = boxes.xBox;
        vHeight = left.height;
        hWidth = bottom.width;

        svg.attr("width", opt.size.width)
            .attr("height", opt.size.height);


        // generator update
        line.x(function (d) { return oScale(d.x); })
            .y(function (d) { return -lScale(d.y); })
            .curve(curveType);

        area.x(function (d) { return oScale(d.x); })
            .y1(function (d) { return -lScale(d.y); })
            .y0(-lScale(0))
            .curve(curveType);


        brush = d3.brush()
            .extent([[0, -lScale.range()[1]], [oScale.range()[1], 0]])
            .filter(brushFilter)
            .on("start", onBrushStart)
            .on("brush", onBrushMove)
            .on("end",   onBrushEnd);


        // dimension re-asign
        gLegend = svg.append("g")
            .attr("class", "gLegend")
            .attr("transform", "translate(" + [left.width + margin.left, 10] + ")");
        gLeft.attr("transform", "translate(" +
            [left.width + margin.left, margin.top] + ")");
        gBottom.attr("transform", "translate(" +
            [left.width + margin.left, margin.top + left.height] + ")");
        gBrush = svg.append("g")
            .attr("class", "gBrush")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gChart = svg.append("g")
            .attr("class", "gChart")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gText = svg.append("g")
            .attr("class", "gText")
            .attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");


        oAxisGen.drawAxis();
        lAxisGen.drawAxis();
        gBrush.call(brush);

        gBrush.select(".selection").on("contextmenu", onBrushContextMenu);
        gBrush.select(".overlay").on("contextmenu", onBlankBrushContextMenu);

        // create legends
        legend.constraintWidth(bottom.width)
            .container(gLegend)
            .data(getLegends())
            .fontFamily(lAxisGen.style.fontFamily)
            .fontSize(lAxisGen.style.fontSize)
            .fontColor(lAxisGen.style.fontColor)
            .constraintedSketch();

        gChart.selectAll("g.series")
              .data(cData)
              .enter()
              .append("g")
              .attr("class", "series")
              .each(function (d, i) {
                  var self = d3.select(this),
                      color = d[0].color;

                  self.append("path")
                      .on("click", onClick)
                      .on("mouseenter", onPathMouseEnter)
                      .on("mouseleave", onPathMouseLeave)
                      .attr("class", "line")
                      //.attr("d", line)
                      .attr("fill", "none")
                      .attr("stroke", color)
                      .attr("stroke-width", lineWidth)
                      .transition()
                      .duration(1500)
                      .attrTween("d", tweenPath(line));

                  self.append("path")
                      .attr("class", "area")
                      //.attr("d", area)
                      .attr("fill", color)
                      .attr("fill-opacity", opt.isArea ? 0.2 : 0)
                      .attr("stroke", "none")
                      .style("pointer-events", "none")
                      .transition()
                      .duration(1500)
                      .attrTween("d", tweenPath(area));


                  self.selectAll("circle")
                      // filter points that not exist(null)
                      .data(d.filter(function (j) { return typeof(j.y) === "number"; }))
                      .enter()
                      .append("circle")
                      .on("contextmenu.circle", onContextMenu)
                      .on("click", onClick)
                      .attr("cx", function (j) { return oScale(j.x); })
                      .attr("cy", function (j) { return -lScale(j.y); })
                      .attr("fill", color)
                      .attr("r", cMin);
              });

        gChart.on("mousemove", onMouseMove)
              .on("mouseleave", onMouseLeave);

        var textCoordinates;
        textCoordinates = [].concat.apply([], cData)
            .map(function (d) {
                return {
                    x   : oScale(d.x),
                    y   : -lScale(d.y),
                    //text: numFix(d.y)
                    text: d.y.toMeasureFormart(opt.rowMeasure.numberFormat)
                };
            });
        gText.selectAll("text")
            .data(textCoordinates)
            .enter()
            .append("text")
            .each(function (d) {
                d3.select(this)
                    .attr("x", d.x).attr("y", d.y).text(d.text);
            });
        chart.label = null;
        chart.label = new d3.util.TextDecorate(gText, 3,
                [[0, -vHeight], [hWidth, 0]], textStyle);
        chart.label.init();

    } // vertical END

    // herizontal layout when opt.isRevers is true
    function herizontal () {

        chart.status = "herizontal";

        lScale.range([0, minGraphDim]);
        oScale.range([0, minGraphDim]);
        lAxis = d3.axisBottom().scale(lScale);
        oAxis = d3.axisLeft().scale(oScale);

        gLeft = svg.append("g").attr("class", "axis axis-y");
        gBottom = svg.append("g").attr("class", "axis axis-x");



        lAxisGen = d3.util.axis.linear({
            scale : lScale,
            axis  : lAxis,
            holder: gBottom,
            orient: "bottom",

            label: labelM,
            style: opt.axisStyle
        });
        oAxisGen = d3.util.axis.ordinal({
            scale : oScale,
            axis  : oAxis,
            holder: gLeft,
            orient: "left",

            label: labelD,
            style: opt.axisStyle
        });


        var boxes = svgSizeCheck(oAxisGen, lAxisGen, opt.size);
        var hWidth, vHeight, left, bottom;
        left   = boxes.yBox;
        bottom = boxes.xBox;
        vHeight = left.height;
        hWidth = bottom.width;

        svg.attr("width", opt.size.width)
            .attr("height", opt.size.height);


        // generator update
        line.x(function (d) { return lScale(d.y); })
            .y(function (d) { return -oScale(d.x); })
            .curve(curveType);

        area.x0(function (d) { return lScale(d.y); })
            .y(function (d) { return -oScale(d.x); })
            .x1(lScale(0))
            .curve(curveType);


        brush = d3.brush()
            .extent([[0, -oScale.range()[1]], [lScale.range()[1], 0]])
            .filter(brushFilter)
            .on("start", onBrushStart)
            .on("brush", onBrushMove)
            .on("end",   onBrushEnd);

        // dimension re-asign
        gLegend = svg.append("g")
            .attr("class", "gLegend")
            .attr("transform", "translate(" + [left.width + margin.left, 10] + ")");
        gLeft.attr("transform", "translate(" +
                [left.width + margin.left, margin.top] + ")");
        gBottom.attr("transform", "translate(" +
                [left.width + margin.left, margin.top + left.height] + ")");
        gBrush = svg.append("g")
            .attr("class", "gBrush")
            .attr("transform", "translate(" + [left.width + margin.left, margin.top + left.height] + ")");
        gChart = svg.append("g")
            .attr("class", "gChart")
            .attr("transform", "translate(" + [left.width + margin.left, margin.top + left.height] + ")");
        gText = svg.append("g")
            .attr("class", "gText")
            .attr("transform", "translate(" + [left.width + margin.left, margin.top + left.height] + ")");

        // create the axis and initial brush
        oAxisGen.drawAxis();
        lAxisGen.drawAxis();
        gBrush.call(brush);

        gBrush.select(".selection").on("contextmenu", onBrushContextMenu);
        gBrush.select(".overlay").on("contextmenu", onBlankBrushContextMenu);

        // create legends
        legend.constraintWidth(bottom.width)
            .container(gLegend)
            .data(getLegends())
            .fontFamily(lAxisGen.style.fontFamily)
            .fontSize(lAxisGen.style.fontSize)
            .fontColor(lAxisGen.style.fontColor)
            .constraintedSketch();

        gChart.selectAll("g.series")
              .data(cData)
              .enter()
              .append("g")
              .attr("class", "series")
              .each(function (d, i) {
                  var self = d3.select(this),
                      color = d[0].color;

                  self.append("path")
                      .on("click", onClick)
                      .on("mouseenter", onPathMouseEnter)
                      .on("mouseleave", onPathMouseLeave)
                      .attr("class", "line")
                      //.attr("d", line)
                      .attr("fill", "none")
                      .attr("stroke", color)
                      .attr("stroke-width", lineWidth)
                      .transition()
                      .duration(1500)
                      .attrTween("d", tweenPath(line));

                  self.append("path")
                      .attr("class", "area")
                      //.attr("d", area)
                      .attr("fill", color)
                      .attr("fill-opacity", opt.isArea ? 0.2 : 0)
                      .attr("stroke", "none")
                      .style("pointer-events", "none")
                      .transition()
                      .duration(1500)
                      .attrTween("d", tweenPath(area));

                  self.selectAll("circle")
                      //.data(d)
                      .data(d.filter(function (j) { return typeof(j.y) === "number"; }))
                      .enter()
                      .append("circle")
                      .on("contextmenu.circle", onContextMenu)
                      .on("click", onClick)
                      .attr("cx", function (j) { return lScale(j.y); })
                      .attr("cy", function (j) { return -oScale(j.x); })
                      .attr("fill", color)
                      .attr("r", cMin);
              });

        gChart.on("mousemove", onMouseMove)
              .on("mouseleave", onMouseLeave);

        var textCoordinates;
        textCoordinates = [].concat.apply([], cData)
            .map(function (d) {
                return {
                    x   : lScale(d.y),
                    y   : -oScale(d.x),
                    //text: numFix(d.y)
                    text: d.y.toMeasureFormart(opt.rowMeasure.numberFormat)
                };
            });
        gText.selectAll("text")
            .data(textCoordinates)
            .enter()
            .append("text")
            .each(function (d) {
                d3.select(this)
                    .attr("x", d.x).attr("y", d.y).text(d.text);
            });
        chart.label = null;
        chart.label = new d3.util.TextDecorate(gText, 3, [[0, -vHeight], [hWidth, 0]], textStyle);
        chart.label.init();

    } // herizontal END

    function normalise () {
        gLeft = null; gBottom = null; gLegend = null;
        gBrush = null; gChart = null; gText = null;
        svg.html(""); // clear all elements

        // initialise cData
        if (!cData) {
            cData = opt.values.map(function (d, i) {
                return d.value.map(function (m, n) {
                    return {
                        x     : ("x" in m) ? m.x : opt.category[n],
                        y     : ("y" in m) ? m.y : m,
                        name  : d.name,
                        color : ("color" in d) ? d.color : (opt.color[i] || "black")
                    };
                });
            });
        }

        var flattenData = [].concat.apply([], cData);
        var uniqueX = _.uniq(_.pluck(flattenData, "x"));

        if (uniqueX.length < opt.category.length) {
            opt.category = _.intersection(opt.category, uniqueX);
        }

        yExtent = d3.extent(flattenData.map(d => { return d.y; }));

        // extend the domain conditionly
        if (yExtent[0] >= 0) {
            yExtent[0] = 0;
            yExtent[1] *= 1.1;
        }
        else {
            yExtent[0] -= ((yExtent[1] - yExtent[0]) * 0.1);
            yExtent[1] += ((yExtent[1] - yExtent[0]) * 0.1);
        }

        lScale = d3.scaleLinear().domain(yExtent);
        oScale = d3.scalePoint().domain(opt.category).padding(0.1);

    } // normalise function END


    // unify initial chart status
    normalise();
    if (opt.isReverse) { herizontal(); } else { vertical(); }


    function getRoundedData (datum) {
        return {
            name: datum.name,
            x: datum.x,
            //y: numFix(datum.y)
            y: datum.y.toMeasureFormart(opt.rowMeasure.numberFormat)
        };
    }

    function onMouseMove () {
        // when brush is active do nothing
        if (d3.brushSelection(gBrush.node())) { return; }

        var mc, // mouse coordinate
            cn, // circle node
            pe, // circle node's parent node element
            dm; // circle node datum

        mc = d3.mouse(this);
        cn = gChart.selectAll("circle")
                .attr("r", 3)
                .nodes()
                .map(function (nd) {
                    var x = +d3.select(nd).attr("cx") - mc[0];
                    var y = +d3.select(nd).attr("cy") - mc[1];

                    return {
                        node: nd,
                        distance: sqrt((x * x) + (y * y))
                    };
                })
                .sort(function (a, b) {
                    return a.distance - b.distance;
                })[0].node;

        pe = cn.parentElement;
        dm = d3.select(cn).datum();

        d3.select(cn).attr("r", cMax);
        d3.select(pe).raise();

        var pText,
            rectBox = cn.getBoundingClientRect(),
            scrollTop = window.pageYOffset,
            scrollLeft = window.pageXOffset;

        try {
            pText = _.template(opt.tipFormat.output());
        } catch (err) {
            pText = _.template("Name: <span><%= name %></span><br/>" +
                               "X: <span><%= x %></span><br/>" +
                               "Y: <span><%= y %></span>");
        }

        tooltip = getTooltip();
        d3.select(tooltip)
            .html("")
            .style("display", "block")
            .append("p")
            .attr("class", "d3-ttip-content")
            .html(pText(getRoundedData(dm)));

        tooltip.style.position = "absolute";
        tooltip.style.left = scrollLeft + rectBox.left +
            0.5 * (rectBox.width - tooltip.offsetWidth) + "px";
        tooltip.style.top  = scrollTop + rectBox.top - tooltip.offsetHeight + 10 + "px";

    } // onMouseMove END

    function onMouseLeave () {
        if (d3.brushSelection(gBrush.node())) { return; }

        gChart.selectAll("circle").attr("r", cMin);
        d3.select(tooltip).style("display", "none");
    }

    function onPathMouseEnter () {
        d3.select(this).attr("stroke-width", lineWidth * 2);
    }

    function onPathMouseLeave () {
        d3.select(this).attr("stroke-width", lineWidth);
    }

    function onContextMenu (d, idx) {
        if (d3.brushSelection(gBrush.node())) {
            return onBrushContextMenu();
        }
        d3.event.preventDefault();
        d3.event.stopPropagation();

        contextMenu.setTargetNode(d)
                   .setCurrentMenu(menuOnSingleItem)
                   .show();
    }

    function onClick (d) {

        // if onselected is null just return
        if (!chart.onselected) { return; }

        var tag = this.tagName.toUpperCase();
        if (tag == "PATH") {
            if (cData.length < 2) {
                return;
            } else {
                chart.onselected([{ field: opt.annexField, value: [ d[0].name ] }]);
            }
        }
        else if (tag == "CIRCLE") {
            if (cData.length < 2) {
                chart.onselected([{
                    field: opt.colDimension,
                    value: [d.x]
                  }]);
            }
            else {
                chart.onselected([
                    { field: opt.colDimension, value: [d.x] },
                    { field: opt.annexField, value: [d.name] }
                ]);
            }
        }

    } // onClick END

    // Brush Events
    function brushGet (range) {
        return function (d) {
            var cc = d3.select(this),
                cx = +cc.attr("cx"),
                cy = +cc.attr("cy"),
                boo = (cx >= range[0][0] &&
                       cx <= range[1][0] &&
                       cy >= range[0][1] &&
                       cy <= range[1][1]);
            cc.attr("r", boo ? cMax : cMin);

            return boo;
        };
    }
    function brushFilter () {
        return (d3.event.button !== 1);
    }
    function onBrushStart () {
        gChart.selectAll("circle")
            .attr("r", cMin)
            .classed("d3-active", false);
        contextMenu.hide();
    }
    function onBrushMove () {
        var range = d3.event.selection ||
                    d3.brushSelection(gBrush.node());

        /* This brush filter based on chart structure and
           circle's physical position on canvas.
           ** WARNNING **:
           Any structure changes on "gBrush" and "gChart",
           this brush filter should be modified accordingly.
        */
        gChart.selectAll("circle")
            .classed("d3-active", brushGet(range));
    }
    function onBrushEnd () {
        var range, circles, arr = [], xVal, names;

        range = d3.event.selection;
        if (!range) { return; } // null

        circles = gChart.selectAll("circle")
            .filter(brushGet(range));

        // if selection is empty or "onselected" is not avaiable, quit.
        if (circles.empty() || !chart.onselected) { return; }

        // Get all data from circles
        circles.each(function (d) { arr.push(d); });
        xVal  = _.chain(arr).pluck("x").uniq().value();
        names = _.chain(arr).pluck("name").uniq().value();


        if (cData.length < 2) { // single line
            chart.onselected([{ field: opt.colDimension, value: xVal }]);
        }
        else {
            chart.onselected([
                { field: opt.colDimension, value: xVal },
                { field: opt.annexField, value: names }
            ]);
        }
    } // onBrushEnd END
    function onBrushContextMenu () {
        d3.event.preventDefault();
        onBrushMove();

        var nodes = gChart.selectAll(".d3-active").nodes();
        if (nodes.length === 0) { return; }

        contextMenu.setTargetNode(nodes)
                   .setCurrentMenu(menuOnBrush)
                   .show();
    }
    function onBlankBrushContextMenu () {
        d3.event.preventDefault();
        contextMenu.setCurrentMenu(menuOnBlank).show();
    }



    chart.curveType = function (type) {
        curveType = type;

        line.curve(type);
        area.curve(type);

        gChart.selectAll("path.line").attr("d", line);
        gChart.selectAll("path.area").attr("d", area);

        return this;
    };

    chart.setColor = function (arr) {
        opt.color = arr;

        _.each(cData, function (group, ii) {
            _.each(group, function (obj) {
                obj.color = arr[ii] || obj.color || "black";
            });
        });

        normalise();
        if (this.status == "vertical") { vertical(); }
        else { herizontal(); }

        return this;
    };

    chart.reverse = function () {

        normalise();
        opt.size.width  = opt.size._w;
        opt.size.height = opt.size._h;

        switch (chart.status) {
            case "vertical": herizontal(); break;
            case "herizontal": vertical(); break;
        }
    };

    chart.delete = function (points) {
        var newData = {}, upload = [];

        _.each(cData, (arr) => { newData[arr[0].name] = arr; });
        points.forEach(function (point) {
            var idx;
            idx = newData[point.name].findIndex((item) => {
                return item.x == point.x;
            });
            if (idx > -1) {
                upload.push(newData[point.name].splice(idx, 1)[0]);
            }
        });

        // filter out empty array(no point in this line and save a legend items).
        cData = _.filter(cData, arr => { return arr.length; });

        normalise();
        switch (chart.status) {
            case "vertical": vertical(); break;
            case "herizontal": herizontal(); break;
        }

        if (this.onRemoveItem && upload.length) {
            this.onRemoveItem([
                { field: opt.colDimension, value: upload.map(d => { return d.x; }) },
                { field: opt.annexField, value: upload.map(d => { return d.name; }) }
            ]);
        }
    };

    chart.setSize = function (size) {
        if (!arguments.length) { return opt.size; }
        opt.size.width  = size.width;
        opt.size.height = size.height;

        svg.attr("width", size.width).attr("height", size.height);
        normalise();
        if (this.status == "vertical") { vertical(); }
        else { herizontal(); }
    };

    chart.tipFormat = function (s) {
        if (!arguments.length) { return opt.tipFormat; }
        opt.tipFormat.input(s);
        return this;
    };

    chart.getLegends = getLegends;

    chart.getScale = function () {
        return lineWidth / maxLineWidth;
    };

    chart.setScale = function (val) {
        if (!arguments.length) { return; }
        if (typeof val === "string") { val = +val; }
        if (val > 1 || val < 0) { return; }

        lineWidth = val * maxLineWidth;
        gChart.selectAll(".line")
            .transition()
            .attr("stroke-width", lineWidth);
    };

    chart.export = function (type) {
        d3.util.exportChart(svg.node(), type || "png");
    };

    chart.removeChart = function () {
        svg.remove();
        if (tooltip) { d3.select(tooltip).remove(); }
        contextMenu.remove();

        tooltip      = null;
        contextMenu  = null;
        container    = null;
        opt          = null;

        for (var pp in chart) { delete chart[pp]; }
        chart = null;
    };


    return chart;

} // lineArea END


    module.exports = {
        build: buildChart
    };

});
