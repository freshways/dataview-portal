﻿
const chart = function (myChart, options) {
  const opt = {
    tipFormat: null,
    MeasureMap: null,
    tooltip: {
      trigger: 'item',
      axisPointer: {
        type: 'shadow'
      },
      formatter: function (args) {
        return opt.colDimension.name + "：" + args.name + "<br/>" + opt.rowMeasure.name + "：" + Number(args.value).toMeasureFormart(opt.rowMeasure.numberFormat);
      }
    }
  };
  _.extend(opt, options);

  const data = [];
  opt.data.sourceData.forEach(function (row) {
    var key = row[opt.colDimension.name];
    var value = row[opt.rowMeasure.name];
    data.push({
      name: key,
      value: value
    })
  });

  //https://github.com/ecomfe/echarts-wordcloud
  let chart_opt = {
    series: [{
      type: 'wordCloud',
      left: 'center',
      top: 'center',
      width: '100%',
      height: '100%',
      right: null,
      bottom: null,
      rotationRange: [-90, 90],   //旋转范围；字符倾斜的角度
      rotationStep: 45,
      gridSize: 8,    //网格尺寸；字符之间的间距(int)
      shape: 'circle',     // Shape can be 'circle', 'cardioid'==苹果OR心, 'diamond'==棱形, 'triangle-forward'==三角形, 'triangle'==三角形, 'pentagon'==五边形, 'star'==五角星
      drawOutOfBound: false, //字体大于画布时，是否呈现;true=呈现
      layoutAnimation: true,
      // Global text style
      textStyle: {
        fontFamily: 'sans-serif',
        fontWeight: 'bold',
        color: () => options.config.MultiColors[Math.floor(Math.random() * options.config.MultiColors.length)]
      },
      emphasis: {
        focus: 'self',
        textStyle: {
          shadowBlur: 10,
          shadowColor: options.config.GlobalColor
        }
      },
      data: data
    }]
  };

  const setOption = function (opt) {
    chart_opt = $.extend(chart_opt, opt);
    myChart.setOption(chart_opt);
  };

  const self = this;
  myChart.onselected = null;
  myChart.onDeleteFilter = null;

  if (opt) {
    setOption(opt);
  }

  myChart.on('click', function () {
    console.log(arguments);
    if (self.onselected) {
      //当前维度
      self.onselected([{
        field: opt.colDimension,
        value: [arguments[0].name]
      }]);
    }
  });

  this.setOption = setOption;
  this.removeChart = function () {
    myChart.dispose();
  }

  this.resize = function () {
    myChart.resize()
  };

  //return myChart;
};
/**
 * 字符云
 */
export const buildChart = function (myChart, data, columnFields, rowFields, options) {
  const cOpt = {};
  const fields = columnFields.concat(rowFields);
  let config = options.chartConfig;
  cOpt.config = config;
  cOpt.color = config.MultiColors
  var dimension = (columnFields.length > 0 && columnFields[0].slaveType === 0) ? columnFields[0] : rowFields[0];
  var measure = (rowFields.length > 0 && rowFields[0].slaveType === 1) ? rowFields[0] : columnFields[0];
  cOpt.colDimension = dimension;
  cOpt.rowMeasure = measure;
  const measureFields = fields.filter(t => t.slaveType === 1);
  const measureMap = {};
  measureFields.forEach(f => {
    measureMap[f.name] = f;
  });
  cOpt.MeasureMap = measureMap;

  //排序
  data = Enumerable.from(data).orderByDescending(function (d) {
    return d[cOpt.rowMeasure.name];
  }).toArray();
  var sourceData = data.concat();//原始数据
  cOpt.data = {sourceData: sourceData};

  const chartInstance = new chart(myChart, cOpt);
  return chartInstance;
};



