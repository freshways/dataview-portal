﻿import {colorHelper} from "@/utils/colorHelper";
import {FloatMenu} from "@/components/contro/floatmenu";
import {buildChart as smtBuildChart} from "@/components/chart/buildChart/echart-trend";


/**
 * 条形图
 */
export const buildChart = function (myChart, data, columnFields, rowFields, options) {
  if (options.isYoy) {
    return smtBuildChart(myChart, data, columnFields, rowFields, options);
  }
  //一级维度
  let dimensionField = columnFields.find(d => d.slaveType === 0);
  //度量
  let measureFields = rowFields.filter(d => d.slaveType === 1);
  let measureField = measureFields[0];
  //条形图呈现方向判定
  let direction = "vertical";
  if (!dimensionField) {
    direction = "horizon";
    dimensionField = rowFields.find(d => d.slaveType === 0);
    measureFields = columnFields.filter(d => d.slaveType === 1);
    measureField = measureFields[0];
  }
  if (!dimensionField || !measureField) {
    return false;
  }
  //二号度量
  const measureField2 = measureFields[1];
  const fields_inside = columnFields.concat(rowFields).filter(d => d.inside);
  const config = options.chartConfig;
  if (dimensionField.dataType == 4) {
    data = _.sortBy(data, d => dimensionField.name)
  }
  //提取一级维度
  const categories = Enumerable.from(data).select(function (d) {
    return d[dimensionField.name] + "|" + fields_inside.map(function (f) {
      return d[f.name];
    }).join("|");
  }).distinct().toArray();

  //轴文字样式
  let axis_nameTextStyle = {
    color: config.axisStyle.fontColor,
    fontSize: config.axisStyle.fontSize,
    fontFamily: config.axisStyle.fontFamily,
  }
  //数据条标签配置
  let labelOption = {
    show: config.textStyle["display"] !== "none",
    distance: 5,
    align: "left",
    verticalAlign: "middle",
    fontSize: config.textStyle["font-size"],
    fontFamily: config.textStyle["font-family"],
    color: config.textStyle["fill"],
    rotate: config.labelRotate
  }

  //图表间距
  let padding = [30, 10, 15, 10];
  if (config.chartPadding && config.chartPadding != '') {
    var arrayPadding = config.chartPadding.split(' ');
    if (arrayPadding.length >= 4) {
      padding = arrayPadding.map(function (d) {
        return parseInt(d);
      });
    }
  }

  const option_chart = {
    color: config.GlobalColor,
    toolbox: {
      show: config.showToolbox == undefined ? true : config.showToolbox,
      //orient: 'vertical',
      //top: 'center',
      right: 10,
      feature: {
        mark: {show: true}
      },
      iconStyle: {
        borderColor: config.axisStyle.fontColor,
        textPosition: "left"
      }
    },
    tooltip: {
      trigger: 'item',
      axisPointer: {
        type: 'cross',
        label: {
          color: colorHelper.invert(axis_nameTextStyle.color),
          backgroundColor: axis_nameTextStyle.color
        }
      }
    },
    legend: {
      data: [],
      textStyle: axis_nameTextStyle,
      type: 'scroll',
      pageIconColor: config.axisStyle.fontColor,
      pageTextStyle: axis_nameTextStyle,
      show: config.showLegend == undefined || config.showLegend,
      itemWidth: 14,
      itemHeight: 14
    },
    calculable: true,
    grid: [{
      top: (config.showLegend == undefined || config.showLegend) ? 20 + padding[0] : padding[0],
      left: padding[3],
      bottom: padding[2],
      right: padding[1],
      containLabel: true
    }],
    series: [],
    others: {
      dimensionField: dimensionField,
      measureField: measureField,
      measureField2: measureField2,
      fields_inside: fields_inside,
      categories: categories,
      data: data,
      direction: direction,
      valley: config.valley,
      peak: config.peak,
      labelOption: labelOption,
      config: config,
      datamarking: options.dataMarking
    }
  };

  config.legendPosition = config.legendPosition || "top";
  if (config.legendPosition == "left") {
    option_chart.legend.x = "left";
  } else if (config.legendPosition == "right") {
    option_chart.legend.x = "right";
    option_chart.toolbox.left = 20;
  }
  //轴线配置
  var axis_axisLine = {
    lineStyle: {
      color: config.axisStyle.tickColor
    },
    show: config.showAxisLine == undefined ? true : config.showAxisLine
  }

  if (direction === "vertical") {
    if (!config.labelPosition || config.labelPosition == "foot") {
      labelOption.position = "insideBottomLeft";
    } else if (config.labelPosition == "head") {
      labelOption.position = "insideTopLeft";
    }
    if (!option_chart.legend.show) {
      option_chart.toolbox.right = "center";
    }
    option_chart.xAxis = [{
      type: 'category',
      axisTick: {show: false},
      data: categories,
      name: config.xAxisLabelRotate ? "" : dimensionField.name,
      nameTextStyle: axis_nameTextStyle,
      axisLine: axis_axisLine,
      axisLabel: {
        color: axis_nameTextStyle.color,
        fontSize: axis_nameTextStyle.fontSize,
        fontFamily: axis_nameTextStyle.fontFamily,
        interval: 0,
        fontWeight: "lighter",
        rotate: config.xAxisLabelRotate != undefined ? config.xAxisLabelRotate : 0,
        show: config.showAxisLabel ? config.showAxisLabel.category : true
      },
      nameLocation: "middle",
      nameGap: config.axisStyle.fontSize * 1.8,
      gridIndex: 0
    }];
    if (config.xAxisName) {
      if (config.xAxisName.show) {
        if (config.xAxisName.name) {
          option_chart.xAxis[0].name = config.xAxisName.name;
        }
      } else {
        option_chart.xAxis[0].name = "";
      }
    }
    option_chart.yAxis = [{
      type: 'value',
      name: measureField.name,
      nameTextStyle: axis_nameTextStyle,
      axisLine: axis_axisLine,
      axisTick: axis_axisLine,
      axisLabel: {
        color: axis_nameTextStyle.color,
        fontSize: axis_nameTextStyle.fontSize,
        fontFamily: axis_nameTextStyle.fontFamily,
        show: config.showAxisLabel ? config.showAxisLabel.value : true
      },
      splitLine: {
        show: config.showSplitLine && (config.splitLineMode == undefined || config.splitLineMode == "all" || config.splitLineMode == "left"),
        lineStyle: {
          color: config.axisStyle.tickColor
        }
      },
      gridIndex: 0
    }];
    if (config.yAxisName) {
      if (config.yAxisName.show) {
        if (config.yAxisName.name) {
          option_chart.yAxis[0].name = config.yAxisName.name;
        }
      } else {
        option_chart.yAxis[0].name = "";
      }
    }
    if (config.valueRange) {
      if (!isNaN(Number(config.valueRange.min)) && config.valueRange.min != null && config.valueRange.min.length) {
        option_chart.yAxis[0].min = Number(config.valueRange.min);
      }
      if (!isNaN(Number(config.valueRange.max)) && config.valueRange.max != null && config.valueRange.max.length) {
        option_chart.yAxis[0].max = Number(config.valueRange.max);
      }
    }
    if (measureField2) {
      option_chart.yAxis.push({
        type: 'value',
        name: measureField2.name,
        nameTextStyle: axis_nameTextStyle,
        axisLine: axis_axisLine,
        axisTick: axis_axisLine,
        splitLine: {
          show: config.showSplitLine && (config.splitLineMode == undefined || config.splitLineMode == "all" || config.splitLineMode == "right"),
          lineStyle: {
            color: config.axisStyle.tickColor
          }
        },
        axisLabel: {
          color: axis_nameTextStyle.color,
          fontSize: axis_nameTextStyle.fontSize,
          fontFamily: axis_nameTextStyle.fontFamily,
          show: config.showAxisLabel ? config.showAxisLabel.value2 : true
        },
        gridIndex: 0
      });
      if (config.yAxisName2) {
        if (config.yAxisName2.show) {
          if (config.yAxisName2.name) {
            option_chart.yAxis[1].name = config.yAxisName2.name;
          }
        } else {
          option_chart.yAxis[1].name = "";
        }
      }
      if (config.valueRange2) {
        if (!isNaN(Number(config.valueRange2.min)) && config.valueRange2.min != null && config.valueRange2.min.length) {
          option_chart.yAxis[1].min = Number(config.valueRange2.min);
        }
        if (!isNaN(Number(config.valueRange2.max)) && config.valueRange2.max != null && config.valueRange2.max.length) {
          option_chart.yAxis[1].max = Number(config.valueRange2.max);
        }
      }
    }
  } else {
    if (!config.labelPosition || config.labelPosition == "foot") {
      labelOption.position = "insideLeft";
    } else {
      labelOption.position = "insideRight";
      labelOption.align = "right";
    }
    option_chart.xAxis = [{
      type: 'value',
      name: measureField.name,
      nameTextStyle: axis_nameTextStyle,
      axisLine: axis_axisLine,
      axisTick: axis_axisLine,
      axisLabel: {
        color: axis_nameTextStyle.color,
        fontSize: axis_nameTextStyle.fontSize,
        fontFamily: axis_nameTextStyle.fontFamily,
        show: config.showAxisLabel ? config.showAxisLabel.value : true
      },
      nameLocation: "middle",
      nameGap: config.axisStyle.fontSize * 1.8,
      splitLine: {
        show: config.showSplitLine && (config.splitLineMode == undefined || config.splitLineMode == "all" || config.splitLineMode == "left"),
        lineStyle: {
          color: config.axisStyle.tickColor
        }
      },
      gridIndex: 0
    }];
    if (config.yAxisName) {
      if (config.yAxisName.show) {
        if (config.yAxisName.name) {
          option_chart.xAxis[0].name = config.yAxisName.name;
        }
      } else {
        option_chart.xAxis[0].name = "";
      }
    }
    if (config.valueRange) {
      if (!isNaN(Number(config.valueRange.min)) && config.valueRange.min != null && config.valueRange.min.length) {
        option_chart.xAxis[0].min = Number(config.valueRange.min);
      }
      if (!isNaN(Number(config.valueRange.max)) && config.valueRange.max != null && config.valueRange.max.length) {
        option_chart.xAxis[0].max = Number(config.valueRange.max);
      }
    }
    option_chart.yAxis = [{
      type: 'category',
      axisTick: {show: false},
      data: categories,
      name: config.xAxisLabelRotate ? "" : dimensionField.name,
      nameTextStyle: axis_nameTextStyle,
      axisLine: axis_axisLine,
      axisLabel: {
        color: axis_nameTextStyle.color,
        fontSize: axis_nameTextStyle.fontSize,
        fontFamily: axis_nameTextStyle.fontFamily,
        interval: 0,
        fontWeight: "lighter",
        rotate: config.xAxisLabelRotate != undefined ? config.xAxisLabelRotate : 0,
        show: config.showAxisLabel ? config.showAxisLabel.category : true
      },
      nameLocation: "start",
      nameGap: config.axisStyle.fontSize * 1.8,
      inverse: true,
      gridIndex: 0
    }];
    if (config.xAxisName) {
      if (config.xAxisName.show) {
        if (config.xAxisName.name) {
          option_chart.yAxis[0].name = config.xAxisName.name;
        }
      } else {
        option_chart.yAxis[0].name = "";
      }
    }
    if (measureField2) {
      option_chart.xAxis.push({
        type: 'value',
        name: measureField2.name,
        nameTextStyle: axis_nameTextStyle,
        axisLine: axis_axisLine,
        axisTick: axis_axisLine,
        splitLine: {
          show: config.showSplitLine && (config.splitLineMode == undefined || config.splitLineMode == "all" || config.splitLineMode == "right"),
          lineStyle: {
            color: config.axisStyle.tickColor
          }
        },
        axisLabel: {
          color: axis_nameTextStyle.color,
          fontSize: axis_nameTextStyle.fontSize,
          fontFamily: axis_nameTextStyle.fontFamily,
          show: config.showAxisLabel ? config.showAxisLabel.value2 : true
        },
        nameLocation: "middle",
        nameGap: config.axisStyle.fontSize * 1.8,
        gridIndex: 0
      });
      if (config.yAxisName2) {
        if (config.yAxisName2.show) {
          if (config.yAxisName2.name) {
            option_chart.xAxis[1].name = config.yAxisName2.name;
          }
        } else {
          option_chart.xAxis[1].name = "";
        }
      }
      if (config.valueRange2) {
        if (!isNaN(Number(config.valueRange2.min)) && config.valueRange2.min != null && config.valueRange2.min.length) {
          option_chart.xAxis[1].min = Number(config.valueRange2.min);
        }
        if (!isNaN(Number(config.valueRange2.max)) && config.valueRange2.max != null && config.valueRange2.max.length) {
          option_chart.xAxis[1].max = Number(config.valueRange2.max);
        }
      }
    }
  }
  const series = {
    name: measureField.name,
    type: "bar",
    data: data.map(d => {
      return d[measureField.name];
    }),
    label: labelOption,
    itemStyle: {
      // 定制显示（按顺序）
      color: function (params) {
        //console.log(params);
        let color = config.MultiColors[params.seriesIndex];
        var c_data = params.data;
        if (params.seriesIndex === 0) {
          if (config.valley && config.valley.enable && c_data < config.valley.value) {
            color = config.valley.color;
          } else if (config.peak && config.peak.enable && c_data > config.peak.value) {
            color = config.peak.color;
          }
        }
        if (config.barFillStyle && Number(config.barFillStyle.mode) === 1) {
          return {
            type: 'linear',
            x: config.barFillStyle.gradientDirection === 'left' ? 1 : 0,
            y: config.barFillStyle.gradientDirection === 'top' ? 1 : 0,
            x2: config.barFillStyle.gradientDirection === 'right' ? 1 : 0,
            y2: config.barFillStyle.gradientDirection === 'bottom' ? 1 : 0,
            colorStops: [{
              offset: 0, color: config.barFillStyle.gradientBeginColor // 0% 处的颜色
            }, {
              offset: 1, color: color // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
        return color;
      }
    },
    emphasis: {
      itemStyle: {
        opacity: 0.9,
        borderType: "dashed",
        borderColor: config.axisStyle.tickColor
      }
    },
    barMaxWidth: config.scale * 100 + "%"
  };

  option_chart.series.push(series);
  option_chart.legend.show = false;
  option_chart.grid[0].top = padding[0];
  if (config.barBorderRadius) {
    series.itemStyle.barBorderRadius = config.barBorderRadius;
  }
  if (measureField2) {
    series.itemStyle.color = config.MultiColors ? (config.MultiColors[0] || "#4E79A7") : "#4E79A7";
    option_chart.legend.data = [measureField.name, measureField2.name];

    option_chart.legend.show = config.showLegend == undefined ? true : config.showLegend;
    option_chart.grid[0].top = option_chart.legend.show ? 50 : 30;
    var serie2 = {
      type: "bar",
      name: measureField2.name,
      data: data.map(function (d) {
        return d[measureField2.name];
      }),
      label: labelOption,
      itemStyle: {
        color: config.MultiColors ? (config.MultiColors[1] || "#A0CBE8") : "#A0CBE8"
      },
      emphasis: {
        itemStyle: {
          opacity: 0.9,
          borderType: "dashed",
          borderColor: config.axisStyle.tickColor
        }
      },
      barMaxWidth: config.scale * 100 + "%"
    }
    if (config.barFillStyle && Number(config.barFillStyle.mode) === 1) {
      serie2.itemStyle.color = {
        type: 'linear',
        x: config.barFillStyle.gradientDirection === 'left' ? 1 : 0,
        y: config.barFillStyle.gradientDirection === 'top' ? 1 : 0,
        x2: config.barFillStyle.gradientDirection === 'right' ? 1 : 0,
        y2: config.barFillStyle.gradientDirection === 'bottom' ? 1 : 0,
        colorStops: [{
          offset: 0, color: config.barFillStyle.gradientBeginColor // 0% 处的颜色
        }, {
          offset: 1, color: config.MultiColors[1] // 100% 处的颜色
        }],
        global: false // 缺省为 false
      }
    }
    if (direction === "vertical") {
      serie2.yAxisIndex = 1;
    } else {
      serie2.xAxisIndex = 1;
    }
    option_chart.series.push(serie2);
    if (config.barBorderRadius) {
      serie2.itemStyle.barBorderRadius = config.barBorderRadius;
    }

  }
  option_chart.others.carousel = config.carousel;
  return new chart(myChart, option_chart);
}

const chart = function (myChart, opt) {
  var _calldata;
  var _mData = opt.others.data;
  var fields_inside = opt.others.fields_inside;
  var config = opt.others.config;
  var labelOption = opt.others.labelOption;
  var datamarkingopt = opt.others.datamarking;
  var datamarkingmapping = {}
  delete opt.others.config;
  delete opt.others.labelOption;
  delete opt.others.data;
  delete opt.others.fields_inside;
  delete opt.others.datamarking;
  //处理数据脱敏
  if (datamarkingopt) {
    datamarkingopt.dimensions.forEach(function (dko) {
      if (dko.DataMarkingEnable) {
        datamarkingmapping[dko.name] = {
          mapping: {}
        }
        opt.others.categories.forEach(function (d) {
          var k = d.split("|")[0];
          datamarkingmapping[dko.name].mapping[k] = k.marking();
        });
      }
    });
  }
  const self = this;
  var chart_opt = {
    brush: {
      toolbox: ['rect', 'clear'],
      throttleType: "debounce",
      throttleDelay: 500
    },
    tooltip: {
      formatter: function (args) {
        var sps = args.name.split("|");
        var nformat = chart_opt.others.measureField.numberFormat;
        if (chart_opt.others.measureField2 && chart_opt.others.measureField2.name == args.seriesName) {
          nformat = chart_opt.others.measureField2.numberFormat;
        }
        var result = chart_opt.others.dimensionField.name + ":" + sps[0] + "<br/>" + args.seriesName + ":" + Number(args.value).toMeasureFormart(nformat);
        if (fields_inside.length) {
          fields_inside.forEach(function (d, i) {
            result += "<br/>" + d.name + ":" + sps[i + 1];
          });
        }
        return result;
      }
    },
    axisPointer: {
      label: {
        formatter: function (args) {
          var result = args.value;
          if (args.seriesData.length) {
            if (!fields_inside.length) {
              result = result.replace("|", "");
            }
          } else {
            result = Number(args.value).toMeasureFormart(chart_opt.others.measureField.numberFormat);
          }
          return result;
        }
      }
    }
  }

  var labelFormatter = function (args) {
    var nformat = chart_opt.others.measureField.numberFormat;
    if (chart_opt.others.measureField2 && chart_opt.others.measureField2.name == args.seriesName) {
      nformat = chart_opt.others.measureField2.numberFormat;
    }
    return args.value.toMeasureFormart(nformat);
  }
  var valueAxisLabelFormatter = function (value) {
    return Number(value).toMeasureFormart(chart_opt.others.measureField.numberFormat);
  }
  var valueAxisLabelFormatter2 = function (value) {
    return Number(value).toMeasureFormart(chart_opt.others.measureField2.numberFormat);
  }
  var axisPointerLabelFormatter = function (arg) {
    //{value: 5988928.373206086, seriesData: Array(0)}
    return Number(arg.value).toMeasureFormart(chart_opt.others.measureField.numberFormat);
  }
  var axisPointerLabelFormatter2 = function (arg) {
    //{value: 5988928.373206086, seriesData: Array(0)}
    return Number(arg.value).toMeasureFormart(chart_opt.others.measureField2.numberFormat);
  }
  var asixLabelFormatter = function (value) {
    var result = value.split("|")[0];
    if (datamarkingopt && datamarkingmapping[opt.others.dimensionField.name]) {
      result = datamarkingmapping[opt.others.dimensionField.name].mapping[result];
    }
    return result;
  }

  var colorDyna = function (params) {
    var color = chart_opt.others.measureField2 ? config.MultiColors[0] : config.GlobalColor;
    var c_data = params.data;
    if (chart_opt.others.valley && chart_opt.others.valley.enable && c_data < chart_opt.others.valley.value) {
      color = chart_opt.others.valley.color;
    }
    else if (chart_opt.others.peak && chart_opt.others.peak.enable && c_data > chart_opt.others.peak.value) {
      color = chart_opt.others.peak.color;
    }
    if (config.barFillStyle && Number(config.barFillStyle.mode) === 1) {
      var result = {
        type: 'linear',
        x: config.barFillStyle.gradientDirection === 'left' ? 1 : 0,
        y: config.barFillStyle.gradientDirection === 'top' ? 1 : 0,
        x2: config.barFillStyle.gradientDirection === 'right' ? 1 : 0,
        y2: config.barFillStyle.gradientDirection === 'bottom' ? 1 : 0,
        colorStops: [{
          offset: 0, color: config.barFillStyle.gradientBeginColor // 0% 处的颜色
        }, {
          offset: 1, color: color // 100% 处的颜色
        }],
        global: false // 缺省为 false
      }
      return result;
    } else {
      return color;
    }
  }

  const setOption = (opt, ext) => {
    if (ext) {
      chart_opt = $.extend(true, chart_opt, opt);
    } else {
      chart_opt = opt;
    }
    chart_opt.series[0].label.formatter = labelFormatter;
    chart_opt.series[0].itemStyle.color = colorDyna;
    if (chart_opt.others.direction == "vertical") {
      chart_opt.xAxis[0].axisLabel.formatter = asixLabelFormatter;
      chart_opt.yAxis[0].axisPointer = {
        label: {
          formatter: axisPointerLabelFormatter
        }
      }
      chart_opt.yAxis[0].axisLabel.formatter = valueAxisLabelFormatter
    } else {
      chart_opt.yAxis[0].axisLabel.formatter = asixLabelFormatter;
      chart_opt.xAxis[0].axisPointer = {
        label: {
          formatter: axisPointerLabelFormatter
        }
      }
      chart_opt.xAxis[0].axisLabel.formatter = valueAxisLabelFormatter;
    }
    if (chart_opt.series.length > 1) {
      chart_opt.series[1].label.formatter = labelFormatter;
      if (chart_opt.others.direction == "vertical") {
        chart_opt.yAxis[1].axisPointer = {
          label: {
            formatter: axisPointerLabelFormatter2
          }
        }
        chart_opt.yAxis[1].axisLabel.formatter = valueAxisLabelFormatter2;
      } else {
        chart_opt.xAxis[1].axisPointer = {
          label: {
            formatter: axisPointerLabelFormatter2
          }
        }
        chart_opt.xAxis[1].axisLabel.formatter = valueAxisLabelFormatter2;
      }
    }
    myChart.setOption(chart_opt);
  };
  //响应点击事件
  myChart.on("click", function (args) {
    if (args.componentType === "series" && args.name) {
      const callData = [{
        field: chart_opt.others.dimensionField,
        value: [args.name.split("|")[0]]
      }];
      if (self.onselected) {
        self.onselected(callData);
      }
    }
  });
  //响应右击事件
  myChart.on("contextmenu", 'series', args => {
    myChart.getDom().oncontextmenu = () => {
      return false
    };
    FloatMenu.show("echart-menus", [{
      label: "保存为图片",
      onclick: function () {
        const url = myChart.getDataURL({
          type: 'png'
        });
        const a = document.createElement("a");
        a.href = url;
        a.download = name;
        a.click();
        document.body.removeChild(a);
      }
    }]);
  });
  //响应框选事件
  myChart.on('brushSelected', args => {
    if (args.batch.length && args.batch[0].selected[0].dataIndex && args.batch[0].selected[0].dataIndex.length > 0) {
      let selectdIndex = args.batch[0].selected[0].dataIndex;
      const categories = [];
      if (selectdIndex) {
        selectdIndex.forEach(t => {
          categories.push(chart_opt.others.categories[t].toString().split("|")[0]);
        })
        const callData = [{
          field: chart_opt.others.dimensionField,
          value: categories
        }];
        _calldata = callData;
        if (self.onselected) {
          self.onselected(callData);
        }
      }
    }
  });

  //轮播的定时器
  let timer;
  //轮播开关
  let state = true;
  //轮播方法
  const carousel = () => {
    let index = 0;
    if (timer) {
      clearInterval(timer)
    }
    if (opt.others.carousel && opt.others.carousel.enable) {
      timer = setInterval(() => {
        if (!state) {
          return false
        }
        index = index + 1 >= chart_opt.series[0].data.length ? 0 : index + 1
        myChart.dispatchAction({
          type: 'downplay',
          seriesIndex: 0,
        })
        myChart.dispatchAction({
          type: 'highlight',
          seriesIndex: 0,
          dataIndex: index
        })
        myChart.dispatchAction({
          type: 'showTip',
          seriesIndex: 0,
          dataIndex: index
        });
      }, opt.others.carousel.speed)
    }
  }
  //鼠标移入，关闭轮播
  myChart.on('mouseover', () => {
    state = false
  })
  //鼠标移出时，开启轮播
  myChart.on('globalout', () => {
    state = true
  })

  if (opt) {
    setOption(opt, true);
    chart_opt.series.forEach(function (d) {
      d.label.formatter = labelFormatter;
    });
  }

  this.update = data => {
    _mData = data;
    var categories = data.map(d=> d[chart_opt.others.dimensionField.name])
    if (chart_opt.xAxis[0].type === "category") {
      chart_opt.xAxis[0].data = categories;
    } else {
      chart_opt.yAxis[0].data = categories;
    }
    chart_opt.series = [];
    var serie = {
      name: chart_opt.others.measureField.name,
      type: "bar",
      data: data.map(function (d) { return d[chart_opt.others.measureField.name]; }),
      label: labelOption,
      itemStyle: {

      },
      emphasis: {
        itemStyle: {
          opacity: 0.9,
          borderType: "dashed",
          borderColor: config.axisStyle.tickColor
        }
      },
      barMaxWidth: config.barMaxWidth > 0 ? config.barMaxWidth : (config.scale * 100 + "%")
    }
    if (config.barBorderRadius) {
      serie.itemStyle.barBorderRadius = config.barBorderRadius;
    }
    if (config.barFillStyle && Number(config.barFillStyle.mode) === 1) {
      serie.itemStyle.color = {
        type: 'linear',
        x: config.barFillStyle.gradientDirection === 'left' ? 1 : 0,
        y: config.barFillStyle.gradientDirection === 'top' ? 1 : 0,
        x2: config.barFillStyle.gradientDirection === 'right' ? 1 : 0,
        y2: config.barFillStyle.gradientDirection === 'bottom' ? 1 : 0,
        colorStops: [{
          offset: 0, color: config.barFillStyle.gradientBeginColor // 0% 处的颜色
        }, {
          offset: 1, color: chart_opt.others.measureField2 ? config.MultiColors[0] : config.GlobalColor // 100% 处的颜色
        }],
        global: false // 缺省为 false
      }
    }
    chart_opt.series.push(serie);
    if (chart_opt.others.measureField2) {
      var serie2 = {
        type: "bar",
        name: chart_opt.others.measureField2.name,
        data: data.map(function (d) { return d[chart_opt.others.measureField2.name]; }),
        label: labelOption,
        itemStyle: {
          color: config.MultiColors ? (config.MultiColors[1] || "#A0CBE8") : "#A0CBE8"
        },
        emphasis: {
          itemStyle: {
            opacity: 0.9,
            borderType: "dashed",
            borderColor: config.axisStyle.tickColor
          }
        },
        barMaxWidth: config.barMaxWidth > 0 ? config.barMaxWidth : (config.scale * 100 + "%")
      }
      if (config.barBorderRadius) {
        serie2.itemStyle.barBorderRadius = config.barBorderRadius;
      }
      if (config.barFillStyle && Number(config.barFillStyle.mode) === 1) {
        serie2.itemStyle.color = {
          type: 'linear',
          x: config.barFillStyle.gradientDirection === 'left' ? 1 : 0,
          y: config.barFillStyle.gradientDirection === 'top' ? 1 : 0,
          x2: config.barFillStyle.gradientDirection === 'right' ? 1 : 0,
          y2: config.barFillStyle.gradientDirection === 'bottom' ? 1 : 0,
          colorStops: [{
            offset: 0, color: config.barFillStyle.gradientBeginColor // 0% 处的颜色
          }, {
            offset: 1, color: config.MultiColors[1] // 100% 处的颜色
          }],
          global: false // 缺省为 false
        }
      }
      if (chart_opt.others.direction === "vertical") {
        serie2.yAxisIndex = 1;
      } else {
        serie2.xAxisIndex = 1;
      }
      chart_opt.series.push(serie2);

    }
    chart_opt.others.categories = categories;
    setOption(chart_opt);
  }
  this.removeChart = () => {
    if (timer) {
      clearInterval(timer)
    }
    if (!myChart.isDisposed()) {
      myChart.off('contextmenu');
      myChart.off('click');
      myChart.dispose()
    }
  };
  carousel()
}


