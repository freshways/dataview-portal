﻿import {FloatMenu} from "@/components/contro/floatmenu"

const chart = function (myChart, opt) {
  var _calldata;
  var _mData = opt.series[0].data;
  var datamarkingopt = opt.others.dataMarking;
  var datamarkingmapping = {}
  delete opt.others.data;
  delete opt.others.datamarking;
  //处理数据脱敏
  if (datamarkingopt) {
    datamarkingopt.dimensions.forEach(function (dko) {
      if (dko.DataMarkingEnable) {
        datamarkingmapping[dko.name] = true;
      }
    });
  }
  const self = this;
  let chart_opt = {
    tooltip: {
      formatter: function (args) {
        return opt.others.dimensionField.name + ":" + args.name + "<br/>" + opt.others.measureField.name + ":" + Number(args.value).toMeasureFormart(opt.others.measureField.numberFormat);
      }
    }
  };

  let labelFormatter = function (args) {
    let name = args.name;
    if (datamarkingopt && datamarkingmapping[opt.others.dimensionField.name]) {
      name = name.marking();
    }
    return opt.others.labelFormatter.replace(/\<|\%|\=|\>/g, "").replace("name", name).replace("value", args.value.toMeasureFormart(chart_opt.others.measureField.numberFormat)).replace("ratio", args.percent + "%");
  };

  const setOption = function (opt, ext) {
    if (ext) {
      chart_opt = $.extend(true, chart_opt, opt);
    } else {
      chart_opt = opt;
    }
    chart_opt.series[0].label.formatter = labelFormatter;
    myChart.setOption(chart_opt);
  };

  //响应点击事件
  myChart.on("click", function (args) {
    if (args.componentType === "series") {
      let calldata = [{
        field: chart_opt.others.dimensionField,
        value: [args.name]
      }];
      if (self.onselected) {
        self.onselected(calldata);
      }
    }
  });

  //响应右击事件
  myChart.on("contextmenu", 'series', args => {
    myChart.getDom().oncontextmenu = () => {
      return false
    };
    const callData = [{
      field: chart_opt.others.dimensionField,
      value: [args.name]
    }];
    FloatMenu.show("echart-menus", [{
      label: "保存为图片",
      onclick: () => {
        const url = myChart.getDataURL({
          type: 'png'
        });
        const a = document.createElement("a");
        a.href = url;
        a.download = name;
        a.click();
        document.body.removeChild(a);
      }
    }, {
      label: "删除选中项",
      onclick: () => {
        _.remove(chart_opt.series[0].data, item => item.name == callData[0].value[0]);
        setOption(chart_opt);
        //清除选框
        myChart.dispatchAction({
          type: "brush",
          areas: []
        });
        //触发删除事件
        if (self.onRemoveItem) {
          self.onRemoveItem(callData);
        }
      }
    }]);
  });

  if (opt) {
    setOption(opt, true);
    chart_opt.series.forEach(function (d) {
      d.label.formatter = labelFormatter;
    });
  }

  this.setOption = setOption;
  this.removeChart = function () {
    myChart.dispose();
  }
  this.update = function (data) {
    _mData = data;
    chart_opt.series[0].data = data.map(function (d) {
      return {
        value: d[chart_opt.others.measureField.name],
        name: d[chart_opt.others.dimensionField.name]
      };
    });
    //提取一级维度
    var categories = Enumerable.from(data).select(function (d) {
      return d[chart_opt.others.dimensionField.name];
    }).toArray();
    chart_opt.legend.data = categories;
    chart_opt.others.categories = categories;
    setOption(chart_opt, true);
  }
  this.resize = myChart.resize
};

//矩形树图
export const buildChart = function (myChart, data, columnFields, rowFields, options) {
  //一级维度
  let dimensionField = columnFields.find(function (d) {
    return d.slaveType === 0;
  });
  //度量
  let measureField = rowFields.find(function (d) {
    return d.slaveType === 1;
  });
  if (!dimensionField) {
    dimensionField = rowFields.find(function (d) {
      return d.slaveType === 0;
    });
    measureField = columnFields.find(function (d) {
      return d.slaveType === 1;
    });
  }
  if (!dimensionField || !measureField) {
    return false;
  }

  let config = options.chartConfig;

  //数据条标签配置
  const labelOption = {
    show: true,
    fontSize: config.textStyle["font-size"],
    fontFamily: config.textStyle["font-family"]
  };

  const series = {
    type: "treemap",
    data: [],
    name: dimensionField.name,
    label: labelOption
  };
  series.data = data.map(function (d) {
    return {
      value: d[measureField.name],
      name: d[dimensionField.name]
    };
  });

  const option = {
    tooltip: {
      trigger: 'item'
    },
    series: [],
    others: {
      dimensionField: dimensionField,
      measureField: measureField,
      data: data,
      labelFormatter: config.labelFormater || "<%=name%>",
      dataMarking: options.dataMarking
    }
  };
  option.series.push(series);
  const chartInstance = new chart(myChart, option);
  return chartInstance;
};




