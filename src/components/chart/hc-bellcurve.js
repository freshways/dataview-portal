/* global Highcharts, Hcharts, d3, undersore, _, require, define */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {
        var measure;

        if (!columnFields.length) { return false; }
        measure = columnFields.find(function (d) {
            return d.slaveType === 1;
        });
        if( !measure ){ return false; }
        measure = measure.name;

        var chart = Hcharts.bellcurve(svgSelector);
        (function () {
            var points, color,
                hConfig = options.chartConfig || {
                    GlobalColor: "#4E79A7",
                    axisStyle: {
                        tickColor : "#000",
                        fontColor : "#000",
                        fontFamily: "Arial",
                        fontSize  : 15
                    }
                };
            points = data.map(d => d[measure]);
            color = hConfig.GlobalColor || "black";

            if (options.size) {
                chart.config.chart = {
                    width: options.size.width,
                    height: options.size.height
                };
            }
            chart.config.yAxis = [];
            chart.config.yAxis[0] = chart.getAxisStyle( hConfig.axisStyle );
            chart.config.yAxis[0].allowDecimals = true;
            chart.config.xAxis = [ chart.getAxisStyle( hConfig.axisStyle ) ];


            chart.config.series = [{
                    id: "s0",
                    type: "bellcurve",
                    xAxis: 0,
                    yAxis: 0,
                    baseSeries: 's1',
                    zIndex: -1,
                    color: color,
                    fillOpacity: 0.1
                },
                {
                    data: points,
                    type: "scatter",
                    visible: false,
                    id: "s1",
                    marker: { radius: 1.5 }
            }];
        })();

        chart.init();
        return chart;
    };


    module.exports = {
        build: buildChart
    };

});
