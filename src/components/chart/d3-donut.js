﻿/**
 * donut chart
 */
/* global document, d3, undersore, _, require, define, window */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {

        var cOpt = {}; // chart options

        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }
        cOpt.colDimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
        cOpt.rowMeasure = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];

        (function () {

            var dKey = cOpt.colDimension.name,
                mKey = cOpt.rowMeasure.name;

            if (cOpt.colDimension.dataType === 4) { // 如果为时间（数值）升序排列
                data.sort(function (a, b) { return a[dKey] - b[dKey]; });
            }

            cOpt.values = data.map(function (d) { return d[mKey]; });
            cOpt.category = data.map(function (d) { return d[dKey]; });

        })();

        var config = options.chartConfig;
        if (!config) {
            config = {
                MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
                scale: 0.65,
                axisStyle: {
                    tickColor: "#000",
                    fontColor: "#000",
                    fontFamily: "Arial",
                    fontSize: 15
                }
            };
        }

        (function () {
            if (!config.MultiColors.length) {
                config.MultiColors = ["#4E79A7", "#A0CBE8", "#F28E2B"];
            }
            var colors = [];
            while (colors.length < cOpt.values.length) {
                colors = colors.concat(config.MultiColors);
            }
            cOpt.color = colors;
        })();


        if ("labelFormater" in config) {
            cOpt.labelFormater = config.labelFormater;
        }
        if ("scale" in config) {
            cOpt.scale = config.scale;
        }
        if ("axisStyle" in config) {
            cOpt.axisStyle = config.axisStyle;
        }
        if (options.size) {
            cOpt.size = options.size;
        }

        return donut(svgSelector, cOpt);
    };


    function donut(selector, options) {

        var opt = {
            //size    : { width: 300, height: 400 },
            category: [],
            values: [],

            scale: 0.8, // control outerRadius [0, 1]
            color: d3.schemeCategory20c,
            /* label格式输出模板
                "<%=name%>"
                "<%=name%>: <%=value%>"
                "<%=name%>: <%=ratio%>"
                "<%=name%>: <%=value%> [<%=ratio%>]"
            */
            labelFormater: "<%=name%>: <%=ratio%>",
            colDimension: null,
            rowMeasure: null,
            axisStyle: {}
        };
        _.extend(opt, options);


        var innerRadius,
            outerRadius,
            margin = { top: 10, right: 10, bottom: 10, left: 10 },
            minDim = 100,    // min required width or height dimension
            arc = d3.arc(),  // pei generator
            tArc = d3.arc(),  // generator while on transition
            lPie = d3.pie().sort(null).value(function (d) { return d.y; }),
            fx = d3.format(".1%"),
            //numFix = d3.format(",." + (opt.rowMeasure.NumberFixed || 0) + "f"),
            //legend = d3.util.hlegend(),
            contextMenu;

        var accessor = (function () {
            var sum = 1;
            var format = opt.rowMeasure.numberFormat;
            var template = "<%= name %>: <%= ratio %>";
            var parser = _.template(template);

            var fn = function (d, i) {
                return parser({
                    name: opt.category[i],
                    value: d.toMeasureFormart(format),
                    ratio: fx(d / sum)
                });
            };

            fn.sum = function (v) {
                if (!arguments.length) { return sum; }
                if (v > 0) { sum = v; }
                return fn;
            };
            fn.subtract = function (v) { sum -= v; return fn; };
            fn.template = function (v) {
                template = v;
                parser = _.template(template);
                return fn;
            };
            return fn;
        })();

        var cData = (function () {
            var randomColor = function () {
                return d3.schemeCategory20[Math.floor(Math.random() * 20)];
            };
            var sum = d3.sum(opt.values, function (d) { return (d > 0 ? d : 0); });
            accessor.sum(sum).template(opt.labelFormater);

            return _.chain(opt.values)
                .map(function (d, i) {
                    return {
                        x: opt.category[i],
                        y: d,
                        color: opt.color[i] || randomColor(),
                        label: accessor(d, i),
                        _index: i
                    };
                })
                .filter(function (d) { return d.y > 0; })
                .value();
        })();
        if (cData.length < 1) { return console.log("传入的数值全为负数，无法绘制饼图！"); }


        var menuOnSingleItem = [
            { title: "删除片区", symbol: "&#10008;", action: "delete" },
            { title: "降序排布", symbol: "&#8650;", action: "descend" },
            { title: "升序排布", symbol: "&#8648;", action: "ascend" },
            { title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg" },
            { title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png" }
        ];
        var menuOnBlank = menuOnSingleItem.slice(1);
        contextMenu = d3.util.getChartContextMenu();
        contextMenu.clickFun = function (command, target) {
            this.hide();
            switch (command.action) {
                case "delete": chart.delete(target); break;
                case "descend": chart.descend(); break;
                case "ascend": chart.ascend(); break;
                case "export": chart.export(command.p1); break;
                default: console.log("... Catch YOU! ...");
            }
        };


        var container = d3.select(selector).node().parentElement;
        if (!opt.size) {
            opt.size = d3.util.getInnerBoxDim(container);
        }

        var svg = d3.select(container)
            .html("")
            .append("svg")
            .attr("class", "Chart-To-Be-Export")
            .attr("width", opt.size.width)
            .attr("height", opt.size.height)
            .attr("version", "1.1")
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .style("font", "12px arial, sans-serif");
        svg.on("contextmenu", onBlankBrushContextMenu);



        var gPie, gLabel, data4pie, data4label;


        function ini_testing() {
            var descent, str, res;
            descent = function (prop) {
                return function (a, b) { return b[prop] - a[prop]; };
            };
            str = cData.map(function (d) { return d.label; })
                .sort(descent("length"));

            if (str.length > 10) { str = str.slice(0, 10); }
            var gg = svg.append("g")
                .attr("font-family", opt.axisStyle.fontFamily || "宋体")
                .attr("font-size", opt.axisStyle.fontSize || 14);

            gg.selectAll("text")
                .data(str)
                .enter()
                .append("text")
                .text(function (d) { return d; });

            res = gg.selectAll("text").nodes()
                .map(function (node) { return node.getBBox(); })
                .sort(descent("width"))[0];
            gg.remove();

            return res;
        }
        function ini_dimension_requisite(labelBox, offset) {
            var minWidth, minHeight,
                innerWidth, innerHeight, translate;

            minWidth = (minDim / 2 + offset + labelBox.width) * 2 + margin.left + margin.right;
            minHeight = (minDim / 2 + offset + labelBox.height) * 2 + margin.top + margin.bottom;

            if (opt.size.width < minWidth) { opt.size.width = minWidth; }
            if (opt.size.height < minHeight) { opt.size.height = minHeight; }

            innerWidth = opt.size.width - margin.left - margin.right - (labelBox.width + offset) * 2;
            innerHeight = opt.size.height - margin.top - margin.bottom - (labelBox.height + offset) * 2;

            outerRadius = Math.min(innerWidth, innerHeight) * 0.5;
            innerRadius = opt.scale ? opt.scale * outerRadius : 0;

            translate = [margin.left + labelBox.width + offset + innerWidth / 2,
            margin.top + labelBox.height + offset + innerHeight / 2];

            if (!gPie) {
                gPie = svg
                    .attr("width", opt.size.width)
                    .attr("height", opt.size.height)
                    .append("g")
                    .attr("class", "gMain gPie")
                    .attr("transform", "translate(" + translate + ")");

                gLabel = gPie.append("g")
                    .attr("class", "axis gLabel")
                    .attr("font-family", opt.axisStyle.fontFamily)
                    .attr("font-size", opt.axisStyle.fontSize)
                    .attr("fill", opt.axisStyle.fontColor);
            }
            else {
                svg.attr("width", opt.size.width)
                    .attr("height", opt.size.height);
                gPie.attr("transform", "translate(" + translate + ")");
            }

            return translate;
        }
        function ini_getSortedLabel(data4pie, fontBox, offset) {
            // 比率有变化
            _.each(cData, function (d) { d.label = accessor(d.y, d._index); });

            var data = _.chain(data4pie)
                .filter(function (p) {
                    return outerRadius * (p.endAngle - p.startAngle) * 0.9 > fontBox.height;
                })
                .map(function (p) {
                    var ang, source, target;
                    ang = p.startAngle + (p.endAngle - p.startAngle) * 0.5 - Math.PI / 2;
                    source = {
                        x: Math.cos(ang) * outerRadius,
                        y: Math.sin(ang) * outerRadius
                    };
                    target = {
                        x: Math.cos(ang) * (outerRadius + offset),
                        y: Math.sin(ang) * (outerRadius + offset)
                    };
                    return {
                        source: source,
                        target: target,
                        label: p.data.label,
                        _index: p.data._index
                    };
                })
                .value();

            function deCollisionUp(arr) {
                if (arr.length < 2) { return; }
                arr.sort(function (a, b) { return b.y - a.y; }); // decend

                var y = arr[0].y,
                    index = 1, node;

                while (index < arr.length) {
                    node = arr[index];
                    if (y < (node.y + fontBox.height)) {
                        node.y = y - fontBox.height - 5;
                    }
                    y = node.y;
                    index++;
                }
            }
            function deCollisionDown(arr) {
                if (arr.length < 2) { return; }
                arr.sort(function (a, b) { return a.y - b.y; }); // acend

                var y = arr[0].y,
                    index = 1, node;

                while (index < arr.length) {
                    node = arr[index];
                    if (y > (node.y - fontBox.height)) {
                        node.y = y + fontBox.height + 5;
                    }
                    y = node.y;
                    index++;
                }
            }

            // left|right --> top|down
            var labels = _.chain(data)
                .map(function (d) { return d.target; })
                .partition(function (d) { return d.x >= 0; }) // divided by X
                .map(function (arr) {                         // divided by Y
                    return _.partition(arr, function (d) { return d.y >= 0; });
                })
                .value();

            labels.forEach(function (arr) {
                deCollisionDown(arr[0]);
                deCollisionUp(arr[1]);
            });

            return data;
        }
        function initial() {
            var offset = 25, pieCenter;

            /* testing label width & height according to local style rules */
            var fontBox = ini_testing();


            // resize svg dimmension and locate gPie
            pieCenter = ini_dimension_requisite(fontBox, offset);


            /* Get data4pie */
            data4pie = lPie(cData);


            /* Get data4label according to outerRadius */
            data4label = ini_getSortedLabel(data4pie, fontBox, offset);
            data4label = _.filter(data4label, function (node) {
                var y = node.target.y;
                return y > (-pieCenter[1] + fontBox.height) &&
                    y < (opt.size.height - pieCenter[1]);
            });

            arc.innerRadius(innerRadius).outerRadius(outerRadius);
            tArc.innerRadius(innerRadius).outerRadius(outerRadius);
        }


        function dataKey(d) { return d._index; }

        function updatePie() {
            /* update/enter/exit */
            var path, line, text;

            path = gPie.selectAll("path")
                .data(data4pie, dataKey);
            path.enter()
                .append("path")
                .on("mouseenter", onMouseEnter)
                .on("mouseleave", onMouseLeave)
                .on("contextmenu", onContextMenu)
                .on("click", onClick)
                .style("cursor", "pointer")
                .merge(path)
                .each(function (d) {
                    d3.select(this)
                        .attr("fill", d.data.color)
                        .attr("d", arc)
                        .attr("stroke", "black")
                        .attr("stroke-width", 0)
                        .append("title")
                        .text(d.data.label);
                });
            path.exit().transition().attr("fill-opacity", 0).remove();


            line = gLabel.selectAll("line")
                .data(data4label, dataKey);
            line.enter()
                .append("line")
                .attr("stroke-wdith", 1)
                .attr("stroke", opt.axisStyle.tickColor)
                .merge(line)
                .attr("x1", d => d.source.x).attr("y1", d => d.source.y)
                .attr("x2", d => d.target.x).attr("y2", d => d.target.y);
            line.exit().transition().attr("stroke-opacity", 0).remove();


            text = gLabel.selectAll("text")
                .data(data4label, dataKey);
            text.enter()
                .append("text")
                .merge(text)
                .attr("text-anchor",
                    d => { return d.target.x >= 0 ? "start" : "end"; })
                .attr("alignment-baseline",
                    d => { return d.target.y >= 0 ? "hanging" : "baseline"; })
                .text(d => d.label)
                .attr("x", d => d.target.x)
                .attr("y", d => d.target.y);
            text.exit().transition().attr("fill-opacity", 0).remove();


            if (!data4label.length) {
                gPie.append("text")
                    .text("提示：您设置的字体高度太高或者图形尺寸太小！")
                    .attr("text-anchor", "middle")
                    .attr("fill-opacity", 1)
                    .attr("y", -(outerRadius + 10))
                    .transition()
                    .duration(3500)
                    .ease(d3.easeQuadOut)
                    .attr("fill-opacity", 0)
                    .remove();
            }

        } // updatePie END



        // EVENTS DEFINE
        function onMouseEnter(d) {
            d3.select(this)
                .transition()
                .ease(d3.easeExpInOut)
                .attrTween("d", function (j) {
                    var f = d3.interpolate(outerRadius, outerRadius * 1.1);
                    return function (t) { return arc.outerRadius(f(t))(j); };
                });
        }

        function onMouseLeave(d) {
            d3.select(this)
                .transition()
                .attrTween("d", function (j) {
                    var f = d3.interpolate(outerRadius * 1.1, outerRadius);
                    return function (t) { return arc.outerRadius(f(t))(j); };
                });
        }

        function onContextMenu(d) {
            d3.event.preventDefault();
            d3.event.stopPropagation();

            // delete pie slice accroding to its "_index"
            var index = d.data._index;

            contextMenu.setTargetNode(index)
                .setCurrentMenu(menuOnSingleItem)
                .show();
        }

        function onBlankBrushContextMenu() {
            d3.event.preventDefault();
            contextMenu.setCurrentMenu(menuOnBlank).show();
        }

        function onClick(d) {
            if (chart.onselected) {
                chart.onselected([{
                    field: opt.colDimension,
                    value: [d.data.x]
                }]);
                gPie.selectAll("path").attr("stroke-width", 0);
                d3.select(this).attr("stroke-width", 2);
            }
        }



        var chart = {
            svg: svg,
            onselected: null
        };



        // layout pie descendly
        chart.descend = function () {
            cData.sort(function (a, b) { return b.y - a.y; });
            initial();
            updatePie();
            return this;
        };
        chart.ascend = function () {
            cData.sort(function (a, b) { return a.y - b.y; });
            initial();
            updatePie();

            return this;
        };

        // delete one slice of pie according to its predefined index
        chart.delete = function (index) {
            if (cData.length < 2) { return alert("至少留1组！"); }
            var pickedItem, pickedIndex;
            pickedIndex = _.findIndex(cData, function (d) { return d._index === index; });
            pickedItem = cData.splice(pickedIndex, 1);
            accessor.subtract(pickedItem[0].y);

            initial();
            updatePie();

            if (this.onRemoveItem) {
                this.onRemoveItem([
                    { field: opt.colDimension, value: [pickedItem[0].x] }
                ]);
            }
        };

        chart.sequenceColor = function (arr) {
            cData.map(function (d, i) {
                d.color = arr[i] || "black";
            });
            initial();
            updatePie();

            return this;
        };

        // if width or height less than the "minDim"
        // ignore the new value and set the dimension to "minDim"
        chart.setSize = function (size) {
            if (!arguments.length) { return opt.size; }
            opt.size = size;

            initial();
            updatePie();

            return this;
        };

        // output legend name and its color attached
        chart.getLegends = function () {
            return cData.map(function (d) {
                return { name: d.x, color: d.color };
            });
        };

        chart.setScale = function (val) {
            if (!arguments.length) { return; }
            if (typeof val === "string") { val = +val; }
            if ((val > 1) || (val < 0)) { return; }

            innerRadius = outerRadius * val;
            arc.innerRadius(innerRadius);

            gPie.selectAll("path").attr("d", arc);
        };
        chart.getScale = function () {
            var maxRadius = opt.size.width * 0.5;
            return outerRadius / maxRadius;
        };

        chart.export = function (type) {
            d3.util.exportChart(svg.node(), type || "png");
        };

        chart.removeChart = function () {
            svg.remove();
            contextMenu.remove();

            contextMenu = null;
            container = null;
            opt = null;

            chart = null;
        };

        initial();
        updatePie();

        return chart;
    }


    module.exports = {
        build: buildChart
    };

});
