﻿/**
 * treemap chart
 */
import d3 from '@/components/d3-util'
import {Tooltip} from "@/components/contro/tooltip";


//预设颜色
var _precolor = ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"];

const buildChart = function (svgSelector, data, columnFields, rowFields, options) {
  var cOpt = {}; // chart options
  // 如果用户输入数据不足直接退出
  if (!columnFields[0] || !rowFields[0] || !data || data.length === 0) {
    return;
  }
  cOpt.colDimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
  cOpt.rowMeasure = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];

  //排序
  data = Enumerable.from(data).orderByDescending(function (d) {
    return d[cOpt.rowMeasure.name];
  }).toArray();
  var sourceData = data.concat();//原始数据

  //样式配置对象
  var config = options.chartConfig;
  if (!config) {
    config = {
      GlobalColor: "#4E79A7",
      MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
      scale: 0.5,
      axisStyle: {
        tickColor: "#000",
        fontColor: "#000",
        fontFamily: "Arial",
        fontSize: 15
      },
      textStyle: {
        "display": "none",
        "font-family": "Arial",
        "font-size": 15,
        "font-style": "normal",
        "font-weight": "normal",
        "fill": "#333",
        "position": "top"
      }
    };
  }

  //获取所有度量字段
  var measureFields_c = Enumerable.from(columnFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields_r = Enumerable.from(rowFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields = measureFields_c.concat(measureFields_r);
  var measureMap = {};
  measureFields.forEach(function (f) {
    measureMap[f.name] = f;
  });
  cOpt.MeasureMap = measureMap;

  (function () {

    var dKey = cOpt.colDimension.name,
      mKey = cOpt.rowMeasure.name;

    if (cOpt.colDimension.dataType === 4) { // 如果为时间（数值）升序排列
      data.sort(function (a, b) {
        return a[dKey] - b[dKey];
      });
    }

    cOpt.values = data.map(function (d) {
      return d[mKey];
    });
    cOpt.category = data.map(function (d) {
      return d[dKey];
    });

    cOpt.tipFormat = options.TooltipFormat;
    if (!cOpt.tipFormat) {
      var tipFormatStr = '';
      tipFormatStr += cOpt.colDimension.name + '：<' + cOpt.colDimension.name + '>';
      tipFormatStr += '\r\n' + cOpt.rowMeasure.name + '：<' + cOpt.rowMeasure.name + '>';
      cOpt.tipFormat = tipFormatStr;
    }
    cOpt.textStyle = options.textStyle || {};
    cOpt.data = {sourceData: sourceData};

  })();

  //var colorMatch = options.colorMark.MultiColors, color;
  //if (colorMatch && colorMatch.length) {
  //    color = cOpt.category.map(function (d, i) {
  //        //console.log(d);
  //        //console.log(i);
  //        var obj = colorMatch.find(function (j) { return j.Key === d; });
  //        return obj ? obj.Color : _precolor[i % 20];
  //    });
  //    cOpt.color = color;
  //}
  var colorMatch = config.MultiColors, color;
  if (colorMatch && colorMatch.length) {
    color = cOpt.category.map(function (d, i) {
      //console.log(d);
      //console.log(i);
      if (i < colorMatch.length) {
        return colorMatch[i];
      } else {
        _precolor[i % 20];
      }
    });
    cOpt.color = color;
  }

  if ("scale" in options) {
    cOpt.scale = options.scale;
  }
  if (options.size) {
    cOpt.size = options.size;
  }

  if ("scale" in config) {
    cOpt.scale = config.scale;
  }
  if ("textStyle" in config) {
    cOpt.textStyle = config.textStyle;
  }

  var datamarkingopt = options.DataMarking;
  var datamarkingmapping = {}
  if (datamarkingopt) {
    datamarkingopt.dimensions.forEach(function (dko) {
      if (dko.DataMarkingEnable) {
        datamarkingmapping[dko.name] = {
          mapping: {}
        }
        cOpt.category.forEach(function (d) {
          var k = d.split("|")[0];
          datamarkingmapping[dko.name].mapping[k] = k.marking();
        });
      }
    });
  }
  cOpt.DatamarkingMap = datamarkingmapping;

  return treemap(svgSelector, cOpt);
};


function treemap(selector, options) {

  var opt = {
    //size    : { width: 300, height: 400 },
    category: [],
    values: [],

    scale: 0.8, // control outerRadius [0, 1]
    color: d3.schemeCategory20c,
    tipFormat: null,
    colDimension: null,
    rowMeasure: null,
    MeasureMap: null,
    DatamarkingMap: null,
    textStyle: {},
    data: null
  };
  _.extend(opt, options);

  var chart = {
    svg: svg,
    onselected: null,
    onDeleteFilter: null
  };

  var padding = 25,
    minDim = 200,    // min required width or height dimension
    f = d3.format(".2%"),
    format = d3.format(",d"),
    contextMenu;

  var cData = opt.values.map(function (d, i) {
    return {
      x: opt.category[i],
      y: d,
      color: opt.color[i] || _precolor[i % 20],
      _index: i
    };
  });

  var getContextmenu = function () {
    return contextMenu || (contextMenu =
      d3.select(document.body).append("div").attr("class", "d3-contextmenu open"));
  };

  var container = d3.select(selector).node().parentElement;
  if (!opt.size) {
    opt.size = d3.util.getInnerBoxDim(container);
  }

  var width = opt.size.width;
  var height = opt.size.height;

  var svg = d3.select(container)
    .html("")
    .append("svg")
    .attr("class", "Chart-To-Be-Export")
    .attr("width", opt.size.width)
    .attr("height", opt.size.height)
    .attr("version", "1.1")
    .attr("xmlns", "http://www.w3.org/2000/svg")
    .style("font", "12px arial, sans-serif");

  function normalise() {
    // assign the dimension
    //if (opt.size.width < minDim) { opt.size.width = minDim; }
    //if (opt.size.height < minDim) { opt.size.height = minDim; }

    svg.html("");

    svg = d3.select(container)
      .html("")
      .append("svg")
      .attr("class", "Chart-To-Be-Export")
      .attr("width", opt.size.width)
      .attr("height", opt.size.height)
      .attr("version", "1.1")
      .attr("xmlns", "http://www.w3.org/2000/svg")
      .style("font", "12px arial, sans-serif");

    //if ($(container).find(".legendPadding").length === 0) {
    //    $(container).prepend('<div class="legendPadding" style="height:20px;"></div>');
    //}
  }

  normalise();

  var data = opt.values.map(function (d, i) {
    return {
      //name: (opt.DatamarkingMap && opt.DatamarkingMap[opt.colDimension.name]) ? opt.DatamarkingMap[opt.colDimension.name].mapping[opt.category[i]] : opt.category[i],
      name: opt.category[i],
      value: d,
      color: opt.color[i] || _precolor[i % 20],
      _index: i
    };
  });

  data = {children: data};

  //console.log(data);

  verticalTreeMap();

  function verticalTreeMap(treeNodes) {
    //定义一个矩形树图布局函数treemap()函数
    var treemap = d3.treemap()
      .tile(d3.treemapResquarify)
      .size([opt.size.width, opt.size.height])
      .round(true)
      .paddingInner(1);

    if (treeNodes === undefined)
      treeNodes = data;

    //生成树形层次结构的数据
    var root = d3.hierarchy(treeNodes)
      .eachBefore(function (d) {
        d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name;
      })
      .sum(sumBySize)
      .sort(function (a, b) {
        return b.height - a.height || b.value - a.value;
      });

    //对root数据进行矩形树布局
    treemap(root);

    //定义每个矩形的画布
    var cell = svg.selectAll("g")
      .data(root.leaves())
      .enter()
      .append("g")
      .attr("transform", function (d) {
        return "translate(" + d.x0 + "," + d.y0 + ")";
      });

    //设置代表树节点的每个矩形的id,宽度，高度以及填充色
    cell.append("rect")
      .attr("id", function (d) {
        return d.data.id;
      })
      .attr("width", function (d) {
        return d.x1 - d.x0;
      })
      .attr("height", function (d) {
        return d.y1 - d.y0;
      })
      .attr("fill", function (d, i) {
        return d.data.color;
      })
      .style("cursor", "pointer")
      //.attr("stroke-width", "2px")
      //.attr("stroke", function (d) {
      //    return "#333333";
      //})
      .on("contextmenu", onContextMenu)
      .on("click", onClick)
      .on("mouseover", function (d, i) {

        var fName = opt.colDimension.name;
        var name = d.data.name;
        var tipFields = [{name: fName, value: name}];

        var dataRow = getDataRow(name);
        if (dataRow.length === 0) {
          for (var f in opt.data.sourceData[0]) {
            if (f != opt.colDimension.name) {
              var tempValue = 0;
              if (opt.MeasureMap[f] !== undefined) {
                tipFields.push({name: f, value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)});
              }
            }
          }
        } else {
          dataRow.forEach(function (row) {
            for (var f in row) {
              if (f != opt.colDimension.name) {
                var tempValue = row[f];
                if (opt.MeasureMap[f] !== undefined) {
                  tipFields.push({name: f, value: Number(tempValue).toMeasureFormart(opt.MeasureMap[f].numberFormat)});
                }
              }
            }
          });
        }
        if (opt.tipFormat) {
          Tooltip.show(opt.tipFormat, tipFields);
        }

        d3.select(this)
          .style("stroke-width", "2px")
          .style("stroke", "#333333")
        ;
      })
      .on("mouseout", function (d, i) {
        Tooltip.hide();
        d3.select(this)
          .style("stroke-width", "1px")
          .style("stroke", "#ffffff");
      })
    ;

    //定义每个文本的画布
    var gText = svg.append("g")
        .selectAll(".place-label")
        .data(root.leaves())
        .enter()
        .append("text")
        .attr("class", "place-label")
        //.attr("transform", function (d) {
        //    return "translate(" + d.x0 + "," + d.y0 +")";
        //})
        //.attr("dx", ".35em")
        //.attr("dy", "1.35em")
        .attr("x", function (d) {
          return d.x0;
        })
        .attr("y", function (d) {
          return d.y0;
        })
      //.text(function (d) {
      //    if (d.data.value > 0) {
      //        var fontSize = opt.textStyle["font-size"];
      //        var width = d.x1 - d.x0;
      //        var word = parseInt(width / fontSize) - 2;
      //        if (word < 0) {
      //            word = 0;
      //        }
      //        var name = d.data.name;
      //        if (name.length > word) {
      //            name = d.data.name.substring(0, word) + '..';
      //        }
      //        //name += "\r\n" + d.data.value;
      //        return name;
      //    }
      //}
      //)
    ;

    gText.selectAll("tspan")
      .data(function (d) {
        var name = d.data.name;
        var value = d.data.value;
        return [name, value];
      })
      .enter()
      .append("tspan")
      .attr("x", function (d) {
        return $(this.parentElement).attr("x");
      })
      .attr("dx", ".35em")
      .attr("dy", "1.35em")
      .text(function (d, index) {
        var parent = $(this.parentElement)[0].__data__;
        var fontSize = opt.textStyle["font-size"];
        var width = parent.x1 - parent.x0;
        var height = parent.y1 - parent.y0;
        var word = parseInt(width / fontSize) - 2;
        if (word < 0) {
          word = 0;
        }
        if (index === 0) {
          var name = d;
          name = (opt.DatamarkingMap && opt.DatamarkingMap[opt.colDimension.name]) ? opt.DatamarkingMap[opt.colDimension.name].mapping[name] : name;
          if (name.length > word) {
            name = name.substring(0, word) + '..';
          }
          return name;
        }

        if ((fontSize * 2) <= (height - 12)) {
          d = Number(d).toMeasureFormart(opt.rowMeasure.numberFormat)
          if (parent.data.color === "red") {
            d = "-" + d;
          }
          return d;
        }
      });

    //opt.textStyle.display = "block";
    chart.label = null;
    //chart.label = new d3.util.TextDecorate(gText, 0, [[0, - height], [width, 0]], opt.textStyle);
    chart.label = new d3.util.TextDecorate(gText, 0, [[0, 0], [0, 0]], opt.textStyle);
    chart.label.top = chart.label.right = chart.label.bottom = chart.label.left = function () {
    };
    chart.label.init();
  }

  // 对节点的子节点个数求和
  function sumByCount(d) {
    return d.children ? 0 : 1;
  }

  // 返回节点的size属性
  function sumBySize(d) {
    if (d.value < 0) {
      d.value = -d.value;
      d.color = "red";
    }
    return d.value;
  }

  // 返回某一行
  function getDataRow(rowKey) {
    if (opt === null) return null;

    var key = opt.colDimension.name;
    var fList = Enumerable.from(opt.data.sourceData).where(
      function (d) {
        if (rowKey === d[key]) {
          return d;
        }
      }
    ).toArray();

    return fList;
  }

  // clere contextmenu
  svg.on("click.foo", function () {
    return contextMenu && contextMenu.style("display", "none");
  });

  // avoid ugly default interface
  svg.on("contextmenu.foo", function () {
    d3.event.preventDefault();
  });

  // EVENTS DEFINE
  function onContextMenu(d) {
    d3.event.preventDefault();

    var context = getContextmenu();
    var commands = [
      {title: "删除片区", symbol: "&#10008;", action: "delete"},
      //{ title: "降序排布", symbol: "&#8650;", action: "descend" },
      //{ title: "升序排布", symbol: "&#8648;", action: "ascend" },

      {title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg"},
      {title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png"}
    ];

    // delete pie slice accroding to its "_index"
    var index = ("_index" in d) ? d._index : d.data._index;

    context
      .html("")
      .style("display", "block")
      .append("ul")
      .attr("class", "dropdown-menu")
      .selectAll("li")
      .data(commands)
      .enter()
      .append("li")
      .append("a")
      .html(function (j) {
        return j.symbol + "&nbsp;" + j.title;
      })
      .on("click", function (j) {
        context.style("display", "none");
        switch (j.action) {
          case "delete":
            chart.delete(index);
            break;
          case "descend":
            chart.descend();
            break;
          case "ascend":
            chart.ascend();
            break;
          case "export":
            chart.export(j.p1);
            break;
        }
      });

    context.style("left", d3.event.clientX + window.pageXOffset + 10 + "px");
    context.style("top", d3.event.clientY + window.pageYOffset + 10 + "px");
  }

  function onClick(d) {
    if (chart.onselected) {
      chart.onselected([{
        field: opt.colDimension,
        value: [d.data.name]
      }]);
    }
  }

  // update tree and legend
  function update(oldData, newData) {
    normalise();
    verticalTreeMap(newData);
  }

  // layout treemap descendly
  chart.descend = function () {
    return this;
  };
  chart.ascend = function () {
    return this;
  };

  // delete one slice of treemap according to its predefined index
  chart.delete = function (index) {
    if (data.children.length < 2) {
      return alert("不能再删了！");
    }
    var oldData = data.children.slice();
    var deleteData = _.filter(data.children, function (d) {
      return d._index === index;
    })[0];
    data = _.filter(data.children, function (d) {
      return d._index !== index;
    });
    data = {children: data};
    update(oldData, data);

    if (this.onRemoveItem) {
      this.onRemoveItem([
        {field: opt.colDimension, value: [deleteData.name]}
      ]);
    }

    //if (chart.onDeleteFilter) {
    //    chart.onDeleteFilter({ Field: opt.colDimension, FilterMode: 1, FilterItems: [deleteData.name], FilterItemsForCube: [] });
    //}
  };

  // customized tooltip content formate
  chart.tipFormat = function (s) {
    if (!arguments.length) {
      return opt.tipFormat;
    }
    opt.tipFormat = s;
    return this;
  };

  // change chart dimension: { width: 500, height: 300 }
  chart.setSize = function (size) {
    if (!arguments.length) {
      return opt.size;
    }
    //计算缩放比例
    var wScale = size.width / opt.size.width;
    var hScale = size.height / opt.size.height;
    var scale = hScale;

    opt.size.width = size.width;
    opt.size.height = size.height;

    // chart redraw
    svg.attr("width", size.width).attr("height", size.height);

    width = opt.size.width;
    height = opt.size.height;

    normalise();
    verticalTreeMap();

    return this;
  };

  // output legend name and its color attached
  chart.getLegends = function () {
    return cData.map(function (d) {
      return {name: d.x, color: d.color};
    });
  };

  chart.setScale = function (val) {
    if (!arguments.length) {
      return;
    }
    if (typeof val === "string") {
      val = +val;
    }
    if ((val > 1) || (val < 0)) {
      return;
    }

    opt.scale = val;
    opt.size.width = width * val;
    opt.size.height = height * val;

    normalise();
    verticalTreeMap();
  };

  chart.getScale = function () {
    return opt.scale;
  };

  chart.export = function (type) {
    d3.util.exportChart(svg.node(), type || "png");
  };

  chart.removeChart = function () {
    svg.remove();
    if (contextMenu) {
      contextMenu.remove();
    }

    contextMenu = null;
    container = null;
    opt = null;

    for (var pp in chart) {
      delete chart[pp];
    }
    chart = null;
  };

  return chart;
}

export const TreeMapChart = {
  build: buildChart
};

