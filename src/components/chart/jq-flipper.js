
/* global $flipper, _, require, define */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {
        var value = Object.values(data[0])[0];

        var cOpt = {};
        cOpt.FlipperTheme = options.TableTheme;

        return $flipper(svgSelector, value, cOpt);
    };
    
    module.exports = {
        build: buildChart
    };
});



























