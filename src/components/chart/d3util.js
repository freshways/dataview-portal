﻿/**
 * 根据d3js扩展的辅助功能
 */
define(function (require, exports, module) {
    var _specifiers = [
            "%Y-%m-%dT%H:%M:%S",
            "%Y-%m-%dT%H:%M:%S.%L",
            "%Y-%m-%d %H:%M:%S.%L",
            "%Y-%m-%d %H:%M:%S",
            "%Y-%m-%d",
            "%Y/%m/%d",
            "%Y年%m月%d日"
    ];
    //将字符串转换为日期
    d3.parseDateTime = function (str, format) {
        if (typeof str==="string") {
            var result = d3.timeParse(format)(str);
            if (result) {
                return result;
            } else {
                for (var i = 0; i < _specifiers.length; i++) {
                    result = d3.timeParse(_specifiers[i])(str);
                    if (result) {
                        return result;
                    }
                }
            }
        }else if(str&&str.getDate){
            return str;
        }
    }
    //补全日期
    d3.completionDateTime = function (str, format) {
        switch (format) {
            case "%Y-%m":
                str += "-1";
                return d3.parseDateTime(str, "%Y-%m-%d");
            case "%Y":
                str += "-1-1";
                return d3.parseDateTime(str, "%Y-%m-%d");
            case "%m":
                var dtnow = new Date();
                str = dtnow.getFullYear() + "-" + str + "-1";
                return d3.parseDateTime(str, "%Y-%m-%d");
            case "%d":
                var dtnow = new Date();
                str = dtnow.getFullYear() + "-1-" + str;
                return d3.parseDateTime(str, "%Y-%m-%d");
            default:
                return d3.parseDateTime(str, format);
        }
    }
});