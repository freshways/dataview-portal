﻿/**
 * d3-mappie
 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {
		/* options.mapConfig for MapPie:
		{
			edgeColor: "red",
			edgeWidth: 1,
			siteNameColor: "blue",
			siteNameShadowColor: "white",
		}
		*/

        var cOpt = {}; // chart options
        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }
        cOpt.colDimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
        cOpt.rowMeasure = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];

        var config = options.chartConfig || {
            MultiColors: ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd"],
            axisStyle: {
                fontColor: "#2929a7",
                fontFamily: "arial",
                fontSize: 13,
                tickColor: "gray"
            },
            textStyle: {
                "display": "block",
                "font-family": "Arial",
                "font-size": 13,
                "font-style": "normal",
                "font-weight": "normal",
                "fill": "#ff0000"
            }
        };

        // var geoCityCode = cOpt.colDimension.GeoMarkerCode;
        // if( !geoCityCode ){
        //     geoCityCode = "330000";
        // }
        cOpt.selector = svgSelector;
        if (options.size) {
            cOpt.size = options.size;
        }

        cOpt.axisStyle = config.axisStyle;
        cOpt.textStyle = config.textStyle;
        cOpt.numberFormat = cOpt.rowMeasure.numberFormat;
        cOpt.colorRange = (function () {
            if (!config.MultiColors || config.MultiColors.length < 5) {
                return ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd"];
            }
            return config.MultiColors;
        })();


        (function () {
            var dKey = cOpt.colDimension.name,
                mKey = cOpt.rowMeasure.name;
            cOpt.staticData = data.map(function (d) {
                return { name: d[dKey], value: d[mKey] };
            });
            //cOpt.staticPorpertyMap = {name: dKey, value: mKey};
        })();


        /* 解析GeoJSON数据路径 */
        var cityQuery, cityPath;
        var geoCityCode = cOpt.colDimension.GeoMarkerCode;
        if (options.geoinfo) { // （A1 图形下拽模式
            $.ajax({ //加载全国省市信息
                url: SiteRootPath + '/Scripts/DataInsight/component/chart/map/mapdata/china-Lng-Lat.json',
                data: {},
                type: "get",
                async: false,
                dataType: "json",
                //cache: true,
                error: function () { alert("获取信息时服务端出错"); },
                success: function (result) { cityQuery = result; }
            });

            if (options.geoinfo.GeoMarker == "Geo-P") {
                // 获取省和直辖市路径
                var city = _.find(cityQuery.Provinces, function (d) {
                    return d.name.indexOf(options.geoinfo.GeoLabel) > -1;
                });
                cityPath = "geometryProvince/" + (city.PkId + "").replace("0000", "");
            }
            else if (options.geoinfo.GeoMarker == "Geo-C") {
                var subCities = Array.prototype.concat.apply([], cityQuery.Provinces.map(d => d.Citys));
                var city = subCities.find(function (d) {
                    return d.name.indexOf(options.geoinfo.GeoLabel) > -1;
                });
                // 获取市区级路径
                cityPath = "geometryCouties/" + city.PkId;
            }
        }
        else { // （A2 图形编辑模式
            if (geoCityCode.indexOf("|") > -1) {
                if (geoCityCode.split("|")[1] == "-1") {
                    // 获取省和直辖市路径（"440000|-1"）
                    cityPath = "geometryProvince/" +
                        geoCityCode.split("|")[0].replace("0000", "");
                }
                else {
                    // 获取市区级路径
                    cityPath = "geometryCouties/" + geoCityCode.split("|")[1];
                }
            }
            else {
                // 如果上述都不行，直接上温州
                cityPath = "geometryCouties/" + (geoCityCode || "330000");
            }
        }


        //return d3Map("MapPie", cOpt).getJSON(SiteRootPath+ "/Scripts/DataInsight/component/chart/map/mapdata/geometryCouties/330300.json");
        return d3Map("MapPie", cOpt)
            .getJSON(SiteRootPath + "/Scripts/DataInsight/component/chart/map/mapdata/" + cityPath + ".json");
    };
    module.exports = {
        build: buildChart
    };

});
