/* global Highcharts, Hcharts, d3, undersore, _, require, define */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {
        if (!columnFields[0] || !rowFields[0]) { return; }

        var chart = Hcharts.radar(svgSelector);
        (function () {
            var xName, yName, zName,
                colDimension,
				annexDimension,
                isMultiCircle,
                categories, categIndex = {}, zeroArr,
                items = {}, colors = [], hConfig;

            if (columnFields[0].slaveType === 1) {
                xName = rowFields[0].name;
                yName = columnFields[0].name;
                colDimension = rowFields[0];
            }
            else {
                xName = columnFields[0].name;
                yName = rowFields[0].name;
				colDimension = columnFields[0];
            }

            try {
                annexDimension = options.colorMark.annexField;
                isMultiCircle = true;
            }
            catch (e) {
                annexDimension = null;
                isMultiCircle = false;
            }

            hConfig = options.chartConfig || {
                MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D",
                              "#59A14F", "#8CD17D", "#B6992D", "#F1CE63",
                              "#499894", "#86BCB6", "#E15759", "#FF9D9A",
                              "#79706E", "#BAB0AC", "#D37295", "#FABFD2",
                              "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
                axisStyle: {
                    tickColor : "#000",
                    fontColor : "#000",
                    fontFamily: "Arial",
                    fontSize  : 15
                },
                textStyle: {
                    "display"     : "none",
                    "font-family" : "Arial",
                    "font-size"   : 15,
                    "font-style"  : "normal",
                    "font-weight" : "normal",
                    "fill"        : "#333",
                    //"position"    : "top"
                }
            };

            categories = _.uniq(data.map(d => d[xName]));
            if (!hConfig.MultiColors.length) {
                hConfig.MultiColors = ["#4E79A7", "#A0CBE8", "#F28E2B"];
            }
            while (colors.length < categories.length) {
                colors = colors.concat(hConfig.MultiColors);
            }


            if (isMultiCircle) {
                zeroArr = "0".repeat(categories.length - 1).split("0").map(() => 0);
                _.each(categories, (d, key) => { categIndex[d] = key; });

                zName = options.colorMark.annexField.name;
                data.forEach((d, index) => {
                    if (!items[ d[zName] ]) {
                        items[ d[zName] ] = {
                            data: zeroArr.slice(),
                            pointPlacement: "on",
                            color: colors[index]
                        };
                    }
                    items[d[zName]].data[ categIndex[ d[xName] ] ] = d[yName];
                });
            }
            else {
                chart.config.legend.enabled = false;
                items.untitled = {
                    data: data.map(d => d[yName]),
                    pointPlacement: "on",
                    color: colors[0]
                };
            }

            _.extend(chart.config.xAxis,
                     chart.getAxisStyle(hConfig.axisStyle));
            chart.config.xAxis.categories = categories;
            _.extend(chart.config.yAxis,
                     chart.getAxisStyle(hConfig.axisStyle));
            _.extend(chart.config.legend, {
                itemStyle: chart.getAxisStyle(hConfig.axisStyle).labels.style
            });


            if (options.size) {
                chart.config.width = options.size.width;
                chart.config.height = options.size.height;
            }
            chart.config.series = _.map(items, (val, key) => {
                val.name = key;
                val.dataLabels = chart.getDataLabels(hConfig.textStyle);
                return val;
            });

            function onClick (e) {
                //console.log(this.series.name);
                var res = [{ field: colDimension, value: [ this.series.name ] }];
                if (chart.onselected) {
                    if (annexDimension) {
                        res.push({ field: annexDimension, value: [ this.category ] });
                    }
                    chart.onselected(res);
                }
            }

            chart.config.plotOptions = {
                series: {
                  point: {
                    events: {
                      "click": onClick
                    }
                  }
                }
            };// plotOptions END


        })();

        chart.init();
        return chart;
    };


    module.exports = {
        build: buildChart
    };

});
