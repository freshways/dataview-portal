/* global Highcharts, Hcharts, d3, undersore, _, require, define */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {
        if (columnFields.length != 2 ||
            (columnFields[0].slaveType + columnFields[1].slaveType) != 1) {
            return;
        }

        var chart = Hcharts.boxPlot(svgSelector);
        var config = chart.config;
        var hConfig = options.chartConfig || {
                GlobalColor: "#4E79A7",
                axisStyle: {
                    tickColor : "#000",
                    fontColor : "#000",
                    fontFamily: "Arial",
                    fontSize  : 15
                }
            };

        (function () {

            var xName, yName, items = {}, color, format;

            if (columnFields[0].slaveType === 0) {
                xName = columnFields[0].name;
                yName = columnFields[1].name;
            }
            else {
                xName = columnFields[1].name;
                yName = columnFields[0].name;
            }
            format = columnFields[0].numberFormat;

            data.forEach(function (curr) {
                if (!items[curr[xName]]) {
                    items[curr[xName]] = [];
                }
                items[curr[xName]].push(curr[yName]);
            });
            color = hConfig.GlobalColor || "black";

            if (options.size) {
                config.chart = {
                    width: options.size.width,
                    height: options.size.height
                };
            }
            config.legend = { enabled: false };
            config.series = [{
                data: _.map(items, function (value, key) {
                    var sorted = value.sort((a, b) => { return a - b; });
                    return {
                        low   : d3.quantile(sorted, 0.05),
                        q1    : d3.quantile(sorted, 0.25),
                        median: d3.quantile(sorted, 0.5),
                        q3    : d3.quantile(sorted, 0.75),
                        high  : d3.quantile(sorted, 0.95),
                        //color : color,
                        name  : key
                    };
                }),
                type: "boxplot"
            }];

            config.xAxis = chart.getAxisStyle(hConfig.axisStyle);
            config.xAxis.title.text = xName;
            config.xAxis.categories = Object.keys(items);

            config.yAxis = chart.getAxisStyle(hConfig.axisStyle);
            config.yAxis.title.text = yName;

            config.plotOptions = {
                boxplot: {
                    fillColor: "transparent",
                    lineWidth: 1,
                    color: color
        //                    medianColor: '#0C5DA5',
        //                    medianWidth: 3,
        //                    stemColor: '#A63400',
        //                    stemDashStyle: 'dot',
        //                    stemWidth: 1,
        //                    whiskerColor: '#3D9200',
        //                    whiskerLength: '20%',
        //                    whiskerWidth: 3
                }
            };
            config.tooltip = {
                formatter: function () {
                    return "上须: <b>" + this.point.high.toMeasureFormart(format) + "</b><br>" +
                           "上枢纽: <b>" + this.point.q3.toMeasureFormart(format) + "</b><br>" +
                           "中位数: <b>" + this.point.median.toMeasureFormart(format) + "</b><br>" +
                           "下枢纽: <b>" + this.point.q1.toMeasureFormart(format) + "</b><br>" +
                           "下须: <b>" + this.point.low.toMeasureFormart(format) + "</b><br>";
                }
            };

        })(); // IIFE END


        chart.init();
        return chart;
    };


    module.exports = {
        build: buildChart
    };

});
