﻿import {colorHelper} from "@/utils/colorHelper";
import * as echarts from 'echarts'
import 'echarts-liquidfill'

const chart = function (dom_container, options) {
  var opt = {
    tipFormat: null,
    MeasureMap: null
  };
  _.extend(opt, options);
  if (opt.config.ThemeType + "" === "999")
    opt.config.ThemeType = "circle";

  var rowData = opt.data.sourceData[0];
  //var mainValue = Math.ceil(rowData[opt.rowMeasure.name]);
  var mainValue = rowData[opt.rowMeasure.name];
  var mainTitle = opt.rowMeasure.name;

  var _animationDuration = 1500;
  //mainValue = 60.8888;
  mainValue += 2;
  var data0 = {
    name: mainTitle,
    value: mainValue,
    direction: 'right',
    itemStyle: {normal: {color: opt.config.MultiColors[0]}}
  };
  var data1 = {value: 0.5, direction: 'right', itemStyle: {normal: {color: opt.config.MultiColors[1]}}};
  var data2 = {value: 0.4, direction: 'right', itemStyle: {normal: {color: opt.config.MultiColors[2]}}};
  var data3 = {value: 0.3, direction: 'right', itemStyle: {normal: {color: opt.config.MultiColors[3]}}};

  var data = [];
  if (!opt.config.ShowConfig || opt.config.ShowConfig.statisticType + "" === "0" || mainValue === 2) {
    data.push(data0);
    //data.push(data1);
    _animationDuration = 0;
  } else {
    //百分比
    data0 = {
      name: mainTitle,
      value: mainValue / 100,
      direction: 'right',
      itemStyle: {normal: {color: opt.config.MultiColors[0]}}
    };
    data.push(data0);
    //data.push(data1);
    //data.push(data2);
    //data.push(data3);
  }

  //https://github.com/ecomfe/echarts-liquidfill
  var chart_opt = {
    series: [{
      type: 'liquidFill',
      //name: 'Liquid Fill',
      radius: '80%',
      waveAnimation: (opt.config.ShowConfig.waveAnimation + "" === "0"),//是否波动
      animationDuration: _animationDuration,//动画时长
      backgroundStyle: {
        //borderWidth: 5,
        //borderColor: 'red',
        color: opt.config.MultiColors[1]  //内圈背景色
      },
      shape: opt.config.ThemeType,   //one of the default symbols: 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow';
      outline: {
        show: (opt.config.ShowConfig.borderType + "" === "0"),
        //borderDistance: 0,
        itemStyle: {
          //borderWidth: 5,
          //borderColor: opt.config.axisStyle.tickColor,
          borderColor: opt.config.GlobalColor //外圈背景色
          //shadowBlur: 20,
          //shadowColor: 'rgba(255, 0, 0, 1)'
        }
      },
      data: data,
      label: {
        color: opt.config.textStyle["fill"],
        //insideColor: (opt.config.ShowConfig.statisticType + "" === "0") ? opt.config.textStyle["fill"] : '#ffffff', //波覆盖文字
        insideColor: '#ffffff', //波覆盖文字
        show: opt.config.textStyle["display"] !== "none",
        fontSize: opt.config.textStyle["font-size"],    //字体大小
        fontFamily: opt.config.textStyle["font-family"],
        formatter: function (param) {
          //return param.seriesName + '\n'
          //+ param.name + '\n'
          // + 'Value: ' + param.value;
          //如果使用{a}\n{b}\nValue: {c} ，a代表系列名称，b代表数据名称，c代表数据值。
          if (!opt.config.ShowConfig || opt.config.ShowConfig.statisticType + "" === "0" || param.value === 2) {
            var num = (Number(param.value) - 2).toMeasureFormart(opt.MeasureMap[mainTitle].numberFormat);
            if (opt.config.labelFormater && opt.config.labelFormater === "<%=value%>") {
              return num;
            } else {
              return param.name + '\n\n' + num;
            }
          } else {
            var num = parseInt(param.value * 100.00 - 2) + '%';
            var value = param.value + '';
            if (opt.MeasureMap[mainTitle].numberFormat && opt.MeasureMap[mainTitle].numberFormat.num_format_e) {
              num = (param.value * 100.00 - 2).toFixed(opt.MeasureMap[mainTitle].numberFormat.num_format_e) + '%';
            }
            if (opt.config.labelFormater && opt.config.labelFormater === "<%=value%>") {
              return num;
            } else {
              return param.name + '\n\n' + num;
            }
          }
        }
      }
    }]
    //,
    //tooltip: {
    //    show: true
    //}
  }

  var setOption = function (opt) {
    chart_opt = $.extend(chart_opt, opt);
    myChart.setOption(chart_opt);
  }

  var self = this;
  var myChart = echarts.init(dom_container);
  myChart.onselected = null;
  myChart.onDeleteFilter = null;

  if (opt) {
    setOption(opt);
  }

  myChart.on('click', function () {
    console.log(arguments);
    if (self.onselected) {
      //当前维度
      self.onselected([{
        field: opt.MeasureMap[arguments[0].name],
        value: [arguments[0].value]
      }]);
    }
  });

  this.setOption = setOption;
  this.removeChart = function () {
    myChart.dispose();
  }

  //return myChart;
};
/**
 * 水球图
 */

const buildChart = function (svgSelector, data, columnFields, rowFields, options) {
  var cOpt = {};

  var config = options.chartConfig;
  if (!config) {
    config = {
      GlobalColor: "#4E79A7",
      MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
      scale: 0.8,
      axisStyle: {
        tickColor: "#000",
        fontColor: "#000",
        fontFamily: "Arial",
        fontSize: 12
      },
      textStyle: {
        "display": "none",
        "font-family": "Arial",
        "font-size": 12,
        "font-style": "normal",
        "font-weight": "normal",
        "fill": "#333",
        "position": "top"
      },
      labelFormater: "<%=name%>:<%=ratio%>",
      ThemeType: 999,
      ShowConfig: {
        statisticType: 0,
        borderType: 0,
        waveAnimation: 1
      }
    }
  }
  if (!config.ShowConfig) {
    config.ShowConfig = {
      statisticType: 0,
      borderType: 0,
      waveAnimation: 1
    };
  }
  cOpt.config = config;

  var dimension = (columnFields.length > 0 && columnFields[0].slaveType === 0) ? columnFields[0] : rowFields[0];
  var measure = (rowFields.length > 0 && rowFields[0].slaveType === 1) ? rowFields[0] : columnFields[0];
  cOpt.colDimension = dimension;
  cOpt.rowMeasure = measure;

  //获取所有度量字段
  var measureFields_c = Enumerable.from(columnFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields_r = Enumerable.from(rowFields).where(
    function (d) {
      if (d.slaveType === 1) {
        return d;
      }
    }
  ).toArray();
  var measureFields = measureFields_c.concat(measureFields_r);
  var measureMap = {};
  measureFields.forEach(function (f) {
    measureMap[f.name] = f;
  });
  cOpt.MeasureMap = measureMap;

  var sourceData = data.concat();//原始数据
  cOpt.data = {sourceData: sourceData};

  var dom_container = $(svgSelector).children('#chart')[0];
  var chartInstance = new chart(dom_container, cOpt);
  return chartInstance;
}

export const LiquidfillChart = {
  build: buildChart
}
