/* global d3, undersore, _, require, define */
// import * as d3 from 'd3'
import d3 from '@/components/d3-util'

const buildChart = function (svgSelector, data, columnFields, rowFields, options) {

  /*
      cOpt.colDimension.dataType:
          1 : String
          2 : Number
          3 : Boolean
          4 : Date in Number ("xxxx" -> year, "xx" -> day/month)

      SlaveType: 0 -> dimension, 1 -> measure
  */

  // 如果用户输入数据不足直接退出
  if (!columnFields[0] || !rowFields[0]) {
    return;
  }

  var cOpt = {}, colDimension, rowMeasure,
    nameKey, xKey, yKey, xKeyFormat, yKeyFormat,
    config = options.chartConfig;

  if (!config) {
    config = {
      GlobalColor: "#4E79A7",
      MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D", "#59A14F", "#8CD17D", "#B6992D", "#F1CE63", "#499894", "#86BCB6", "#E15759", "#FF9D9A", "#79706E", "#BAB0AC", "#D37295", "#FABFD2", "#B07AA1", "#D4A6C8", "#9D7660", "#D7B5A6"],
      scale: 0.8,
      axisStyle: {
        tickColor: "#000",
        fontColor: "#000",
        fontFamily: "Arial",
        fontSize: 15
      },
      textStyle: {
        "display": "none",
        "font-family": "Arial",
        "font-size": 15,
        "font-style": "normal",
        "font-weight": "normal",
        "fill": "#333",
        "position": "top"
      }
    };
  }

  if (columnFields[0].slaveType === 0) { // 维度存在于columnFields
    xKey = columnFields[0].name;
    yKey = rowFields[0].name;

    colDimension = columnFields[0];
    rowMeasure = rowFields[0];

    xKeyFormat = null;
    yKeyFormat = rowMeasure.numberFormat || null;
  } else if (rowFields[0].slaveType === 0) { // 维度存在于rowFields
    xKey = rowFields[0].name;
    yKey = columnFields[0].name;

    colDimension = rowFields[0];
    rowMeasure = columnFields[0];

    xKeyFormat = null;
    yKeyFormat = rowMeasure.numberFormat || null;
  } else { // 全都是度量(目前都是这种形式)
    xKey = rowFields[0].name;
    yKey = columnFields[0].name;

    colDimension = columnFields[0];
    rowMeasure = rowFields[0];

    xKeyFormat = rowMeasure.numberFormat || null;
    yKeyFormat = colDimension.numberFormat || null;
  }

  // ColorMark.GlobalColor
  if (options.colorMark && options.colorMark.annexField) {
    nameKey = options.colorMark.annexField.name;
  } else {
    nameKey = null;
  }


  var getColor = (function () {
    var colors = config.MultiColors;
    if (!colors.length) {
      colors = ["#4E79A7", "#A0CBE8", "#F28E2B"];
    }
    while (colors.length < data.length) {
      colors = colors.concat(config.MultiColors);
    }

    return function (i) {
      return colors[i];
    };
  })();

  if (nameKey) { // 多个点的情形
    cOpt.values = data.map(function (d, i) {
      return {
        x: d[xKey],
        y: d[yKey],
        name: d[nameKey],
        //color : getColor(d[nameKey])
        color: getColor(i)
      };
    });
    cOpt.tipFormat = d3.util.tipParse(
      [{key: xKey, value: "x", format: xKeyFormat},
        {key: yKey, value: "y", format: yKeyFormat},
        {key: nameKey, value: "name"}],
      options.TooltipFormat
    );
  } else { // 一个点的情形
    cOpt.values = data.map(function (d, i) {
      return {
        x: d[xKey],
        y: d[yKey],
        //color : options.colorMark.GlobalColor || "black"
        color: getColor(i)
      };
    });

    // 这里多出一个format参数配合后面的tooltip配置修改：getRoundedData
    cOpt.tipFormat = d3.util.tipParse(
      [{key: xKey, value: "x", format: xKeyFormat},
        {key: yKey, value: "y", format: yKeyFormat}],
      options.TooltipFormat
    );
  }

  cOpt.scale = (("scale" in config) ? config.scale : 0.5);
  cOpt.textStyle = config.textStyle || {};
  cOpt.axisStyle = config.axisStyle || {};
  cOpt.annexField = options.colorMark.annexField || {};
  cOpt.colDimension = colDimension;
  cOpt.rowMeasure = rowMeasure;
  if (options.size) {
    cOpt.size = options.size;
  }

  return scatter(svgSelector, cOpt);
};


function scatter(selector, options) {

  var opt = {
    //size: { width: 450, height: 300 },

    // [{x:  1, y: 2, z: 3, type: "symbolCross", name: "abc" }, {...}]
    // "z", "type", "color" and "name" are optional
    // "x": could be either dimension or measurment variant, [number / string]
    // "y": measurment variant
    values: [],

    scale: 0.5, // use for adjusting relative symbol size

    // symbolCircle, symbolCross, symbolDiamond, symbolStar, symbolTriangle
    symboleName: "symbolCircle",
    textStyle: {},
    axisStyle: {},

    isReverse: false,
    tipFormat: null,

    AnnexField: {},
    colDimension: null,
    rowMeasure: null
  };

  _.extend(opt, options);

  // "oScale" stands for ordinary scale
  var xScale, yScale, zScale,
    xExtent, yExtent, zExtent,
    xAxis, yAxis,
    margin = {top: 10, right: 10, bottom: 10, left: 10},
    minGraphDim = 100,
    xLbel = opt.rowMeasure.name || "",
    yLbel = opt.colDimension.name || "",
    ceil = Math.ceil,
    cMin = 64, cMax = 625, // circle radius
    //numFix = d3.format(",." + (opt.rowMeasure.NumberFixed || 0) + "f"),
    cData,
    brush,
    textStyle = {
      "display": "none",    // "none" or "block"
      "font-family": "Arial",   // "Arial", "Helvetica", "Times"
      "font-size": 12,
      "font-style": "normal",  // "normal" or "italic"
      "font-weight": "normal",  // "normal" or "bold"
      "fill": "#ff0000", // text color
      "position": "top"      // "top", "right", "bottom" or "left"
    },
    symbol, // scatter shape and size
    symboleSizeScale,

    tooltip, contextMenu, legend,
    svg, gLeft, gBottom, gBrush, gChart, gText, gLegend,
    chart = {};

  _.extend(textStyle, opt.textStyle);

  // z determine symbole size
  chart.isZExist = ("z" in opt.values[0]);

  // ordinal scale vs linear scale
  chart.isOrdinalX = ((typeof opt.values[0].x == "string") ? true : false);

  // legends is available or not
  chart.isNameExsit = "name" in opt.values[0];

  // initialise status
  chart.status = opt.isReverse ? "herizontal" : "vertical";

  var formator = {};
  opt.tipFormat._map.forEach(function (d) {
    formator[d.value] = d.format;
  });

  // singolton function
  var getTooltip = function () {
    return tooltip || (tooltip = d3.select(document.body)
      .append("div")
      .attr("class", "d3-ttip")
      .node());
  };

  var menuOnSingleItem = [
    {title: "删除节点", symbol: "&#10008;", action: "delete"},
    {title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg"},
    {title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png"}
  ];
  var menuOnBlank = menuOnSingleItem.slice(1);
  var menuOnBrush = [
    {title: "删除框选节点", symbol: "&#10008;", action: "deleteNodes"}
  ];

  contextMenu = d3.util.getChartContextMenu();
  contextMenu.clickFun = function (command, target) {
    this.hide();
    switch (command.action) {
      case "delete":
        chart.delete([target._index]);
        break;
      case "setSymbol":
        chart.setSymbol(command.p1);
        break;
      case "export" :
        chart.export(command.p1);
        break;
      case "deleteNodes":
        chart.delete(target.map(node => {
          return d3.select(node).datum()._index;
        }));
        break;
    }
  };

  var container = d3.select(selector).node().parentElement;
  if (!opt.size) {
    opt.size = d3.util.getInnerBoxDim(container);
  }
  opt.size._w = opt.size.width;
  opt.size._h = opt.size.height;

  svg = d3.select(container)
    .html("")
    .append("svg")
    .attr("class", "Chart-To-Be-Export")
    .attr("width", opt.size.width)
    .attr("height", opt.size.height)
    .attr("version", "1.1")
    .attr("xmlns", "http://www.w3.org/2000/svg")
    .style("font", "12px arial, sans-serif");

  function getLegends() {
    if (!chart.isNameExsit || !cData) {
      return [];
    }
    var lSet = d3.set(), legendData = [];
    cData.forEach(function (d) {
      if (!lSet.has(d.name)) {
        lSet.add(d.name);
        legendData.push(d);
      }
    });
    return legendData;
  }

  function getRoundedData(dd) {
    var obj = {};

    if ("x" in dd) {
      obj.x = (formator.x ? dd.x.toMeasureFormart(formator.x) : dd.x);
    }
    if ("y" in dd) {
      obj.y = (formator.y ? dd.y.toMeasureFormart(formator.y) : dd.y);
    }
    if ("name" in dd) {
      obj.name = dd.name;
    }

    return obj;
  }

  // EVENTS
  function onMouseEnter(d) {
    if (d3.brushSelection(gBrush.node())) {
      return;
    } // brush show time

    var target = d3.select(this), template;
    target.classed("d3-active", true);

    try {
      template = _.template(opt.tipFormat.output());
    } catch (err) {
      template = _.template("X: <span><%= x %></span><br/>" +
        "Y: <span><%= y %></span>");
    }

    tooltip = getTooltip();

    d3.select(tooltip)
      .html("")
      .style("display", "block")
      .append("p")
      .attr("class", "d3-ttip-content")
      .html(template(getRoundedData(d)));

    var scrollLeft, scrollTop, rectBox;
    scrollLeft = window.pageXOffset;
    scrollTop = window.pageYOffset;
    rectBox = this.getBoundingClientRect();

    tooltip.style.left = scrollLeft + rectBox.left +
      0.5 * (rectBox.width - tooltip.offsetWidth) + "px";
    tooltip.style.top = scrollTop + rectBox.top - tooltip.offsetHeight + 10 + "px";
  }

  function onMouseLeave(d) {
    if (d3.brushSelection(gBrush.node())) {
      return;
    }
    d3.select(this).classed("d3-active", false);
    d3.select(tooltip).style("display", "none");
  }

  function onContextMenu(d) {
    d3.event.preventDefault();
    if (d3.brushSelection(gBrush.node())) {
      return onBrushContextMenu();
    }
    contextMenu.setTargetNode(d)
      .setCurrentMenu(menuOnSingleItem)
      .show();

  } // onContextMenu END
  function onClick(d) {
    if (!chart.onselected) {
      return;
    }
    chart.onselected([{
      field: opt.annexField,
      value: [d.name]
    }]);
  }


  // Brush
  function brushGet(range) {
    if (chart.status === "vertical") {
      return function (d) {
        var cx = xScale(d.x), cy = -yScale(d.y);
        return cx >= range[0][0] && cx <= range[1][0] &&
          cy >= range[0][1] && cy <= range[1][1];
      };
    } else if (chart.status === "herizontal") {
      return function (d) {
        var cx = yScale(d.y), cy = -xScale(d.x);
        return cx >= range[0][0] && cx <= range[1][0] &&
          cy >= range[0][1] && cy <= range[1][1];
      };
    }
  }

  function brushFilter() {
    return (d3.event.button !== 1);
  }

  function onBrushStart() {
    gChart.selectAll("path").classed("d3-active", false);
    contextMenu.hide();
  }

  function onBrushMove() {
    var range = d3.event.selection ||
      d3.brushSelection(gBrush.node());

    gChart.selectAll("path")
      .classed("d3-active", brushGet(range));

  } // onBrushMove END
  function onBrushEnd() {
    var range, paths, arr = [], xVal, names;

    range = d3.brushSelection(this);
    if (!chart.onselected || !range) {
      return;
    }

    paths = gChart.selectAll("path").filter(brushGet(range));
    paths.each(function (d) {
      arr.push(d);
    });
    xVal = _.chain(arr).pluck("x").uniq().value();
    names = _.chain(arr).pluck("name").uniq().value();

    chart.onselected([
      {field: opt.annexField, value: names}
    ]);

  } // onBrushEnd END
  function onBrushContextMenu() {
    d3.event.preventDefault();
    onBrushMove();

    var nodes = gChart.selectAll(".d3-active").nodes();
    //if ((nodes.length === 0) || (nodes.length === cData.length)) { return; }

    contextMenu.setTargetNode(nodes)
      .setCurrentMenu(menuOnBrush)
      .show();
  }

  function onBlankBrushContextMenu() {
    d3.event.preventDefault();
    contextMenu.setCurrentMenu(menuOnBlank).show();
  }


  function init() {
    if (!cData) {
      cData = opt.values.map(function (d, i) {
        d._index = i;
        return d;
      });
    }

    opt.size.width = opt.size._w;
    opt.size.height = opt.size._h;

    svg.html("")
      .attr("width", opt.size.width)
      .attr("height", opt.size.height);
    gLeft = gBottom = gBrush = gChart = gText = gLegend = null;
    gLeft = svg.append("g").attr("class", "axis gLeft");
    gBottom = svg.append("g").attr("class", "axis gBottom");
    gBrush = svg.append("g").attr("class", "gBrush");
    gChart = svg.append("g").attr("class", "gChart");
    gText = svg.append("g").attr("class", "gText");
    gLegend = svg.append("g").attr("class", "gLegend");

    symboleSizeScale = d3.scaleLinear().range([cMin, cMax]);
    if (chart.isZExist) {
      symboleSizeScale.domain(d3.extent(cData.map(function (d) {
        return d.z;
      })));
    } else {
      symboleSizeScale.domain([0, 1]);
    }

    if (chart.isNameExsit) {
      legend = d3.util.hlegend().groupMaxLength(90);
    }

    // Creating a new symbol when on initialising
    // Or utilise the original one and update "type" and "size".
    symbol = symbol || (d3.symbol()
      .type(function (d) {
        return d3[d.type || ""] || d3[opt.symboleName] || d3.symbolCircle;
      })
      .size(function (d) {
        return chart.isZExist ? symboleSizeScale(d.z) : symboleSizeScale(opt.scale);
      }));

    scale_Axis_Setting(cData);

    if (chart.status === "vertical") {
      verticalLayout();
    } else if (chart.status === "herizontal") {
      herizontalLayout();
    }

  } // init END

  function herizontalLayout() {
    chart.status = "herizontal";

    yScale.range([0, minGraphDim]);
    xScale.range([0, minGraphDim]);
    yAxis = d3.axisBottom().scale(yScale);
    xAxis = d3.axisLeft().scale(xScale);

    var xAxisGen, yAxisGen, xConfig = {
      scale: xScale,
      axis: xAxis,
      holder: gLeft,
      orient: "left",
      label: xLbel,
      style: opt.axisStyle
    };

    if (chart.isOrdinalX) {
      xAxisGen = d3.util.axis.ordinal(xConfig);
    } else {
      xAxisGen = d3.util.axis.linear(xConfig);
    }

    yAxisGen = d3.util.axis.linear({
      scale: yScale,
      axis: yAxis,
      holder: gBottom,
      orient: "bottom",
      label: yLbel,
      style: opt.axisStyle
    });

    var hWidth, vHeigth, left, bottom;
    setSVGLayout(hWidth, vHeigth, left, bottom,
      xAxisGen, yAxisGen, xScale, yScale);

    var update = gChart.selectAll("path").data(cData);
    update
      .enter()
      .append("path")
      .on("mouseenter", onMouseEnter)
      .on("mouseleave", onMouseLeave)
      .on("contextmenu", onContextMenu)
      .on("click", onClick)
      .merge(update)
      .each(function (d) {
        var self = d3.select(this);
        self.attr("d", symbol)
          .attr("transform", "translate(" + [yScale(d.y), -xScale(d.x)] + ")")
          .attr("fill", "transparent")
          .attr("stroke", d.color || "black")
          .attr("stroke-width", 1);
      });

    update.exit().remove();

    var textCoordinates;
    textCoordinates = cData.map(function (d) {
      return {
        x: yScale(d.y),
        y: -xScale(d.x),
        //text: numFix(d.y),
        //text: d.y.toMeasureFormart(opt.rowMeasure.numberFormat),
        text: formator.y ? d.y.toMeasureFormart(formator.y) : d.y,
        _index: d._index
      };
    });

    gText.selectAll("text")
      .data(textCoordinates)
      .enter()
      .append("text")
      .each(function (d) {
        d3.select(this)
          .attr("x", d.x).attr("y", d.y).text(d.text);
      });

    chart.label = null;
    chart.label = new d3.util.TextDecorate(
      gText, 5, [[0, -vHeigth], [hWidth, 0]], textStyle);
    chart.label.init();

  } // herizontalLayout END

  function verticalLayout() {
    chart.status = "vertical";

    xScale.range([0, minGraphDim]);
    yScale.range([0, minGraphDim]);
    xAxis = d3.axisBottom().scale(xScale);
    yAxis = d3.axisLeft().scale(yScale);

    var xAxisGen, yAxisGen, xConfig = {
      scale: xScale,
      axis: xAxis,
      holder: gBottom,
      orient: "bottom",
      label: xLbel,
      style: opt.axisStyle
    };

    if (chart.isOrdinalX) {
      xAxisGen = d3.util.axis.ordinal(xConfig);
    } else {
      xAxisGen = d3.util.axis.linear(xConfig);
    }
    yAxisGen = d3.util.axis.linear({
      scale: yScale,
      axis: yAxis,
      holder: gLeft,
      orient: "left",
      label: yLbel,
      style: opt.axisStyle
    });

    var hWidth, vHeigth, left, bottom;
    setSVGLayout(hWidth, vHeigth, left, bottom,
      yAxisGen, xAxisGen, yScale, xScale);

    var update = gChart.selectAll("path").data(cData);
    update
      .enter()
      .append("path")
      .on("mouseenter", onMouseEnter)
      .on("mouseleave", onMouseLeave)
      .on("contextmenu", onContextMenu)
      .on("click", onClick)
      .merge(update)
      .each(function (d) {
        var self = d3.select(this);
        self.attr("d", symbol)
          .attr("transform", "translate(" + [xScale(d.x), -yScale(d.y)] + ")")
          .attr("fill", "transparent")
          .attr("stroke", d.color || "black")
          .attr("stroke-width", 1);
      });

    update.exit().remove();

    var textCoordinates;
    textCoordinates = cData.map(function (d) {
      return {
        x: xScale(d.x),
        y: -yScale(d.y),
        //text: numFix(d.y),
        //text: d.y.toMeasureFormart(opt.rowMeasure.numberFormat),
        text: formator.y ? d.y.toMeasureFormart(formator.y) : d.y,
        _index: d._index
      };
    });

    gText.selectAll("text")
      .data(textCoordinates)
      .enter()
      .append("text")
      .each(function (d) {
        d3.select(this)
          .attr("x", d.x).attr("y", d.y).text(d.text);
      });

    chart.label = null;
    chart.label = new d3.util.TextDecorate(gText,
      //Math.sqrt(symbolSizeScale(opt.scale)),
      5,
      [[0, -vHeigth], [hWidth, 0]],
      textStyle);
    chart.label.init();

  } // verticalLayout END

  function setSVGLayout(hWidth, vHeigth, left, bottom,
                        leftGen, btmGen, leftScale, btmScale) {

    var temp, yBox, xBox, size, svgSize = opt.size;

    // Step1: get width of yAxis
    var yWidth = leftGen.getSize().width;

    if (chart.isNameExsit) {
      margin.top = leftGen._renderResults.tick.height + 15;
    } else {
      margin.top = 10;
    }

    // Step2: set xAxis scale range and get xAxis height
    temp = svgSize.width - margin.left - margin.right - yWidth;
    var xHeight = btmGen.range([0,
      temp < minGraphDim ? minGraphDim : temp])
      .getSize().height;

    // Step3: set yAxis range (height)
    temp = svgSize.height - margin.top - margin.bottom - xHeight;
    leftGen.range([0,
      temp < minGraphDim ? minGraphDim : temp]);

    // re-assure
    yBox = leftGen.getSize();
    xBox = btmGen.getSize();
    size = {
      width: yBox.width + xBox.width + margin.left + margin.right,
      height: xBox.height + yBox.height + margin.top + margin.bottom
    };

    if (svgSize.width < size.width) {
      svgSize.width = size.width;
    }
    if (svgSize.height < size.height) {
      svgSize.height = size.height;
    }

    left = leftGen.getSize();
    bottom = btmGen.getSize();
    hWidth = bottom.width;
    vHeigth = left.height;

    svg.attr("width", svgSize.width)
      .attr("height", svgSize.height);


    brush = null;
    brush = d3.brush()
      .extent([[0, -vHeigth], [btmScale.range()[1], 0]])
      .filter(brushFilter)
      .on("start", onBrushStart)
      .on("brush", onBrushMove)
      .on("end", onBrushEnd);

    // re-assign dimension for each group and generate axis
    gLeft.attr("transform", "translate(" + [left.width + margin.left, margin.top] + ")");
    gBottom.attr("transform", "translate(" + [left.width + margin.left, margin.top + vHeigth] + ")");
    gBrush.attr("transform", "translate(" + [left.width + margin.left, margin.top + vHeigth] + ")");
    gChart.attr("transform", "translate(" + [left.width + margin.left, margin.top + vHeigth] + ")");
    gText.attr("transform", "translate(" + [left.width + margin.left, margin.top + vHeigth] + ")");
    gLegend.attr("transform", "translate(" + [left.width + margin.left, 10] + ")");


    leftGen.drawAxis();
    btmGen.drawAxis();
    gBrush.call(brush);

    if (chart.isNameExsit) {
      legend.constraintWidth(hWidth)
        .container(gLegend)
        .data(getLegends())
        .fontFamily(leftGen.style.fontFamily)
        .fontSize(leftGen.style.fontSize)
        .fontColor(leftGen.style.fontColor)
        .constraintedSketch();
    }

    gBrush.select(".selection").on("contextmenu", onBrushContextMenu);
    gBrush.select(".overlay").on("contextmenu", onBlankBrushContextMenu);

  } // setSVGLayout END

  function scale_Axis_Setting(data) {

    function filter(p) {
      return function (d) {
        return d[p];
      };
    }

    function enLarge(arr) {
      var delta;
      if (arr[0] == arr[1]) {
        delta = arr[0] * 0.5 || 1;
        return [arr[0] - delta, arr[0] + delta];
      }

      delta = (arr[1] - arr[0]) * 0.1;
      return [arr[0] - delta, arr[1] + delta];
    }

    yExtent = d3.extent(data, filter("y"));
    yExtent = enLarge(yExtent);
    yScale = d3.scaleLinear().domain(yExtent);

    if (chart.isOrdinalX) {
      xExtent = data.map(function (d) {
        return d.x;
      });
      xScale = d3.scalePoint().domain(xExtent).padding(0.3);
    } else {
      xExtent = d3.extent(data, filter("x"));
      xExtent = enLarge(xExtent);
      xScale = d3.scaleLinear().domain(xExtent);
    }

    if (chart.isZExist) {
      zExtent = d3.extent(data, filter("z"));
      zScale = d3.scaleLinear().domain(zExtent).range([cMin, cMax]);
    }

  } // scale and axis setting END


  chart.getLegends = getLegends;
  chart.delete = function (indexes) {
    if (indexes.length === 0) {
      return;
    }
    if (indexes.length === cData.length) {
      return alert("至少需保留 1 个节点。");
    }

    var values = [];
    indexes.forEach(function (index) {
      var pos = _.findIndex(cData, function (d) {
        return d._index === index;
      });
      if (pos > -1) {
        values.push(cData.splice(pos, 1)[0]);
        init();
      }
    });


    if (this.onRemoveItem && values.length) {
      this.onRemoveItem([{
        field: opt.annexField,
        value: values.map(d => d.name)
      }]);
    }
  };

  // customized tooltip content formate
  chart.tipFormat = function (s) {
    if (!arguments.length) {
      return opt.tipFormat;
    }
    opt.tipFormat.input(s);
    return this;
  };

  // symbolCircle, symbolCross, symbolDiamond, symbolStar, symbolTriangle
  chart.setSymbol = function (str) {
    symbol.type(function (d) {
      return d3[str] || d3[opt.symboleName] || d3.symbolCircle;
    });
    gChart.selectAll("path").attr("d", symbol);
  };

  chart.reSize = function (size) {
    if (!arguments.length) {
      return opt.size;
    }
    opt.size.width = opt.size._w = size.width;
    opt.size.height = opt.size._h = size.height;

    init();

    return this;
  };

  chart.getScale = function () {
    return opt.scale;
  };

  chart.setScale = function (val) {
    if (typeof val === "string") {
      val = +val;
    }
    if (val > 1 || val < 0) {
      return;
    }
    if (this.isZExist) {
      return;
    }

    opt.scale = val;
    symbol.size(symboleSizeScale(opt.scale));
    gChart.selectAll("path").attr("d", symbol);
  };

  chart.reverse = function () {
    if (chart.status === "vertical") {
      herizontalLayout();
    } else if (chart.status === "herizontal") {
      verticalLayout();
    }
    init();
  };

  chart.export = function (type) {
    d3.util.exportChart(svg.node(), type || "png");
  };

  chart.removeChart = function () {
    svg.remove();
    if (tooltip) {
      d3.select(tooltip).remove();
    }
    contextMenu.remove();

    tooltip = null;
    contextMenu = null;
    container = null;
    opt = null;
    legend = null;

    chart = null;
  };

  //chart.init().draw();
  init();

  return chart;

} // scatter function END


export const ScatterChart = {
  build: buildChart
};

