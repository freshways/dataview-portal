/* global d3, undersore, _, document, window */
/* jshint -W119 */

import d3 from '@/components/d3-util'


const buildChart = function (svgSelector, data, columnFields, rowFields, options) {

  const cOpt = {};
  _.extend(cOpt, options.chartConfig || {});
  const measureField = columnFields.concat(rowFields).find(function (d) {
    return d.slaveType == 1;
  });
  if (!measureField && !data.length) {
    return;
  }
  cOpt.rightText = parseInt(data[0][measureField.name]);
  if (!cOpt.leftText) {
    cOpt.leftText = measureField.name;
  }
  cOpt.countDown = (options.autoUpdate || 1) * 60 * 1000 * 0.8;

  return flipper(svgSelector, cOpt);
};


function flipper(selector, options) {

  let opt = {
    leftText: "XXX",
    leftTextColor: "gary",
    leftTextFamily: "华文细黑",
    leftTextSize: 13,
    rightText: 237493,
    rightTextColor: "#fa8c16",
    rightTextFamily: "微软雅黑",
    rightTextSize: 25,
    align: "left", // "left", "center", "right" or ""
    rectBgColor: "#595959",
    rectBgPadding: {top: 3, right: 15, bottom: 3, left: 15},
    rectBgMargin: {top: 5, right: 5, bottom: 5, left: 5},
    countDown: 1 * 60 * 1000 * 0.8
  };

  _.extend(opt, options);
  if (opt.rightTextSize < opt.leftTextSize) {
    opt.leftTextSize = opt.rightTextSize;
  }

  function number2string(digit) {
    return digit.toString().split("");
  }

  function textRenderTesting(holder) {
    let size = {width: 0, height: 0}, leftTextRect;
    holder.html("")
      .selectAll("text")
      .data(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
      .enter()
      .append("text")
      .text(d => d)
      .attr("font-family", opt.rightTextFamily)
      .attr("font-size", opt.rightTextSize)
      .each(function () {
        let rect = this.getBBox();
        size.width = Math.max(size.width, rect.width);
        size.height = Math.max(size.height, rect.height);
      });


    let text = holder.append("text")
      .text(opt.leftText)
      .attr("font-family", opt.leftTextFamily)
      .attr("font-size", opt.leftTextSize);
    leftTextRect = text.node().getBBox();
    leftTextRect = {
      width: Math.ceil(leftTextRect.width),
      height: Math.ceil(leftTextRect.height)
    };
    size.width = Math.ceil(size.width);
    size.height = Math.ceil(size.height);
    holder.html("");


    return {
      leftTextRect: leftTextRect,
      digitDim: size
    };
  } // textRenderTesting END

  function dataGenerator() {

    let digitSize, leftTextSize, textInput = [], rect;
    let xMargin = Math.max(opt.rectBgMargin.left,
      opt.rectBgMargin.right);

    function getLayout() {
      let increment = opt.rectBgMargin.left;
      let layout = textInput.map(function (d) {
        let obj = {
          text: d,
          x: increment,
          y: opt.rectBgMargin.top,
          width: rect.width,
          height: rect.height,
        };
        increment += rect.width + xMargin;
        return obj;
      });
      let right = {
        width: increment - xMargin + opt.rectBgMargin.left + opt.rectBgMargin.right,
        height: rect.height + opt.rectBgMargin.top + opt.rectBgMargin.bottom,
        layout: layout
      };
      return {
        right: right,
        chart: {
          width: opt.rectBgMargin.left + leftTextSize.width + right.width,
          height: right.height
        }
      };
    }

    return {
      digitSize: function (_) {
        if (!arguments.length) {
          return digitSize;
        }
        digitSize = _;
        rect = {
          width: digitSize.width +
            opt.rectBgPadding.left +
            opt.rectBgPadding.right,
          height: digitSize.height +
            opt.rectBgPadding.top +
            opt.rectBgPadding.bottom,
        };
        return this;
      },
      leftTextSize: function (_) {
        if (!arguments.length) {
          return leftTextSize;
        }
        leftTextSize = _;
        return this;
      },
      textInput: function (_) {
        if (!arguments.length) {
          return textInput;
        }
        textInput = _;
        return this;
      },
      getLayout: getLayout
    };

  } // dataGenerator END

  function flip(time) {
    return function (text) {
      text.transition()
        .duration(time || 3000)
        .tween("text", function (j) {
          let that = d3.select(this),
            test = "0123456789",
            i = d3.interpolateNumber(+that.text(), +j.text);
          if (test.indexOf(that.text()) == -1 ||
            test.indexOf(j.text) == -1) {
            return function () {
              that.text(j.text);
            };
          }
          return function (t) {
            that.text(Math.floor(i(t)));
          };
        });
    };
  }

  /**
   * @param {d3-selection} svg
   * @param {String} align : "left", "center" or "right"
   */
  function alignSVG(svg, align) {
    if (align == "center") {
      svg.style("margin-left", "auto").style("margin-right", "auto");
    } else if (align == "right") {
      svg.style("margin-left", "auto").style("margin-right", 0);
    }
  }

  function gEnter(d) {
    let self = d3.select(this);
    self.attr("class", "textBlock")
      .attr("transform", "translate(" + [d.x, d.y] + ")");

    self.append("rect")
      .attr("width", d.width)
      .attr("height", d.height)
      .attr("rx", 3)
      .attr("fill", opt.rectBgColor);
    self.append("text")
      .attr("fill", opt.rightTextColor)
      .attr("font-family", opt.rightTextFamily)
      .attr("font-size", opt.rightTextSize)
      .attr("x", opt.rectBgPadding.left)
      .attr("y", d.height / 2 + opt.rightTextSize * 0.1)
      .attr("alignment-baseline", "middle")
      .text(0);
  }

  let svg, svgHolder, dataEngine = dataGenerator(), testResults;
  let timer; // d3.timer()

  function init() {
    let layout, chartSize, rightLayout, parent;
    let node = $(selector).children('#chart')[0];
    parent = d3.select(node).node()
    svgHolder = d3.select(parent).html("")
      .append("div")
      .attr("class", "d3-svgHolder");
    svg = svgHolder
      .append("svg")
      .style("display", "block")
      .attr("width", 10)
      .attr("height", 10)
      .attr("version", "1.1")
      .attr("xmlns", "http://www.w3.org/2000/svg");

    alignSVG(svg, opt.align);

    testResults = textRenderTesting(svg);

    dataEngine
      .digitSize(testResults.digitDim)
      .leftTextSize(testResults.leftTextRect)
      .textInput(["0"]);
    layout = dataEngine.getLayout();
    chartSize = layout.chart;
    rightLayout = layout.right;


    svg.append("g")
      .attr("class", "leftText")
      .attr("transform", "translate(" +
        [opt.rectBgMargin.left, 0] + ")")
      .append("text")
      .text(opt.leftText)
      .attr("fill", opt.leftTextColor)
      .attr("font-family", opt.leftTextFamily)
      .attr("font-size", opt.leftTextSize)
      .attr("alignment-baseline", "middle")
      .attr("y", chartSize.height / 2);
    svg.append("g")
      .attr("class", "rightText")
      .attr("transform", "translate(" +
        [chartSize.width - rightLayout.width, 0] + ")")
      .selectAll("g")
      .attr("class", "textBlock")
      .data(rightLayout.layout)
      .enter()
      .append("g")
      .each(gEnter);

    update([{abc: opt.rightText}],
      {rightText: 0, countDown: 3000});
  }

  function setValue(val) { // [{ abc: 23434 }]
    let layout, chartSize, rightLayout, fresh;

    dataEngine.textInput(number2string(val));
    layout = dataEngine.getLayout();
    chartSize = layout.chart;
    rightLayout = layout.right;

    fresh = svg.select(".rightText")
      .selectAll(".textBlock")
      .data(rightLayout.layout);

    // Data re-bind
    fresh.each(function (d) {
      let g = d3.select(this);
      g.select("rect").datum(d);
      g.select("text").datum(d);
    });

    fresh.enter()
      .append("g")
      .attr("class", "textBlock")
      .each(gEnter)
      .merge(fresh)
      .selectAll("text")
      .each(function () {
        d3.select(this)
          .text(function (d) {
            return d.text;
          });
      });
    fresh.exit().remove();

    svg.attr("width", chartSize.width)
      .attr("height", chartSize.height);
  }

  /**
   * @param {Array} data : [{abc: 2334345}]
   * @param {Object} obj : opt
   */
  function update(data, obj) {
    let previous, current, delta, interpolater;
    if (!obj) {
      obj = opt;
    }

    current = parseInt(_.values(data[0])[0]);
    previous = obj.rightText;
    interpolater = d3.interpolateNumber(previous, current);
    obj.rightText = current;
    delta = 1 / (obj.countDown || 5000) * 20;

    if (timer) {
      setValue(previous);
      timer.stop();
    }

    let t = 0;
    timer = d3.timer(function () {
      if (t >= 1) {
        setValue(current);
        timer.stop();
      } else {
        setValue(Math.floor(interpolater(t)));
      }
      t += delta;
    });
  }

  // Chart initialisation:
  init();


  return {
    update: update,
    reSize: function () {
    },
    removeChart: function () {
      svgHolder.remove();
      svgHolder = this.update = this.reSize = null;
    }
  };
}


export const FlipperChart = {
  build: buildChart
};



































