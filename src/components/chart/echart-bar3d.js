﻿import {colorHelper} from "@/utils/colorHelper";
import * as echarts from 'echarts'
import 'echarts-gl'

const chart = function (dom_container, opt) {
  var datamarkingopt = opt.others.datamarking;
  var datamarkingmapping = {}
  delete opt.others.datamarking;
  //处理数据脱敏
  if (datamarkingopt) {
    datamarkingopt.dimensions.forEach(function (dko) {
      if (dko.DataMarkingEnable) {
        datamarkingmapping[dko.name] = true;
      }
    });
  }
  var self = this;
  var myChart = echarts.init(dom_container);
  var chart_opt = {
    tooltip: {
      trigger: 'item',
      formatter: function (args) {
        var encode = chart_opt.series[0].encode;
        return encode.x + ":" + args.data[encode.x] + "<br>" + encode.y + ":" + args.data[encode.y] + "<br>" + encode.z + ":" + args.data[encode.z].toMeasureFormart(chart_opt.others.measureField.numberFormat);
      }
    },
    xAxis3D: {
      axisLabel: {
        formatter: function (value) {
          var result = value;
          if (datamarkingopt && datamarkingmapping[opt.others.dimensions[0].name]) {
            result = result.marking();
          }
          return result;
        }
      }
    },
    yAxis3D: {
      axisLabel: {
        formatter: function (value) {
          var result = value;
          if (datamarkingopt && datamarkingmapping[opt.others.dimensions[1].name]) {
            result = result.marking();
          }
          return result;
        }
      }
    }
  }
  var labelFormatter = function (args) {
    return args.value[chart_opt.others.measureField.name].toMeasureFormart(chart_opt.others.measureField.numberFormat);
  }
  var setOption = function (opt) {
    chart_opt = $.extend(true, chart_opt, opt);
    chart_opt.series.forEach(function (d) {
      d.label.formatter = labelFormatter;
    });
    myChart.setOption(chart_opt);
  }
  //响应点击事件
  myChart.on("click", function (args) {
    if (args.componentType === "series") {
      var d0 = chart_opt.others.dimensions[0];
      var d1 = chart_opt.others.dimensions[1];
      var calldata = [{
        field: d0,
        value: [args.data[d0.name]]
      }, {
        field: d1,
        value: [args.data[d1.name]]
      }];
      if (self.onselected) {
        self.onselected(calldata);
      }
    }
  });
  if (opt) {
    setOption(opt);
  }
  this.setOption = setOption;
  this.removeChart = function () {
    myChart.dispose();
  }
  this.update = function (data) {
    chart_opt.visualMap.min = Enumerable.from(data).min(function (d) {
      return d[chart_opt.others.measureField.name];
    });
    chart_opt.visualMap.max = Enumerable.from(data).min(function (d) {
      return d[chart_opt.others.measureField.name];
    });
    chart_opt.dataset.source = data;
    setOption(chart_opt);
  }
  this.resize = myChart.resize
};
/**
 * 3D条形图
 */
const buildChart = function (svgSelector, data, columnFields, rowFields, options) {
  var fields = columnFields.concat(rowFields);
  var dimensions = fields.filter(function (d) {
    return d.slaveType === 0;
  });
  if (dimensions.length < 2) {
    return false;
  }
  var measureField = fields.find(function (d) {
    return d.slaveType === 1;
  });
  if (!measureField) {
    return false;
  }
  var datefield = dimensions.find(function (d) {
    return d.dataType === 4;
  });
  if (datefield) {
    data = Enumerable.from(data).orderBy(function (d) {
      return d[datefield.name];
    }).toArray();
  }
  var config = options.chartConfig;
  if (!config) {
    config = {
      GlobalColor: "#4E79A7",
      scale: 0.8,
      axisStyle: {
        tickColor: "#000",
        fontColor: "#000",
        fontFamily: "Arial",
        fontSize: 12
      },
      textStyle: {
        "display": "none",
        "font-family": "Arial",
        "font-size": 12,
        "font-style": "normal",
        "font-weight": "normal",
        "fill": "#333",
        "position": "top"
      }
    }
  }
  var globalcolor = config.GlobalColor;
  //轴文字样式
  var axis_nameTextStyle = {
    color: config.axisStyle.fontColor,
    fontSize: config.axisStyle.fontSize,
    fontFamily: config.axisStyle.fontFamily
  }
  //数据条标签配置
  var labelOption = {
    show: config.textStyle["display"] !== "none",
    distance: 5,
    textStyle: {
      fontSize: config.textStyle["font-size"],
      fontFamily: config.textStyle["font-family"],
      color: config.textStyle["fill"]
    }
  };

  //轴线配置
  var axis_axisLine = {
    lineStyle: {
      color: config.axisStyle.tickColor
    }
  }
  var option_chart = {
    grid3D: {
      splitLine: axis_axisLine
    },
    xAxis3D: {
      type: 'category',
      name: dimensions[0].name,
      nameTextStyle: axis_nameTextStyle,
      axisLine: axis_axisLine,
      axisTick: axis_axisLine,
      splitLine: axis_axisLine,
      axisLabel: axis_nameTextStyle,
      axisPointer: {
        show: false
      }
    },
    yAxis3D: {
      type: 'category',
      name: dimensions[1].name,
      nameTextStyle: axis_nameTextStyle,
      axisLine: axis_axisLine,
      axisTick: axis_axisLine,
      splitLine: axis_axisLine,
      axisLabel: axis_nameTextStyle,
      axisPointer: {
        show: false
      }
    },
    zAxis3D: {
      type: "value",
      nameTextStyle: axis_nameTextStyle,
      name: measureField.name,
      axisLine: axis_axisLine,
      axisTick: axis_axisLine,
      splitLine: axis_axisLine,
      axisLabel: axis_nameTextStyle,
      axisPointer: {
        show: false
      }
    },
    visualMap: {
      min: Enumerable.from(data).min(function (d) {
        return d[measureField.name];
      }),
      max: Enumerable.from(data).max(function (d) {
        return d[measureField.name];
      }),
      dimension: measureField.name,
      calculable: true,
      inRange: {
        color: [colorHelper.calc_gradient_color(globalcolor, 100, 10), globalcolor]
      },
      textStyle: {
        color: config.axisStyle.fontColor
      }
    },
    dataset: {
      source: data
    },
    series: [
      {
        type: 'bar3D',
        shading: 'lambert',
        encode: {
          x: dimensions[0].name,
          y: dimensions[1].name,
          z: measureField.name
        },
        label: labelOption
      }
    ],
    others: {
      dimensions: dimensions,
      measureField: measureField,
      datamarking: options.DataMarking
    }
  }
  const dom_container = $(svgSelector).children('#chart')[0];
  const chartInstance = new chart(dom_container, option_chart);
  return chartInstance;
};
export const Bar3DChart = {
  build: buildChart
}

