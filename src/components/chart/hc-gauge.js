/* global Highcharts, Hcharts, d3, undersore, _, require, define */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {

        /*
        options.RankMark = [{Key: "xx", Value: 123}, {Key: "xx", Value: 123}]
        data = [{"统计(数量)":10000}]
        options.gauge = { max: 23, min: 3454 };
        */

        var range, label, current, chart, hConfig;

        label = _.keys(data[0])[0];
        current = data[0][label];
        hConfig = options.chartConfig || {
            MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D"],
            axisStyle: {
                tickColor : "#000", fontColor : "#000",
                fontFamily: "Arial", fontSize  : 15
            },
            textStyle: {
                "display"     : "none",
                "font-family" : "Arial",
                "font-size"   : 15,
                "font-style"  : "normal",
                "font-weight" : "normal",
                "fill"        : "#333",
            }
        };
        chart = Hcharts.gauge(svgSelector);


        // At lest 4 colors to be given: first 3 for plotBand, last one for pointer
        if (!hConfig.MultiColors || hConfig.MultiColors.length < 4) {
            hConfig.MultiColors = ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D"];
        }


        // After setting the current value
        // we can use some prototype methods, such as:
        // getCurrent, halfLayout, getRange
        chart.config.series[0].data = [current];
        chart.config.series[0].dataLabels = chart.getDataLabels(hConfig.textStyle);


        // Assign the max & min value of Y axis
        if (!options.gauge) { chart.halfLayout(); }
        else {
            range = chart.validateCheck(options.gauge);
            if (range) {
                chart.config.yAxis.min = range.min;
                chart.config.yAxis.max = range.max;
            }
            else { chart.halfLayout(); }
        }


        _.extend(chart.config.yAxis, chart.getAxisStyle(hConfig.axisStyle));
        _.extend(chart.config.yAxis.title, { text: label, y: 40 });
        chart.config.yAxis.plotBands = chart.getPlotBands(
            chart.config.yAxis.min,
            chart.config.yAxis.max,
            hConfig.MultiColors
        );

        // Assign the pointer color
        chart.config.plotOptions = {
            gauge: chart.getGauge(hConfig.MultiColors[3])
        };


        chart.init();
        return chart;
    };


    module.exports = {
        build: buildChart
    };

});
