﻿/* global d3, undersore, _, document, window */
/* jshint -W119 */

/**
 * bar chart
 */

define(function (require, exports, module) {
    var modules={
        ShareData:require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options){

        // 如果用户输入数据不足直接退出
        if (!columnFields[0] || !rowFields[0]) { return; }
        var dimension = columnFields[0].slaveType === 0 ? columnFields[0] : rowFields[0];
        var measure   = rowFields[0].slaveType === 1 ? rowFields[0] : columnFields[0];

        var reverse, tipFormat, color, scale,
            config = options.chartConfig;
        if (!config) {
            config = {
                GlobalColor: "#4E79A7",
                MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B", "#FFBE7D"],
                scale: 0.8,
                axisStyle: {
                    tickColor : "#000",
                    fontColor : "#000",
                    fontFamily: "Arial",
                    fontSize  : 15
                },
                textStyle: {
                    "display"     : "none",
                    "font-family" : "Arial",
                    "font-size"   : 15,
                    "font-style"  : "normal",
                    "font-weight" : "normal",
                    "fill"        : "#333",
                    "position"    : "top"
                }
            };
        }
        (function () {

            var dKey = dimension.name,
                mKey = measure.name;

            tipFormat = d3.util.tipParse(
                [{ key: dKey, value: "x" }, { key: mKey, value: "y" }],
                options.TooltipFormat);

            color = config.MultiColors || [];
            if (color.length < 4) {
                color = color.concat(["#00082a", "#293042", "#60909c", "#ff8252"]);
            }
        })();


        /* SlaveType: 1 -> dimension || 0 -> measure */
        reverse = !!columnFields[0].slaveType;
        scale = ("scale" in config) ? config.scale : 0.8;


        return trend (svgSelector, {
            values: data,
            isReverse: reverse,
            scale: scale,
            color: color,
            tipFormat: tipFormat,
            colDimension: dimension,
            rowMeasure: measure,
            size: options.size,
            textStyle: config.textStyle || {},
            axisStyle: config.axisStyle || {}
        });
    };



function trend (selector, options) {
    var opt = {
        //size    : { width: 450, height: 300 },
        values  : [],
        color   : d3.schemeCategory20c,
        isReverse   : false,

        scale   : 0.8, // range: [0, 1], oScale padding
        tipFormat   : null,
        colDimension: null,
        rowMeasure  : null,
        textStyle   : {},
        axisStyle   : {}
    };

    opt = _.extend(opt, options);

    // output object
    var chart = {
        status: opt.isReverse ? "herizontal" : "vertical",
        onselected: null
    };

    var container = d3.select(selector).node().parentElement;
    if (!opt.size) {
        opt.size = d3.util.getInnerBoxDim (container);
    }
    opt.size._w = opt.size.width;
    opt.size._h = opt.size.height;

    var svg, gMain, gLeft, gRight, gBottom,
        gTop, gBar, gLegend, gText, gBrush, // d3 selections

        sLeft, sRight, sBottom, sTop, barSclae, // scales
        lineWidth = 2, symbolSize = 84,
        percentFormat = d3.format(".1%"),
        tooltip, contextMenu,
        minGraphDim = 100, innerHeight, innerWidth,
        line = d3.line(),
        abs = Math.abs, ceil = Math.ceil,
        textOffset = 3, tickMap = [],
        textStyle = {
            "display"     : "none",    // "none" or "block"
            "font-family" : "Arial",   // "Arial", "Helvetica", "Times"
            "font-size"   : 12,
            "font-style"  : "normal",  // "normal" or "italic"
            "font-weight" : "normal",  // "normal" or "bold"
            "fill"        : "#ff0000", // text color
            "position"    : "top"      // "top", "right", "bottom" or "left"
        },
        margin = { top: 30, right: 10, bottom: 10, left: 10 },
        legendState = { current: false, compare: false, YoYRate: false, QoQRate: false },
        percentLimit = 100, cData;

    _.extend(textStyle, opt.textStyle);

    svg = d3.select(container)
        .html("")
        .append("svg")
        .attr("class", "Chart-To-Be-Export")
        .attr("width", opt.size.width)
        .attr("height", opt.size.height)
        .attr("version", "1.1")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .style("font", "12px arial, sans-serif")
        .on("contextmenu", onBlankContextMenu);

    var getTooltip = function () {
        return tooltip || (
            tooltip = d3.select(document.body)
                .append("div")
                .attr("class", "d3-ttip")
                .node());
    };
    var menuOnSingleItem = [
        {title: "删除本组节点", symbol: "&#10008;", action: "delete"},
        { title: "导出SVG", symbol: "&#x1F4F7;", action: "export", p1: "svg" },
        { title: "导出PNG", symbol: "&#x1F4F7;", action: "export", p1: "png" }
    ];
    var menuOnBlank = menuOnSingleItem.slice(1);
    var menuOnBrush = [{title: "删除框选节点", symbol: "&#10008;", action: "deleteAll"}];
    contextMenu = d3.util.getChartContextMenu();
    contextMenu.clickFun = function (command, target) {
        this.hide();
        switch (command.action) {
            case "delete" : chart.delete([target.x]); break;
            case "deleteAll" : chart.delete(target); break;
            case "export" : chart.export(command.p1); break;
            default: console.log("... Catch YOU! ...");
        }
    };


    function dataTransform () {
        var dimName = opt.colDimension.name,
            meaName = opt.rowMeasure.name,
            yExtent,
            rExtent;

        cData = { bars: [], YoYRate: [], QoQRate: [],
                  category: [],
                  textPoints: { current: [], compare: [], YoYRate: [], QoQRate: [] }
                };

        function validCheck (val) {
            return typeof(val) == "number" ?
                    (abs(val / 100) >= percentLimit ? null : val / 100) :
                    null;
        }
        opt.values.reduce(function (acc, cur) {
            var x = cur[dimName];
            acc.category.push(x);

            if ("YoYValue" in cur) {
                acc.bars.push([{
                        x: x,
                        y: cur[meaName] ? cur[meaName] : 0,
                        color: opt.color[0],
                        type: "current"
                    },
                       { x: x,
                        y: cur.YoYValue ? cur[meaName] - cur.YoYValue : 0,
                        color: opt.color[1],
                        type: "compare"
                       }]
                    );
            }
            else {
                acc.bars.push([{
                        x: x,
                        y: cur[meaName] ? cur[meaName] : 0,
                        color: opt.color[0],
                        type: "current"
                    }]);
            }

            if ("YoYRate" in cur) {
                acc.YoYRate.push({
                    x: x,
                    y: validCheck(cur.YoYRate),
                    color: opt.color[2],
                    type: "YoYRate"
                });
            }
            if ("QoQRate" in cur) {
                acc.QoQRate.push({
                    x: x,
                    y: validCheck(cur.QoQRate),
                    color: opt.color[3],
                    type: "QoQRate"
                });
            }
            return acc;
        }, cData);

        (function () {
            var yVlaues = [], rValues = [];

            cData.bars.forEach(d => {
                Array.prototype.push.apply(yVlaues, d.map(j => j.y));
            });
            yExtent = d3.extent(yVlaues);
            if (yExtent[0] > 0) { yExtent[0] = 0; }
            yExtent = [yExtent[0] * 1.1, yExtent[1] * 1.1];


            rValues = Array.prototype.concat.call(
                [], cData.YoYRate, cData.QoQRate).map(d => d.y);
            rValues = _.filter(rValues, function (d) { return d !== null; });
            rExtent = d3.extent(rValues);

            if (rExtent[0] > 0) { rExtent[0] = 0; }
            rExtent = [rExtent[0] * 1.1, rExtent[1] * 1.1];
        })();

        cData.yExtent = yExtent;
        cData.rExtent = rExtent;
    }
    function createLine (obj) {
        return function (pNode) {
            var gg = pNode.append("g").attr("class", obj.name);
            var symbol = d3.symbol().type(obj.symbolType).size(symbolSize);
            var points = obj.points.filter(d => { return d.y !== null; });
            cData.textPoints[obj.name] = points.map(d => {
                return {
                    x: obj.line.x()(d),
                    y: obj.line.y()(d) - textOffset,
                    text: percentFormat(d.y)
                };
            });
            gg.append("path")
                .datum(obj.points)
                .attr("class", "line")
                .attr("d", obj.line)
                .attr("fill", "none")
                .attr("stroke", obj.color)
                .attr("stroke-width", lineWidth);
            gg.selectAll(".point")
                .data(points)
                .enter()
                .append("path")
                .attr("class", "point")
                .attr("transform", d => {
                    return "translate(" + [obj.line.x()(d), obj.line.y()(d)] + ")";
                })
                .attr("fill", obj.color)
                .attr("stroke", "white")
                .attr("d", symbol());
        };
    }
    function createLegend (data) {
        /* data:
        [
            { label: "now", className: "gBar", symboleName, color: "blue", crossLine: false },
        ]
        */
        return function (ss) {
            var symbol = d3.symbol().size(symbolSize);
            ss.selectAll("g")
            .data(data)
            .enter()
            .append("g")
            .each(function (d) {
                var self = d3.select(this);
                var lineSpan = Math.sqrt(symbolSize) * 4;
                self.attr("class", d.className + " legend");
                if (d.crossLine) {
                    self.append("line")
                        .attr("x2", lineSpan)
                        .attr("stroke", d.color);
                    self.append("path")
                        .attr("d", symbol.type(d3[d.symboleName])())
                        .attr("fill", d.color)
                        .attr("transform", "translate(" + lineSpan / 2 + ",0)");
                    self.append("text").text(d.label)
                        .attr("dy", 3)
                        .attr("transform", "translate(" + (lineSpan + 3) + ",0)");
                }
                else {
                    self.append("path")
                        .attr("d", symbol.type(d3[d.symboleName])())
                        .attr("fill", d.color);
                    self.append("text").text(d.label)
                        .attr("dy", 3)
                        .attr("transform", "translate(" + (Math.sqrt(symbolSize) + 3) + ",0)");
                }
            });

            var acc = 0;
            ss.selectAll("g").each(function (d) {
                var box = this.getBBox();
                this.setAttribute("transform", "translate(" + [acc, box.height] + ")");
                acc += box.width + 8;
            });
        };
    }
    function createLineAndLegend (content, line, fontStyle) {
        var legendData = [];
        var legends = {
            current: { label: "当前值", className: "current",
                    symboleName: "symbolSquare", color: opt.color[0], crossLine: false },
            compare: { label: "同比值", className: "compare",
                    symboleName: "symbolSquare", color: opt.color[1], crossLine: false },
            YoYRate: { label: "同比率", className: "YoYRate",
                    symboleName: "symbolCircle", color: opt.color[2], crossLine: true },
            QoQRate: { label: "环比率", className: "QoQRate",
                    symboleName: "symbolTriangle", color: opt.color[3], crossLine: true }
        };

        if (cData.bars[0].length == 2) {
            legendData.push(legends.current, legends.compare);
        }
        else {
            legendData.push(legends.current);
        }
        if (cData.YoYRate.length) {
            legendData.push(legends.YoYRate);
            content.call(createLine({
                name: "YoYRate",
                points: cData.YoYRate,
                color: opt.color[2],
                symbolType: d3.symbolCircle,
                line: line
            }));
        }
        if (cData.QoQRate.length) {
            legendData.push(legends.QoQRate);
            content.call(createLine({
                name: "QoQRate",
                points: cData.QoQRate,
                color: opt.color[3],
                symbolType: d3.symbolTriangle,
                line: line
            }));
        }

        if (legendData.length) {
            gLegend
                .style("cursor", "pointer")
                .style("user-select", "none")
                .attr("font-size", fontStyle.fontSize)
                .attr("font-family", fontStyle.fontFamily)
                .attr("fill", fontStyle.fontColor)
                .call(createLegend(legendData))
                .selectAll("g")
                .on("click", onLegendClick);
        }
        createTextLabel();
    }
    function createTextLabel () {
        gText.attr("style", "pointer-events: none; user-select: none;")
            .attr("text-anchor", "middle")
            .selectAll("g")
            .data(_.keys(cData.textPoints))
            .enter()
            .append("g")
            .attr("class", d => d)
            .each(function (name) {
                d3.select(this)
                    .selectAll("text")
                    .data(cData.textPoints[name])
                    .enter()
                    .append("text")
                    .attr("x", d => d.x)
                    .attr("y", d => d.y)
                    .text(d => d.text);
            });

        // create text decoration API
        chart.label = new d3.util.TextDecorate(
            gText, textOffset, [[0, -innerHeight], [innerWidth, 0]], textStyle);
        chart.label.init();
    }
    function createBrush () {
        var bs, parsePoint;
        if (chart.status == "vertical") {
            bs = d3.brushX();
            parsePoint = (d) => { return sBottom(d.x) + sBottom.step() * 0.5; };
        }
        else {
            bs = d3.brushY();
            parsePoint = (d) => { return -sLeft(d.x) - sLeft.step() * 0.5; };
        }


        function onStart () {
            gMain.selectAll(".d3-active")
                 .classed("d3-active", false);
            contextMenu.hide();
        }
        function onBrush () {
            var range = d3.event.selection;
            gMain.selectAll(".point, .bar")
                .classed("d3-active", function (d) {
                    var point = parsePoint(d);
                    return point >= range[0] && point <= range[1];
                });
        }
        function onEnd () {
            var range = d3.event.selection;
            if (!range) { return; }
            var values = _.filter(tickMap, d => {
                    return d.pos >= range[0] && d.pos <= range[1];
                })
                .map(d => d.tick);

            if (chart.onselected) {
                chart.onselected([{ field: opt.colDimension, value: values }]);
            }
        }

        bs.extent([[0, -innerHeight], [innerWidth, 0]])
            .on("start", onStart)
            .on("brush", onBrush)
            .on("end", onEnd);
        return bs;
    }

    function vertical () {
        chart.status = "vertical";

        gLeft = svg.append("g").attr("class", "gLeft axis");
        gRight = svg.append("g").attr("class", "gRight axis");
        gBottom = svg.append("g").attr("class", "gBottom axis");
        gMain = svg.append("g").attr("class", "gMain");
        gBrush = gMain.append("g").attr("class", "gBrush");
        gLegend = svg.append("g").attr("class", "gLegend");
        gBar = gMain.append("g").attr("class", "gBar");
        gText = gMain.append("g").attr("class", "gText");


        barSclae = d3.scaleBand()
                .domain(cData.bars[0].length == 2 ? [0, 1] : [0])
                .round(true)
                .paddingOuter(1 - opt.scale);
        line.x(d => { return sBottom(d.x) + sBottom.step() * 0.5; })
            .y(d => -sRight(d.y))
            .defined(d => { return d.y !== null; })
            .curve(d3.curveMonotoneX);

        sLeft   = d3.scaleLinear().domain(cData.yExtent).range([0, minGraphDim]);
        sRight  = d3.scaleLinear().domain(cData.rExtent).range([0, minGraphDim]);
        sBottom = d3.scaleBand().domain(cData.category).range([0, minGraphDim])
                    .round(true).padding(0);

        var drawLeft, drawRight, drawBottom;
        drawLeft = d3.util.axis.linear({
            scale : sLeft,
            axis  : d3.axisLeft().scale(sLeft),
            holder: gLeft,
            orient: "left",

            label: opt.rowMeasure.name || "",
            style: opt.axisStyle
        });
        drawRight = d3.util.axis.linear({
            scale : sRight,
            axis  : d3.axisRight().scale(sRight),
            holder: gRight,
            orient: "right",
            tickFormate: ".0%",

            style: opt.axisStyle
        });
        drawBottom = d3.util.axis.ordinal({
            scale : sBottom,
            axis  : d3.axisBottom().scale(sBottom),
            holder: gBottom,
            orient: "bottom",

            label: opt.colDimension.name || "",
            style: opt.axisStyle
        });

        (function (yLeft, yRight, xAxis, svgSize) {
            var temp, yLBox, yRBox, xBox, size;

            // Step1: get width of yAxis
            var lWidth = yLeft.getSize().width;
            var rWidth = yRight.getSize().width;

            margin.top = yLeft._renderResults.tick.height + 15;

            // Step2: set xAxis scale range and get xAxis height
            temp = svgSize.width - margin.left - margin.right - (lWidth + rWidth);
            var xHeight = xAxis.range([0,
                            temp < minGraphDim ? minGraphDim : temp])
                        .getSize().height;

            // Step3: set yAxis range (height)
            temp = svgSize.height - margin.top - margin.bottom - xHeight;
            yLeft.range([0,
                temp < minGraphDim ? minGraphDim : temp]);
            yRight.range(yLeft.range().slice());

            // re-assure
            yLBox = yLeft.getSize();
            yRBox = yRight.getSize();
            xBox = xAxis.getSize();
            size = {
                width: yLBox.width + yRBox.width + xBox.width + margin.left + margin.right,
                height: xBox.height + yLBox.height + margin.top + margin.bottom
            };

            if (svgSize.width < size.width) { svgSize.width = size.width; }
            if (svgSize.height < size.height) { svgSize.height = size.height; }

            innerHeight = yLBox.height;
            innerWidth = xBox.width;

            svg.attr("width", opt.size.width)
                .attr("height", opt.size.height);
            yLeft.holder.attr("transform", "translate(" +
                    [margin.left + yLBox.width, margin.top] + ")");
            yRight.holder.attr("transform", "translate(" +
                    [margin.left + yLBox.width + innerWidth, margin.top] + ")");
            xAxis.holder.attr("transform", "translate(" +
                    [margin.left + yLBox.width, margin.top + innerHeight] + ")");
            gLegend.attr("transform", "translate(" +
                    [margin.left + yLBox.width, 5] + ")");
            gMain.attr("transform", "translate(" +
                    [margin.left + yLBox.width, margin.top + innerHeight] + ")");


        })(drawLeft, drawRight, drawBottom, opt.size);

        drawLeft.drawAxis();
        drawRight.drawAxis();
        drawBottom.drawAxis();
        gBottom.selectAll(".tick").each(function (d) {
            var str = this.getAttribute("transform");
            var pos = +str.slice(str.indexOf("(") + 1, str.indexOf(","));
            tickMap.push({ tick: d, pos: pos });
        });
        barSclae.range([0, sBottom.step()]);



        // create a brush
        gBrush.call(createBrush());

        gBar.selectAll("g")
            .data(cData.bars)
            .enter()
            .append("g")
            .each(function (dd) {
                var gg = d3.select(this);
                gg.attr("transform", "translate(" + sBottom(dd[0].x) + ",0)");
                gg.selectAll("rect")
                    .data(dd)
                    .enter()
                    .append("rect")
                    .each(function (jj, ii) {
                        d3.select(this)
                            .attr("class", ii ? "compare bar" : "current bar")
                            .attr("width", barSclae.step())
                            .attr("height", abs(sLeft(jj.y) - sLeft(0)))
                            .attr("y", jj.y > 0 ? -sLeft(jj.y) : -sLeft(0))
                            .attr("x", barSclae(ii))
                            .attr("fill", jj.color);

                        // cData.textPoints.current vs cData.textPoints.compare
                        cData.textPoints[ii ? "compare" : "current"].push({
                            x0: dd[0].x,
                            x: sBottom(dd[0].x) + barSclae(ii) + barSclae.step() * 0.5,
                            y: -sLeft(jj.y) - textOffset,
                            tag: ii ? "compare" : "current",
                            text: jj.y.toMeasureFormart(opt.rowMeasure.numberFormat)
                        });
                    });
            });

        createLineAndLegend(gMain, line, drawLeft.style);


    } // vertical END
    function herizontal () {
        chart.status = "herizontal";

        gLeft = svg.append("g").attr("class", "gLeft axis");
        gTop = svg.append("g").attr("class", "gTop axis");
        gBottom = svg.append("g").attr("class", "gBottom axis");
        gMain = svg.append("g").attr("class", "gMain");
        gBrush = gMain.append("g").attr("class", "gBrush");
        gLegend = svg.append("g").attr("class", "gLegend");
        gBar = gMain.append("g").attr("class", "gBar");
        gText = gMain.append("g").attr("class", "gText");

        barSclae = d3.scaleBand()
                .domain(cData.bars[0].length == 2 ? [0, 1] : [0])
                .round(true)
                .paddingOuter(1 - opt.scale);

        line.x(d => { return sTop(d.y); })
            .y(d => -sLeft(d.x) - sLeft.step() * 0.5)
            .defined(d => { return d.y !== null; })
            .curve(d3.curveMonotoneY);

        sTop    = d3.scaleLinear().domain(cData.rExtent).range([0, minGraphDim]);
        sBottom = d3.scaleLinear().domain(cData.yExtent).range([0, minGraphDim]);
        sLeft   = d3.scaleBand().domain(cData.category).range([0, minGraphDim])
                    .round(true).padding(0);

        var drawLeft, drawTop, drawBottom;
        drawLeft = d3.util.axis.ordinal({
            scale : sLeft,
            axis  : d3.axisLeft().scale(sLeft),
            holder: gLeft,
            orient: "left",

            label: opt.colDimension.name || "",
            style: opt.axisStyle
        });
        drawTop = d3.util.axis.linear({
            scale : sTop,
            axis  : d3.axisTop().scale(sTop),
            holder: gTop,
            orient: "top",
            tickFormate: ".0%",

            style: opt.axisStyle
        });
        drawBottom = d3.util.axis.linear({
            scale : sBottom,
            axis  : d3.axisBottom().scale(sBottom),
            holder: gBottom,
            orient: "bottom",

            label: opt.rowMeasure.name || "",
            style: opt.axisStyle
        });


        (function (yLeft, xTop, xBottom, svgSize) {
            var temp, yBox, topBox, btnBox, size;

            // Step1: get width of yAxis
            var lWidth = yLeft.getSize().width;

            margin.top = yLeft._renderResults.tick.height + 15;

            // Step2: set xAxis scale range and get xAxis height
            temp = svgSize.width - margin.left - margin.right - lWidth;
            var topHeight = xTop.range([0,
                            temp < minGraphDim ? minGraphDim : temp])
                        .getSize().height;
            var btnHeight = xBottom.range(xTop.range().slice())
                        .getSize().height;

            // Step3: set yAxis range (height)
            temp = svgSize.height - margin.top - margin.bottom - (topHeight + btnHeight);
            yLeft.range([0,
                temp < minGraphDim ? minGraphDim : temp]);


            // re-assure
            yBox = yLeft.getSize();
            topBox = xTop.getSize();
            btnBox = xBottom.getSize();
            size = {
                width: yBox.width + topBox.width + margin.left + margin.right,
                height: topBox.height + btnBox.height +
                        yBox.height + margin.top + margin.bottom
            };

            if (svgSize.width < size.width) { svgSize.width = size.width; }
            if (svgSize.height < size.height) { svgSize.height = size.height; }

            innerHeight = yBox.height;
            innerWidth = topBox.width;

            svg.attr("width", opt.size.width)
                .attr("height", opt.size.height);
            yLeft.holder.attr("transform", "translate(" +
                    [margin.left + yBox.width, margin.top + topBox.height] + ")");
            xTop.holder.attr("transform", "translate(" +
                    [margin.left + yBox.width, margin.top + topBox.height] + ")");
            xBottom.holder.attr("transform", "translate(" +
                    [margin.left + yBox.width, margin.top + topBox.height + innerHeight] + ")");
            gLegend.attr("transform", "translate(" +
                    [margin.left + yBox.width, 5] + ")");
            gMain.attr("transform", "translate(" +
                    [margin.left + yBox.width, margin.top + topBox.height + innerHeight] + ")");


        })(drawLeft, drawTop, drawBottom, opt.size);

        drawLeft.drawAxis();
        drawTop.drawAxis();
        drawBottom.drawAxis();

        gLeft.selectAll(".tick").each(function (d) {
            var str = this.getAttribute("transform");
            var pos = +str.slice(str.indexOf(",") + 1, str.indexOf(")"));
            tickMap.push({ tick: d, pos: pos - innerHeight });
        });

        barSclae.range([0, sLeft.step()]);
        gBrush.call(createBrush());


        gBar.selectAll("g")
            .data(cData.bars)
            .enter()
            .append("g")
            .each(function (dd) {
                var gg = d3.select(this);
                gg.attr("transform", "translate(0," +
                        (-sLeft(dd[0].x) - sLeft.step()) + ")");
                gg.selectAll("rect")
                    .data(dd)
                    .enter()
                    .append("rect")
                    .each(function (jj, ii) {
                        d3.select(this)
                            .attr("class", ii ? "compare bar" : "current bar")
                            .attr("width", abs(sBottom(jj.y) - sBottom(0)))
                            .attr("height", barSclae.step())
                            .attr("x", jj.y > 0 ? sBottom(0) : sBottom(jj.y))
                            .attr("y", barSclae(ii))
                            .attr("fill", jj.color);

                        // cData.textPoints.current vs cData.textPoints.compare
                        cData.textPoints[ii ? "compare" : "current"].push({
                            x0: dd[0].x,
                            x: sBottom(jj.y) + textOffset,
                            y: -sLeft(dd[0].x) - sLeft.step() + barSclae(ii) + barSclae.step() * 0.5,
                            tag: ii ? "compare" : "current",
                            text: jj.y.toMeasureFormart(opt.rowMeasure.numberFormat)
                        });
                    });
            });

        createLineAndLegend(gMain, line, drawLeft.style);

    } // herizontal END
    function drawChart (isReverse) {
        svg.html("");
        gMain = gLeft = gRight = gBottom = gTop = gBar = gLegend = gText = undefined;
        cData.textPoints = { current: [], compare: [], YoYRate: [], QoQRate: [] };
        tickMap = [];

        if (isReverse) {
            (chart.status == "vertical" ? herizontal : vertical)();
        }
        else {
            (chart.status == "vertical" ? vertical : herizontal)();
        }

        gMain.selectAll(".bar")
            .on("click", onClick)
            .on("contextmenu", onContextMenu)
            .on("mouseenter", onMouseEnter)
            .on("mouseleave", onMouseLeave);
        gMain.selectAll(".point")
            .on("click", onClick)
            .on("contextmenu", onContextMenu)
            .on("mouseenter", onPointMouseEnter)
            .on("mouseleave", onPointMouseLeave);

        gMain.selectAll(".line")
            .on("contextmenu", function () {
                if (d3.brushSelection(gBrush.node())) { return onBrushContextMenu(); }
            });
        gBrush.select(".selection")
            .on("contextmenu", onBrushContextMenu);
    }


    // Events
    function getRoundedData (dd) {
        return { x: dd.x,
                 y: dd.y.toMeasureFormart(opt.rowMeasure.numberFormat) };
    }
    function getPercent (dd) {
        return { x: dd.x,
                 y: d3.format(".1%")(dd.y) };
    }
    function onMouseEnter (dd) {
        if (d3.brushSelection(gBrush.node())) { return; }

        var target = d3.select(this),
            rectBox = this.getBoundingClientRect(),
            pText;

        try {
            pText = _.template(opt.tipFormat.output());
        }
        catch (err) {
            pText = _.template("X: <span><%= x %></span><br/>" +
                               "Y: <span><%= y %></span>");
        }

        // if (dd.type == "current") { ... remain ... }
        if (dd.type == "compare") {
            pText = _.template("对比组值：<span><%= y %></span>");
        }
        if (dd.type == "YoYRate") {
            pText = _.template("同比比率：<span><%= y %></span>");
        }
        if (dd.type == "QoQRate") {
            pText = _.template("环比比率：<span><%= y %></span>");
        }

        target.attr("fill", "#d8f210");

        tooltip = getTooltip();
        d3.select(tooltip)
            .html("")
            .style("display", "block")
            .append("p")
            .attr("class", "d3-ttip-content")
            .html(this.tagName.toUpperCase() == "RECT" ? pText(getRoundedData(dd)) : pText(getPercent(dd)));
        var scrollTop  = window.pageYOffset,
            scrollLeft = window.pageXOffset;

        tooltip.style.left = scrollLeft + rectBox.left +
            0.5 * (rectBox.width - tooltip.offsetWidth) + "px";
        tooltip.style.top  = scrollTop + rectBox.top - tooltip.offsetHeight + 10 + "px";
    }
    function onMouseLeave (dd) {
        if (d3.brushSelection(gBrush.node())) { return; }

        var target = d3.select(this);
        target.attr("fill", dd.color);
        d3.select(tooltip).style("display", "none");
    }
    function onPointMouseEnter (dd) {
        if (d3.brushSelection(gBrush.node())) { return; }
        onMouseEnter.call(this, dd);
        d3.select(this.parentNode)
            .select("path")
            .attr("stroke-dasharray", "10 5");
    }
    function onPointMouseLeave (dd) {
        if (d3.brushSelection(gBrush.node())) { return; }
        onMouseLeave.call(this, dd);
        d3.select(this.parentNode)
            .select("path")
            .attr("stroke-dasharray", "none");
    }
    function onContextMenu (dd) {
        d3.event.preventDefault();
        d3.event.stopPropagation();
        if (d3.brushSelection(gBrush.node())) { return onBrushContextMenu(); }
        contextMenu.setTargetNode(dd)
                   .setCurrentMenu(menuOnSingleItem)
                   .show();
    }
    function onBrushContextMenu () {
        d3.event.preventDefault();
        d3.event.stopPropagation();

        var range = d3.brushSelection(gBrush.node());
        if (!range) { return; }
        var ticks = _.filter(tickMap, d => {
                return d.pos >= range[0] && d.pos <= range[1];
            })
            .map(d => d.tick);
        contextMenu.setTargetNode(ticks)
                   .setCurrentMenu(menuOnBrush)
                   .show();
    }
    function onBlankContextMenu () {
        d3.event.preventDefault();
        contextMenu.setTargetNode(null)
                   .setCurrentMenu(menuOnBlank)
                   .show();
    }
    function onLegendClick () {

        var self = d3.select(this);
        var name = self.datum().className;

        if ( !legendState[name] ) {
            self.call(disableLegend, name);
        }
        else {
            self.call(enableLegend, name);
        }
    }
    function onClick (dd) {
        if (chart.onselected) {
            chart.onselected([{
                field: opt.colDimension, value: [ dd.x ]
            }]);
        }
    }
    function triggerLegendClick () {
        gLegend.selectAll(".legend")
            .each(function (dd) {
                if (legendState[dd.className]) {
                    d3.select(this).call(disableLegend, dd.className);
                }
            });
    }
    function disableLegend (legend, name) {
        legendState[name] = true;
        legend.attr("fill", "lightgray");
        gMain.selectAll("rect." + name).style("display", "none");
        gMain.selectAll("g." + name).style("display", "none");
        gText.select("." + name).style("visibility", "hidden");
    }
    function enableLegend (legend, name) {
        legendState[name] = false;
        legend.attr("fill", null);
        gMain.selectAll("rect." + name).style("display", "block");
        gMain.selectAll("g." + name).style("display", "block");
        gText.select("." + name).style("visibility", "visible");
    }


    //chart.setColor = function () {};
    chart.delete = function (arr) {
        var ii, index, removeItems = [];
        if (arr.length >= opt.values.length) { return alert("请至少留下一组数据！"); }

        for (ii = 0; ii < arr.length; ii++) {
            index = _.findIndex(opt.values,
                    item => { return item[opt.colDimension.name] == arr[ii]; });
            if (index > -1) {
                opt.values.splice(index, 1);
                removeItems.push(arr[ii]);
            }
        }

        dataTransform();
        drawChart();
        triggerLegendClick();

        if (this.onRemoveItem) {
            this.onRemoveItem([
                { field: opt.colDimension, value: removeItems }
            ]);
        }
    };
    chart.reverse = function () {
        opt.size.width  = opt.size._w;
        opt.size.height = opt.size._h;
        drawChart(true);
        triggerLegendClick();
    };
    chart.tipFormat = function (s) {
        if (!arguments.length) { return opt.tipFormat; }
        opt.tipFormat.input(s);
        return this;
    };
    chart.setSize = function (size) {
        if (!arguments.length) { return opt.size; }
        opt.size.width  = size.width;
        opt.size.height = size.height;

        // chart redraw
        svg.attr("width", size.width).attr("height", size.height);
        drawChart();
        triggerLegendClick();

        return this;
    };
    chart.getScale = function () {
        return opt.scale;
    };
    chart.setScale = function (val) {
        if (typeof val === "string") { val = +val; }
        if (val > 1 || val < 0) { return; }

        barSclae.paddingOuter(1 - val);
        opt.scale = 1 - val;

        if (this.status == "vertical") {
            gMain.selectAll(".gBar g").each(function () {
                var gg = d3.select(this);
                gg.selectAll("rect")
                    .attr("width", barSclae.step())
                    .attr("x", (j, i) => { return barSclae(i); });
            });
            gText.selectAll(".current, .compare")
                .selectAll("text")
                .each(function (dd) {
                    dd.x = sBottom(dd.x0) +
                        barSclae(dd.tag == "current" ? 0 : 1) +
                        barSclae.step() * 0.5;
                    this.setAttribute("x", dd.x);
                });
        }
        else {
            gMain.selectAll(".gBar g").each(function () {
                var gg = d3.select(this);
                gg.selectAll("rect")
                    .attr("height", barSclae.step())
                    .attr("y", (j, i) => { return barSclae(i); });
            });
            gText.selectAll(".current, .compare")
                .selectAll("text")
                .each(function (dd) {
                    dd.y = -sLeft(dd.x0) -
                        sLeft.step() +
                        barSclae(dd.tag == "current" ? 0 : 1) +
                        barSclae.step() * 0.5;
                    this.setAttribute("y", dd.y);
                });
        }
    };
    chart.export = function (type) {
        d3.util.exportChart(svg.node(), type || "png");
    };
    chart.removeChart = function () {
        gMain = gLeft = gRight = gBottom = gTop = gBar = gLegend = gText = undefined;
        svg.remove();

        if (tooltip) { d3.select(tooltip).remove(); }
        contextMenu.remove();

        tooltip = contextMenu = container = opt = undefined;

        for (var pp in chart) { delete chart[pp]; }
        chart = undefined;
    };

    // Run:
    dataTransform();
    drawChart();


    return chart;
}


    module.exports = {
        build: buildChart
    };

});
