/* global Highcharts, Hcharts, d3, undersore, _, require, define */
/* jshint -W119 */

define(function (require, exports, module) {
    var modules = {
        ShareData: require('./../../data/sharedata')
    };

    var buildChart = function (svgSelector, data, columnFields, rowFields, options) {

        var range, label, current, chart, hConfig;

        label = _.keys(data[0])[0];
        current = data[0][label];
        chart = Hcharts.solidgauge(svgSelector);

        hConfig = options.chartConfig || {
            GlobalColor: "#FFFFFF",
            MultiColors: ["#4E79A7", "#A0CBE8", "#F28E2B"],
            axisStyle: {
                tickColor : "#000", fontColor : "#000",
                fontFamily: "Arial", fontSize  : 15
            },
            textStyle: {
                "display"     : "block",
                "font-family" : "Arial",
                "font-size"   : 15,
                "font-style"  : "normal",
                "font-weight" : "normal",
                "fill"        : "#333",
            }
        };

        // 前3个为档位色，最后一个为背景色
        if (!hConfig.MultiColors || hConfig.MultiColors.length < 3) {
            hConfig.MultiColors = ["#4E79A7", "#F28E2B", "#c70733"];
        }

        chart.config.yAxis.stops = [0.1, 0.6, 0.8].map(function (d, i) {
            return [d, hConfig.MultiColors[i]];
        });
        chart.config.pane.background.backgroundColor = hConfig.GlobalColor;

        // After setting the current value
        // we can use some prototype methods, such as:
        // getCurrent, halfLayout, getRange
        chart.config.series[0].data = [current];
        chart.config.series[0].dataLabels = chart.getDataLabels(hConfig.textStyle);


        // Assign the max & min value of Y axis
        if (!options.gauge) { chart.halfLayout(); }
        else {
            range = chart.validateCheck(options.gauge);
            if (range) {
                chart.config.yAxis.min = range.min;
                chart.config.yAxis.max = range.max;
            }
            else { chart.halfLayout(); }
        }


        _.extend(chart.config.yAxis, chart.getAxisStyle(hConfig.axisStyle));
        _.extend(chart.config.yAxis.title,
                 { text: label, y: 40 });

        chart.init();
        return chart;
    };


    module.exports = {
        build: buildChart
    };

});
