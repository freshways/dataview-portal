import * as d3 from 'd3'
/* globals window, d3, _, document, Date, arguments  */
/* jshint -W104, -W109, -W119 */

if (!d3 || !d3.version.match(/4[.\d]+/)) {
  console.log("%cd3 is not loaded or not the version of \"4.x\"!", "background: green; color: red; font-size: 30px;");
}
// In case of multipe reload
// if (d3["util"]) {
//   return;
// }
// clear all tooltip and content menu boxes
d3.select(window).on("click", function () {
  d3.selectAll(".d3-ttip").style("display", "none");
  d3.selectAll(".d3-contextmenu").style("display", "none");
});
/* ------------ utility functions ------------ */
// shallow copy properties form "source" to "target"
var extend = function (target, source) {
  var pp;
  for (pp in source) {
    if (source.hasOwnProperty(pp)) {
      target[pp] = source[pp];
    }
  }
  return target;
};
// getBBox from one or more texts
var getTBox = function (texts, fontStyle) {
  if (!fontStyle) {
    fontStyle = "12px arial, sans-serif";
  }
  var svg = d3.select(document.body)
    .append("svg")
    .style("display", "block")
    .style("position", "absolute")
    .style("left", "-1000000px")
    .style("font", fontStyle)
    .attr("width", 100)
    .attr("height", 100);
  var msg = null, box;
  // if texts is an array of text
  if (texts.map) {
    msg = [];
    svg.selectAll("text")
      .data(texts)
      .enter()
      .append("text")
      .text(function (d) {
        return d;
      })
      .each(function (d) {
        box = this.getBBox();
        msg.push({text: d, width: box.width, height: box.height});
      });
  }
  // if texts is a string
  if ((typeof texts) === "string") {
    box = svg.append("text").text(texts).node().getBBox();
    msg = {text: texts, width: box.width, height: box.height};
  }
  // remove svg element after finish
  svg.remove();
  return msg;
};

// constructor "T" inherited from constructor "S"
function inherit(T, S) {
  function F() {
  }

  F.prototype = S.prototype;
  T.prototype = new F();
  T.prototype.constructor = T;
}

var sqrt = Math.sqrt;

function setterGetterPrototypeFun() {
  var self = this, arr = Array.prototype.slice.apply(arguments);
  arr.forEach(function (item) {
    self.prototype[item] = function (val) {
      if (!arguments.length) {
        return this["_" + item];
      }
      this["_" + item] = val;
      return this;
    };
  });
}

var Legend = (function () {
  function groupInnerLayout(dd, group, symbol, symbolSize, legend) {
    group.append("path")
      .attr("d", dd.symbol ? symbol.type(d3[dd.symbol]) : symbol.type(legend._symbolType))
      .attr("fill", dd.color)
      .attr("transform", "translate(" +
        [symbolSize * 0.5, legend._fontSize - symbolSize * 0.5] + ")");
    group.append("text")
      .attr("x", symbolSize + 5)
      .attr("y", legend._fontSize)
      .text(dd.name);
  }

  function verticalLayout(legend) {
    var symbol, symbolSize, height;
    symbol = d3.symbol()
      .type(legend._symbolType)
      .size(legend._symbolSize);
    symbolSize = Math.ceil(Math.sqrt(legend._symbolSize));
    return function (d, index) {
      var group = d3.select(this);
      groupInnerLayout(d, group, symbol, symbolSize, legend);
      height = height || this.getBBox().height;
      group.attr("transform", "translate(" +
        [symbolSize * 0.5, (height + 10) * index + 15] + ")");
    };
  }

  function horizontalLayout(legend) {
    var symbol = d3.symbol()
      .type(legend._symbolType)
      .size(legend._symbolSize);
    var symbolSize = Math.ceil(Math.sqrt(legend._symbolSize));
    return function (d, index) {
      var group = d3.select(this);
      groupInnerLayout(d, group, symbol, symbolSize, legend);
    };
  }

  // Base class for VLegend and HLegend
  function Base() {
    //this._fontStyle = "12px arial, sans-serif";
    this._fontFamily = "Arial";
    this._fontSize = 14;
    this._fontColor = "black";
    this._symbolType = d3.symbolSquare;
    this._symbolSize = 125;
    this._data = null;
    this._container = null; // d3 selection instance
    // only take effect when utilised on special sketch
    // Both available for VLegend and HLegend
    this._groupMaxLength = null;
  }

  setterGetterPrototypeFun.call(Base, "fontFamily", "fontSize", "fontColor", "symbolType", "symbolSize", "data", "container", "groupMaxLength");
  Base.prototype.getSize = function () {
    return this._container ? this._container.node().getBBox() : null;
  };
  Base.prototype.init = function () {
    return this._container
      .attr("dominant-baseline", "baseline")
      //.style("font", this._fontStyle)
      .attr("font-family", this._fontFamily)
      .attr("font-size", this._fontSize)
      .attr("fill", this._fontColor)
      .html("")
      .selectAll("g")
      .data(this._data)
      .enter()
      .append("g")
      .attr("class", "d3-lgd-item");
  };
  // only when normal layout can not accomadated at the "constraintWidth"
  // Setting "groupMaxLength" before running this function
  Base.prototype.textConstraintDefsInit = function () {
    if (!this._groupMaxLength) {
      throw Error("Give me the max text length first!");
    }
    var defs = this._container.append("defs");
    var time = (new Date()).getTime();
    var obj = {
      gradient: "g-" + time,
      mask: "m-" + time
    };
    defs.append("linearGradient")
      .attr("id", obj.gradient)
      .selectAll("stop")
      .data([
        {o: "0%", s: "rgba(255,255,255,1)"},
        {o: "80%", s: "rgba(255,255,255,1)"},
        {o: "100%", s: "rgba(0,0,0,0)"}
      ])
      .enter()
      .append("stop")
      .attr("offset", function (d) {
        return d.o;
      })
      .attr("stop-color", function (d) {
        return d.s;
      });
    defs.append("mask")
      .attr("id", obj.mask)
      .append("rect")
      .attr("fill", "url(#" + obj.gradient + ")")
      .attr("x", 0)
      .attr("y", -100)
      .attr("width", this._groupMaxLength)
      .attr("height", 1000);
    return this;
  };

  // Horizontal layout legend
  function HLegend() {
    Base.apply(this, Array.prototype.slice.apply(arguments));
    this._constraintWidth = null;
  }

  inherit(HLegend, Base);
  setterGetterPrototypeFun.call(HLegend, "constraintWidth");
  HLegend.prototype.sketch = function () {
    this.init().each(horizontalLayout(this));
    // Layout
    var groups = this._container.selectAll(".d3-lgd-item").nodes(), index = 0,
      pos = Math.ceil(Math.sqrt(this._symbolSize)) * 0.5;
    while (index < groups.length) {
      groups[index].setAttribute("transform", "translate(" + pos + ",0)");
      pos += groups[index].getBBox().width + 10;
      index++;
    }
    return this;
  };
  HLegend.prototype.constraintedSketch = function () {
    if (!this._constraintWidth) {
      throw Error("You should specify the availble with first!");
    }
    if (this.sketch().getSize().width <= this._constraintWidth) {
      return;
    }
    this.textConstraintDefsInit();
    var maskID = this._container.select("mask").attr("id");
    var self = this;
    var groups = this._container.selectAll(".d3-lgd-item").nodes(), forwardGroup, rearGroup, text, index = 0,
      count = groups.length;
    forwardGroup = this._container.append("g").attr("class", "d3-lgd-sub").node();
    rearGroup = this._container.append("g").attr("class", "d3-lgd-sub").node();
    groups.forEach(function (node, ii) {
      if (node.getBBox().width > self._groupMaxLength) {
        d3.select(node)
          .attr("mask", "url(#" + maskID + ")")
          .select("text")
          .append("title")
          .text(function (d) {
            return d["name"];
          });
      }
      node.setAttribute("transform", "translate(" + ii * (self._groupMaxLength + 10) + ",0)");
    });
    // ---
    do {
      forwardGroup.appendChild(groups[index]);
      rearGroup.appendChild(groups[count - index - 1]);
      index++;
    } while ((index * 2 * this._groupMaxLength + (index * 2 - 1) * 10) < this._constraintWidth &&
    index <= Math.floor(count / 2));
    // remove the other legend items where located in the middle of the line
    groups.forEach(function (item) {
      if (!item.parentElement.classList.contains("d3-lgd-sub")) {
        d3.select(item).remove();
      }
    });
    groups.length = 0;
    groups = undefined;
    // remove the last item in forwardGroup and rearGroup
    if (forwardGroup.querySelectorAll("g").length >= 2) {
      d3.select(forwardGroup.lastElementChild).remove();
      d3.select(rearGroup.lastElementChild).remove();
    }
    text = this._container.append("text").text(". . .").style("font-weight", "bold");
    // relocate the rearGroup x-position
    (function () {
      var b1 = forwardGroup.getBBox(), b2 = rearGroup.getBBox(),
        width = forwardGroup.querySelectorAll("g").length * (self._groupMaxLength + 10) - 10,
        b3 = text.node().getBBox();
      text.attr("transform", "translate(" + [width + 10, b1.height * 0.5] + ")");
      rearGroup.setAttribute("transform", "translate(" + -(b2.x - b1.x - width - b3.width - 25) + ",0)");
    })();
  };
  HLegend.prototype.natureSketch = function () {
    if (!this._constraintWidth) {
      throw Error("You should specify the availble with first!");
    }
    if (this.sketch().getSize().width <= this._constraintWidth) {
      return;
    }
    var groups = this._container.selectAll(".d3-lgd-item").nodes(), forwardGroup, rearGroup, text, index = 0,
      count = groups.length;
    forwardGroup = this._container.append("g").attr("class", "d3-lgd-sub").node();
    rearGroup = this._container.append("g").attr("class", "d3-lgd-sub").node();
    // ---
    do {
      forwardGroup.appendChild(groups[index]);
      rearGroup.appendChild(groups[count - index - 1]);
      index++;
    } while ((forwardGroup.getBBox().width + rearGroup.getBBox().width) < this._constraintWidth &&
    index <= Math.floor(count / 2));
    // remove the other legend items where located in the middle of the line
    groups.forEach(function (item) {
      if (!item.parentElement.classList.contains("d3-lgd-sub")) {
        d3.select(item).remove();
      }
    });
    groups.length = 0;
    groups = undefined;
    // remove the last item in forwardGroup and rearGroup
    d3.select(forwardGroup.lastElementChild).remove();
    d3.select(rearGroup.lastElementChild).remove();
    text = this._container.append("text").text(". . .").style("font-weight", "bold");
    // relocate the rearGroup x-position
    (function () {
      var b1 = forwardGroup.getBBox(), b2 = rearGroup.getBBox(), b3 = text.node().getBBox();
      text.attr("transform", "translate(" + [b1.width + 15, b1.height * 0.5] + ")");
      rearGroup.setAttribute("transform", "translate(" + -(b2.x - b1.x - b1.width - b3.width - 20) + ",0)");
    })();
  };

  // Vertical layout legend
  function VLegend() {
    Base.apply(this, Array.prototype.slice.apply(arguments));
    this._constraintHeight = null;
  }

  inherit(VLegend, Base);
  setterGetterPrototypeFun.call(VLegend, "constraintHeight");
  VLegend.prototype.sketch = function () {
    this.init().each(verticalLayout(this));
    return this;
  };
  VLegend.prototype.natureSketch = function () {
    if (!this._constraintHeight) {
      throw Error("You should specify the availble height first!");
    }
    if (this.sketch().getSize().height <= this._constraintHeight) {
      return;
    }
    var items, count, itemHeight, biSlice, forwardGroup, rearGroup, text;
    items = this._container.selectAll(".d3-lgd-item");
    count = items.nodes().length;
    if (!count) {
      return;
    }
    itemHeight = this._container.select(".d3-lgd-item").node().getBBox().height;
    biSlice = Math.floor((this._constraintHeight + 10) / (itemHeight + 10));
    if ((biSlice % 2) === 0) {
      biSlice = Math.floor(biSlice / 2) - 1;
    } else {
      biSlice = Math.floor(biSlice / 2);
    }
    forwardGroup = this._container.append("g").attr("class", "d3-lgd-sub").node();
    rearGroup = this._container.append("g").attr("class", "d3-lgd-sub").node();
    items.each(function (d, i) {
      if (i >= 0 && i < biSlice) {
        forwardGroup.appendChild(this);
      } else if (i >= biSlice && i < (count - biSlice)) {
        d3.select(this).remove();
      } else {
        rearGroup.appendChild(this);
      }
    });
    text = this._container.append("text")
      .text(". . .")
      .style("font-weight", "bold");
    (function () {
      var b1 = forwardGroup.getBBox(), b2 = rearGroup.getBBox();
      text.attr("transform", "translate(10," + (b1.x + b1.height + itemHeight + 5) + ")");
      rearGroup.setAttribute("transform", "translate(0," + -(b2.y - b1.y - b1.height - itemHeight - 10) + ")");
    })();
  };
  return {
    vlegend: function () {
      return new VLegend();
    },
    hlegend: function () {
      return new HLegend();
    }
  };
})(); // legend is OVER
function getSymbol(str) {
  var symbolArray = ["circle", "cross", "diamond", "square", "star", "triangle", "wye"];
  var index = symbolArray.indexOf(str.toLowerCase());
  if (index != -1) {
    return d3.symbols[index];
  } else {
    return d3.symbolCircle;
  }
}

var getChartContextMenu = (function () {
  function ChartContextMenu() {
    this.menuBox = null; // lazy load in "show" Fun
    this.targetNode = null; // specified in realtime
    this.currentMenuList = []; // specified in realtime
    // "clickFun" should be definded before using.
    // And should be able to deal with all situations in "currentMenuList"
    // menuItem is one member of "currentMenuList",
    // which including "title", "action" and other properties prepared in purpose.
    this.clickFun = function (menuItem, targetNode) {
      throw Error("Define one before invoking!");
    };
    /* Example:
    function (menuItem, targetNode) {
        this.hide();
        switch (menuItem.action) {
            case "delete" : chart.delete([d3.select(targetNode).datum()._index]); break;
            case "ascend" : chart.ascend(); break;
            case "descend": chart.descend(); break;
            case "export" : chart.export(menuItem.p1); break;
            default: console.log("... Catch YOU! ...");
        }
    };
    */
  }

  ChartContextMenu.prototype.setTargetNode = function (node) {
    if (!arguments.length) {
      return this.targetNode;
    }
    this.targetNode = node;
    return this;
  };
  ChartContextMenu.prototype.setCurrentMenu = function (menuList) {
    if (!arguments.length) {
      return this.currentMenuList;
    }
    this.currentMenuList = menuList;
    return this;
  };
  ChartContextMenu.prototype.show = function () {
    if (!this.menuBox) {
      this.menuBox = d3.select(document.body)
        .append("div")
        .attr("class", "d3-contextmenu open");
    }
    if (!this.currentMenuList.length) {
      return;
    }
    var self = this;
    this.menuBox.html("")
      .append("ul")
      .attr("class", "dropdown-menu")
      .selectAll("li")
      .data(this.currentMenuList) // defined in real-time
      .enter()
      .append("li")
      .append("a")
      .html(function (j) {
        return j.symbol + "&nbsp;" + j.title;
      })
      .on("click", function (menu) {
        self.clickFun(menu, self.targetNode);
      });
    this.menuBox
      .style("display", "block")
      .style("left", d3.event.clientX + window.pageXOffset + 10 + "px")
      .style("top", d3.event.clientY + window.pageYOffset + 10 + "px");
  };
  ChartContextMenu.prototype.hide = function () {
    return this.menuBox && this.menuBox.style("display", "none");
  };
  ChartContextMenu.prototype.remove = function () {
    return this.menuBox && this.menuBox.remove();
  };
  return function () {
    return new ChartContextMenu();
  };
})();
/*
    function axisLinearLayout () {

        var _scale, _axis, _label = "", _orient = "bottom", // bottom | left

            // text height at the default font family and font size
            // in d3 axis group element
            tHeight = 16,

            // text width per character at the deault font style
            mWidth = 7,

            // SI-prefix
            fm = d3.format(".1s"),

            // if the scale is scaleTime
            _isDate = false,

            // if the scale used for percentage
            _isPercent = false,

            // 45 tilted
            isTilt = false,

            abs = Math.abs;

        function draw (sc) {
            switch (_orient) {
                case "left": linearLeft(sc); break;
                case "right": linearRight (sc); break;
                case "bottom": linearBottom (sc); break;
                case "top": linearTop (sc); break;
            }
        }

        draw.scale = function (v) {
            if (!arguments.length) { return _scale; }
            _scale = v;
            return this;
        };
        draw.axis = function (v) {
            if (!arguments.length) { return _axis; }
            _axis = v;
            return this;
        };
        draw.label = function (v) {
            if (!arguments.length) { return _label; }
            _label = v;
            return this;
        };
        draw.orient = function (v) {
            if (!arguments.length) { return _orient; }
            _orient = v;
            return this;
        };
        draw.isDate = function (v) {
            if (!arguments.length) { return _isDate; }
            _isDate = v;
            return this;
        };
        draw.isPercent = function (v) {
            if (!arguments.length) { return _isPercent; }
            _isPercent = v;
            return this;
        };

        function getLongestTick () {
            var ff = _isDate ? (_axis.tickFormat() || d3.timeFormat("%Y-%m-%d")) :
                (_isPercent ? d3.format(".0%") : d3.format(".1s"));
            return _scale.ticks()
                    .map(function (d) { return ff(d); })
                    .sort(function (a, b) { return b.length - a.length; })[0];
        }

        draw.getSize = function () {
            var hh, ww, mm, temp, rr = _scale.range();

            if (_orient == "bottom" || _orient == "top") {
                hh = _axis.tickPadding() + _axis.tickSize();
                if (_isDate) {
                    temp = getLongestTick().length * mWidth;
                    if (abs((rr[1] - rr[0]) / 10) < temp) { // tilted 45 degree
                        hh += Math.ceil(temp / sqrt(2));
                        isTilt = true;
                    }
                }

                return {
                    width: abs(_scale.range()[1] - _scale.range()[0]),
                    height: _label ? (hh + tHeight * 2) : (hh + tHeight)
                };

            } else { // left/right orientation

                mm = getLongestTick();
                ww = _axis.tickPadding() + _axis.tickSize() + (mm.length * mWidth);

                return {
                    width: _label ? (ww + tHeight + 5) : (ww + 5),
                    height: abs(_scale.range()[1] - _scale.range()[0])
                };
            }
        };

        // drawing functions
        function linearBottom (sc) {
            var dSpan = _scale.domain();
            var rSpan = _scale.range();

            // The date scale will be formated by itself accessor function:
            //      timeAxis.tickArguments([10, "%Y-%m-%d"])
            // else: the data will be formated in SI style if it is a huge number.
            if (!_isDate && ( (dSpan[1] > 1e3) || (dSpan[0] < -1e3)) ) { _axis.ticks(5, "s"); }
            if (_isPercent) { _axis.ticks(5, ".0%"); }

            sc.call(_axis.scale(_scale));

            var hh = draw.getSize().height; // @TODO: determin the ticks are tilted
            var vSpan = _axis.tickSize() * sqrt(2);

            sc.selectAll(".axis-label").remove();
            if (isTilt) {
                sc.selectAll(".tick text")
                     .attr("x", 0)
                     .attr("y", 0)
                     .attr("dy", 0)
                     .attr("text-anchor", _orient == "bottom" ? "end" : "start")

                        // TODO: top/bottom
                     .attr("transform", function () {
                        return _orient == "bottom" ?
                                "rotate(-45)translate(" + [-vSpan, vSpan] + ")" :
                                "rotate(-45)translate(" + [vSpan, -vSpan] + ")";
                    });
            }
            if (_label) {
                sc.append("text")
                    .text(_label)
                    .attr("class", "axis-label")
                    .attr("fill", "#000")
                    .attr("text-anchor", "middle")
                    .attr("transform", function () {
                    return _orient == "bottom" ?
                        "translate(" + [(rSpan[1] - rSpan[0]) * 0.5, hh - tHeight + 2] + ")" :
                        "translate(" + [(rSpan[1] - rSpan[0]) * 0.5, -(hh - tHeight + 2)] + ")";
                });
            }
        }

        function linearLeft (sc) {

            var dSpan = _scale.domain();
            var rSpan = _scale.range();
            //if ((dSpan[1] > 1e3) || (dSpan[0] < -1e3)) { _axis.ticks(5, "s"); }
            if (!_isDate && ( (dSpan[1] > 1e3) || (dSpan[0] < -1e3)) ) { _axis.ticks(5, "s"); }
            if (_isPercent) { _axis.ticks(5, ".0%"); }

            sc.call(_axis.scale(_scale.copy().domain(dSpan.slice().reverse())));

            sc.selectAll(".axis-label").remove();
            if (_label) {
                sc.append("text")
                    .text(_label)
                    .attr("class", "axis-label")
                    .attr("fill", "#000")
                    .attr("text-anchor", "middle")
                    .attr("transform", function () {
                        return _orient == "left" ?
                            "rotate(-90)translate(" + [-abs(rSpan[1] - rSpan[0]) * 0.5, -(draw.getSize().width - tHeight)] + ")" :
                            "rotate(90)translate(" + [abs(rSpan[1] - rSpan[0]) * 0.5, -(draw.getSize().width - tHeight * 1.5)] + ")";
                    });
            }
        }

        function linearRight (sc) { linearLeft (sc); }
        function linearTop (sc) { linearBottom (sc); }

        return draw;
    } // axisLinearLayout END

    function axisOrdinalLayout () {
        var _scale, _axis, _label = "", _orient = "bottom", // bottom/top/left/right

            // text height at the default font family and font size
            // in d3 axis group element
            tHeight = 16,

            // text width per character at the deault font style
            mWidth = 6,

            // d3 selection for rendering
            _context = null,

            // array that recorde all ticks string dimension
            ticks = null,

            abs  = Math.abs,
            sqrt = Math.sqrt;

        function draw (sc) {
            switch (_orient) {
                case "left": leftOrient(sc); break;
                case "right": rightOrient (sc); break;
                case "bottom": bottomOrient (sc); break;
                case "top": topOrient (sc); break;
            }
        }

        draw.context = function (v) {
            if (!arguments.length) { return _context; }
            _context = v;

            return this;
        };
        draw.scale = function (v) {
            if (!arguments.length) { return _scale; }
            _scale = v;
            ticks = null; // ** any ordinal scale change will reflush this value **

            return this;
        };
        draw.axis = function (v) {
            if (!arguments.length) { return _axis; }
            _axis = v;

            return this;
        };
        draw.label = function (v) {
            if (!arguments.length) { return _label; }
            _label = v;
            return this;
        };
        draw.orient = function (v) {
            if (!arguments.length) { return _orient; }
            _orient = v;
            return this;
        };


        draw.getSize = function () {
            var inf = clearTickCollapse(), hh, ww;

            if (_orient == "bottom" || _orient == "top") {

                if (inf.tickLength < _scale.step()) {
                    hh = _axis.tickPadding() + _axis.tickSize();
                } else {
                    hh = _axis.tickPadding() + _axis.tickSize() +
                            (inf.tickLength / sqrt(2)) + 3;
                }

                return {
                    width: abs(_scale.range()[1] - _scale.range()[0]),
                    height: _label ? (hh + tHeight * 2) : (hh + tHeight)
                };
            } else { // left orientation

                ww = _axis.tickPadding() + _axis.tickSize() + inf.tickLength;

                return {
                    width: _label ? (ww + tHeight + 5) : (ww + 5),
                    height: abs(inf.rSpan[1] - inf.rSpan[0])
                };
            }
        };

        function getLongestTick (texts) {
            if (texts.length > 20) {
                texts = texts.slice()
                    .sort((a, b) => { return b.length - a.length; })
                    .slice(0, 20);
            }
            _context.html("").selectAll("text").data(texts)
                .enter()
                .append("text").text(d => d);

            var box = _context.selectAll("text")
                .nodes()
                .map(node => { return node.getBBox(); })
                .sort((a, b) => { return b.width - a.width; })[0];
            _context.html("");
            return box;
        }
        function clearTickCollapse () {
            var dSpan = _scale.domain(),
                rSpan = _scale.range(),
                bandWidth = _scale.step(),
                tLength;

            if (_context) {
                tLength = getLongestTick(dSpan).width;
            }
            else {
                ticks = ticks ? ticks : getTBox(dSpan);
                tLength = ticks.sort(function (a, b) { return b.width - a.width; })[0].width;
            }


            if ((_orient == "bottom" || _orient == "top") &&
                ((tHeight + 5) > bandWidth)) {
                rSpan = [rSpan[0], rSpan[0] + ((tHeight + 5) * dSpan.length)];
                _scale.range(rSpan);
            }

            if ((_orient == "left" || _orient == "right") &&
                ((tHeight + 3) > bandWidth)) {
                rSpan = [rSpan[0], rSpan[0] + ((tHeight + 3) * dSpan.length)];
                _scale.range(rSpan);
            }

            return {
                domain : dSpan,
                rSpan: rSpan,
                tickLength: tLength
            };
        }


        // drawing functions
        function bottomOrient (sc) {

            var inf = clearTickCollapse();
            var dSpan     = inf.domain,
                rSpan     = inf.rSpan,
                tLength   = inf.tickLength,
                bandWidth = _scale.step();


            function createAxis (isTiled) {

                sc.call(_axis.scale(_scale));

                // clear labels
                sc.selectAll(".axis-label").remove();

                var vSpan = _axis.tickSize() * sqrt(2);
                if (isTiled) {
                    sc.selectAll(".tick text")
                     .attr("x", 0)
                     .attr("y", 0)
                     .attr("dy", 0)
                     .attr("text-anchor", _orient == "bottom" ? "end" : "start")
                     .attr("transform", function () {
                        return _orient == "bottom" ?
                            "rotate(-45)translate(" + [-vSpan, vSpan] + ")" :
                            "rotate(-45)translate(" + [vSpan, -vSpan] + ")";
                    });
                }

                if (_label) { // create label
                    sc.append("text")
                    .text(_label)
                    .attr("class", "axis-label")
                    .attr("fill", "#000")
                    .attr("text-anchor", "middle")
                    .attr("transform", function () {
                        var tx = abs(rSpan[1] - rSpan[0]) * 0.5,
                            ty = _axis.tickPadding() + _axis.tickSize();

                        if (isTiled) {
                            return _orient == "bottom" ?
                                ("translate(" + [tx, ty + (tLength / sqrt(2)) + tHeight + 3] + ")") :
                                ("translate(" + [tx, -(ty + (tLength / sqrt(2)) + tHeight + 3)] + ")");
                        }
                        else {
                            return _orient == "bottom" ?
                                ("translate(" + [tx, ty + tHeight + 5] + ")") :
                                ("translate(" + [tx, -(ty + tHeight + 5)] + ")");
                        }
                    });
                }

            } // createAxis END

            if (tLength < bandWidth) { return createAxis(false); }
            return createAxis(true);

        } // bottomOrient END
        function leftOrient (sc) {

            var inf = clearTickCollapse();
            var dSpan     = inf.domain,
                rSpan     = inf.rSpan,
                tLength   = inf.tickLength;

            sc.call(_axis.scale(_scale.copy().domain(dSpan.slice().reverse())));
            sc.selectAll(".axis-label").remove();

            if (_label) {
                sc.append("text")
                .text(_label)
                .attr("class", "axis-label")
                .attr("fill", "#000")
                .attr("text-anchor", "middle")
                .attr("transform", function () {
                    var tx = abs(rSpan[1] - rSpan[0]) * 0.5,
                        ty = _axis.tickPadding() + _axis.tickSize() + tLength + 3;
                    return _orient == "left" ?
                        "rotate(-90)translate(" + [-tx, -ty] + ")" :
                        "rotate(90)translate(" + [tx, -ty] + ")";
                });
            }
        } // leftOrient END
        function rightOrient (sc) { leftOrient(sc); }
        function topOrient (sc) { bottomOrient(sc); }

        return draw;

    } // axisOrdinalLayout END
*/
var axisLayout = (function () {
  // setter-getter factory
  function factory(proportyName) {
    return function (_) {
      if (!arguments.length) {
        return this[proportyName];
      }
      this[proportyName] = _;
      return this;
    };
  }

  function renderingTest(arr) {
    if (this._renderResults) {
      return this._renderResults;
    }
    var results = {}, container;
    container = this.holder.append("g");
    container.selectAll("text")
      .data(arr)
      .enter()
      .append("text")
      .attr("font-family", this.style.fontFamily)
      .attr("font-size", this.style.fontSize)
      .text(function (d) {
        return d.text;
      })
      .each(function (d) {
        var box = this.getBBox();
        results[d.tag] = {
          width: box.width,
          height: box.height,
          text: d.text
        };
      });
    container.remove();
    this._renderResults = results;
    return results;
  }

  function addAxisLabel() {
    if (!this.label) {
      return;
    }
    var axisBox, test, label;
    axisBox = this.getSize();
    /* redering test result is cached
        during getSize() */
    test = this.renderingTest();
    label = this.holder.append("text")
      .attr("class", "axis-label")
      .attr("text-anchor", "middle")
      .text(this.label);
    switch (this.orient) {
      case "left":
        label.attr("transform", "rotate(-90) translate(" +
          [-axisBox.height / 2, -axisBox.width + test.label.height] +
          ")");
        break;
      case "right":
        label.attr("transform", "rotate(90) translate(" +
          [axisBox.height / 2, -axisBox.width + test.label.height] +
          ")");
        break;
      case "bottom":
        label.attr("transform", "translate(" +
          [axisBox.width / 2, axisBox.height - 5] +
          ")");
        break;
      case "top":
        label.attr("transform", "translate(" +
          [axisBox.width / 2, -axisBox.height + test.label.height] +
          ")");
        break;
    }
  }

  function getLinearLongestTick() {
    var flag = true, precise = 0, ff, arr;
    while (flag && precise < 4) {
      this.tickFormate = "." + precise + "s";
      ff = this.scale.tickFormat(this.tickArguments, this.tickFormate);
      arr = this.scale.ticks(this.tickArguments).map(ff);
      if (arr.length == _.uniq(arr).length) {
        flag = false;
      }
      precise++;
    }
    return arr.sort(function (a, b) {
      return b.length - a.length;
    })[0];
    /*
    var f = this.scale.tickFormat(this.tickArguments,
                                  this.tickFormate);
    return this.scale.ticks(this.tickArguments).map(f)
        .sort((a, b) => { return b.length - a.length; })[0];
    */
  }

  function getOrdinalLongestTick() {
    var longString = this.scale.domain().slice()
      .sort(function (a, b) {
        return b.length - a.length;
      })[0];
    if (this.maxTickLength &&
      this.maxTickLength < longString.length) {
      longString = longString.slice(0, this.maxTickLength) + "...";
    }
    return longString;
  }

  function getLinearAxisDim() {
    var longTick = this.getLongestTick();
    var testResults = this.renderingTest([
      {tag: "tick", text: longTick},
      {tag: "label", text: this.label}
    ]);
    if (this.orient == "left" || this.orient == "right") {
      return portrialAxisDim.call(this, testResults);
    } else {
      return landscapeAxisDim.call(this, testResults);
    }
  }

  function getOrdinalAxisDim() {
    var longTick = this.getLongestTick();
    var testResults = this.renderingTest([
      {tag: "tick", text: longTick},
      {tag: "label", text: this.label}
    ]);
    if (this.orient == "left" || this.orient == "right") {
      return portrailOrdinalDim.call(this, testResults);
    } else { // orient: "top" || "bottom"
      return landscapeOrdinalDim.call(this, testResults);
    }
  }

  function portrialAxisDim(res) {
    var width, height;
    width = this.axis.tickSize() +
      this.axis.tickPadding() +
      res.tick.width;
    height = Math.abs(this.scale.range()[1] -
      this.scale.range()[0]);
    width = this.label ?
      (width + res.label.height + this.labelPadding) :
      width;
    return {width: width, height: height};
  }

  function landscapeAxisDim(res) {
    var width, height;
    width = Math.abs(this.scale.range()[1] -
      this.scale.range()[0]);
    height = this.axis.tickSize() +
      this.axis.tickPadding() +
      res.tick.height;
    height = this.label ?
      (height + res.label.height + this.labelPadding) :
      height;
    return {width: width, height: height};
  }

  function portrailOrdinalDim(res) {
    var _this = this;
    var width, height;
    width = this.axis.tickSize() +
      this.axis.tickPadding() +
      res.tick.width;
    width = this.label ?
      (width + res.label.height + this.labelPadding) :
      width;
    (function () {
      var minHeight, qty, range;
      qty = _this.scale.domain().length;
      minHeight = qty * res.tick.height + ((qty - 1) * 5);
      range = _this.scale.range();
      height = range[1] - range[0];
      if (minHeight > height) {
        range[1] = range[0] + minHeight;
        _this.scale.range(range);
        height = minHeight;
      }
    })();
    return {width: width, height: height};
  }

  function landscapeOrdinalDim(res) {
    var width, height;

    function tiltLaout() {
      var minWidth, qty, range;
      qty = this.scale.domain().length;
      range = this.scale.range();
      minWidth = res.tick.height * qty + ((qty - 1) * 5);
      width = range[1] - range[0];
      if (minWidth > width) {
        range[1] = range[0] + minWidth;
        this.scale.range(range);
        width = minWidth;
      }
    }

    if ((this.scale.bandwidth() || this.scale.step()) <
      res.tick.width) {
      this.tiltPoise = true;
      tiltLaout.call(this);
    } else {
      this.tiltPoise = false;
    }
    width = this.scale.range()[1] - this.scale.range()[0];
    height = this.axis.tickSize() + this.axis.tickPadding();
    if (this.tiltPoise) {
      height += (res.tick.width / Math.sqrt(2) +
        res.tick.height / Math.sqrt(2));
    } else {
      height += res.tick.height;
    }
    if (this.label) {
      height += (this.labelPadding + res.label.height);
    }
    return {width: width, height: height};
  }

  function styleDecorate() {
    this.holder.selectAll("text")
      .attr("font-family", this.style.fontFamily)
      .attr("font-size", this.style.fontSize)
      .attr("fill", this.style.fontColor);
    this.holder.selectAll(".tick line, .domain")
      .attr("stroke", this.style.tickColor);
  }

  function drawLinearAxis() {
    var _this = this;
    (function () {
      var reversed = _this.scale.range().slice().reverse();
      if (_this.orient == "left" || _this.orient == "right") {
        _this.axis.scale(_this.scale.copy().range(reversed));
      }
    })();
    this.axis.tickArguments([
      this.tickArguments, this.tickFormate
    ]);
    this.holder.call(this.axis);
    addAxisLabel.call(this);
    this.drawStyle();
    this.drawCallback();
  } // drawLinearAxis END
  function drawOrdinalAxis() {
    var _this = this;
    (function () {
      var reversed = _this.scale.range().slice().reverse();
      if (_this.orient == "left" || _this.orient == "right") {
        _this.axis.scale(_this.scale.copy().range(reversed));
      }
    })();
    this.holder.call(this.axis);
    addAxisLabel.call(this);
    this.drawStyle();
    this.drawCallback();
  }

  function ordinalCallBack() {
    var vSpan, dm, texts, sqrt2 = Math.sqrt(2), max = this.maxTickLength;
    texts = this.holder.selectAll(".tick text");

    function addEllipsis() {
      var tick = d3.select(this), str = tick.text();
      if (str.length > max) {
        tick.text(str.slice(0, max) + "...");
        tick.append("title").text(str);
      }
    }

    if (max &&
      ((this.orient == "left" || this.orient == "right") ||
        ((this.scale.step() || this.scale.bandwidth()) < this._renderResults.tick.width))) {
      texts.each(addEllipsis);
    }
    /*-- only landscape orientation will need tilt poise layout --*/
    if (!this.tiltPoise) {
      return;
    }
    texts.attr("y", null).attr("dy", null);
    vSpan = this.axis.tickSize() + this.axis.tickPadding();
    if (this.orient == "bottom") {
      vSpan += this._renderResults.tick.height / sqrt2;
      dm = vSpan / sqrt2;
      texts.attr("text-anchor", "end")
        .attr("transform", "rotate(-45) translate(" + [-dm, dm] + ")");
    } else if (this.orient == "top") {
      dm = vSpan / sqrt2;
      texts.attr("text-anchor", "start")
        .attr("transform", "rotate(-45) translate(" + [dm, -dm] + ")");
    }
  }

  var AxisBase = {
    scale: null,
    axis: null,
    holder: null,
    label: "",
    orient: "bottom",
    //Linear:  tickFormate: ".0s",
    //Linear:  tickArguments: 5,
    //Ordinal: maxTickLength : 8
    //Ordinal: tiltPoise : false,
    labelPadding: 5,
    style: {
      fontColor: "black", fontFamily: "arial",
      fontSize: 14, tickColor: "black"
    },
    getLongestTick: getLinearLongestTick,
    getSize: getLinearAxisDim,
    range: function (_) {
      if (!arguments.length) {
        return this.scale.range();
      }
      this.scale.range(_);
      return this;
    },
    /**
     * Run renderingTest will cach the results
     * Run reinstateTest will de-cach the results.
     * If setting have change you should reinstate the test results,
     * such as: fontSize, fontFamily
     */
    renderingTest: renderingTest,
    reinstateTest: function () {
      this._renderResults = null;
      return this;
    },
    /**
     * "drawAxis" have emplement "drawStyle" and "drawCallback" functions and run internaly
     * "drawStyle" - could used to update styles apt for future need
     * "drawCallback" - more operations if you need.
     */
    drawAxis: drawLinearAxis,
    drawStyle: styleDecorate,
    drawCallback: function () {
    },
    extend: function (config) {
      var instance = Object.create(this);
      var style = JSON.parse(JSON.stringify(this.style));
      for (var key in config) {
        if (config.hasOwnProperty(key)) {
          if (key == "style") {
            _.extend(style, config.style);
            instance.style = style;
          } else {
            instance[key] = config[key];
          }
        }
      }
      return instance;
    }
  }; // axisBase END
  function linearAxis(config) {
    if (!config.tickFormate) {
      config.tickFormate = ".0s";
    }
    if (!config.tickArguments) {
      config.tickArguments = 5;
    }
    return Object.create(AxisBase).extend(config);
  }

  function ordinalAxis(config) {
    var opt = {
      getLongestTick: getOrdinalLongestTick,
      getSize: getOrdinalAxisDim,
      drawAxis: drawOrdinalAxis,
      drawCallback: ordinalCallBack,
      maxTickLength: 8,
      tiltPoise: false
    };
    _.extend(opt, config);
    return Object.create(AxisBase).extend(opt);
  }

  return {
    linear: linearAxis,
    ordinal: ordinalAxis
  };
})(); // IIFE END
/*
    node: HTMLElement
    return node's inner dimension that the node can hold without overflow.
    Warnning: padding should NOT be set as negative value.
*/
function getInnerBoxDim(node) {
  var css, width, height;
  css = window.getComputedStyle(node, null);
  width = node.clientWidth -
    (parseFloat(css.paddingLeft) || 0) -
    (parseFloat(css.paddingRight) || 0);
  height = node.clientHeight -
    (parseFloat(css.paddingTop) || 0) -
    (parseFloat(css.paddingBottom) || 0);
  // when userAgent is Chrome broswer
  if (!!window["chrome"] && !!window["chrome"]["webstore"]) {
    node.style["-webkit-box-sizing"] = "content-box";
    node.style["box-sizing"] = "content-box";
  }
  return {width: width, height: height};
}

/* tooltip parsing class */
function TipParse(mapping, input) {
  // [{ key: "月(登记时间)", value: "x" }, { key: "统计(人数)", value: "y" }]
  this._map = mapping;
  // "月(登记时间):<月(登记时间)>\n统计(人数):<统计(人数)>"
  this._input = input || "";
  // Default regular expression:
  this._regExp = /<[^<>]+?>/g;
}

TipParse.prototype.regExp = function (reg) {
  if (arguments.length < 1) {
    return this._regExp;
  }
  this._regExp = reg;
  return this;
};
TipParse.prototype.input = function (str) {
  if (arguments.length < 1) {
    return this._input;
  }
  this._input = str;
  return this;
};
TipParse.prototype.output = function () {
  if (!this._map || !this._map.length) {
    return console.error('Map should be offered!');
  }
  var userContent, keyPropoties, result;
  // default results if any thing goes wrong
  result = this._map.map(function (obj) {
    return obj.key + " : " +
      "<span><%= " + obj.value + " %></span>";
  })
    .join("<br>");
  this._input = this._input.trim();
  if (!this._input) {
    return result;
  }
  // force checking from the begining
  this._regExp.lastIndex = 0;
  // "abc:<xxx>\ndef:<xxx>" -> ["abc:", "\ndef:", ""]
  userContent = this._input.split(this._regExp);
  // ["<xxx>", "<xxx>", "<xxx>"]
  keyPropoties = this._input.match(this._regExp);
  // if nothing matched: null
  if (!keyPropoties) {
    return result;
  }
  keyPropoties = keyPropoties.map(function (str) {
    var name = str.slice(1, str.length - 1); // "<xxx>" -> "xxx"
    var pair = this._map.find(function (d) {
      return d.key === name;
    });
    if (pair) {
      return "<span><%= " + pair.value + " %></span>";
    } else {
      return "<span></span>";
    }
  }, this);
  result = _.chain(userContent).zip(keyPropoties).flatten().value().join("");
  result = result.replace(/\n/g, "<br>");
  return result;
};

/*
这个模块建立在以下几个前提下：
1、容器模块内的text元素在初始化前均绑定了数据，
    形式如：{ x: 1, y: 2, text: "abc" },
    这个点将做为后续修改的参考点;

2、在调整文字的样式时，动态调整了部分越界文字的位置，
    这种调整动用了getBBox函数，故此容器必须是可见状态；
    在display设置成"none"状态后，除调整display特性有效以外其他特性均无法调整；

3、options参数形式如下，初始化状态会用到：
    {
        "font-family" : "Arial",   // "Arial", "Helvetica", "Times New Roman", "Times"
        "font-size"   : 12,
        "font-style"  : "normal",  // "normal" or "italic"
        "font-weight" : "normal",  // "normal" or "bold"
        "fill"        : "#ff0000", // text color
        "position"    : "top"      // "top", "right", "bottom" or "left"
    }
    后续如果添加其他特性，需要修改TextDecorate.prototype.trigger函数和初始化init函数；

4、参数释义：
    new TextDecorate (container, offset, boundary, options)
    container : <g>元素的d3对象
    offset    :  偏离值，默认值为3
    boundary  : <g>元素的有效范围，防止文字出界。形式： [左上角点, 右下角]
    options   :  见上面的说明

    除offset有默认值外，其他值 **必须** 提供
*/
function TextDecorate(container, offset, boundary, options) {
  this.container = container;
  this.offset = offset || 3;
  this.boundary = boundary;
  this.options = options || {};
  this.event = d3.dispatch("attribute", "position");
  this.event.on("attribute", function (attr, value) {
    var that = this;
    if (attr === "display") {
      this.attr(attr, value);
      return;
    }
    if (this.container.attr("display") === "none") {
      return;
    }
    this.attr(attr, value);
    this.container.selectAll("text").each(function () {
      that.touchBoundaryAdjust(this);
    });
  });
  this.event.on("position", function (position) {
    if (this.container.attr("display") === "none") {
      return;
    }
    this[position]();
  });
  return this;
}

TextDecorate.prototype.init = function () {
  var keys = _.keys(this.options);
  keys = keys.filter(function (d) {
    return (d !== "position") && (d !== "display");
  });
  for (var i = 0; i < keys.length; i++) {
    this.container.attr(keys[i], this.options[keys[i]]);
  }
  this.trigger("position", this.options.position || "top");
  // If "display" eq to "none" text box can not rendered
  this.container.attr("display", this.options.display)
    .style("text-shadow", "0px 0px 3px white")
    .style("pointer-events", "none")
    .style("-moz-user-select", "none")
    .style("-ms-user-select", "none")
    .style("user-select", "none");
};
TextDecorate.prototype.proxy = function (fn) {
  var that = this;
  return function () {
    if (!that.container || !that.boundary) {
      return new Error("请指定容器和容器边界！");
    }
    fn.apply(that, arguments);
  };
};
TextDecorate.prototype.touchBoundaryAdjust = function (node) {
  // node -> SVGText Element
  var box = node.getBBox();
  var self = d3.select(node);
  var temp;
  box.right = box.x + box.width;
  box.bottom = box.y + box.height;
  if (box.x < this.boundary[0][0]) {
    temp = +self.attr("x");
    self.attr("x", temp + (this.boundary[0][0] - box.x));
  }
  if (box.right > this.boundary[1][0]) {
    temp = +self.attr("x");
    self.attr("x", temp - (box.right - this.boundary[1][0]));
  }
  if (box.y < this.boundary[0][1]) {
    temp = +self.attr("y");
    self.attr("y", this.boundary[0][1] + box.height);
  }
  if (box.bottom > this.boundary[1][1]) {
    temp = +self.attr("y");
    self.attr("y", this.boundary[1][1] - box.height);
  }
};
TextDecorate.prototype.top = function () {
  var that = this;
  this.container.attr("text-anchor", "middle");
  this.container.selectAll("text")
    .each(function (d) {
      var self = d3.select(this);
      self.attr("y", d.y - that.offset).attr("x", d.x);
      that.touchBoundaryAdjust(this);
    });
};
TextDecorate.prototype.right = function () {
  var that = this;
  this.container.attr("text-anchor", "start");
  this.container.selectAll("text")
    .each(function (d) {
      var self = d3.select(this);
      self.attr("y", d.y).attr("x", d.x + that.offset);
      that.touchBoundaryAdjust(this);
    });
};
TextDecorate.prototype.bottom = function () {
  var that = this;
  this.container.attr("text-anchor", "middle");
  this.container.selectAll("text")
    .each(function (d) {
      var self = d3.select(this);
      var box = this.getBBox();
      self.attr("y", d.y + box.height + that.offset).attr("x", d.x);
      that.touchBoundaryAdjust(this);
    });
};
TextDecorate.prototype.left = function () {
  var that = this;
  this.container.attr("text-anchor", "end");
  this.container.selectAll("text")
    .each(function (d) {
      var self = d3.select(this);
      self.attr("y", d.y).attr("x", d.x - that.offset);
      that.touchBoundaryAdjust(this);
    });
};
TextDecorate.prototype.attr = function (attr, val) {
  this.container.attr(attr, val);
  return this;
};
TextDecorate.prototype.trigger = function (name, val) {
  switch (name) {
    case "display":
    case "font-family":
    case "font-size":
    case "font-style":
    case "font-weight":
    case "fill":
      this.event.call("attribute", this, name, val);
      this.options[name] = val;
      break;
    case "position":
      this.event.call("position", this, val);
      this.options.position = val;
      break;
    case "offset":
      this.offset = (typeof val === "string") ? +val : val;
      this.trigger("position", this.options.position);
      break;
    default:
      console.log("名称有误，没有查到相应的事件");
  }
};
// svg export modular
// selector: svg element node or string selector: ".Chart-To-Be-Export"
function exportChart(selector, fileType, fileName) {
  function svgURI(svg) {
    var imgsrc = getUIR(svg);
    var aTag = document.createElement("a");
    document.body.appendChild(aTag);
    aTag.download = fileName + ".svg";
    aTag.href = imgsrc;
    aTag.click();
    document.body.removeChild(aTag);
  }

  function pngURI(svg) {
    var imgsrc = getUIR(svg);
    var canvas = document.createElement("canvas");
    canvas.setAttribute("width", svg.getAttribute("width"));
    canvas.setAttribute("height", svg.getAttribute("height"));
    canvas.style.cssText = "position:absolute;left:1e+08cm;top:1e+08cm;";
    document.body.appendChild(canvas);
    var context = canvas.getContext("2d");
    var image = new Image;
    image.src = imgsrc;
    image.onload = function () {
      context.drawImage(image, 0, 0);
      var canvasdata = canvas.toDataURL("image/png");
      var aTag = document.createElement("a");
      document.body.appendChild(aTag);
      aTag.download = fileName + ".png";
      aTag.href = canvasdata;
      aTag.click();
      document.body.removeChild(aTag);
      document.body.removeChild(canvas);
    };
  }

  function getUIR(svg) {
    var dupSVG, div, html;
    dupSVG = svg.cloneNode(true);
    div = document.createElement("div");
    dupSVG.setAttribute("version", "1.1");
    dupSVG.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    div.appendChild(dupSVG);
    html = div.innerHTML;
    return "data:image/svg+xml;base64," +
      window.btoa(window.unescape(encodeURIComponent(html)));
  }

  function expt(svg, type) {
    if (type === "png") {
      pngURI(svg);
    } else if (type === "svg") {
      svgURI(svg);
    }
  }

  if (!fileType) {
    fileType = "png";
  }
  if (!fileName) {
    fileName = "Pic_" + (new Date()).getTime();
  }
  if (selector instanceof SVGSVGElement) {
    expt(selector, fileType);
  } else if (typeof selector === "string") {
    document.querySelectorAll(selector)
      .forEach(function (svg) {
        if (svg instanceof SVGSVGElement) {
          expt(svg, fileType);
        }
      });
  }
} // exportChart
 d3.util = {
  extend: extend,
  getTBox: getTBox,
  inherit: inherit,
  vlegend: Legend.vlegend,
  hlegend: Legend.hlegend,
  //axisLLayout: axisLinearLayout,
  //axisOLayout: axisOrdinalLayout,
  axis: axisLayout,
  getInnerBoxDim: getInnerBoxDim,
  tipParse: function (mapping, input) {
    return new TipParse(mapping, input);
  },
  TextDecorate: TextDecorate,
  exportChart: exportChart,
  getChartContextMenu: getChartContextMenu
};

export default d3

