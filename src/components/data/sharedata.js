/**
 * 共享数据
 */

var fields_column = [];
var fields_row = [];
var colormark = {};
const dict_fontfamily = [{name: "Arial", css: "Arial, Helvetica, sans-serif"},
  {name: "Arial Black", css: "'Arial Black', Gadget, sans-serif"},
  {name: "Arial Narrow", css: "'Arial Narrow', sans-serif"},
  {name: "Verdana", css: "Verdana, Geneva, sans-serif"},
  {name: "Georgia", css: "Georgia, serif"},
  {name: "Times New Roman", css: "'Times New Roman', Times, serif"},
  {name: "Trebuchet MS", css: "'Trebuchet MS', Helvetica, sans-serif"},
  {name: "Courier New", css: "'Courier New', Courier, monospace"},
  {name: "Impact", css: "Impact, Charcoal, sans-serif"},
  {name: "Comic Sans MS", css: "'Comic Sans MS', cursive"},
  {name: "Tahoma", css: "Tahoma, Geneva, sans-serif"},
  {name: "Courier", css: "Courier, monospace"},
  {name: "Lucida Sans Unicode", css: "'Lucida Sans Unicode', 'Lucida Grande', sans-serif"},
  {name: "Lucida Console", css: "'Lucida Console', Monaco, monospace"},
  {name: "Garamond", css: "Garamond, serif"},
  {name: "MS Sans Serif", css: "'MS Sans Serif', Geneva, sans-serif"},
  {name: "MS Serif", css: "'MS Serif', 'New York', sans-serif"},
  {name: "Palatino Linotype", css: "'Palatino Linotype', 'Book Antiqua', Palatino, serif"},
  {name: "Symbol", css: "Symbol, sans-serif"},
  {name: "Bookman Old Style", css: "'Bookman Old Style', serif"},
  {name: "宋体", css: "宋体"},
  {name: "微软雅黑", css: "微软雅黑"},
  {name: "华文细黑", css: "华文细黑"},
  {name: "黑体", css: "黑体"}
];
var dbtypes = [{name: "String", value: 16}, {name: "Int32", value: 11}, {name: "DateTime", value: 6}];
var aggregatemodes = ["不聚合", "统计", "最小值", "最大值", "总和", "平均值"];

export const ShareData = {
  Columns: fields_column,
  Rows: fields_row,
  ColorMark: colormark,
  hasAggre: true,
  Measures: [],
  Dimensions: [],
  Fonts: dict_fontfamily,
  DbTypes: dbtypes,
  FontSize: [8, 9, 10, 11, 12, 14, 16, 18, 20],
  AggregateModes: aggregatemodes
}
