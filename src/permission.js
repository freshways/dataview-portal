import router from '@/router'
import {ElMessage} from 'element-plus'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'
import {getChartList} from "@/api/chart";
import {store} from '@/components/chart/common/store'

NProgress.configure({showSpinner: false}) // NProgress Configuration

router.beforeEach((to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = to.meta.title + ' - 数据可视化' ;

  if (store.chartDict.length === 0) {
    getChartList().then(() => {
      console.debug("加载系统参数成功！")
    }).catch(error => {
      ElMessage.error('加载系统参数失败，请检查网络连接！');
    }).finally(() => {
      next()
      NProgress.done()
    })
  } else {
    next()
    NProgress.done()
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
