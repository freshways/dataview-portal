import service from '@/utils/request'

const path = '/queryLog'

/**
 * 获取列表
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export function getQueryLogList(data) {
  return service.post(path + "/list", data)
}
