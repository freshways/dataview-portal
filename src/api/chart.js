import service from '@/utils/request'
import {store} from "@/components/chart/common/store";

const path = '/chart'

/**
 * 获取图表目录项
 * @param data
 * @returns {*}
 */
export function getChartList(data) {
  return new Promise((resolve, reject) => {
    service.get(path + "/list").then(resp => {
      resp.data.forEach(t => {
        t.icon = JSON.parse(t.icon);
        t.rule = JSON.parse(t.rule);
      })
      store.chartDict = resp.data
      resolve(resp)
    }).catch(error => {
      reject(error)
    })
  })
}

/**
 * 删除指定的图表目录
 * @param workBookId
 * @returns {Promise<AxiosResponse<any>>}
 */
export function delChart(id) {
  return service.delete(path + "/" + id)
}

/**
 * 保存图表目录项
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export function saveChart(data) {
  return service.post(path, data)
}





