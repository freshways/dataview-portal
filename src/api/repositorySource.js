import service from '@/utils/request'

const path = '/RepositorySource'

export function getList(data) {
  return service.get(path + "/getList", {
    params: data
  })
}

/**
 * 删除
 * @param id
 * @returns {Promise<AxiosResponse<any>>}
 */
export function del(id) {
  return service.get(path + "/delRepositorySource", {
    params: {
      id: id
    }
  })
}

/**
 * 连接测试
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export function validConnection(data) {
  return service.post(path + "/validConnection", data)
}

/**
 * 保存
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export function save(data) {
  return service.post(path + "/save", data)
}

