import service from '@/utils/request'

const path = '/log'

/**
 * 获取日志文件目录
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export function getLogFiles() {
  return service.get(path)
}

/**
 * 获取指定的日志文件
 * @param fileName
 * @returns {Promise<AxiosResponse<any>>}
 */
export function getLogFileByName(fileName) {
  return service.post(path + "/" + fileName)
}
