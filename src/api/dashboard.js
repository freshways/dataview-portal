import service from '@/utils/request'

const path = '/dashboard'

/**
 * 获取数据源列表
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export function getList(data) {
  return service.get(path + "/getList", {
    params: data
  })
}

/**
 * 获取单个数据源对象
 * @param id
 * @returns {Promise<AxiosResponse<any>>}
 */
export function getDashboard(id) {
  return service.get(path + "/" + id)
}


/**
 * 删除数据源
 * @param id
 * @returns {Promise<AxiosResponse<any>>}
 */
export function delDashboard(id) {
  return service.delete(path + "/" + id)
}

/**
 * 保存数据源
 * @param data
 * @returns {Promise<AxiosResponse<any>>}
 */
export function saveDashboard(data) {
  return service.post(path, data)
}

export function parsToObj(dashboard) {
  if (typeof dashboard.gridItems === "string") {
    dashboard.gridItems = JSON.parse(dashboard.gridItems)
  }
  if (typeof dashboard.globalFilter === "string") {
    dashboard.globalFilter = JSON.parse(dashboard.globalFilter)
  }
  if (typeof dashboard.style === "string") {
    dashboard.style = JSON.parse(dashboard.style)
  }
  return dashboard
}

export function parsToString(dashboard) {
  let newObj = Object.assign({}, dashboard);
  if (typeof newObj.gridItems === "object") {
    newObj.gridItems = JSON.stringify(dashboard.gridItems)
  }
  if (typeof newObj.globalFilter === "object") {
    newObj.globalFilter = JSON.stringify(dashboard.globalFilter)
  }
  if (typeof newObj.style === "object") {
    newObj.style = JSON.stringify(dashboard.style)
  }
  return newObj
}
