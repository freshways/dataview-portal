import {createStore} from 'vuex'
import getters from './getters'
import dataSource from './modules/dataSource'
import repositorySource from './modules/repositorySource'
import workBook from "@/store/modules/workBook";
import dashboard from "@/store/modules/dashboard";

export default createStore({
  getters, modules: {
    dataSource, repositorySource,workBook,dashboard
  }
})

