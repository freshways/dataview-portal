const getters = {
  dataSource: state => state.dataSource,
  repositorySource: state => state.repositorySource,
  workBook: state => state.workBook,
  dashboard: state => state.dashboard
}
export default getters
