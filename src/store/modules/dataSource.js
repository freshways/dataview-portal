const state = {
  currentDataSource:{},
  editViewShow:false
}

const mutations = {
  SET_DATA: (state, data) => {
    state.currentDataSource = data
  },
  CLEAR_DATA:(state) => {
    state.currentDataSource = {}
  },
  SET_SHOW:(state,data) =>{
    state.editViewShow = data
  }
}

const actions = {
  setDataSource({ commit }, dataSource) {
    if (typeof dataSource.preDataFields == 'string') {
      dataSource.preDataFields = JSON.parse(dataSource.preDataFields)
    }
    if (typeof dataSource.chooseTables == 'string') {
      dataSource.chooseTables = JSON.parse(dataSource.chooseTables)
    }
    if (typeof dataSource.tableRelations == 'string') {
      dataSource.tableRelations = JSON.parse(dataSource.tableRelations)
    }
    commit('SET_DATA', dataSource)
  },
  clearDataSource({ commit }) {
    commit('CLEAR_DATA')
  },
  setViewShow({ commit }, { data }){
    commit('SET_SHOW', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
