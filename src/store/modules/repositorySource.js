const state = {
  currentData:{},
  viewShow:false
}

const mutations = {
  SET_DATA: (state, data) => {
    state.currentData = data
  },
  CLEAR_DATA:(state) => {
    state.currentData = {}
  },
  SET_SHOW:(state,data) =>{
    state.viewShow = data
  }
}

const actions = {
  setData({ commit }, { obj }) {
    commit('SET_DATA', obj)
  },
  clearData({ commit }) {
    commit('CLEAR_DATA')
  },
  setViewShow({ commit }, { data }){
    commit('SET_SHOW', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
