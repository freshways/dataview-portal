const state = {
  currentData: {},
  viewShow: false
}

const mutations = {
  SET_DATA: (state, data) => {
    state.currentData = data
  },
  CLEAR_DATA: (state) => {
    state.currentData = {}
  },
  SET_SHOW: (state, data) => {
    state.viewShow = data
  }
}

const actions = {
  setData({commit}, workBook) {
    if (typeof workBook.chartConfig === 'string') {
      workBook.chartConfig = JSON.parse(workBook.chartConfig)
    }
    if (typeof workBook.rows === 'string') {
      workBook.rows = JSON.parse(workBook.rows)
    }
    if (typeof workBook.columns === 'string') {
      workBook.columns = JSON.parse(workBook.columns)
    }
    if (typeof workBook.filters === 'string') {
      workBook.filters = JSON.parse(workBook.filters)
    }
    if (typeof workBook.colorMark === 'string') {
      workBook.colorMark = JSON.parse(workBook.colorMark)
      if(workBook.colorMark&&workBook.colorMark.annexField){
        const subDimension =[]
        subDimension.push(workBook.colorMark.annexField)
        workBook.subDimension = subDimension
      }
    }
    if (typeof workBook.urlParams === 'string') {
      workBook.urlParams = JSON.parse(workBook.urlParams)
    }
    if (typeof workBook.limit === 'string') {
      workBook.limit = JSON.parse(workBook.limit)
    }
    if (typeof workBook.rankMark === 'string') {
      workBook.rankMark = JSON.parse(workBook.rankMark)
    }
    commit('SET_DATA', workBook)
  },
  clearData({commit}) {
    commit('CLEAR_DATA')
  },
  setViewShow({commit}, {data}) {
    commit('SET_SHOW', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
