import {createRouter, createWebHashHistory} from 'vue-router'
import Layout from '../layout'


const routes = [
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    children: [{
      path: 'index',
      name: 'index',
      component: () => import('../views/Home/index'),
      meta: {title: '首页', icon: 'index'}
    }]
  },
  {
    path: '/workbook/preview',
    name: 'workBookView',
    component: () => import('@/views/WorkBook/preview'),
    meta: {title: '图表预览', icon: 'table'}
  },
  {
    path: '/workbook/edit',
    component: Layout,
    children: [
      {
        path: '/workBook/edit',
        name: 'workBookEdit',
        component: () => import('@/views/WorkBook/edit'),
        meta: {title: '图表编辑', icon: 'table'}
      }
    ]
  },
  {
    path: '/dashboard/edit',
    component: Layout,
    children: [
      {
        path: '/dashboard/edit',
        name: 'dashboardEdit',
        component: () => import('@/views/dashboard/edit'),
        meta: {title: '分析编辑', icon: 'table'}
      }
    ]
  },
  {
    path: '/dashboard/preview',
    name: 'dashboardView',
    component: () => import('@/views/dashboard/edit'),
    meta: {title: '分析预览', icon: 'table'}
  },
  {
    path: '/test',
    name: 'test',
    component: () => import('@/views/test'),
    meta: {title: '测试界面', icon: 'table'}
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})



export default router
