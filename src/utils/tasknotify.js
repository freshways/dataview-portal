﻿import {Guid} from './guid'
import Swal from 'sweetalert2'
//任务通知栏

/*
 *任务状态值:wait,success,error
 * */

//当前任务
const tasks = new Map()

const Toast = Swal.mixin({
  toast: true,
  position: 'bottom-end',
  showConfirmButton: false,
  timer: 2000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})


//更新任务
export function updateTask(task) {
  // if (!task.show) {
  //   return task;
  // }
  const iconClass = task.status == 'error' ? 'el-icon-error' : task.status == 'success' ? 'el-icon-success' : 'el-icon-loading'
  if (task.msg) {
    if (!task.id) {
      task.id = Guid.NewGuid().ToString('D')
      let app;
      // setTimeout(() => {
      // app = ElNotification({
      //   message: task.msg,
      //   iconClass: iconClass,
      //   duration: 0,
      //   position: 'bottom-right'
      // })
      app = Toast.fire({
        title: task.msg,
      })
      Swal.showLoading()
      tasks.set(task.id, app)
      // }, tasks.size * 5);
    } else {
      if (task.status == 'success' || task.status == 'error') {
        if (tasks.get(task.id)) {
          Swal.close(tasks.get(task.id))
          // tasks.get(task.id).close();
          tasks.delete(task.id);
          // ElNotification({
          //   message: task.msg,
          //   iconClass: iconClass,
          //   position: 'bottom-right'
          // })
          Toast.fire({
            icon: task.status,
            title: task.msg
          })
        }
      }
    }
  }
  return task;
}


