/// <reference path="./../../global.d.ts" />
/**
 * 提供对象的辅助函数
 */
let _delayTimer;


//克隆对象
const clone = function (obj) {
  var newobj;
  if (Array.isArray(obj)) {
    var newarray = [];
    obj.forEach(function (item) {
      newarray.push(clone(item));
    });
    newobj = newarray;
  } else if (typeof obj === "object" && obj) {
    newobj = {};
    for (var key in obj) {
      var prop = obj[key];
      newobj[key] = clone(prop);
    }
  } else {
    newobj = obj;
  }
  return newobj;
};
//将对象转换为FormUrlEncoded
const toFormUrlEncoded = function (obj) {
  var result = "";
  for (var key in obj) {
    result += key + "=" + obj[key] + "&";
  }
  if (result.length) {
    result = result.substr(0, result.length - 1);
  }
  return result;
};
/**
 * 比较两个对象
 * @param left {Object} 待比较的对象1
 * @param right {Object} 待比较的对象2
 * @param deepCompare {Boolean} 是否深度比较
 */
const isEqual = function (left, right, deepCompare) {
  var result = true;
  for (var k in left) {
    var leftfield = left[k];
    var rightfield = right[k];
    if (rightfield === undefined) {
      result = false;
      break;
    }
    if (typeof leftfield === "object") {
      if (deepCompare) {
        result = isEqual(leftfield, rightfield, deepCompare);
        if (!result) {
          break;
        }
      }
    } else if (leftfield != rightfield) {
      result = false;
      break;
    }
  }
  return result;
};
/**
 * 延迟执行
 * @param func {Function} 待执行函数体
 * @param timeout {Number} 延迟时间，单位毫秒，空值默认1000
 */
const delayExecute = function (func, timeout) {
  if (_delayTimer) {
    clearTimeout(_delayTimer);
  }
  _delayTimer = setTimeout(func, timeout || 1000);
};

export const ObjectHelper = {
  clone: clone, toFormUrlEncoded: toFormUrlEncoded, isEqual: isEqual
}
//# sourceMappingURL=objecthepler.js.map
