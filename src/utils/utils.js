const URL_PARAMS = {};
(location.search || '').substr(1).split('&').forEach(function (item) {
  const kv = item.split('=');
  URL_PARAMS[kv[0]] = kv[1];
});

export {URL_PARAMS};

/**
 * 判断数组中对象的指定属性是否有重复
 * @param arr
 * @param key
 * @returns {boolean}
 */
export function isRepeat(arr, key) {
  let obj = new Map()
  for (let i = 0; i < arr.length; i++) {
    if (obj.get(arr[i][key])) {
      return arr[i][key]
    } else {
      obj.set(arr[i][key], 1)
    }
  }
  return false;
}

/**
 * 度量格式化
 * @param numberFormat
 * @returns {string}
 */
export function toMeasureFormart(numberFormat, obj) {
  if (numberFormat === undefined) {
    numberFormat = {};
    numberFormat.num_format_e = 0;
    numberFormat.num_format_s = -1;
    numberFormat.num_format_p = {prefix: "", subfix: ""};
    numberFormat.num_format_i = true;
  }
  var num = obj;
  var sPre = "";
  if (numberFormat.num_format_s > -1) {
    num = num / (10000 * numberFormat.num_format_s);
    if (numberFormat.num_format_s === 1) {
      sPre = "万";
    } else if (numberFormat.num_format_s === 100) {
      sPre = "百万";
    } else if (numberFormat.num_format_s === 1000) {
      sPre = "千万";
    } else if (numberFormat.num_format_s === 10000) {
      sPre = "亿";
    } else if (numberFormat.num_format_s === 0.01) {
      sPre = "%";
      num = num * (10000 * numberFormat.num_format_s) * 100;
    }
  }
  num = num.toFixed(numberFormat.num_format_e);
  if (numberFormat.num_format_i) {
    num = Number(num).toThousands();
  }
  var num_format_msg = numberFormat.num_format_p.prefix + num + sPre + numberFormat.num_format_p.subfix;
  return num_format_msg;
};

const calculateLayout = () => {
  //手动触发窗口resize事件
  if (document.createEvent) {
    const event = document.createEvent("HTMLEvents");
    event.initEvent("resize", true, true);
    window.dispatchEvent(event);
  } else if (document.createEventObject) {
    window.fireEvent("onresize");
  }
}

export const doResize = _.debounce(calculateLayout, 500, {trailing: true})


