import ElementPlus from 'element-plus'
import 'dayjs/locale/zh-cn'
import 'bootstrap/dist/css/bootstrap.min.css'
import './element-variables.scss'
import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as filters from '@/filter/index';
import VueFullscreen from 'vue-fullscreen';
import VueGridLayout from 'vue-grid-layout'
import VueApexCharts from "vue3-apexcharts";
import dataV from '@jiaminghi/data-view'

import "@/assets/css/visualization.css";
import '@/permission'

const app = createApp(App)
app.config.globalProperties.$dateFormat = filters.dateFormat
app.use(ElementPlus, {size: 'mini'})
app.use(dataV)
app.use(VueFullscreen)
app.use(VueApexCharts);
app.use(store).use(router).use(VueGridLayout).mount('#app')



